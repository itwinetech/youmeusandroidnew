﻿using System;

namespace YouMeUsAndroid
{
	public class GAEventCategory
	{
		public static String DiscoverButton { get { return "Discover Button"; } set { } } 
		public static String MenuButton { get { return "Menu Button"; } set { } }
		public static String ArrowButton { get { return "Arrow Button"; } set { } }
		public static String MenuListItem { get { return "Menu List Item"; } set { } }
		public static String InterestListRowButton { get { return "Interest List Rowbutton"; } set { } }
		public static String EventListRowButton { get { return "Event List Rowbutton"; } set { } }
		public static String EventListRow { get { return "Event Listrow"; } set { } }
		public static String LetsGoButton { get { return "LetsGo Button"; } set { } }
		public static String Lable1 { get { return "Lable1(Popular Interests or Suggested Events)"; } set { } }
		public static String Lable2 { get { return "Lable2(My Interests or My Events)"; } set { } }
		public static String Search { get { return "Search"; } set { } }
		public static String ManualLocationSearch { get { return "City Name"; } set { } }
		public static String DescriptionPageJoinorUnjoinButton { get { return "Event DescriptionPage JoinorUnjoin Button"; } set { } }
		public static String SpinnerClick { get { return "DropDown List"; } set { } }
	}
}

