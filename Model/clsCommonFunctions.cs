﻿using System;
using Android.Net;
using Android.Content;
using Android.App;
using System.IO;
using Android.Graphics;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks; 


namespace YouMeUsAndroid
{
	public class ClsCommonFunctions
	{
		
		const string strMailIDPattern = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
			@"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";
		//to check the internet connectivity for the application
		internal string fnGetAppFolderPathInExternalDir(Context context)
		{
			return System.IO.Path.Combine( Android.OS.Environment.ExternalStorageDirectory.AbsolutePath,context.GetString(Resource.String.app_name));
		}
		internal Boolean FnIsConnected(Context context)
		{
			try
			{
				var connectionManager = (ConnectivityManager)context.GetSystemService (Context.ConnectivityService); 
				NetworkInfo networkInfo = connectionManager.ActiveNetworkInfo; 
				if (networkInfo != null && networkInfo.IsConnected) 
				{
					return true;
				}
			}
			catch(Exception ex)
			{
				Console.WriteLine ( ex.Message );
				//ensure access network state is enbled
				return false;
			}
			return false;
		} 
		//alert message with ok option
		internal void FnAlertMsg (string strTitle,string strMsg,Context context)
		{
			AlertDialog alertMsg = new AlertDialog.Builder (context).Create ();
			alertMsg.SetCancelable (false);
			alertMsg.SetTitle (strTitle);
			alertMsg.SetMessage (strMsg);
			alertMsg.SetButton ("OK", delegate (object sender, DialogClickEventArgs e) 
			{
				if (e.Which  == -1) 
				{
					alertMsg.Dismiss ();
					alertMsg=null;
				}
			});
			alertMsg.Show ();
		} 

		//to create new folder in sd card

		internal string FnGetExternalStoragepath()
		{
			return Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
		}
		internal bool FnIsMediaMounted()
		{
			string strState = Android.OS.Environment.ExternalStorageState;
			return strState.Equals ( Android.OS.Environment.MediaMounted );
		}
		internal bool FnCreateFolderInSDCard(string strMainFolder,string strSubFolderName,out string strCreatedFolderPath)
		{
			bool isSuccess = false;
			string strOutParameter=string.Empty;
			bool isMediaMounted = FnIsMediaMounted ();
			if ( isMediaMounted )
			{
				try
				{
					string strRootDirectory	= FnGetExternalStoragepath();
					string strProjectFolder = System.IO.Path.Combine ( strRootDirectory , strMainFolder );
					strOutParameter=strProjectFolder;
					if ( !Directory.Exists ( strProjectFolder ) )
					{
						Directory.CreateDirectory ( strProjectFolder );
					}
					if(!string.IsNullOrEmpty(strSubFolderName))
					{
						string strSubFolderToCreate = System.IO.Path.Combine ( strProjectFolder , strSubFolderName );
						strOutParameter=strSubFolderToCreate;
						if ( !Directory.Exists ( strSubFolderToCreate ) )
						{
							Directory.CreateDirectory ( strSubFolderToCreate );
						}
					}
					isSuccess=true;
				}
				catch
				{
					isSuccess= false;
				}
			}
			strCreatedFolderPath = strOutParameter;
			return isSuccess;
		}

		internal bool FnEmailValidation(string strmail)
		{
			Regex rex = new Regex (strMailIDPattern);
			return rex.IsMatch (strmail);
		}
		internal bool FnRemoteResultValidate(string strResult,Context context,ProgressDialog progress)
		{
			bool validate = true;
			switch(strResult)
			{
				case "Exception":
					progress.Dismiss ();
					progress = null;
					FnAlertMsg ( context.GetString ( Resource.String.app_name ) , context.GetString ( Resource.String.no_server_response ) , context ); 
					validate = false;
					break;
				case "no_sdcard":
					progress.Dismiss ();
					progress = null;
					FnAlertMsg (  context.GetString ( Resource.String.app_name ) ,  context.GetString ( Resource.String.no_sdcard ) ,  context ); 
					validate = false;
					break;
				case "UnableToReach":
					progress.Dismiss ();
					progress = null;
					FnAlertMsg (  context.GetString ( Resource.String.app_name ) ,  context.GetString ( Resource.String.not_reachable) ,  context ); 
					validate = false;
					break;
				default:
					validate = true;
					break;
			}
			return validate;
		}
		// If you would like to create a circle of the image set pixels to half the width of the image.
		internal static  Bitmap FnGetRoundedCornerBitmap(Bitmap bitmap, int pixels)
		{
			Bitmap output = null;

			try
			{
				output = Bitmap.CreateBitmap(bitmap.Width, bitmap.Height, Bitmap.Config.Argb8888);
				Canvas canvas = new Canvas(output);

				Color color = new Color(66, 66, 66);
				Paint paint = new Paint();
				Rect rect = new Rect(0, 0, bitmap.Width, bitmap.Height);
				RectF rectF = new RectF(rect);
				float roundPx = pixels;

				paint.AntiAlias = true;
				canvas.DrawARGB(0, 0, 0, 0);
				paint.Color = color;
				canvas.DrawRoundRect(rectF, roundPx, roundPx, paint);

				paint.SetXfermode(new PorterDuffXfermode(PorterDuff.Mode.SrcIn));
				canvas.DrawBitmap(bitmap, rect, rect, paint);
			}
			catch (System.Exception err)
			{
				System.Console.WriteLine ("GetRoundedCornerBitmap Error - " + err.Message);
			}

			return output;
		}
		//To Load Large image bitmap efficiently , to avoid the out of memoory exception
		internal static Bitmap FnDecodeSampledBitmapFromFile(string strFilePath,int reqWidth, int reqHeight)
		{
			// First decode with inJustDecodeBounds=true to check dimensions
			var options = new BitmapFactory.Options {
				InJustDecodeBounds = true,
			};
			using (var dispose = BitmapFactory.DecodeFile(strFilePath, options)) {
			}

			// Calculate inSampleSize
			options.InSampleSize = CalculateInSampleSize(options, reqWidth, reqHeight);

			// Decode bitmap with inSampleSize set
			options.InJustDecodeBounds = false;
			return BitmapFactory.DecodeFile(strFilePath,  options);
		}
		internal static int CalculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
		{
			// Raw height and width of image
			var height = (float)options.OutHeight;
			var width = (float)options.OutWidth;
			var inSampleSize = 1D;

			if (height > reqHeight || width > reqWidth)
			{
				inSampleSize = width > height
					? height/reqHeight
					: width/reqWidth;
			}

			return (int) inSampleSize;
		} 

		internal InterestsDetails FnReadJsonFile(string strFolderCreated)
		{
			InterestsDetails objAndroidClass=null;
			try
			{
				string strExtrasPath = System.IO.File.ReadAllText ( strFolderCreated );
				objAndroidClass=JsonConvert.DeserializeObject<InterestsDetails> (strExtrasPath);
			}
			catch
			{
				objAndroidClass = null;
			}
			return objAndroidClass;
		}

		internal EventLocation FnReadEventJsonFile(string strFolderCreated)
		{
			EventLocation objAndroidClass=null;
			try
			{
				string strExtrasPath = System.IO.File.ReadAllText ( strFolderCreated );
				objAndroidClass=JsonConvert.DeserializeObject<EventLocation> (strExtrasPath);
			}
			catch
			{
				objAndroidClass = null;
			}
			return objAndroidClass;
		}

		internal static  Bitmap FnDownloadImages(string imageUri)
		{
			Bitmap imageBitmap = null;
			if ( !string.IsNullOrEmpty ( imageUri ) )
			{
				var s = new HttpClient ();

				using ( var webClient = new WebClient () )
				{
					
					var imageBytes = webClient.DownloadData ( imageUri );
					if ( imageBytes != null && imageBytes.Length > 0 )
					{
						imageBitmap = BitmapFactory.DecodeByteArray ( imageBytes , 0 , imageBytes.Length );
					}
				}

			}
			return imageBitmap;
		}
		//to get bitmap option current image with it's height and width 
		internal async  Task<BitmapFactory.Options> GetBitmapOptionsOfImage(string strFileName)
		{
			BitmapFactory.Options options = new BitmapFactory.Options
			{
				InJustDecodeBounds = true //avoids memory allocation during decoding
			};
			// The result will be null because InJustDecodeBounds == true.
			await BitmapFactory.DecodeFileAsync(strFileName, options);

			int imageHeight = options.OutHeight;
			int imageWidth = options.OutWidth;
			Console.WriteLine("height : " + imageHeight + " width : " + imageWidth);
			//			_originalDimensions.Text = string.Format("Original Size= {0}x{1}", imageWidth, imageHeight); 
			return options;
		}
		#region "fnConvertByteArray"
		//convert original image to byte[] 
		internal async Task< byte[]> FnConvertByteArray(string strImagePath)
		{
			byte[] photo;
			Bitmap bitmap = BitmapFactory.DecodeFile(strImagePath);
			MemoryStream stream = new MemoryStream();
			bitmap.Compress(Bitmap.CompressFormat.Jpeg, 50, stream);
			photo = stream.ToArray();

			return photo;
		}
		#endregion
		public  void FnDeleteDirectory(string targetDir)
		{
			if (Directory.Exists(targetDir))
			{
				string[] Files = Directory.GetFiles(targetDir);
				string[] dir = Directory.GetDirectories(targetDir);

				foreach (string file in Files)
				{
					System.IO.File.SetAttributes(file, FileAttributes.Normal);
					System.IO.File.Delete(file);
				}
				foreach (string directory in dir)
				{
					FnDeleteDirectory(directory);
				}
				Directory.Delete(targetDir, false);
			}
		}
	}
}