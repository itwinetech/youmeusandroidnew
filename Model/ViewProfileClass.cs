﻿using System;
using System.Collections.Generic;

namespace YouMeUsAndroid
{
	public class UserCover
	{
		public string Idusers { get; set; }
		public string Fname { get; set; }
		public string Lname { get; set; }
		public string Email { get; set; }
		public string Active { get; set; }
		public string Dateofbirth { get; set; }
		public string Originalimage { get; set; }
		public string Modifiedimage { get; set; }
		public string Typeofimage { get; set; }
		public string CoverProfile { get; set; }
	}

	public class Member
	{
		public string Idusers { get; set; }
		public string Lname { get; set; }
		public string Idgroups { get; set; }
		public string Isanadmin { get; set; }
		public string Fname { get; set; }
		public string Modifiedimage { get; set; }
		public string GroupMembersLogo { get; set; }
	}

	public class GroupDetail
	{
		public string Idgroups { get; set; }
		public string Groupname { get; set; }
		public string Urlkeyword { get; set; }
		public string Modifiedlogo { get; set; }
		public string GroupAdmin { get; set; }
		public string Groupdesc { get; set; }
		public string usrcnt { get; set; }
		public string Idusergroups { get; set; }
		public string Privacysetting { get; set; }
		public string Isamember { get; set; }
		public string Closedgroupflag { get; set; }
		public string Groupslogo { get; set; }
		public string ismemberornot { get; set; }
		public string SessionUserClosedgrpflag { get; set; }
		public List<Member> members { get; set; }
		public string memberscount { get; set; }
	}

	public class Member2
	{
		public string Idinterestmaster { get; set; }
		public string Fname { get; set; }
		public string Idusers { get; set; }
		public string Modifiedimage { get; set; }
		public string InterestUsersLogo { get; set; }
	}

	public class InterestDetail
	{
		public string usrcnt { get; set; }
		public string Idinterestmaster { get; set; }
		public string Interestname { get; set; }
		public string Url_keyword { get; set; }
		public string Interestdesc { get; set; }
		public string Interestlogo { get; set; }
		public string usrcount { get; set; }
		public string Isliked { get; set; }
		public string InterestLogo { get; set; }
		public string fallowingornot { get; set; }
		public List<Member2> members { get; set; }
		public string memberscount { get; set; }
	}

	public class ViewProfile
	{
		public string DOBprivacyflag { get; set; }
		public string Idusers { get; set; }
		public string Fname { get; set; }
		public string Lname { get; set; }
		public string Email { get; set; }
		public string Active { get; set; }
		public string Sex { get; set; }
		public object Originalimage { get; set; }
		public object Modifiedimage { get; set; }
		public object Dateofbirth { get; set; }
		public string currentcity { get; set; }
		public string UserProfile { get; set; }
		public UserCover UserCover { get; set; }
		public List<GroupDetail> GroupDetails { get; set; }
		public List<InterestDetail> InterestDetails { get; set; }
		public string Cover { get; set; }
		public string Coverimage { get; set; }
		public object profile { get; set; }
		public object profileimage { get; set; }
	}

	public class ViewProfileClass
	{
		public ViewProfile ViewProfile { get; set; }
	}
}
