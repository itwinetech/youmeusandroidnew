﻿using System;
using System.Text.RegularExpressions;

namespace YouMeUsAndroid
{
	public static class Constants
	{
		//General
		public const string strAppName="YouMeUs";
		public const string strFillValidationError="Enter all fields";
		public const string strEmailValidationError="You have entered an invalid e-mail address";
		public const string strPasswordValidationError="The password and confirmation password do not match";
		public const string strNoInternet="No internet connection. Make sure your network connectivity in active and try again.";
		public const string strRegisteredSuccessfully="An activation Email has been sent to your Email address. Please verify the same";
		public const string strAccountExists="{0} is already a YouMeUs account. If it's yours please sign in";
		public const string strLoadingMsg = "Please Wait...";
		public const string strEventLodingMsg="Loading events...";
		public const string strInterestsLodingMsg="Loading interests...";
		public const string strServserError="Server not responding! try again later";
		public const string strGPSDisableErrorMsg="Your GPS seems to be disabled, do you want to enable it?";
		public const string strInterestsExistsMsg="You are already following this interests";
		public const string strEventsExistsMsg="You are already following this events";
		public const string strEventUnfollowMsg="Do you want to unjoin this event";
		public const string strInterestsUnfollowMsg="Do you want to unfollow this interest";
		public const string strGroupUnfollowMsg = "Do you want to unfollow this group";

		//google manual location
		public const string strGoogleMapGeoLocationApiKey="AIzaSyAk64y1pP3XDdoS6NYCAPKvW8xB_G0uqw0";
		public const string strGoogleAutoCompleteSearchURL="https://maps.googleapis.com/maps/api/place/autocomplete/json";
		public const string strGoogleLocationURL= "https://maps.googleapis.com/maps/api/geocode/json?";
		public const string strFolderPath = "/storage/emulated/0/YouMeUs/Camera Images";

	}
}