﻿using System;
using System.Threading.Tasks;
using System.Text;
using System.Net;

namespace YouMeUsAndroid
{
	public class RestServiceCall
	{

		//SignUp
		//++++++++++++++  production url     +++++++++++++++++//
		const string strSignUpURL = "http://www.youmeus.com/helloservice/signup";
		//++++++++++++++ test url      +++++++++++++++++//
		//const string strSignUpURL="http://192.168.1.20/youmeusencrypted/helloservice/signup"; 
		async internal Task<string> CallSignUpService(string strFname,string strEmail,string strPasswd)
		{
			StringBuilder builder = new StringBuilder (strSignUpURL);
			builder.Append ("?fname="+strFname+"").Append("&email="+strEmail+"").Append("&pwd="+strPasswd+"");
			string strConcatenatedURL = builder.ToString ();
			builder.Clear ();
			builder = null;
			string strResult =await CallService (strConcatenatedURL);
			return strResult;
		}

		//SignIn
		//++++++++++++++  production url     +++++++++++++++++//
		const string strLoginURL = "http://www.youmeus.com/helloservice/loginservice";
		//++++++++++++++ test url      +++++++++++++++++//
		//const string strLoginURL = "http://192.168.1.20/youmeusencrypted/helloservice/loginservice";
		async internal Task<string> CallLoginService(string strUsername,string strPassword)
		{
			string strResult;
			StringBuilder builder = new StringBuilder (strLoginURL);
			builder.Append ( "?username=" + strUsername + "&password=" + strPassword + "" );
			string strConcatenatedURL = builder.ToString ();
			builder.Clear ();
			builder = null; 
			strResult = await CallService ( strConcatenatedURL ); 
			return strResult;
		} 
		//Forgetpassword
		//++++++++++++++  production url     +++++++++++++++++//
		const string strForgetpassword = "http://www.youmeus.com/helloservice/fnforgotpassword";
		//++++++++++++++ test url      +++++++++++++++++//
		//const string strForgetpassword="http://192.168.1.20/youmeusencrypted/helloservice/fnforgotpassword";
		async internal Task<string>CallForgotPasswordService (string strEmail)
		{
			StringBuilder builder = new StringBuilder (strForgetpassword);
			builder.Append ( "?email=" + strEmail + "" );
			string strConcatenatedURL = builder.ToString ();
			builder.Clear ();
			builder = null;
			string strResult = await CallService (strConcatenatedURL);
			return strResult;
		}
		//Change Password
		//++++++++++++++  production url     +++++++++++++++++//
		const string strChangepassword = "http://www.youmeus.com/helloservice/fnchangepassword";
		//++++++++++++++ test url      +++++++++++++++++//
		//const string strChangepassword = "http://192.168.1.20/youmeusencrypted/helloservice/fnchangepassword";
		async internal Task<string> CallChangePasswordService(string strUserId,string strOldPassword,string strNewPassword)
		{
			StringBuilder builder = new StringBuilder(strChangepassword);
			builder.Append("?iduser=" + strUserId + "").Append("&oldpw=" + strOldPassword + "").Append("&newpw=" + strNewPassword + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			string strResult = await CallService(strConcatenatedURL);
			return strResult;
		}

		#region "Events"
		//events
		//++++++++++++++  production url     +++++++++++++++++//
		const string strNearByEventAPI="http://www.youmeus.com/helloservice/events_basedon_geolocation";
		//++++++++++++++ test url      +++++++++++++++++//
		//const string strNearByEventAPI="http://192.168.1.20/youmeusencrypted/helloservice/events_basedon_geolocation";

		async internal Task<string> CallEventService(string strLatitudelongtitude,string strAddress,string strDistance)
		{
			string strResult;
			StringBuilder builder = new StringBuilder (strNearByEventAPI);
			builder.Append ( "?latlong=" + strLatitudelongtitude + "&cityname=" + strAddress + "&distanceinkm="+strDistance);
			string strConcatenatedURL = builder.ToString ();
			builder.Clear ();
			builder = null; 
			strResult = await CallService ( strConcatenatedURL ); 
			return strResult;
		}
		//User Myevents
		//++++++++++++++  production url     +++++++++++++++++//
		const string strMyEventAPI = "http://www.youmeus.com/helloservice/myevents";
		//++++++++++++++ test url      +++++++++++++++++//
		//const string strMyEventAPI ="http://192.168.1.20/youmeusencrypted/helloservice/myevents";

		async internal Task<string> CallMyEventService(string strEmail)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strMyEventAPI);
			builder.Append("?email=" + strEmail + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}

		//++++++++++++++  production url     +++++++++++++++++//
		const string strUserEventsFetechAPI = "http://www.youmeus.com/helloservice/myevents";
		//++++++++++++++ test url      +++++++++++++++++//
		//const string strUserEventsFetechAPI = "http://192.168.1.20/youmeusencrypted/helloservice/myevents";
		async internal Task<string> CallUserEventsFetechService(string strEmail)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strUserEventsFetechAPI);
			builder.Append("?email=" + strEmail + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}
		//++++++++++++++  production url     +++++++++++++++++//
		const string strEventsStoreAPI = "http://www.youmeus.com/helloservice/attendevents";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strEventsStoreAPI = "http://192.168.1.20/youmeusencrypted/helloservice/attendevents";
		async internal Task<string> CallEventsStoreService(string strEventId, string strEmail)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strEventsStoreAPI);
			builder.Append("?idevent=" + strEventId + "&email=" + strEmail + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}
		#endregion

		#region "Interests"
		//Top Most 15 interests(only)
		//++++++++++++++   production url    +++++++++++++++++//
		const string strInterestsAPI="http://www.youmeus.com/helloservice/PopularInterests";
		//++++++++++++++  test url     +++++++++++++++++//
		//const string strInterestsAPI="http://192.168.1.20/youmeusencrypted/helloservice/PopularInterests";

		async internal Task<string> CallInterestsService()
		{
			string strResult;
			StringBuilder builder = new StringBuilder (strInterestsAPI);
			string strConcatenatedURL = builder.ToString ();
			builder.Clear ();
			builder = null; 
			strResult = await CallService ( strConcatenatedURL ); 
			return strResult;
		}
		//++++++++++++++  production url     +++++++++++++++++//
		const string strInterestsStoreAPI = "http://www.youmeus.com/helloservice/followintrests";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strInterestsStoreAPI = "http://192.168.1.20/youmeusencrypted/helloservice/followintrests";
		async internal Task<string> CallInterestsStoreService(string strInterestId,string strEmail)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strInterestsStoreAPI);
			builder.Append("?interestid=" + strInterestId + "&email=" + strEmail + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}
		//++++++++++++++  production url     +++++++++++++++++//
		const string strUserInterestsFetechAPI = "http://www.youmeus.com/helloservice/myinterests";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strUserInterestsFetechAPI = "http://192.168.1.20/youmeusencrypted/helloservice/myinterests";
		async internal Task<string> CallUserInterestsFetechService(string strEmail)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strUserInterestsFetechAPI);
			builder.Append("?email=" + strEmail + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}
		//++++++++++++++  production url     +++++++++++++++++//
		const string strTagInterestSearch = "http://www.youmeus.com/helloservice/taginterests";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strTagInterestSearch = "http://192.168.1.20/youmeusencrypted/helloservice/taginterests";
			async internal Task<string> CallTagInterestSearchService()
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strTagInterestSearch);
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}

		//Full interests
		//++++++++++++++   production url    +++++++++++++++++//
		const string strFullInterestsAPI="http://www.youmeus.com/helloservice/interests";
		//++++++++++++++  test url     +++++++++++++++++//
		//const string strFullInterestsAPI="http://192.168.1.20/youmeusencrypted/helloservice/interests";

		async internal Task<string> CallFullInterestsService()
		{
			string strResult;
			StringBuilder builder = new StringBuilder (strFullInterestsAPI);
			string strConcatenatedURL = builder.ToString ();
			builder.Clear ();
			builder = null; 
			strResult = await CallService ( strConcatenatedURL ); 
			return strResult;
		}
		#endregion

		#region "Groups"
		//JoinOrUnjoinGroup
		//++++++++++++++  production url     +++++++++++++++++//
		const string strJoinOrUnjoinGroupAPI = "http://www.youmeus.com/helloservice/joingroupservices";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strJoinOrUnjoinGroupAPI = "http://192.168.1.20/youmeusencrypted/helloservice/joingroupservices";
		async internal Task<string> CallJoinOrUnjoinGroupService(string strGroupId,string strUserId)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strJoinOrUnjoinGroupAPI);
			builder.Append("?Idgroups=" + strGroupId + "").Append("&idusers=" + strUserId + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}

		//MyGroups
		//++++++++++++++  production url     +++++++++++++++++//
		const string strMyGroupsAPI = "http://www.youmeus.com/helloservice/mygroups";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strMyGroupsAPI ="http://192.168.1.20/youmeusencrypted/helloservice/mygroups";
		async internal Task<string> CallUserGroupsFetechService(string strEmail)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strMyGroupsAPI);
			builder.Append("?email=" + strEmail + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}

		//Groupssuggested
		//++++++++++++++  production url     +++++++++++++++++//
		const string strGroupssuggestedAPI = "http://www.youmeus.com/helloservice/groupssuggested";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strGroupssuggestedAPI ="http://192.168.1.20/youmeusencrypted/helloservice/groupssuggested";
		async internal Task<string> CallGroupsSuggestedService(string strEmail)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strGroupssuggestedAPI);
			builder.Append("?email=" + strEmail + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}

		//TagGroupList
		//++++++++++++++  production url     +++++++++++++++++//
		const string strTagGroupListAPI = "http://www.youmeus.com/helloservice/grouplistser";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strTagGroupListAPI = "http://192.168.1.20/youmeusencrypted/helloservice/grouplistser";

		async internal Task<string> CallTagGroupListService()
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strTagGroupListAPI);
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}


		#endregion

		string strAboutMeURL="";
		async internal Task<string> ProfileList(string strEmail)
		{
			if(MainActivity.strTypeLogin=="YouMeUsLogin")
			{
				//++++++++++++++  production url     +++++++++++++++++//
				strAboutMeURL = "http://www.youmeus.com/helloservice/getuserlist";

				//++++++++++++++ test url      +++++++++++++++++//
				//strAboutMeURL="http://192.168.1.20/youmeusencrypted/helloservice/getuserlist";
			}
			else if(MainActivity.strTypeLogin=="GoogleLogin")
			{
				//++++++++++++++  production url     +++++++++++++++++//
				strAboutMeURL = "http://www.youmeus.com/helloservice/gmaillogin";

				//++++++++++++++ test url      +++++++++++++++++//
				//strAboutMeURL="http://192.168.1.20/youmeusencrypted/helloservice/gmaillogin";
			}
			else if(MainActivity.strTypeLogin=="FacebookLogin")
			{
				//++++++++++++++  production url     +++++++++++++++++//
				strAboutMeURL = "http://www.youmeus.com/helloservice/fblogin";

				//++++++++++++++ test url      +++++++++++++++++//
				//strAboutMeURL="http://192.168.1.20/youmeusencrypted/helloservice/fblogin";
			}
			StringBuilder builder = new StringBuilder (strAboutMeURL);
			builder.Append ( "?email=" + strEmail + "" );
			string strConcatenatedURL = builder.ToString ();
			builder.Clear ();
			builder = null;
			string strResult = await CallService (strConcatenatedURL);
			return strResult;
		}

		//GooglePlaceCityNameService
		async internal Task<string> CallGooglePlaceCityNameService(string strSearchValue )
		{
			string strResult;
			//StringBuilder builder = new StringBuilder (strSearchValue);
			string strFullPath = string.Format ( "{0}{1}{2}", Constants.strGoogleAutoCompleteSearchURL ,"?input="+ strSearchValue ,"&key="+Constants.strGoogleMapGeoLocationApiKey );
			string strConcatenatedURL = strFullPath.ToString ();
			strResult = await CallService ( strConcatenatedURL ); 
			return strResult;
		} 

		//GooglePlaceService manuallocation
		async internal Task<string> CallGooglePlaceService(string strSearchLocationValue )
		{

			string strGeoCode = string.Format ("address={0}",strSearchLocationValue);
			string strResult = string.Format ("{0}{1}",Constants.strGoogleLocationURL,strGeoCode);
			string strConcatenatedURL = strResult.ToString ();
			strResult = await CallService ( strConcatenatedURL ); 
			return strResult;
		} 

		#region "Google Login"
		//google login
			const string googleUrl="https://www.googleapis.com/oauth2/v1/userinfo";
			async internal Task<string> GetDataFromGoogle(string accessTokenValue)
			{
				StringBuilder builder = new StringBuilder (googleUrl);
				builder.Append ( "?access_token=" + accessTokenValue + "" );
				string strConcatenatedURL = builder.ToString ();

				builder.Clear ();
				builder = null; 
				string strResult = await CallService (strConcatenatedURL); 
				return strResult;
			}
		//++++++++++++++  production url     +++++++++++++++++//
		const string googleSignUPUrl = "http://www.youmeus.com/helloservice/gmailsignup";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string googleSignUPUrl="http://192.168.1.20/youmeusencrypted/helloservice/gmailsignup";
		async internal Task<string> CallGoogleSignUp(string strGivenName,string strFamilyName,string strEmail,string strGender)
		{
			StringBuilder builder = new StringBuilder (googleSignUPUrl);
			builder.Append ("?given_name="+strGivenName+"").Append("&family_name="+strFamilyName+"").Append("&email="+strEmail+"").Append("&gender="+strGender+"");
			string strConcatenatedURL = builder.ToString ();

			builder.Clear ();
			builder = null; 
			string strResult = await CallService (strConcatenatedURL); 
			return strResult;
		}
		#endregion

		#region "Facebook SignUp"
		//++++++++++++++  production url     +++++++++++++++++//
		const string facebookSignUPUrl = "http://www.youmeus.com/helloservice/fbsignup";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string facebookSignUPUrl = "http://192.168.1.20/youmeusencrypted/helloservice/fbsignup";
		async internal Task<string> CallFacebookSignUp(string strGivenName,string strEmail, string strGender)
		{
			StringBuilder builder = new StringBuilder(googleSignUPUrl);
			builder.Append("?name=" + strGivenName + "").Append("&email=" + strEmail + "").Append("&gender=" + strGender + "");
			string strConcatenatedURL = builder.ToString();

			builder.Clear();
			builder = null;
			string strResult = await CallService(strConcatenatedURL);
			return strResult;
		}
		#endregion

		#region "Chat Member List"
		//++++++++++++++  production url     +++++++++++++++++//
		const string strChatMemberlistAPI = "http://www.youmeus.com/helloservice/showmessagesmember";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strChatMemberlistAPI = "http://192.168.1.20/youmeusencrypted/helloservice/showmessagesmember";
		async internal Task<string> CallChatMemberService(string strUserId)
		{
			StringBuilder builder = new StringBuilder(strChatMemberlistAPI);
			builder.Append("?iduser=" + strUserId + "");
			//builder.Append("?iduser=" + "749" + "");
			string strConcatenatedURL = builder.ToString();

			builder.Clear();
			builder = null;
			string strResult = await CallService(strConcatenatedURL);
			return strResult;
		}

		//++++++++++++++  production url     +++++++++++++++++//
		const string strIndividualAPI = "http://www.youmeus.com/helloservice/showmessagesindividual";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strIndividualAPI = "http://192.168.1.20/youmeusencrypted/helloservice/showmessagesindividual";
		async internal Task<string> CallIndividualAPIService(string strIdUserFrom,string strIdUserTo)
		{
			StringBuilder builder = new StringBuilder(strIndividualAPI);
			builder.Append("?Iduserfrom=" + strIdUserFrom + "&Iduserto="+strIdUserTo +"");
			string strConcatenatedURL = builder.ToString();

			builder.Clear();
			builder = null;
			string strResult = await CallService(strConcatenatedURL);
			return strResult;
		}



		#endregion


		#region " Notification "

		#region "Updated Notification Count"	
		//++++++++++++++  production url     +++++++++++++++++//
		const string strUpdatenotifycountAPI = "http://www.youmeus.com/helloservice/updatenotifycount";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strUpdatenotifycountAPI = "http://192.168.1.20/youmeusencrypted/helloservice/updatenotifycount";
		async internal Task<string> CallUpdatenotifycountService(string userId, string TypeOfContent, string TypeId, string IdGroup)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strUpdatenotifycountAPI);
			builder.Append("?iduser=" + userId + "&typeofcontent=" + TypeOfContent + "&typeid=" + TypeId + "&idgroup=" + IdGroup + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}
		#endregion

		#region "Notification"

		//++++++++++++++  production url     +++++++++++++++++//
		const string strNotificationAPI = "http://www.youmeus.com/helloservice/notifications";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strNotificationAPI = "http://192.168.1.20/youmeusencrypted/helloservice/notifications";
		async internal Task<string> CallFullNotificationService(string userId)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strNotificationAPI);
			builder.Append("?iduser=" + userId + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}
		#endregion

		#region "Like UnLike Post in Notifications" 
		//++++++++++++++  production url     +++++++++++++++++//
		const string strLikeUnLikeNotificationAPI = "http://www.youmeus.com/helloservice/ajaxpostlul";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strLikeUnLikeNotificationAPI = "http://192.168.1.20/youmeusencrypted/helloservice/ajaxpostlul";

		async internal Task<string> CallLikeUnLikeNotificationAPI(string IdPost, string userId)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strLikeUnLikeNotificationAPI);
			builder.Append("?idpost=" + IdPost + "&iduser=" + userId + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}
		#endregion


		#region "Comment in Notifications content"
		//++++++++++++++  production url     +++++++++++++++++//
		const string strCommentOnNotificationContentAPI = "http://www.youmeus.com/helloservice/ajaxpostcomment";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strCommentOnNotificationContentAPI = "http://192.168.1.20/youmeusencrypted/helloservice/ajaxpostcomment";
		async internal Task<string> CallCommentOnNotificationContentAPI(string userId, string IdPost, string CommentText)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strCommentOnNotificationContentAPI);
			builder.Append("?iduser=" + userId + "&idpost=" + IdPost + "&commenttext=" + CommentText + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}
		#endregion

		#region "View Profile Service"
		//++++++++++++++  production url     +++++++++++++++++//
		const string strViewProfileAPI = "http://www.youmeus.com/helloservice/ViewProfile";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strViewProfileAPI = "http://192.168.1.20/youmeusencrypted/helloservice/ViewProfile";
		async internal Task<string> CallViewProfileService(string strUserId, string strViewPermision)
		{
			StringBuilder builder = new StringBuilder(strViewProfileAPI);
			builder.Append("?iduser=" + strUserId + "").Append("&viewpermision=" + strViewPermision + "");
			string strConcatenatedURL = builder.ToString();

			builder.Clear();
			builder = null;
			string strResult = await CallService(strConcatenatedURL);
			return strResult;
		}
		#endregion

		#region "Group Accept Reject" 
		//++++++++++++++  production url     +++++++++++++++++//
		const string strGroupAcceptRejectAPI = "http://www.youmeus.com/helloservice/fnrequestapproval";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strGroupAcceptRejectAPI = "http://192.168.1.20/youmeusencrypted/helloservice/fnrequestapproval";
		async internal Task<string> CallGroupAcceptRejectAPI(string MemberId, string IdUserGroups, string Flag, string IdGroups)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strGroupAcceptRejectAPI);
			builder.Append("?memberid=" + MemberId + "&Idusergroups=" + IdUserGroups + "&flag=" + Flag + "&idgroups=" + IdGroups + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}
		#endregion



		#region "Group Members And Requests" 
		//++++++++++++++  production url     +++++++++++++++++//
		const string strGroupMembersAndRequestsAPI = "http://www.youmeus.com/helloservice/getmygroupmembers";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strGroupMembersAndRequestsAPI = "http://192.168.1.20/youmeusencrypted/helloservice/getmygroupmembers";
		async internal Task<string> CallGroupMembersAndRequestsAPI(string GroupId)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strGroupMembersAndRequestsAPI);
			builder.Append("?Idgroups=" + GroupId + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}
		#endregion

		#endregion


		#region "dropdown pop"
		//Help
		//++++++++++++++  production url     +++++++++++++++++//
		const string strHelpAPI = "http://www.youmeus.com/helloservice/Help";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strHelpAPI = "http://192.168.1.20/youmeusencrypted/helloservice/Help";

		async internal Task<string> CallHelpService()
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strHelpAPI);
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}
		//AboutUs
		//++++++++++++++  production url     +++++++++++++++++//
		const string strAboutUsAPI = "http://www.youmeus.com/helloservice/Aboutus";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strAboutUsAPI = "http://192.168.1.20/youmeusencrypted/helloservice/Aboutus";
		async internal Task<string> CallAboutUsService()
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strAboutUsAPI);
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}
		//Contact YouMeUs
		//++++++++++++++  production url     +++++++++++++++++//
		const string strContactYouMeUsAPI = "http://www.youmeus.com/helloservice/ContanctYoumeus";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strContactYouMeUsAPI = "http://192.168.1.20/youmeusencrypted/helloservice/ContanctYoumeus";
		async internal Task<string> CallContactService()
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strContactYouMeUsAPI);
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}


		//Terms and Condition
		//++++++++++++++  production url     +++++++++++++++++//
		const string strTermsandConditionAPI = "http://www.youmeus.com/helloservice/TermsConditions";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strTermsandConditionAPI = "http://192.168.1.20/youmeusencrypted/helloservice/TermsConditions";
		async internal Task<string> CallTermsAndConditionAPIService()
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strTermsandConditionAPI);
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}

		//Home feed
		//++++++++++++++  production url     +++++++++++++++++//
		const string strHomeFeedAPI = "http://www.youmeus.com/helloservice/homefeeds";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strHomeFeedAPI = "http://192.168.1.20/youmeusencrypted/helloservice/homefeeds";
		async internal Task<string> CallHomeFeedAPIService(string strIdUser,string strTarkLoad)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strHomeFeedAPI);
			builder.Append("?iduser=" + strIdUser +"&trackload="+strTarkLoad + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}
		//++++++++++++++  production url     +++++++++++++++++//
		const string strPromotationAPI = "http://www.youmeus.com/helloservice/homefeedspromotion";
		//++++++++++++++ test url      +++++++++++++++++//
		//const string strPromotationAPI = "http://192.168.1.20/youmeusencrypted/helloservice/homefeedspromotion";
		async internal Task<string> CallPromotationAPI(string strIdUser)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strPromotationAPI);
			builder.Append("?iduser=" + strIdUser + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}

		#region "Comment in Notifications content"
		//++++++++++++++  production url     +++++++++++++++++//
		const string strCommentOnNotificationCAPI = "http://www.youmeus.com/helloservice/ajaxpostcomment";

		//++++++++++++++ test url      +++++++++++++++++//
		//const string strCommentOnNotificationCAPI = "http://192.168.1.20/youmeusencrypted/helloservice/ajaxpostcomment";
		async internal Task<string> CallHomeCommentAPI(string userId, string IdPost, string CommentText,string strCommentsinto)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strCommentOnNotificationCAPI);
			builder.Append("?iduser=" + userId + "&idpost=" + IdPost + "&commenttext=" + CommentText + "&Commentsinto="+strCommentsinto +"");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}
		#endregion
		//++++++++++++++  production url     +++++++++++++++++//
		const string strPostHideAPI = "http://www.youmeus.com/helloservice/reportpost";
		//++++++++++++++ test url      +++++++++++++++++//
		//const string strPostHideAPI = "http://192.168.1.20/youmeusencrypted/helloservice/reportpost";
			async internal Task<string> CallHidePostAPI(string strIdPost)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strPostHideAPI);
			builder.Append("?idpost=" + strIdPost +"");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}
		//++++++++++++++  production url     +++++++++++++++++//
		const string strEventLikeAPI = "http://www.youmeus.com/helloservice/eventlike";
		//++++++++++++++ test url      +++++++++++++++++//
		//const string strEventLikeAPI = "http://192.168.1.20/youmeusencrypted/helloservice/eventlike";
		async internal Task<string> CallEventLikeOrUnlikeAPI(string IdEvent, string userId)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strEventLikeAPI);
			builder.Append("?idevent=" + IdEvent + "&iduser=" + userId + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}

		//++++++++++++++  production url     +++++++++++++++++//
		const string strHomeEventsNearByEventAPI = "http://www.youmeus.com/helloservice/HomefeedseventsBasedonGeolocation";
		//++++++++++++++ test url      +++++++++++++++++//
		//const string strHomeEventsNearByEventAPI="http://192.168.1.20/youmeusencrypted/helloservice/HomefeedseventsBasedonGeolocation";

		async internal Task<string> CallHomeEventsEventService(string strLatitudelongtitude, string strAddress, string strDistance)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strHomeEventsNearByEventAPI);
			builder.Append("?latlong=" + strLatitudelongtitude + "&cityname=" + strAddress + "&distanceinkm=" + strDistance);
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}

		//++++++++++++++  production url     +++++++++++++++++//
		const string strPostOnGroupWall = "http://www.youmeus.com/helloservice/fnposttext";
		//++++++++++++++ test url      +++++++++++++++++//
		//const string strPostOnGroupWall="http://192.168.1.20/youmeusencrypted/helloservice/fnposttext";
		async internal Task<string> CallPostOnGroupDescription(string strIdPostTo, string strIdPostFrom, string strTextContent, string strVideoURL, string strTypeOfPost)
		{
			StringBuilder builder = new StringBuilder(strPostOnGroupWall);
			builder.Append("?Idpoststo=" + strIdPostTo + "").Append("&Idpostfrom=" + strIdPostFrom + "").Append("&Postcontent=" + strTextContent + "").Append("&Videourl=" + strVideoURL + "").Append("&Typeofpost=" + strTypeOfPost + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			string strResult = await CallService(strConcatenatedURL);
			return strResult;
		}

		//++++++++++++++  production url     +++++++++++++++++//
		const string strChatMemberAPI = "http://www.youmeus.com/helloservice/newmessageto";
		//++++++++++++++ test url      +++++++++++++++++//
		//const string strChatMemberAPI = "http://192.168.1.20/youmeusencrypted/helloservice/newmessageto";
		async internal Task<string> CallChatMemberAPI(string strUserId)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strChatMemberAPI);
			builder.Append("?iduser=" + strUserId + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}
		//++++++++++++++  production url     +++++++++++++++++//
		const string strLatestMessageAPI = "http://www.youmeus.com/helloservice/latestmessage";
		//++++++++++++++ test url      +++++++++++++++++//
		//const string strLatestMessageAPI = "http://192.168.1.20/youmeusencrypted/helloservice/latestmessage";
		async internal Task<string> CallLatestMessageAPI(string strUserId)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strLatestMessageAPI);
			builder.Append("?iduser=" + strUserId + "");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}
		//++++++++++++++  production url     +++++++++++++++++//
		const string strChatUpdateSeenAPI = "http://www.youmeus.com/helloservice/chatupdateseenat";
		//++++++++++++++ test url      +++++++++++++++++//
		//const string strChatUpdateSeenAPI = "http://192.168.1.20/youmeusencrypted/helloservice/chatupdateseenat";
		async internal Task<string> CallChatUpdateSeenAPI(string strUserId,string strTo)
		{
			string strResult;
			StringBuilder builder = new StringBuilder(strChatUpdateSeenAPI);
			builder.Append("?iduser=" + strUserId + "&to="+strTo +"");
			string strConcatenatedURL = builder.ToString();
			builder.Clear();
			builder = null;
			strResult = await CallService(strConcatenatedURL);
			return strResult;
		}
		#endregion
		//callservice
		async Task<string> CallService(string strURL)
		{ 
			WebClient client = new WebClient ();
			client.Credentials = CredentialCache.DefaultCredentials;
			string strResult;
			try
			{
				strResult=await client.DownloadStringTaskAsync (new Uri(strURL));
			}
			catch(Exception e)
			{
				Console.WriteLine ("Exception:"+ e.Message);
				strResult = "Exception";
			}
			finally
			{
				client.Dispose ();
				client = null; 
			} 
			return strResult;
		}  
	}
}

