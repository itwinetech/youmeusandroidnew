﻿using Android.App;
using Android.Views;
using Android.Graphics;
using System.Collections.Generic;
using System.Linq;
using YouMeUsAndroid;
using System;
using System.IO;
using Android.Widget;
using Java.Net;
using System.Threading.Tasks;
using Square.Picasso;
using Android.Text;
using Android.Webkit;
using System.Text.RegularExpressions;

namespace YouMeUsAndroid

{
	#region " AutoComplete (text with image) adapter"//tag interests
	public class TagInterestAutoCompleteListAdapter : BaseAdapter<Taginterest>, IFilterable
	{
		string[] items;
		ArrayFilterr filterr;
		public string[] OriginalItems
		{
			get { return this.items; }
			set { this.items = value; }
		}
		Activity _context;
		List<Taginterest> myinterestList;
		internal event Action<Taginterest> ItemClick;
		ViewHolderTagInterest objViewHolderTagInterest;
		public TagInterestAutoCompleteListAdapter(Activity context, List<Taginterest> myinterestList1)
		{
			_context = context;
			myinterestList = myinterestList1;
		}
		public override Taginterest this[int position]
		{
			get { return this.myinterestList[position]; }
		}
		public override int Count
		{
			get { return this.myinterestList.Count(); }
		}

		public Filter Filter
		{
			get
			{
				if (filterr == null)
				{
					filterr = new ArrayFilterr();
					filterr.OriginalData = this.OriginalItems;
					filterr.SAdapter = this;
				}
				return filterr;
			}
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;

			if (view == null)
			{
				if (CreateEventsActivity.strActivityName == "CreateGroupActivity")
				{
					view = this._context.LayoutInflater.Inflate(Resource.Layout.CustomInterestTagLayout, null);
					objViewHolderTagInterest = new ViewHolderTagInterest();
					objViewHolderTagInterest.lblName = view.FindViewById<TextView>(Resource.Id.lblInterestName);
					objViewHolderTagInterest.image = view.FindViewById<ImageView>(Resource.Id.imgInterestLogo);
					objViewHolderTagInterest.Initialize(view);
					view.Tag = objViewHolderTagInterest;
				}
				else if (CreateEventsActivity.strActivityName == "CreateEventsActivity")
				{
					view = this._context.LayoutInflater.Inflate(Resource.Layout.CustomLayout, null);
					objViewHolderTagInterest = new ViewHolderTagInterest();
					objViewHolderTagInterest.lblName = view.FindViewById<TextView>(Resource.Id.lblNameCL);
					objViewHolderTagInterest.Initialize(view);
					view.Tag = objViewHolderTagInterest;
				}
			}
			else
			{
				objViewHolderTagInterest = view.Tag as ViewHolderTagInterest;
			}
			objViewHolderTagInterest.ViewClicked = () =>
			{
				ItemClick(myinterestList[position]);

			};
			if (CreateEventsActivity.strActivityName == "CreateGroupActivity")
			{
				objViewHolderTagInterest.lblName.Text = myinterestList[position].name;
				Picasso.With(_context).Load(myinterestList[position].Interestlogo).Into(objViewHolderTagInterest.image);
			}
			else if (CreateEventsActivity.strActivityName == "CreateEventsActivity")
			{
				objViewHolderTagInterest.lblName.Text = myinterestList[position].name;
			}
			return view;
		}

	}
	public class ArrayFilterr : Filter

	{
		string[] originalData;
		public string[] OriginalData
		{
			get { return this.originalData; }
			set { this.originalData = value; }
		}

		TagInterestAutoCompleteListAdapter adapter;
		public TagInterestAutoCompleteListAdapter SAdapter
		{
			get { return adapter; }
			set { this.adapter = value; }
		}

		protected override Filter.FilterResults PerformFiltering(Java.Lang.ICharSequence constraint)
		{

			FilterResults oreturn = new FilterResults();
			if (constraint == null || constraint.Length() == 0)
			{
				oreturn.Values = this.OriginalData;
				oreturn.Count = this.OriginalData.Length;
			}
			else
			{
				string[] actualResults = new string[this.originalData.Length];
				int i = 0;
				foreach (string str in this.originalData)
				{
					if (str.ToUpperInvariant().StartsWith(constraint.ToString().ToUpperInvariant()))
					{
						actualResults[i] = str;
						i++;
					}
				}
				oreturn.Values = actualResults;
				oreturn.Count = actualResults.Length;
			}

			return oreturn;
		}

		protected override void PublishResults(Java.Lang.ICharSequence constraint, Filter.FilterResults results)
		{
			if (results.Count == 0)
				this.SAdapter.NotifyDataSetInvalidated();
			else
			{
				SAdapter.OriginalItems = (string[])results.Values;
				SAdapter.NotifyDataSetChanged();
			}
		}
	}

	#region " AutoComplete (text with image) adapter"//tag interests
	public class NewMessageAutoCompleteListAdapter : BaseAdapter<NewMessgeToList>, IFilterable
	{
		string[] items;
		ArrayNewMessageFilterr filterr;
		public string[] OriginalItems
		{
			get { return this.items; }
			set { this.items = value; }
		}
		Activity _context;
		List<NewMessgeToList> myinterestList;
		internal event Action<NewMessgeToList> ItemClick;
		ViewHolderTagInterest objViewHolderTagInterest;
		public NewMessageAutoCompleteListAdapter(Activity context, List<NewMessgeToList> myinterestList1)
		{
			_context = context;
			myinterestList = myinterestList1;
		}
		public override NewMessgeToList this[int position]
		{
			get { return this.myinterestList[position]; }
		}
		public override int Count
		{
			get { return this.myinterestList.Count(); }
		}

		public Filter Filter
		{
			get
			{
				if (filterr == null)
				{
					filterr = new ArrayNewMessageFilterr();
					filterr.OriginalData = this.OriginalItems;
					filterr.SAdapter = this;
				}
				return filterr;
			}
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;

			if (view == null)
			{
				
					view = this._context.LayoutInflater.Inflate(Resource.Layout.CustomInterestTagLayout, null);
					objViewHolderTagInterest = new ViewHolderTagInterest();
					objViewHolderTagInterest.lblName = view.FindViewById<TextView>(Resource.Id.lblInterestName);
					objViewHolderTagInterest.image = view.FindViewById<ImageView>(Resource.Id.imgInterestLogo);
					objViewHolderTagInterest.Initialize(view);
					view.Tag = objViewHolderTagInterest;
				
			}
			else
			{
				objViewHolderTagInterest = view.Tag as ViewHolderTagInterest;
			}
			objViewHolderTagInterest.ViewClicked = () =>
			{
				ItemClick(myinterestList[position]);

			};

				objViewHolderTagInterest.lblName.Text = myinterestList[position].Fname;
				Picasso.With(_context).Load(myinterestList[position].Modifiedimage).Into(objViewHolderTagInterest.image);


			return view;
		}

	}
	#endregion

	public class ArrayNewMessageFilterr : Filter

	{
		string[] originalData;
		public string[] OriginalData
		{
			get { return this.originalData; }
			set { this.originalData = value; }
		}

		NewMessageAutoCompleteListAdapter adapter;
		public NewMessageAutoCompleteListAdapter SAdapter
		{
			get { return adapter; }
			set { this.adapter = value; }
		}

		protected override Filter.FilterResults PerformFiltering(Java.Lang.ICharSequence constraint)
		{

			FilterResults oreturn = new FilterResults();
			if (constraint == null || constraint.Length() == 0)
			{
				oreturn.Values = this.OriginalData;
				oreturn.Count = this.OriginalData.Length;
			}
			else
			{
				string[] actualResults = new string[this.originalData.Length];
				int i = 0;
				foreach (string str in this.originalData)
				{
					if (str.ToUpperInvariant().StartsWith(constraint.ToString().ToUpperInvariant()))
					{
						actualResults[i] = str;
						i++;
					}
				}
				oreturn.Values = actualResults;
				oreturn.Count = actualResults.Length;
			}

			return oreturn;
		}

		protected override void PublishResults(Java.Lang.ICharSequence constraint, Filter.FilterResults results)
		{
			if (results.Count == 0)
				this.SAdapter.NotifyDataSetInvalidated();
			else
			{
				SAdapter.OriginalItems = (string[])results.Values;
				SAdapter.NotifyDataSetChanged();
			}
		}
	}


	//viewholder TagInterest class
	public class ViewHolderTagInterest : Java.Lang.Object
	{
		internal TextView lblName;
		internal ImageView image;
		public Action ViewClicked { get; set; }

		public void Initialize(View view)
		{
			view.Click += delegate (object sender, EventArgs e)
			{
				ViewClicked();
			};
		}
	}
	#endregion

	#region " AutoComplete (text) adapter"//tag groups
	public class TagGroupAutoCompleteListAdapter : BaseAdapter<SelectDropdownList>, IFilterable
	{
		string[] items;
		ArrayFilterrGroup filterr;
		public string[] OriginalItems
		{
			get { return this.items; }
			set { this.items = value; }
		}
		Activity _context;
		List<SelectDropdownList> myinterestList;
		internal event Action<SelectDropdownList> ItemClick;
		ViewHolderTagGroup objViewHolderTagGroup;
		public TagGroupAutoCompleteListAdapter(Activity context, List<SelectDropdownList> myinterestList1)
		{
			_context = context;
			myinterestList = myinterestList1;
		}
		public override SelectDropdownList this[int position]
		{
			get { return this.myinterestList[position]; }
		}
		public override int Count
		{
			get { return this.myinterestList.Count(); }
		}

		public Filter Filter
		{
			get
			{
				if (filterr == null)
				{
					filterr = new ArrayFilterrGroup();
					filterr.OriginalData = this.OriginalItems;
					filterr.SAdapter = this;
				}
				return filterr;
			}
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;

			if (view == null)
			{

				view = this._context.LayoutInflater.Inflate(Resource.Layout.CustomLayout, null);
				objViewHolderTagGroup = new ViewHolderTagGroup();
				objViewHolderTagGroup.lblName = view.FindViewById<TextView>(Resource.Id.lblNameCL);
				objViewHolderTagGroup.Initialize(view);
				view.Tag = objViewHolderTagGroup;

			}
			else
			{
				objViewHolderTagGroup = view.Tag as ViewHolderTagGroup;
			}
			objViewHolderTagGroup.ViewClicked = () =>
			{
				ItemClick(myinterestList[position]);

			};
			objViewHolderTagGroup.lblName.Text = myinterestList[position].Groupname;
			return view;
		}
	}

	public class ArrayFilterrGroup : Filter

	{
		string[] originalData;
		public string[] OriginalData
		{
			get { return this.originalData; }
			set { this.originalData = value; }
		}

		TagGroupAutoCompleteListAdapter adapter;
		public TagGroupAutoCompleteListAdapter SAdapter
		{
			get { return adapter; }
			set { this.adapter = value; }
		}

		protected override Filter.FilterResults PerformFiltering(Java.Lang.ICharSequence constraint)
		{

			FilterResults oreturn = new FilterResults();
			if (constraint == null || constraint.Length() == 0)
			{
				oreturn.Values = this.OriginalData;
				oreturn.Count = this.OriginalData.Length;
			}
			else
			{
				string[] actualResults = new string[this.originalData.Length];
				int i = 0;
				foreach (string str in this.originalData)
				{
					if (str.ToUpperInvariant().StartsWith(constraint.ToString().ToUpperInvariant()))
					{
						actualResults[i] = str;
						i++;
					}
				}
				oreturn.Values = actualResults;
				oreturn.Count = actualResults.Length;
			}

			return oreturn;
		}

		protected override void PublishResults(Java.Lang.ICharSequence constraint, Filter.FilterResults results)
		{
			if (results.Count == 0)
				this.SAdapter.NotifyDataSetInvalidated();
			else
			{
				SAdapter.OriginalItems = (string[])results.Values;
				SAdapter.NotifyDataSetChanged();
			}
		}
	}


	//viewholder TagGrou pclass
	public class ViewHolderTagGroup : Java.Lang.Object
	{
		internal TextView lblName;
		internal ImageView image;
		public Action ViewClicked { get; set; }

		public void Initialize(View view)
		{
			view.Click += delegate (object sender, EventArgs e)
			{
				ViewClicked();
			};
		}
	}

	#endregion


	#region "Interest"

	#region "Popular Interest Adapter"
	public class InterestAdapterClass : BaseAdapter<PopularInterest>
	{
		List<PopularInterest> _items;
		List<PopularInterest> myinterestList;
		Activity _context;
		private int posizione;
		int[] statoChk;
		int count = 0;
		ViewHolderIntrestClass objViewHolderIntrestClass;

		internal event Action<PopularInterest> FollowUnFollowButtonClick;
		internal event Action<PopularInterest> InterestRowClick;
		//bool r;
		public InterestAdapterClass(Activity context, List<PopularInterest> interestList, List<PopularInterest> myinterestList1)
		{
			_items = interestList;
			_context = context;

			myinterestList = myinterestList1;
			this.statoChk = new int[interestList.Count];
			for (int i = 0; i < interestList.Count; i++)
			{
				statoChk[i] = 0;
			}

		}
		public override PopularInterest this[int position]
		{
			get { return _items[position]; }
		}

		public override int Count
		{
			get { return _items.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			posizione = position;
			//string imageSrc = strPath + "/" + _items [position].Interestlogo;   
			View view = convertView;
			count = 0;
			if (view == null)
			{
				view = this._context.LayoutInflater.Inflate(Resource.Layout.CustomInterestLayout, parent, false);

				objViewHolderIntrestClass = new ViewHolderIntrestClass();
				objViewHolderIntrestClass.imgInterests = view.FindViewById<ImageView>(Resource.Id.imgInterests);
				objViewHolderIntrestClass.btnFollowing = view.FindViewById<Button>(Resource.Id.btnFollowing);
				objViewHolderIntrestClass.lblInterestsName = view.FindViewById<TextView>(Resource.Id.lblInterestsTitle);

				objViewHolderIntrestClass.imgIntrestFollower1 = view.FindViewById<ImageView>(Resource.Id.img1CustomIL);
				objViewHolderIntrestClass.imgIntrestFollower2 = view.FindViewById<ImageView>(Resource.Id.img2CustomIL);
				objViewHolderIntrestClass.imgIntrestFollower3 = view.FindViewById<ImageView>(Resource.Id.img3CustomIL);
				objViewHolderIntrestClass.imgIntrestFollower4 = view.FindViewById<ImageView>(Resource.Id.img4CustomIL);
				objViewHolderIntrestClass.lblIntrestMembersList = view.FindViewById<TextView>(Resource.Id.lblGroupMemberNumCustomIL);
				objViewHolderIntrestClass.Initialize(view);
				view.Tag = objViewHolderIntrestClass;
			}
			else
			{
				objViewHolderIntrestClass = (ViewHolderIntrestClass)view.Tag;
			}
			objViewHolderIntrestClass.ViewClicked = () =>
			{
				FollowUnFollowButtonClick(_items[position]);

			};
			objViewHolderIntrestClass.ViewRowClicked = () =>
			{
				InterestRowClick(_items[position]);

			};
			if (statoChk[position] == 0)
			{
				objViewHolderIntrestClass.btnFollowing.SetBackgroundResource(Resource.Drawable.addfollowing); // set unchecked"
			}
			else
			{
				objViewHolderIntrestClass.btnFollowing.SetBackgroundResource(Resource.Drawable.correctfollowing); // set checked"
			}

			List<PopularInterest> listValue = myinterestList.Where(x => x.Idinterestmaster.Equals(_items[position].Idinterestmaster)).ToList();
			if (listValue.Count > 0)
			{
				if (listValue[0].Idinterestmaster == _items[position].Idinterestmaster)
				{
					objViewHolderIntrestClass.btnFollowing.SetBackgroundResource(Resource.Drawable.correctfollowing);
				}
				else
				{
					objViewHolderIntrestClass.btnFollowing.SetBackgroundResource(Resource.Drawable.addfollowing);
				}
			}

			objViewHolderIntrestClass.lblInterestsName.Text = _items[position].Interestname;
			Picasso.With(_context).Load(_items[position].Interestlogo).Into(objViewHolderIntrestClass.imgInterests);
			if (_items[position].Followers != null)
			{
				if (_items[position].totalfollowers == "1")
				{
					objViewHolderIntrestClass.lblIntrestMembersList.Text = _items[position].totalfollowers + " " + "Member";
					objViewHolderIntrestClass.imgIntrestFollower1.Visibility = ViewStates.Visible;
					objViewHolderIntrestClass.imgIntrestFollower2.Visibility = ViewStates.Invisible;
					objViewHolderIntrestClass.imgIntrestFollower3.Visibility = ViewStates.Invisible;
					objViewHolderIntrestClass.imgIntrestFollower4.Visibility = ViewStates.Invisible;
				}
				else if (_items[position].totalfollowers == "2")
				{
					objViewHolderIntrestClass.lblIntrestMembersList.Text = _items[position].totalfollowers + " " + "Members";
					objViewHolderIntrestClass.imgIntrestFollower1.Visibility = ViewStates.Visible;
					objViewHolderIntrestClass.imgIntrestFollower2.Visibility = ViewStates.Visible;
					objViewHolderIntrestClass.imgIntrestFollower3.Visibility = ViewStates.Invisible;
					objViewHolderIntrestClass.imgIntrestFollower4.Visibility = ViewStates.Invisible;
				}
				else if (_items[position].totalfollowers == "3")
				{
					objViewHolderIntrestClass.lblIntrestMembersList.Text = _items[position].totalfollowers + " " + "Members";
					objViewHolderIntrestClass.imgIntrestFollower1.Visibility = ViewStates.Visible;
					objViewHolderIntrestClass.imgIntrestFollower2.Visibility = ViewStates.Visible;
					objViewHolderIntrestClass.imgIntrestFollower3.Visibility = ViewStates.Visible;
					objViewHolderIntrestClass.imgIntrestFollower4.Visibility = ViewStates.Invisible;
				}
				else 
				{
					objViewHolderIntrestClass.lblIntrestMembersList.Text = _items[position].totalfollowers + " " + "Members";
					objViewHolderIntrestClass.imgIntrestFollower1.Visibility = ViewStates.Visible;
					objViewHolderIntrestClass.imgIntrestFollower2.Visibility = ViewStates.Visible;
					objViewHolderIntrestClass.imgIntrestFollower3.Visibility = ViewStates.Visible;
					objViewHolderIntrestClass.imgIntrestFollower4.Visibility = ViewStates.Visible;
				}
				foreach (var groupFollowers in _items[position].Followers)
				{

					if (count <= 3)
					{

						if (count == 0)
						{
							Picasso.With(_context).Load(groupFollowers.Modifiedimage).Into(objViewHolderIntrestClass.imgIntrestFollower1);
						}
						else if (count == 1)
						{
							Picasso.With(_context).Load(groupFollowers.Modifiedimage).Into(objViewHolderIntrestClass.imgIntrestFollower2);
						}
						else if (count == 2)
						{
							Picasso.With(_context).Load(groupFollowers.Modifiedimage).Into(objViewHolderIntrestClass.imgIntrestFollower3);
						}
						else if (count == 3)
						{
							Picasso.With(_context).Load(groupFollowers.Modifiedimage).Into(objViewHolderIntrestClass.imgIntrestFollower4);
						}
						count++;
					}
					else
					{
						break;
					}
				}


			}
			return view;
		}
	}
	#endregion

	#region "My Interest Adapter"
	public class MyInterestAdapterClass : BaseAdapter<PopularInterest>
	{
		List<PopularInterest> _items;
		Activity _context;
		ViewHolderIntrestClass objViewHolderIntrestClass;
		private int posizione;
		int[] statoChk;
		int count = 0;
		internal event Action<PopularInterest> FollowUnFollowButtonClick;
		internal event Action<PopularInterest> InterestRowClick;
		public MyInterestAdapterClass(Activity context, List<PopularInterest> interestList)
		{
			_items = interestList;
			_context = context;
			this.statoChk = new int[interestList.Count];
			for (int i = 0; i < interestList.Count; i++)
			{
				statoChk[i] = 0;
			}
		}
		public override PopularInterest this[int position]
		{
			get { return _items[position]; }
		}

		public override int Count
		{
			get { return _items.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			//string imageSrc = strPath +"/"+ _items [position].Interestlogo;   
			View view = convertView;
			count = 0;

			if (view == null)
			{
				view = this._context.LayoutInflater.Inflate(Resource.Layout.CustomInterestLayout, parent, false);

				objViewHolderIntrestClass = new ViewHolderIntrestClass();
				objViewHolderIntrestClass.imgInterests = view.FindViewById<ImageView>(Resource.Id.imgInterests);
				objViewHolderIntrestClass.btnFollowing = view.FindViewById<Button>(Resource.Id.btnFollowing);
				objViewHolderIntrestClass.lblInterestsName = view.FindViewById<TextView>(Resource.Id.lblInterestsTitle);

				objViewHolderIntrestClass.imgIntrestFollower1 = view.FindViewById<ImageView>(Resource.Id.img1CustomIL);
				objViewHolderIntrestClass.imgIntrestFollower2 = view.FindViewById<ImageView>(Resource.Id.img2CustomIL);
				objViewHolderIntrestClass.imgIntrestFollower3 = view.FindViewById<ImageView>(Resource.Id.img3CustomIL);
				objViewHolderIntrestClass.imgIntrestFollower4 = view.FindViewById<ImageView>(Resource.Id.img4CustomIL);
				objViewHolderIntrestClass.lblIntrestMembersList = view.FindViewById<TextView>(Resource.Id.lblGroupMemberNumCustomIL);

				objViewHolderIntrestClass.Initialize(view);
				view.Tag = objViewHolderIntrestClass;


			}
			else
			{
				objViewHolderIntrestClass = (ViewHolderIntrestClass)view.Tag;
			}
			objViewHolderIntrestClass.ViewClicked = () =>
			{
				FollowUnFollowButtonClick(_items[position]);

			};
			objViewHolderIntrestClass.ViewRowClicked = () =>
			{
				InterestRowClick(_items[position]);

			};

			if (SlidingTabActivity.isSelectedTitle == 2)
			{
				objViewHolderIntrestClass.btnFollowing.SetBackgroundResource(Resource.Drawable.correctfollowing);
			}
			if (statoChk[position] == 0)
			{
				objViewHolderIntrestClass.btnFollowing.SetBackgroundResource(Resource.Drawable.correctfollowing); // set unchecked"
			}
			else
			{
				objViewHolderIntrestClass.btnFollowing.SetBackgroundResource(Resource.Drawable.addfollowing); // set checked"
			}
			objViewHolderIntrestClass.lblInterestsName.Text = _items[position].Interestname;
			Picasso.With(_context).Load(_items[position].UrlInterestlogo).Into(objViewHolderIntrestClass.imgInterests);

			if (_items[position].Followers != null)
			{
				if (_items[position].totalfollowers == "1")
				{
					objViewHolderIntrestClass.lblIntrestMembersList.Text = _items[position].totalfollowers + " " + "Member";
					objViewHolderIntrestClass.imgIntrestFollower1.Visibility = ViewStates.Visible;
					objViewHolderIntrestClass.imgIntrestFollower2.Visibility = ViewStates.Invisible;
					objViewHolderIntrestClass.imgIntrestFollower3.Visibility = ViewStates.Invisible;
					objViewHolderIntrestClass.imgIntrestFollower4.Visibility = ViewStates.Invisible;
				}
				else if (_items[position].totalfollowers == "2")
				{
					objViewHolderIntrestClass.lblIntrestMembersList.Text = _items[position].totalfollowers + " " + "Members";
					objViewHolderIntrestClass.imgIntrestFollower1.Visibility = ViewStates.Visible;
					objViewHolderIntrestClass.imgIntrestFollower2.Visibility = ViewStates.Visible;
					objViewHolderIntrestClass.imgIntrestFollower3.Visibility = ViewStates.Invisible;
					objViewHolderIntrestClass.imgIntrestFollower4.Visibility = ViewStates.Invisible;
				}
				else if (_items[position].totalfollowers == "3")
				{
					objViewHolderIntrestClass.lblIntrestMembersList.Text = _items[position].totalfollowers + " " + "Members";
					objViewHolderIntrestClass.imgIntrestFollower1.Visibility = ViewStates.Visible;
					objViewHolderIntrestClass.imgIntrestFollower2.Visibility = ViewStates.Visible;
					objViewHolderIntrestClass.imgIntrestFollower3.Visibility = ViewStates.Visible;
					objViewHolderIntrestClass.imgIntrestFollower4.Visibility = ViewStates.Invisible;
				}
				else
				{
					objViewHolderIntrestClass.lblIntrestMembersList.Text = _items[position].totalfollowers + " " + "Members";
					objViewHolderIntrestClass.imgIntrestFollower1.Visibility = ViewStates.Visible;
					objViewHolderIntrestClass.imgIntrestFollower2.Visibility = ViewStates.Visible;
					objViewHolderIntrestClass.imgIntrestFollower3.Visibility = ViewStates.Visible;
					objViewHolderIntrestClass.imgIntrestFollower4.Visibility = ViewStates.Visible;
				}
				foreach (var groupFollowers in _items[position].Followers)
				{

					if (count <= 3)
					{

						if (count == 0)
						{
							Picasso.With(_context).Load(groupFollowers.Modifiedimage).Into(objViewHolderIntrestClass.imgIntrestFollower1);
						}
						else if (count == 1)
						{
							Picasso.With(_context).Load(groupFollowers.Modifiedimage).Into(objViewHolderIntrestClass.imgIntrestFollower2);
						}
						else if (count == 2)
						{
							Picasso.With(_context).Load(groupFollowers.Modifiedimage).Into(objViewHolderIntrestClass.imgIntrestFollower3);
						}
						else if (count == 3)
						{
							Picasso.With(_context).Load(groupFollowers.Modifiedimage).Into(objViewHolderIntrestClass.imgIntrestFollower4);
						}
						count++;
					}
					else
					{
						break;
					}
				}

			}
			return view;
		}
	}
	#endregion

	#region "ViewHolderIntrestClass"
	public class ViewHolderIntrestClass : Java.Lang.Object
	{
		//LinearLayout LinearLayoutListInterestSub;
		internal ImageView imgInterests;
		internal Button btnFollowing;
		internal TextView lblInterestsName;
		internal TextView lblIntrestMembersList;

		internal ImageView imgIntrestFollower1;
		internal ImageView imgIntrestFollower2;
		internal ImageView imgIntrestFollower3;
		internal ImageView imgIntrestFollower4;

		//Activity context;
		public Action ViewClicked { get; set; }
		public Action ViewRowClicked { get; set; }
		public void Initialize(View view)
		{
			view.Click += delegate
			{
				ViewRowClicked();
			};
			btnFollowing.Click += delegate (object sender, EventArgs e)
			{
				ViewClicked();
				if (SlidingTabActivity.isSelectedTitle == 1)
				{
					btnFollowing.SetBackgroundResource(Resource.Drawable.correctfollowing);
				}
				else if (SlidingTabActivity.isSelectedTitle == 2)
				{
					btnFollowing.SetBackgroundResource(Resource.Drawable.addfollowing);
				}

			};

		}
	}

	#endregion

	#endregion

	#region "Events"

	#region "Suggested Events Adapter"
	public class EventsAdapterClass : BaseAdapter<EventsNearbyLocation>
	{
		List<EventsNearbyLocation> _items;
		List<EventsNearbyLocation> lstMyEventList;
		Activity _context;
		ViewHolderEvents objViewHolderEvents;
		internal event Action<EventsNearbyLocation> GoogleMapClick;
		internal event Action<EventsNearbyLocation> AddRemoveButtonClick;
		internal event Action<EventsNearbyLocation> EventRowClicke;
		private int posizione;
		int[] statoChk;
		public EventsAdapterClass(Activity context, List<EventsNearbyLocation> lstEventList, List<EventsNearbyLocation> _lstMyEventList)
		{
			_items = lstEventList;
			lstMyEventList = _lstMyEventList;
			_context = context;
			this.statoChk = new int[lstEventList.Count];
			for (int i = 0; i < lstEventList.Count; i++)
			{
				statoChk[i] = 0;
			}
		}
		public override EventsNearbyLocation this[int position]
		{
			get { return _items[position]; }
		}

		public override int Count
		{
			get { return _items.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			//string imageSrc = strPath +"/"+ _items [position].Eventlogo;   
			View view = convertView;

			if (view == null)
			{
				view = this._context.LayoutInflater.Inflate(Resource.Layout.CustomEventLayout, parent, false);
				objViewHolderEvents = new ViewHolderEvents();

				objViewHolderEvents.btnPlusMark = view.FindViewById<Button>(Resource.Id.btnAddEvents);
				objViewHolderEvents.btnCost = view.FindViewById<Button>(Resource.Id.btnCostEvent);
				objViewHolderEvents.imgEventLogo = view.FindViewById<ImageView>(Resource.Id.imageEvent);
				objViewHolderEvents.lblEventName = view.FindViewById<TextView>(Resource.Id.lblEventTitle);
				objViewHolderEvents.lblEventDateTime = view.FindViewById<TextView>(Resource.Id.lblEventDateTime);
				objViewHolderEvents.lblEventAddress = view.FindViewById<TextView>(Resource.Id.lblEventAddress);

				objViewHolderEvents.Initialize(view);

				view.Tag = objViewHolderEvents;
			}
			else
			{
				objViewHolderEvents = (ViewHolderEvents)view.Tag;
				//objViewHolderEvents = view.Tag as ViewHolderEvents;
			}
			objViewHolderEvents.ViewClicked = () =>
			{
				AddRemoveButtonClick(_items[position]);

			};
			objViewHolderEvents.ViewRowClicked = () =>
			{
				EventRowClicke(_items[position]);
			};
			objViewHolderEvents.ViewGoogleMap= () =>
			{
				GoogleMapClick(_items[position]);

			};
			if (statoChk[position] == 0)
			{
				objViewHolderEvents.btnPlusMark.SetBackgroundResource(Resource.Drawable.plusmark);   // set unchecked"
			}
			else
			{
				objViewHolderEvents.btnPlusMark.SetBackgroundResource(Resource.Drawable.bluecheckmark);  // set checked"
			}
			List<EventsNearbyLocation> listValue = lstMyEventList.Where(x => x.Idevents.Equals(_items[position].Idevents)).ToList();
			if (listValue.Count > 0)
			{
				if (listValue[0].Idevents == _items[position].Idevents)
				{
					objViewHolderEvents.btnPlusMark.SetBackgroundResource(Resource.Drawable.bluecheckmark);
				}
				else
				{
					objViewHolderEvents.btnPlusMark.SetBackgroundResource(Resource.Drawable.plusmark);
				}
			}
			objViewHolderEvents.lblEventName.SetTypeface(null, TypefaceStyle.Bold);
			objViewHolderEvents.lblEventName.Text = _items[position].Eventname.ToUpper();

			DateTime datetime = Convert.ToDateTime(_items[position].date);
			string strEventDate = datetime.ToString("MMMMM dd yyyy");

			if (_items[position].EventEndtime == "24:00:00")
			{

				objViewHolderEvents.lblEventDateTime.Text = strEventDate + " at " + _items[position].Eventtime + "-" + "24:00";
			}
			else
			{
				objViewHolderEvents.lblEventDateTime.Text = strEventDate + " at " + _items[position].Eventtime + "-" + Convert.ToDateTime(_items[position].EventEndtime).ToString("HH:mm");//+"-"+_items[position].EventEndtime;
			}
			if (_items[position].Ispaidevent == "1")
			{
				objViewHolderEvents.btnCost.SetBackgroundResource(Resource.Drawable.shoping_cart);
			}
			else
			{
				objViewHolderEvents.btnCost.SetBackgroundResource(Resource.Drawable.freeimage);
			}
			objViewHolderEvents.lblEventAddress.Text = _items[position].Address;
			if (!string.IsNullOrEmpty(_items[position].Eventlogo))
			{
				Picasso.With(_context).Load(_items[position].Eventlogo).Into(objViewHolderEvents.imgEventLogo);
			}
			else
			{
				objViewHolderEvents.imgEventLogo.SetImageResource(Resource.Drawable.noimages);
			}
			if(SlidingTabActivity.isPosition)
			{
				if (_items[position].Eventname.Equals(GoogleMapActivity.StrEventName))
				{
					
				}
				else
				{
					
				}
			}
			return view;
		}
	}
	#endregion

	#region "MyEvents Adapter"
	public class MyEventsAdapterClass : BaseAdapter<EventsNearbyLocation>
	{
		List<EventsNearbyLocation> _items;
		Activity _context;
		ViewHolderEvents objViewHolderEvents;
		internal event Action<EventsNearbyLocation> GoogleMapClick;
		internal event Action<EventsNearbyLocation> AddRemoveButtonClick;
		internal event Action<EventsNearbyLocation> EventRowClick;
		private int posizione;
		int[] statoChk;
		public MyEventsAdapterClass(Activity context, List<EventsNearbyLocation> lstEventList)
		{
			_items = lstEventList;
			_context = context;
			this.statoChk = new int[lstEventList.Count];
			for (int i = 0; i < lstEventList.Count; i++)
			{
				statoChk[i] = 0;
			}
		}
		public override EventsNearbyLocation this[int position]
		{
			get { return _items[position]; }
		}

		public override int Count
		{
			get { return _items.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			//string imageSrc = strPath +"/"+ _items [position].Eventlogo;   
			View view = convertView;

			if (view == null)
			{
				view = this._context.LayoutInflater.Inflate(Resource.Layout.CustomEventLayout, parent, false);
				objViewHolderEvents = new ViewHolderEvents();

				objViewHolderEvents.btnPlusMark = view.FindViewById<Button>(Resource.Id.btnAddEvents);
				objViewHolderEvents.btnCost = view.FindViewById<Button>(Resource.Id.btnCostEvent);
				objViewHolderEvents.imgEventLogo = view.FindViewById<ImageView>(Resource.Id.imageEvent);
				objViewHolderEvents.lblEventName = view.FindViewById<TextView>(Resource.Id.lblEventTitle);
				objViewHolderEvents.lblEventDateTime = view.FindViewById<TextView>(Resource.Id.lblEventDateTime);
				objViewHolderEvents.lblEventAddress = view.FindViewById<TextView>(Resource.Id.lblEventAddress);

				objViewHolderEvents.Initialize(view);

				view.Tag = objViewHolderEvents;
			}
			else
			{
				objViewHolderEvents = (ViewHolderEvents)view.Tag;
			}
			objViewHolderEvents.ViewClicked = () =>
			{
				AddRemoveButtonClick(_items[position]);
			};
			objViewHolderEvents.ViewRowClicked = () =>
			{
				EventRowClick(_items[position]);
			};
			objViewHolderEvents.ViewGoogleMap = () =>
			 {
				 GoogleMapClick(_items[position]);

			 };
			if (statoChk[position] == 0)
			{
				objViewHolderEvents.btnPlusMark.SetBackgroundResource(Resource.Drawable.bluecheckmark);  // set unchecked"
			}
			else
			{
				objViewHolderEvents.btnPlusMark.SetBackgroundResource(Resource.Drawable.plusmark);   // set checked"
			}
			if (SlidingTabActivity.isSelectedTitle == 2)
			{
				objViewHolderEvents.btnPlusMark.SetBackgroundResource(Resource.Drawable.bluecheckmark);
			}
			objViewHolderEvents.lblEventName.SetTypeface(null, TypefaceStyle.Bold);
			objViewHolderEvents.lblEventName.Text = _items[position].Eventname.ToUpper();

			DateTime datetime = Convert.ToDateTime(_items[position].date);
			string strEventDate = datetime.ToString("MMMMM dd yyyy");

			if (_items[position].EventEndtime == "24:00:00")
			{
				objViewHolderEvents.lblEventDateTime.Text = strEventDate + " at " + _items[position].Eventtime + "-" + "24:00";
			}
			else
			{
				if (_items[position].EventEndtime == null || _items[position].EventEndtime == "")
				{
					objViewHolderEvents.lblEventDateTime.Text = strEventDate + " at " + _items[position].Eventtime + "-" + "00:00";

				}
				else
				{

					objViewHolderEvents.lblEventDateTime.Text = strEventDate + " at " + _items[position].Eventtime + "-" + Convert.ToDateTime(_items[position].EventEndtime).ToString("HH:mm");
				}
			}

			if (_items[position].Ispaidevent == "1")
			{
				objViewHolderEvents.btnCost.SetBackgroundResource(Resource.Drawable.shoping_cart);
			}
			else
			{
				objViewHolderEvents.btnCost.SetBackgroundResource(Resource.Drawable.freeimage);
			}
			objViewHolderEvents.lblEventAddress.Text = _items[position].Address;
			if (!string.IsNullOrEmpty(_items[position].UrlEventslogo))
			{
				Picasso.With(_context).Load(_items[position].UrlEventslogo).Into(objViewHolderEvents.imgEventLogo);
			}
			else
			{
				objViewHolderEvents.imgEventLogo.SetImageResource(Resource.Drawable.noimages);
			}

			return view;
		}
	}
	#endregion


	#region "ViewHolderIntrestClass"
	public class ViewHolderEvents : Java.Lang.Object
	{
		public ImageView imgEventLogo;
		public Button btnPlusMark;
		public Button btnCost;
		public TextView lblEventName;
		public TextView lblEventDateTime;
		public TextView lblEventAddress;
		public Action ViewClicked { get; set; }
		public Action ViewGoogleMap { get; set;}
		public Action ViewRowClicked { get; set; }
		public void Initialize(View view)
		{
			view.Click += delegate (object sender, EventArgs e)
			{
				ViewRowClicked();
			};
			btnPlusMark.Click += delegate
			{
				ViewClicked();
				if (SlidingTabActivity.isSelectedTitle == 1)
				{
					btnPlusMark.SetBackgroundResource(Resource.Drawable.bluecheckmark);
				}
				else if (SlidingTabActivity.isSelectedTitle == 2)
				{
					btnPlusMark.SetBackgroundResource(Resource.Drawable.plusmark);
				}
			};
			lblEventAddress.Click += delegate
			{
				ViewGoogleMap();
			};
		}
	}
	#endregion

	#endregion



	#region "Groups"

	#region "Suggested group"
	public class GroupsAdapterClass : BaseAdapter<Mygroup>
	{
		List<Mygroup> _items;
		List<Mygroup> myGroupList;
		Activity _context;
		private int posizione;
		int[] statoChk;
		int count = 0;
		internal static string isaMember = "";
		ViewHolderGroupClass objViewHolderGroupClass;
		internal event Action<Mygroup> GroupRowClick;
		internal event Action<Mygroup> GroupFollowUnFollowButtonClick;
		public GroupsAdapterClass(Activity context, List<Mygroup> lstGroupList, List<Mygroup> myGroupList1)
		{
			_items = lstGroupList;
			_context = context;

			myGroupList = myGroupList1;
			this.statoChk = new int[lstGroupList.Count];
			for (int i = 0; i < lstGroupList.Count; i++)
			{
				statoChk[i] = 0;
			}

		}
		public override Mygroup this[int position]
		{
			get { return _items[position]; }
		}

		public override int Count
		{
			get { return _items.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			posizione = position;
			//string imageSrc = strPath + "/" + _items [position].Interestlogo;   
			View view = convertView;
			count = 0;
			if (view == null)
			{
				view = this._context.LayoutInflater.Inflate(Resource.Layout.CustomGroupLayout, parent, false);

				objViewHolderGroupClass = new ViewHolderGroupClass();
				objViewHolderGroupClass.imgGroups = view.FindViewById<ImageView>(Resource.Id.imgGroupProfileCustomGL);

				objViewHolderGroupClass.imgFollower1 = view.FindViewById<ImageView>(Resource.Id.img1CustomGL);
				objViewHolderGroupClass.imgFollower2 = view.FindViewById<ImageView>(Resource.Id.img2CustomGL);
				objViewHolderGroupClass.imgFollower3 = view.FindViewById<ImageView>(Resource.Id.img3CustomGL);
				objViewHolderGroupClass.imgFollower4 = view.FindViewById<ImageView>(Resource.Id.img4CustomGL);

				objViewHolderGroupClass.btnJoin = view.FindViewById<Button>(Resource.Id.btnJoin);
				objViewHolderGroupClass.lblGroupName = view.FindViewById<TextView>(Resource.Id.lblGroupNameCustomGL);
				objViewHolderGroupClass.lblGroupDetails = view.FindViewById<TextView>(Resource.Id.lblDetailsCustomGL);
				objViewHolderGroupClass.lblMembersList = view.FindViewById<TextView>(Resource.Id.lblGroupMemberNumCustomGL);
				objViewHolderGroupClass.Initialize(view);
				view.Tag = objViewHolderGroupClass;
			}
			else
			{
				objViewHolderGroupClass = (ViewHolderGroupClass)view.Tag;
			}
			objViewHolderGroupClass.ViewClicked = () =>
			{
				GroupFollowUnFollowButtonClick(_items[position]);
				foreach (var values in _items[position].usersgroupstatus)
				{
					isaMember = values.Isamember;
				}
			};
			objViewHolderGroupClass.ViewRowClicked = () =>
			  {
				  GroupRowClick(_items[position]);
			  };
			if (statoChk[position] == 0)
			{
				objViewHolderGroupClass.btnJoin.SetBackgroundResource(Resource.Drawable.join); // set unchecked"
			}
			else
			{
				objViewHolderGroupClass.btnJoin.SetBackgroundResource(Resource.Drawable.joined); // set checked"
			}




			List<Mygroup> listValue = myGroupList.Where(x => x.Idgroups.Equals(_items[position].Idgroups)).ToList();
			if (listValue.Count > 0)
			{
				if (listValue[0].Idgroups == _items[position].Idgroups)
				{
					if (listValue[0].Isamember == "2")
					{
						objViewHolderGroupClass.btnJoin.SetBackgroundResource(Resource.Drawable.request);
					}
					else if (listValue[0].Isamember == "1")
					{
						objViewHolderGroupClass.btnJoin.SetBackgroundResource(Resource.Drawable.joined);
					}
				}
				else
				{
					objViewHolderGroupClass.btnJoin.SetBackgroundResource(Resource.Drawable.join);
				}

			}
			objViewHolderGroupClass.lblGroupDetails.Visibility = ViewStates.Invisible;
			objViewHolderGroupClass.lblGroupName.Text = _items[position].Groupname;
			//objViewHolderGroupClass.lblGroupDetails.SetText(Html.FromHtml(_items[position].Groupdesc), TextView.BufferType.Spannable);

			Picasso.With(_context).Load(_items[position].Groupslogo).Into(objViewHolderGroupClass.imgGroups);
			if (_items[position].GroupFollowers != null)
			{
				if (_items[position].totalfollowers == "1")
				{
					objViewHolderGroupClass.lblMembersList.Text = _items[position].totalfollowers + " " + "Member";
					objViewHolderGroupClass.imgFollower1.Visibility = ViewStates.Visible;
					objViewHolderGroupClass.imgFollower2.Visibility = ViewStates.Invisible;
					objViewHolderGroupClass.imgFollower3.Visibility = ViewStates.Invisible;
					objViewHolderGroupClass.imgFollower4.Visibility = ViewStates.Invisible;
				}
				else if(_items[position].totalfollowers == "2")
				{
					objViewHolderGroupClass.lblMembersList.Text = _items[position].totalfollowers + " " + "Members";
					objViewHolderGroupClass.imgFollower1.Visibility = ViewStates.Visible;
					objViewHolderGroupClass.imgFollower2.Visibility = ViewStates.Visible;
					objViewHolderGroupClass.imgFollower3.Visibility = ViewStates.Invisible;
					objViewHolderGroupClass.imgFollower4.Visibility = ViewStates.Invisible;
				}
				else if (_items[position].totalfollowers == "3")
				{
					objViewHolderGroupClass.lblMembersList.Text = _items[position].totalfollowers + " " + "Members";
					objViewHolderGroupClass.imgFollower1.Visibility = ViewStates.Visible;
					objViewHolderGroupClass.imgFollower2.Visibility = ViewStates.Visible;
					objViewHolderGroupClass.imgFollower3.Visibility = ViewStates.Visible;
					objViewHolderGroupClass.imgFollower4.Visibility = ViewStates.Invisible;
				}
				else 
				{
					objViewHolderGroupClass.lblMembersList.Text = _items[position].totalfollowers + " " + "Members";
					objViewHolderGroupClass.imgFollower1.Visibility = ViewStates.Visible;
					objViewHolderGroupClass.imgFollower2.Visibility = ViewStates.Visible;
					objViewHolderGroupClass.imgFollower3.Visibility = ViewStates.Visible;
					objViewHolderGroupClass.imgFollower4.Visibility = ViewStates.Visible;
				}
				foreach (var groupFollowers in _items[position].GroupFollowers)
				{
					
					if (count <= 3 )
					{

						if (count == 0 )
						{
							Picasso.With(_context).Load(groupFollowers.Modifiedimage).Into(objViewHolderGroupClass.imgFollower1);
						}
						else if (count == 1)
						{
							Picasso.With(_context).Load(groupFollowers.Modifiedimage).Into(objViewHolderGroupClass.imgFollower2);
						}
						else if (count == 2)
						{
							Picasso.With(_context).Load(groupFollowers.Modifiedimage).Into(objViewHolderGroupClass.imgFollower3);
						}
						else if (count == 3)
						{
							Picasso.With(_context).Load(groupFollowers.Modifiedimage).Into(objViewHolderGroupClass.imgFollower4);
						}
						count++;
					}
					else
					{
						break;
					}
				}
			}

			return view;
		}
	}
	#endregion

	#region "My Group Adapter"
	public class MyGroupAdapterClass : BaseAdapter<Mygroup>
	{
		List<Mygroup> _items;
		Activity _context;
		ViewHolderGroupClass objViewHolderGroupClass;
		private int posizione;
		int[] statoChk;
		internal event Action<Mygroup> MyGroupRowClick;
		internal event Action<Mygroup> GroupFollowUnFollowButtonClick;
		public MyGroupAdapterClass(Activity context, List<Mygroup> lstGroupList)
		{
			_items = lstGroupList;
			_context = context;
			this.statoChk = new int[lstGroupList.Count];
			for (int i = 0; i < lstGroupList.Count; i++)
			{
				statoChk[i] = 0;
			}
		}
		public override Mygroup this[int position]
		{
			get { return _items[position]; }
		}

		public override int Count
		{
			get { return _items.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			//string imageSrc = strPath +"/"+ _items [position].Interestlogo;   
			View view = convertView;
			int count = 0;

			if (view == null)
			{
				view = this._context.LayoutInflater.Inflate(Resource.Layout.CustomGroupLayout, parent, false);

				objViewHolderGroupClass = new ViewHolderGroupClass();
				objViewHolderGroupClass.imgGroups = view.FindViewById<ImageView>(Resource.Id.imgGroupProfileCustomGL);

				objViewHolderGroupClass.imgFollower1 = view.FindViewById<ImageView>(Resource.Id.img1CustomGL);
				objViewHolderGroupClass.imgFollower2 = view.FindViewById<ImageView>(Resource.Id.img2CustomGL);
				objViewHolderGroupClass.imgFollower3 = view.FindViewById<ImageView>(Resource.Id.img3CustomGL);
				objViewHolderGroupClass.imgFollower4 = view.FindViewById<ImageView>(Resource.Id.img4CustomGL);

				objViewHolderGroupClass.btnJoin = view.FindViewById<Button>(Resource.Id.btnJoin);
				objViewHolderGroupClass.lblGroupName = view.FindViewById<TextView>(Resource.Id.lblGroupNameCustomGL);
				objViewHolderGroupClass.lblGroupDetails = view.FindViewById<TextView>(Resource.Id.lblDetailsCustomGL);
				objViewHolderGroupClass.lblMembersList = view.FindViewById<TextView>(Resource.Id.lblGroupMemberNumCustomGL);
				objViewHolderGroupClass.Initialize(view);
				view.Tag = objViewHolderGroupClass;

			}
			else
			{
				objViewHolderGroupClass = (ViewHolderGroupClass)view.Tag;
			}
			objViewHolderGroupClass.ViewClicked = () =>
			{
				GroupFollowUnFollowButtonClick(_items[position]);

			};
			objViewHolderGroupClass.ViewRowClicked = () =>
			  {
				  MyGroupRowClick(_items[position]);
			  };
			if (SlidingTabActivity.isSelectedTitle == 2)
			{
				objViewHolderGroupClass.btnJoin.SetBackgroundResource(Resource.Drawable.joined);
			}
			if (statoChk[position] == 0)
			{
				objViewHolderGroupClass.btnJoin.SetBackgroundResource(Resource.Drawable.joined); // set unchecked"
			}
			else
			{
				objViewHolderGroupClass.btnJoin.SetBackgroundResource(Resource.Drawable.join); // set checked"
			}
			objViewHolderGroupClass.lblGroupDetails.Visibility = ViewStates.Invisible;
			objViewHolderGroupClass.lblGroupName.Text = _items[position].Groupname;
			//objViewHolderGroupClass.lblGroupDetails.SetText(Html.FromHtml(_items[position].Groupdesc), TextView.BufferType.Spannable);
			if (!string.IsNullOrEmpty(_items[position].Groupslogo))
			{
				Picasso.With(_context).Load(_items[position].Groupslogo).Into(objViewHolderGroupClass.imgGroups);
			}
			else
			{
				objViewHolderGroupClass.imgGroups.SetImageResource(Resource.Drawable.noimages);
			}
			if (_items[position].GroupFollowers != null)
			{
				if (_items[position].totalfollowers == "1")
				{
					objViewHolderGroupClass.lblMembersList.Text = _items[position].totalfollowers + " " + "Member";
					objViewHolderGroupClass.imgFollower1.Visibility = ViewStates.Visible;
					objViewHolderGroupClass.imgFollower2.Visibility = ViewStates.Invisible;
					objViewHolderGroupClass.imgFollower3.Visibility = ViewStates.Invisible;
					objViewHolderGroupClass.imgFollower4.Visibility = ViewStates.Invisible;
				}
				else if (_items[position].totalfollowers == "2")
				{
					objViewHolderGroupClass.lblMembersList.Text = _items[position].totalfollowers + " " + "Members";
					objViewHolderGroupClass.imgFollower1.Visibility = ViewStates.Visible;
					objViewHolderGroupClass.imgFollower2.Visibility = ViewStates.Visible;
					objViewHolderGroupClass.imgFollower3.Visibility = ViewStates.Invisible;
					objViewHolderGroupClass.imgFollower4.Visibility = ViewStates.Invisible;
				}
				else if (_items[position].totalfollowers == "3")
				{
					objViewHolderGroupClass.lblMembersList.Text = _items[position].totalfollowers + " " + "Members";
					objViewHolderGroupClass.imgFollower1.Visibility = ViewStates.Visible;
					objViewHolderGroupClass.imgFollower2.Visibility = ViewStates.Visible;
					objViewHolderGroupClass.imgFollower3.Visibility = ViewStates.Visible;
					objViewHolderGroupClass.imgFollower4.Visibility = ViewStates.Invisible;
				}
				else
				{
					objViewHolderGroupClass.lblMembersList.Text = _items[position].totalfollowers + " " + "Members";
					objViewHolderGroupClass.imgFollower1.Visibility = ViewStates.Visible;
					objViewHolderGroupClass.imgFollower2.Visibility = ViewStates.Visible;
					objViewHolderGroupClass.imgFollower3.Visibility = ViewStates.Visible;
					objViewHolderGroupClass.imgFollower4.Visibility = ViewStates.Visible;
				}
				foreach (var groupFollowers in _items[position].GroupFollowers)
				{
					if (count <= 3)
					{

						if (count == 0)
						{
							Picasso.With(_context).Load(groupFollowers.Modifiedimage).Into(objViewHolderGroupClass.imgFollower1);
						}
						else if (count == 1)
						{
							Picasso.With(_context).Load(groupFollowers.Modifiedimage).Into(objViewHolderGroupClass.imgFollower2);
						}
						else if (count == 2)
						{
							Picasso.With(_context).Load(groupFollowers.Modifiedimage).Into(objViewHolderGroupClass.imgFollower3);
						}
						else if (count == 3)
						{
							Picasso.With(_context).Load(groupFollowers.Modifiedimage).Into(objViewHolderGroupClass.imgFollower4);
						}
						count++;
					}
					else
					{
						break;
					}
				}
			}
			return view;
		}
	}


	#endregion

	#region "ViewHolderGroupClass"
	public class ViewHolderGroupClass : Java.Lang.Object
	{
		//LinearLayout LinearLayoutListInterestSub;
		internal ImageView imgGroups;
		internal Button btnJoin;
		internal TextView lblGroupName;
		internal TextView lblGroupDetails;
		internal TextView lblMembersList;

		internal ImageView imgFollower1;
		internal ImageView imgFollower2;
		internal ImageView imgFollower3;
		internal ImageView imgFollower4;

		//Activity context;
		public Action ViewClicked { get; set; }
		public Action ViewRowClicked{ get; set; }
		public void Initialize(View view)
		{
			view.Click += delegate (object sender, EventArgs e)
			{
				ViewRowClicked();
			};
			btnJoin.Click += delegate (object sender, EventArgs e)
			{
				ViewClicked();
				if (SlidingTabActivity.isSelectedTitle == 1)
				{
					if (GroupsAdapterClass.isaMember == "1")
					{
						if (SlidingTabActivity.isGroupButtonTrue)
						{
							btnJoin.SetBackgroundResource(Resource.Drawable.join);
							SlidingTabActivity.isGroupButtonTrue = false;

						}
						else
						{
							btnJoin.SetBackgroundResource(Resource.Drawable.joined);
						}
					}
					else if (GroupsAdapterClass.isaMember == "2")
					{
						if (SlidingTabActivity.isGroupButtonTrue)
						{
							btnJoin.SetBackgroundResource(Resource.Drawable.join);
							SlidingTabActivity.isGroupButtonTrue = false;
						}
						else
						{
							btnJoin.SetBackgroundResource(Resource.Drawable.request);
						}
					}
				}
				else if (SlidingTabActivity.isSelectedTitle == 2)
				{
					btnJoin.SetBackgroundResource(Resource.Drawable.join);
				}

			};
		}
	}
	#endregion

	#endregion

	#region "Message Chat Adapter"

	public class MessageChatListAdapterClass : BaseAdapter<Chatmemberlist>
	{
		Activity _context;
		List<Chatmemberlist> _items;
		ViewHolderChatClass objViewHolderChatClass;

		internal event Action<Chatmemberlist> ChatRowClick;
		public MessageChatListAdapterClass(Activity context, List<Chatmemberlist> lstChatList)
		{
			_items = lstChatList;
			_context = context;



		}
		public override Chatmemberlist this[int position]
		{
			get { return _items[position]; }
		}

		public override int Count
		{
			get { return _items.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;

			if (view == null)
			{
				view = this._context.LayoutInflater.Inflate(Resource.Layout.CustomMemberChatLayout, parent, false);

				objViewHolderChatClass = new ViewHolderChatClass();
				objViewHolderChatClass.imgChatPrrofile = view.FindViewById<ImageView>(Resource.Id.imgChatProfile);
				objViewHolderChatClass.lblMemberName = view.FindViewById<TextView>(Resource.Id.lblChatUserName);
				objViewHolderChatClass.lblLastChat = view.FindViewById<TextView>(Resource.Id.lblComment);
				objViewHolderChatClass.lblLastSeenTime = view.FindViewById<TextView>(Resource.Id.lblTime);

				objViewHolderChatClass.Initialize(view);
				view.Tag = objViewHolderChatClass;
			}
			else
			{
				objViewHolderChatClass = (ViewHolderChatClass)view.Tag;
			}
			objViewHolderChatClass.ViewRowClicked = () =>
			{
				ChatRowClick(_items[position]);

			};
			objViewHolderChatClass.lblMemberName.Text = _items[position].Fname;
			if(_items[position].Typeofmesasge=="0")
			{
				objViewHolderChatClass.lblLastChat.Text = _items[position].Messagecontent;
			}
			else if (_items[position].Typeofmesasge == "1")
			{
				var strImageName = _items[position].Messagecontent.Substring(_items[position].Messagecontent.LastIndexOf("/", StringComparison.Ordinal) + 1);
				objViewHolderChatClass.lblLastChat.Text =strImageName;

			}
			else if (_items[position].Typeofmesasge == "2")
			{
				var strImageName = _items[position].Messagecontent.Substring(_items[position].Messagecontent.LastIndexOf("/", StringComparison.Ordinal) + 1);
				objViewHolderChatClass.lblLastChat.Text =strImageName;

			}


			objViewHolderChatClass.lblLastSeenTime.Text = _items[position].Seen_at;
			Picasso.With(_context).Load(_items[position].Modifiedimage).Into(objViewHolderChatClass.imgChatPrrofile);
			return view;
		}
	}



	#region "ViewHolderChatClass"
	public class ViewHolderChatClass : Java.Lang.Object
	{
		internal ImageView imgChatPrrofile;
		internal TextView lblMemberName;
		internal TextView lblLastChat;
		internal TextView lblLastSeenTime;
		public Action ViewRowClicked { get; set; }

		public void Initialize(View view)
		{
			view.Click += delegate (object sender, EventArgs e)
		{
			ViewRowClicked();
		};

		}
	}

	#endregion

	#endregion


	#region MyNotification Adapter
	public class NotificationAdapterClass : BaseAdapter<Notification>
	{
		List<Notification> _Notification;
		Activity _context;
		int[] statoChk;
		ViewHolderNotificationClass objViewHolderNotificationClass;
		internal event Action<Notification> NotificationRowClick;

		public NotificationAdapterClass(Activity context, List<Notification> NotificationList)
		{
			_Notification = NotificationList;
			_context = context;

			this.statoChk = new int[NotificationList.Count];
			for (int i = 0; i < NotificationList.Count; i++)
			{
				statoChk[i] = 0;
			}
		}
		public override Notification this[int position]
		{
			get { return _Notification[position]; }
		}

		public override int Count
		{
			get { return _Notification.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;

			if (view == null)
			{
				view = this._context.LayoutInflater.Inflate(Resource.Layout.NotificationLayout, parent, false);

				objViewHolderNotificationClass = new ViewHolderNotificationClass();
				objViewHolderNotificationClass.rlyNotification= view.FindViewById<RelativeLayout>(Resource.Id.rlyCustomNotification);
				objViewHolderNotificationClass.imgNotificationImage = view.FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.imgNotificationImage);
				objViewHolderNotificationClass.lblNotificationText = view.FindViewById<TextView>(Resource.Id.lblNotificationText);
				objViewHolderNotificationClass.lblNotificationDate = view.FindViewById<TextView>(Resource.Id.lblNotificationDate);
				objViewHolderNotificationClass.Initialize(view);
				view.Tag = objViewHolderNotificationClass;
			}
			else 
			{
				objViewHolderNotificationClass = (ViewHolderNotificationClass)view.Tag;
			}
			objViewHolderNotificationClass.ViewNotificationRowClicked = () =>
			{
				NotificationRowClick(_Notification[position]);
				Console.WriteLine(_Notification[position]);
			};
			if (_Notification[position].Created_at != null)
			{
				if (_Notification[position].Iduserviewnotification == null)
				{
					objViewHolderNotificationClass.rlyNotification.SetBackgroundColor(Color.LightGray);
				}
				else
				{
					objViewHolderNotificationClass.rlyNotification.SetBackgroundColor(Color.White);
				}
				//if(_Notification[position].)
				objViewHolderNotificationClass.lblNotificationText.Text = _Notification[position].message;
				objViewHolderNotificationClass.lblNotificationDate.Text = _Notification[position].Created_at;
				if (!string.IsNullOrEmpty(_Notification[position].Modifiedimage))
				{
					objViewHolderNotificationClass.imgNotificationImage.Visibility = ViewStates.Visible;
					Picasso.With(_context).Load(_Notification[position].Modifiedimage).Into(objViewHolderNotificationClass.imgNotificationImage);
				}
				else
				{
					//objViewHolderNotificationClass.imgNotificationImage.SetImageResource(Resource.Drawable.noimages);
				}
			}
			else
			{
				objViewHolderNotificationClass.imgNotificationImage.Visibility = ViewStates.Gone;
				objViewHolderNotificationClass.lblNotificationText.Text = "";
				objViewHolderNotificationClass.lblNotificationDate.Text = "";
			}
			return view;
		}
	}

#endregion

#region "ViewHolderNotificationClass"
public class ViewHolderNotificationClass : Java.Lang.Object
	{
		public Refractored.Controls.CircleImageView imgNotificationImage;
		public RelativeLayout rlyNotification;
		public TextView lblNotificationText;
		public TextView lblNotificationDate;
		public Action ViewNotificationRowClicked { get; set; }
		public void Initialize(View view)
		{
			view.Click += delegate (object sender, EventArgs e)
			{
				ViewNotificationRowClicked();
			};
		}
	}
	#endregion

	#region My Comment On Notification Content Adapter
	public class CommentOnNotificationAdapterClass : BaseAdapter<NotificationComment>
	{
		List<NotificationComment> _NotificationComment;
		Activity _context;
		int[] statoChk;
		ViewHolderNotificationCommentClass objViewHolderNotificationCommentClass;

		public CommentOnNotificationAdapterClass(Activity context, List<NotificationComment> CommentsList)
		{
			_NotificationComment = CommentsList;
			_context = context;

			this.statoChk = new int[CommentsList.Count];
			for (int i = 0; i < CommentsList.Count; i++)
			{
				statoChk[i] = 0;
			}
		}
		public override NotificationComment this[int position]
		{
			get { return _NotificationComment[position]; }
		}

		public override int Count
		{
			get { return _NotificationComment.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;

			if (view == null)
			{
				view = this._context.LayoutInflater.Inflate(Resource.Layout.CustomCommentLayout, parent, false);



				objViewHolderNotificationCommentClass = new ViewHolderNotificationCommentClass();
				objViewHolderNotificationCommentClass.imgCommentProfileCCL = view.FindViewById<ImageView>(Resource.Id.imgCommentProfileCCL);
				objViewHolderNotificationCommentClass.lblUserNameCCL = view.FindViewById<TextView>(Resource.Id.lblUserNameCCL);
				objViewHolderNotificationCommentClass.lblTimeCCL = view.FindViewById<TextView>(Resource.Id.lblTimeCCL);
				objViewHolderNotificationCommentClass.lblCommentCCL = view.FindViewById<TextView>(Resource.Id.lblCommentCCL);
				objViewHolderNotificationCommentClass.Initialize(view);
				view.Tag = objViewHolderNotificationCommentClass;
			}
			else {
				objViewHolderNotificationCommentClass = (ViewHolderNotificationCommentClass)view.Tag;
			}

			objViewHolderNotificationCommentClass.lblUserNameCCL.Text = _NotificationComment[position].Fname;// .Fname +" Created the Group "+ _Notification [position].toname;
			objViewHolderNotificationCommentClass.lblTimeCCL.Text = _NotificationComment[position].Created_at;
			objViewHolderNotificationCommentClass.lblCommentCCL.Text = _NotificationComment[position].Comment;
			if (!string.IsNullOrEmpty(_NotificationComment[position].Postuserlogo))
			{
				Picasso.With(_context).Load(_NotificationComment[position].Postuserlogo).Into(objViewHolderNotificationCommentClass.imgCommentProfileCCL);

			}
			else {
				objViewHolderNotificationCommentClass.imgCommentProfileCCL.SetImageResource(Resource.Drawable.noimages);
			}
			return view;
		}
	}
	#endregion

	#region "ViewHolderNotificationCommentClass"
	public class ViewHolderNotificationCommentClass : Java.Lang.Object
	{
		public ImageView imgCommentProfileCCL;
		public TextView lblUserNameCCL;
		public TextView lblTimeCCL;
		public TextView lblCommentCCL;
		public void Initialize(View view)
		{
			view.Click += delegate (object sender, EventArgs e)
			{
				//ViewNotificationRowClicked ();
			};
		}
	}
	#endregion

	#region Group Member/Approval Adapter
	public class GroupMembersAdapterClass : BaseAdapter<GroupMember>
	{
		List<GroupMember> _GroupMember;
		Activity _context;
		int[] statoChk;
		ViewHolderGroupMemberClass objViewHolderGroupMemberClass;

		public GroupMembersAdapterClass(Activity context, List<GroupMember> GroupMemberList)
		{
			_GroupMember = GroupMemberList;
			_context = context;

			this.statoChk = new int[GroupMemberList.Count];
			for (int i = 0; i < GroupMemberList.Count; i++)
			{
				statoChk[i] = 0;
			}
		}
		public override GroupMember this[int position]
		{
			get { return _GroupMember[position]; }
		}

		public override int Count
		{
			get { return _GroupMember.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;

			if (view == null)
			{
				view = this._context.LayoutInflater.Inflate(Resource.Layout.CustomGroupMemberLayout, parent, false);
				objViewHolderGroupMemberClass = new ViewHolderGroupMemberClass();
				objViewHolderGroupMemberClass.imgGroupMemberCGML = view.FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.imgGroupMemberCGML);
				objViewHolderGroupMemberClass.lblGroupMemberNameCGML = view.FindViewById<TextView>(Resource.Id.lblGroupMemberNameCGML);
				objViewHolderGroupMemberClass.Initialize(view);
				view.Tag = objViewHolderGroupMemberClass;
			}
			else
			{
				objViewHolderGroupMemberClass = (ViewHolderGroupMemberClass)view.Tag;
			}
			objViewHolderGroupMemberClass.lblGroupMemberNameCGML.Text = _GroupMember[position].Fname;
			if (!string.IsNullOrEmpty(_GroupMember[position].groupmembersuserlogo))
			{
				Picasso.With(_context).Load(_GroupMember[position].groupmembersuserlogo).Into(objViewHolderGroupMemberClass.imgGroupMemberCGML);
			}
			else {
				objViewHolderGroupMemberClass.imgGroupMemberCGML.SetImageResource(Resource.Drawable.noimages);
			}
			return view;
		}
	}
	#endregion


	#region "ViewHolderGroupMemberClass"
	public class ViewHolderGroupMemberClass : Java.Lang.Object
	{
		public Refractored.Controls.CircleImageView imgGroupMemberCGML;
		public TextView lblGroupMemberNameCGML;

		public void Initialize(View view)
		{
			view.Click += delegate (object sender, EventArgs e)
			{
				//ViewNotificationRowClicked ();
			};
		}
	}
	#endregion

	#region Group Request Adapter
	public class GroupMembersRequestAdapterClass : BaseAdapter<GroupInvitation>
	{
		List<GroupInvitation> _GroupInvitation;
		Activity _context;
		int[] statoChk;
		ViewHolderGroupMemberRequestClass objViewHolderGroupMemberRequestClass;
		internal event Action<GroupInvitation> AcceptGroupInvitationButtonClick;
		internal event Action<GroupInvitation> RejectGroupInvitationButtonClick;
		public GroupMembersRequestAdapterClass(Activity context, List<GroupInvitation> GroupInvitationList)
		{
			_GroupInvitation = GroupInvitationList;
			_context = context;

			this.statoChk = new int[GroupInvitationList.Count];
			for (int i = 0; i < GroupInvitationList.Count; i++)
			{
				statoChk[i] = 0;
			}
		}
		public override GroupInvitation this[int position]
		{
			get { return _GroupInvitation[position]; }
		}

		public override int Count
		{
			get { return _GroupInvitation.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;

			if (view == null)
			{
				view = this._context.LayoutInflater.Inflate(Resource.Layout.CustomGroupRequestLayout, parent, false);
				objViewHolderGroupMemberRequestClass = new ViewHolderGroupMemberRequestClass();
				objViewHolderGroupMemberRequestClass.imgGroupRequestUserCGRL = view.FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.imgGroupRequestUserCGRL);
				objViewHolderGroupMemberRequestClass.lblGroupRequestUserNameCGRL = view.FindViewById<TextView>(Resource.Id.lblGroupRequestUserNameCGRL);
				objViewHolderGroupMemberRequestClass.btnGroupRequestAcceptCGRL = view.FindViewById<Button>(Resource.Id.btnGroupRequestAcceptCGRL);
				objViewHolderGroupMemberRequestClass.btnGroupRequestRejectCGRL = view.FindViewById<Button>(Resource.Id.btnGroupRequestRejectCGRL);

				objViewHolderGroupMemberRequestClass.Initialize(view);
				view.Tag = objViewHolderGroupMemberRequestClass;
			}
			else {
				objViewHolderGroupMemberRequestClass = (ViewHolderGroupMemberRequestClass)view.Tag;
			}

			objViewHolderGroupMemberRequestClass.ViewRowBtnAcceptClicked = () =>
			{
				AcceptGroupInvitationButtonClick(_GroupInvitation[position]);
			};
			objViewHolderGroupMemberRequestClass.ViewRowBtnRejectClicked = () =>
			{
				RejectGroupInvitationButtonClick(_GroupInvitation[position]);
			};

			objViewHolderGroupMemberRequestClass.lblGroupRequestUserNameCGRL.Text = _GroupInvitation[position].Fname + " " + _GroupInvitation[position].Lname;
			if (!string.IsNullOrEmpty(_GroupInvitation[position].grouprequestsuserlogo))
			{
				Picasso.With(_context).Load(_GroupInvitation[position].grouprequestsuserlogo).Into(objViewHolderGroupMemberRequestClass.imgGroupRequestUserCGRL);
			}
			else {
				objViewHolderGroupMemberRequestClass.imgGroupRequestUserCGRL.SetImageResource(Resource.Drawable.noimages);
			}
			return view;
		}
	}
	#endregion


	#region "ViewHolderGroupMemberRequestClass"
	public class ViewHolderGroupMemberRequestClass : Java.Lang.Object
	{
		public Refractored.Controls.CircleImageView imgGroupRequestUserCGRL;
		public TextView lblGroupRequestUserNameCGRL;
		public Button btnGroupRequestAcceptCGRL;
		public Button btnGroupRequestRejectCGRL;
		public Action ViewRowBtnAcceptClicked { get; set; }
		public Action ViewRowBtnRejectClicked { get; set; }
		public void Initialize(View view)
		{
			view.Click += delegate (object sender, EventArgs e)
			{
			//ViewNotificationRowClicked ();


			};
			btnGroupRequestAcceptCGRL.Click += delegate (object sender, EventArgs e)
			{
				ViewRowBtnAcceptClicked();
			};
			btnGroupRequestRejectCGRL.Click += delegate (object sender, EventArgs e)
			{
				ViewRowBtnRejectClicked();
			};
		}
	}
	#endregion


	#region "Home Adapter"
	public class HomeFeedAdapterClass : BaseAdapter<Orgfeed>
	{
		List<Orgfeed> _items;
		Activity _context;
		private int posizione;
		int[] statoChk;
		int count = 0;
		int k;
		ViewHolderHomeFeedsClass objViewHolderHomeFeedsClass;

		internal event Action<Orgfeed> PostGroupNameClick;
		internal event Action<Orgfeed> PostTitleClick;
		internal event Action<Orgfeed> PostDownArrowClick;

		internal event Action<Orgfeed> EventAddressClick;
		internal event Action<Orgfeed> EventNameClick;
		internal event Action<Orgfeed> EventBuyTicketClick;


		internal event Action<Orgfeed> LikeClick;
		internal event Action<Orgfeed> CommentClick;
		internal event Action<Orgfeed> ShareClick;
		int pixel;
		int heightInDp;
		int widthInDp;
		public HomeFeedAdapterClass(Activity context, List<Orgfeed> lstHomeFeed)
		{
			_items = lstHomeFeed;
			_context = context;


			this.statoChk = new int[lstHomeFeed.Count];
			for (int i = 0; i < lstHomeFeed.Count; i++)
			{
				statoChk[i] = 0;
			}

		}
		public override Orgfeed this[int position]
		{
			get { return _items[position]; }
		}

		public override int Count
		{
			get { return _items.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			posizione = position;
			//string imageSrc = strPath + "/" + _items [position].Interestlogo;   
			View view = convertView;
			count = 0;
			if (view == null)
			{
				view = this._context.LayoutInflater.Inflate(Resource.Layout.CustomHomeFeedLayout, parent, false);

				objViewHolderHomeFeedsClass = new ViewHolderHomeFeedsClass();
				objViewHolderHomeFeedsClass.rlyImagePostLayout = view.FindViewById<RelativeLayout>(Resource.Id.rlyImageLayout);
				objViewHolderHomeFeedsClass.rlyCreateEventLayout = view.FindViewById<RelativeLayout>(Resource.Id.rlyEventLayoutCHFL);

				objViewHolderHomeFeedsClass.lblEventName= view.FindViewById<TextView>(Resource.Id.lblEventTitleNameCHFL);
				objViewHolderHomeFeedsClass.lblEventDescription= view.FindViewById<TextView>(Resource.Id.lblEventDescriptionCHFL);
				objViewHolderHomeFeedsClass.lblEventAddress= view.FindViewById<TextView>(Resource.Id.lblEventAddressCHFL);
				objViewHolderHomeFeedsClass.lblEventDateAndTime= view.FindViewById<TextView>(Resource.Id.lblEventDateTimeCHFL);
				objViewHolderHomeFeedsClass.btnBuyTicketOrFree= view.FindViewById<Button>(Resource.Id.btnFreeCHFL);

				objViewHolderHomeFeedsClass.videoView = view.FindViewById<WebView>(Resource.Id.videoView1);
				objViewHolderHomeFeedsClass.imgImagePost= view.FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.imgUserProfileICHFL);
				objViewHolderHomeFeedsClass.lblImagePostFName= view.FindViewById<TextView>(Resource.Id.lblUserNameICHFL);
				objViewHolderHomeFeedsClass.lblImagePostGroupName= view.FindViewById<TextView>(Resource.Id.lblGroupNameICHFL);
				objViewHolderHomeFeedsClass.lblImagePostDayAgo= view.FindViewById<TextView>(Resource.Id.lblTimeICHFL);
				objViewHolderHomeFeedsClass.imgImageArrow= view.FindViewById<ImageView>(Resource.Id.imgRightArrowICHFL);
				objViewHolderHomeFeedsClass.imgDownArrow= view.FindViewById<ImageView>(Resource.Id.imgDownArrowICHFL);
				objViewHolderHomeFeedsClass.lblPostContent= view.FindViewById<TextView>(Resource.Id.lblDescriptionICHFL);
				objViewHolderHomeFeedsClass.imgPostImage=view.FindViewById<ImageView>(Resource.Id.imgPostedImageICHFL);
				objViewHolderHomeFeedsClass.lblDescription= view.FindViewById<TextView>(Resource.Id.lblDetailICHFL);
				objViewHolderHomeFeedsClass.lblTitle= view.FindViewById<TextView>(Resource.Id.lblTitleDetailsICHFL);
				objViewHolderHomeFeedsClass.lblLikeCount= view.FindViewById<TextView>(Resource.Id.lblNumberOfLikesICHFL);
				objViewHolderHomeFeedsClass.lblLike= view.FindViewById<TextView>(Resource.Id.lblLike);
				objViewHolderHomeFeedsClass.lblShareCount= view.FindViewById<TextView>(Resource.Id.lblNumberOfSharesICHFL);
				objViewHolderHomeFeedsClass.lblShare= view.FindViewById<TextView>(Resource.Id.lblShare);
				objViewHolderHomeFeedsClass.imgUserImage= view.FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.imgCommentProfileCCL);
				objViewHolderHomeFeedsClass.rlyPostLayout = view.FindViewById<RelativeLayout>(Resource.Id.rlyPostLayout);
				objViewHolderHomeFeedsClass.rlyLikeCommentShareLayout = view.FindViewById<RelativeLayout>(Resource.Id.lnyFooterLayoutICHFL);
				objViewHolderHomeFeedsClass.lblShareText = view.FindViewById<TextView>(Resource.Id.lblShareICHFL);
				objViewHolderHomeFeedsClass.lblDotLine = view.FindViewById<TextView>(Resource.Id.lblViewICHFL);
				objViewHolderHomeFeedsClass.rlyCommentLayout= view.FindViewById<RelativeLayout>(Resource.Id.rlyCustomChatLayout);
				objViewHolderHomeFeedsClass.lblCommentUserName= view.FindViewById<TextView>(Resource.Id.lblUserNameCCL);
				objViewHolderHomeFeedsClass.lblCommentUser= view.FindViewById<TextView>(Resource.Id.lblCommentCCL);
				objViewHolderHomeFeedsClass.lblCommentTime= view.FindViewById<TextView>(Resource.Id.lblTimeCCL);
				objViewHolderHomeFeedsClass.lblComment = view.FindViewById<TextView>(Resource.Id.lblComment);
				objViewHolderHomeFeedsClass.imgShare = view.FindViewById<ImageView>(Resource.Id.imgShare);
				objViewHolderHomeFeedsClass.imgLike = view.FindViewById<ImageView>(Resource.Id.imgLike);

				objViewHolderHomeFeedsClass.Initialize(view);
				view.Tag = objViewHolderHomeFeedsClass;
			}
			else
			{
				objViewHolderHomeFeedsClass = (ViewHolderHomeFeedsClass)view.Tag;
			}
			//event
			objViewHolderHomeFeedsClass.ViewEventAddressClicked = () =>
			{
				EventAddressClick(_items[position]);
			};
			objViewHolderHomeFeedsClass.ViewEventNameClicked = () =>
			{
				EventNameClick(_items[position]);
			};
			objViewHolderHomeFeedsClass.ViewEventBuyTicketClicked = () =>
			{
				EventBuyTicketClick(_items[position]);
			};
			//post
			objViewHolderHomeFeedsClass.ViewPostGroupNameClicked = () =>
			{
				PostGroupNameClick(_items[position]);
			};
			objViewHolderHomeFeedsClass.ViewPostTitleClicked = () =>
			{
				PostTitleClick(_items[position]);
			};
			objViewHolderHomeFeedsClass.ViewPostDownArrowClicked = () =>
			{
				PostDownArrowClick(_items[position]);
			};


			objViewHolderHomeFeedsClass.ViewLike = () =>
			{
				LikeClick(_items[position]);

			};
			objViewHolderHomeFeedsClass.ViewComment = () =>
			{
				CommentClick(_items[position]);
			};
			objViewHolderHomeFeedsClass.ViewShare = () =>
			{
				ShareClick(_items[position]);
			};

			if (_items[position].allfeedsflag == 1)//post
			{
				if (_items[position].Typeofpost == "1")//text,video,description
				{
					objViewHolderHomeFeedsClass.rlyImagePostLayout.Visibility = ViewStates.Visible;
					objViewHolderHomeFeedsClass.rlyPostLayout.Visibility = ViewStates.Visible;
					objViewHolderHomeFeedsClass.rlyLikeCommentShareLayout.Visibility = ViewStates.Visible;
					objViewHolderHomeFeedsClass.rlyCreateEventLayout.Visibility = ViewStates.Gone;
					objViewHolderHomeFeedsClass.imgImageArrow.Visibility = ViewStates.Visible;
					objViewHolderHomeFeedsClass.imgDownArrow.Visibility = ViewStates.Visible;
					objViewHolderHomeFeedsClass.lblShareCount.Visibility = ViewStates.Visible;
					objViewHolderHomeFeedsClass.lblShareText.Visibility = ViewStates.Visible;
					objViewHolderHomeFeedsClass.imgShare.Visibility = ViewStates.Visible;
					objViewHolderHomeFeedsClass.lblShare.Visibility = ViewStates.Visible;
					objViewHolderHomeFeedsClass.imgPostImage.Visibility = ViewStates.Gone;
					objViewHolderHomeFeedsClass.lblImagePostGroupName.SetTextColor(Color.ParseColor("#339AFF"));
					objViewHolderHomeFeedsClass.videoView.Visibility = ViewStates.Gone;
					objViewHolderHomeFeedsClass.lblImagePostFName.Text = _items[position].Fname;
					objViewHolderHomeFeedsClass.lblImagePostGroupName.Text = _items[position].Groupname;
					objViewHolderHomeFeedsClass.lblImagePostDayAgo.Text = _items[position].Created_at;
					objViewHolderHomeFeedsClass.lblImagePostFName.Text = _items[position].Fname;
					objViewHolderHomeFeedsClass.lblImagePostGroupName.Text = _items[position].Groupname;
					objViewHolderHomeFeedsClass.lblImagePostDayAgo.Text = _items[position].Created_at;
					objViewHolderHomeFeedsClass.lblPostContent.SetText(Html.FromHtml(_items[position].Postcontent), TextView.BufferType.Spannable);
					objViewHolderHomeFeedsClass.lblDescription.SetText(Html.FromHtml(_items[position].description), TextView.BufferType.Spannable);

					objViewHolderHomeFeedsClass.lblTitle.Text = _items[position].title;

					objViewHolderHomeFeedsClass.lblLikeCount.Text = _items[position].postlikes.totallikes.ToString();
					objViewHolderHomeFeedsClass.lblShareCount.Text = _items[position].totalshares.totalshares.ToString();

					Picasso.With(_context).Load(_items[position].Modifiedimage).Into(objViewHolderHomeFeedsClass.imgImagePost);

					if(string.IsNullOrEmpty(_items[position].Videourl))
					{
						objViewHolderHomeFeedsClass.videoView.Visibility = ViewStates.Gone;
						objViewHolderHomeFeedsClass.imgPostImage.Visibility = ViewStates.Visible;
						if (string.IsNullOrEmpty(_items[position].image))
						{
							objViewHolderHomeFeedsClass.imgPostImage.Visibility = ViewStates.Gone;
						}
						else
						{
							Picasso.With(_context).Load(_items[position].image).Into(objViewHolderHomeFeedsClass.imgPostImage);
						}
					}
					else
					{
						objViewHolderHomeFeedsClass.videoView.Visibility = ViewStates.Visible;
						int videoHeight;
						int videoWidth;
						var matrics = _context.Resources.DisplayMetrics;
						widthInDp = matrics.WidthPixels;
						heightInDp = matrics.HeightPixels;

						videoWidth = matrics.WidthPixels + 460;
						videoHeight = matrics.HeightPixels / 2;

						string strYouTube = @"<html><body><embed width=""videoWidth"" height=""videoHeight"" src=""{0}""></body></html>";
						try
						{
							string strYouTubeURL = strYouTube.Replace("videoWidth", videoWidth.ToString()).Replace("videoHeight", videoHeight.ToString());
							string strVideoURL = string.Empty;
							if (!string.IsNullOrEmpty(_items[position].url))
								strVideoURL = _items[position].url;

							const string regExpPattern = @"youtu(?:\.be|be\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)";
							//for Vimeo: vimeo\.com/(?:.*#|.*/videos/)?([0-9]+)
							var regEx = new Regex(regExpPattern);
							var match = regEx.Match(strVideoURL);
							string strYUrl = match.Success ? match.Groups[0].Value : null;
							 var s=match.Success ? match.Groups[1].Value : null;

							string strVideoUrl=string.Empty;
							if (!string.IsNullOrEmpty(s))
							{
							 	strVideoUrl = string.Format("http://youtube.com/embed/{0}", s);
							}
							else 
							{
								//Toast.MakeText(this, "Video url is not in correct format", ToastLength.Long).Show();
								//return;
							}

							objViewHolderHomeFeedsClass.videoView.Settings.LoadWithOverviewMode = true;
							objViewHolderHomeFeedsClass.videoView.Settings.UseWideViewPort = true;
							objViewHolderHomeFeedsClass.videoView.Settings.PluginsEnabled = true;
							objViewHolderHomeFeedsClass.videoView.Settings.JavaScriptEnabled = true;
							objViewHolderHomeFeedsClass.videoView.Settings.DomStorageEnabled = true;
							objViewHolderHomeFeedsClass.videoView.Settings.SetRenderPriority(WebSettings.RenderPriority.High);
							objViewHolderHomeFeedsClass.videoView.Settings.BuiltInZoomControls = true;
							objViewHolderHomeFeedsClass.videoView.Settings.JavaScriptCanOpenWindowsAutomatically = true;
							objViewHolderHomeFeedsClass.videoView.Settings.AllowFileAccess = true;
							objViewHolderHomeFeedsClass.videoView.SetWebChromeClient(new WebChromeClient());
							objViewHolderHomeFeedsClass.videoView.LoadDataWithBaseURL("", string.Format(strYouTubeURL, strVideoUrl), "text/html", "UTF-8", "");

						}
						catch
						{
							Console.WriteLine("cannot load youtube video");
						}

					}
					//likes
					if (_items[position].Flag == "1")
					{
						objViewHolderHomeFeedsClass.lblLike.SetTextColor(Color.ParseColor("#339AFF"));
						objViewHolderHomeFeedsClass.lblLike.Text = "UnLike";
						objViewHolderHomeFeedsClass.imgLike.SetImageResource(Resource.Drawable.blue);
						//SlidingTabActivity.strFlag = objViewHolderHomeFeedsClass.lblLike.Text;
					}
					else
					{
						objViewHolderHomeFeedsClass.lblLike.SetTextColor(Color.ParseColor("#808080"));
						objViewHolderHomeFeedsClass.lblLike.Text = "Like";
						objViewHolderHomeFeedsClass.imgLike.SetImageResource(Resource.Drawable.like);
						//SlidingTabActivity.strFlag = objViewHolderHomeFeedsClass.lblLike.Text;
					}
					//comment only last one
					if (_items[position].comments != null)
					{
						objViewHolderHomeFeedsClass.rlyCommentLayout.Visibility = ViewStates.Visible;
						objViewHolderHomeFeedsClass.lblDotLine.Visibility = ViewStates.Visible;
						var commentList = _items[position].comments.LastOrDefault();
						Picasso.With(_context).Load(commentList.Modifiedimage).Into(objViewHolderHomeFeedsClass.imgUserImage);
						objViewHolderHomeFeedsClass.lblCommentUserName.Text = commentList.Fname;
						objViewHolderHomeFeedsClass.lblCommentUser.Text = commentList.Comments;
						objViewHolderHomeFeedsClass.lblCommentTime.Text = commentList.Created_at;

					}
					else
					{
						objViewHolderHomeFeedsClass.rlyCommentLayout.Visibility = ViewStates.Gone;
						objViewHolderHomeFeedsClass.lblDotLine.Visibility = ViewStates.Gone;
					}
				}
				else if (_items[position].Typeofpost == "2")//image,text,description
				{

					objViewHolderHomeFeedsClass.rlyImagePostLayout.Visibility = ViewStates.Visible;
					objViewHolderHomeFeedsClass.rlyPostLayout.Visibility = ViewStates.Visible;
					objViewHolderHomeFeedsClass.rlyLikeCommentShareLayout.Visibility = ViewStates.Visible;
					objViewHolderHomeFeedsClass.rlyCreateEventLayout.Visibility = ViewStates.Gone;
					objViewHolderHomeFeedsClass.imgImageArrow.Visibility = ViewStates.Visible;
					objViewHolderHomeFeedsClass.imgDownArrow.Visibility = ViewStates.Visible;
					objViewHolderHomeFeedsClass.lblShareCount.Visibility = ViewStates.Visible;
					objViewHolderHomeFeedsClass.lblShareText.Visibility = ViewStates.Visible;
					objViewHolderHomeFeedsClass.imgShare.Visibility = ViewStates.Visible;
					objViewHolderHomeFeedsClass.lblShare.Visibility = ViewStates.Visible;
					objViewHolderHomeFeedsClass.lblImagePostGroupName.SetTextColor(Color.ParseColor("#339AFF"));
					objViewHolderHomeFeedsClass.videoView.Visibility = ViewStates.Gone;
					objViewHolderHomeFeedsClass.lblImagePostFName.Text = _items[position].Fname;
					objViewHolderHomeFeedsClass.lblImagePostGroupName.Text = _items[position].Groupname;
					objViewHolderHomeFeedsClass.lblImagePostDayAgo.Text = _items[position].Created_at;
					objViewHolderHomeFeedsClass.lblPostContent.SetText(Html.FromHtml(_items[position].Postcontent), TextView.BufferType.Spannable);
					objViewHolderHomeFeedsClass.lblDescription.SetText(Html.FromHtml(_items[position].description), TextView.BufferType.Spannable);

					objViewHolderHomeFeedsClass.lblTitle.Text = _items[position].title;

					objViewHolderHomeFeedsClass.lblLikeCount.Text = _items[position].postlikes.totallikes.ToString();
					objViewHolderHomeFeedsClass.lblShareCount.Text = _items[position].totalshares.totalshares.ToString();

					Picasso.With(_context).Load(_items[position].Modifiedimage).Into(objViewHolderHomeFeedsClass.imgImagePost);
					if (string.IsNullOrEmpty(_items[position].Imagename))
					{
						objViewHolderHomeFeedsClass.imgPostImage.Visibility = ViewStates.Gone;
					}
					else
					{
						objViewHolderHomeFeedsClass.imgPostImage.Visibility = ViewStates.Visible;
						Picasso.With(_context).Load(_items[position].Imagename).Into(objViewHolderHomeFeedsClass.imgPostImage);
					}
					//likes
					if (_items[position].Flag == "1")
					{
						objViewHolderHomeFeedsClass.lblLike.SetTextColor(Color.ParseColor("#339AFF"));
						objViewHolderHomeFeedsClass.lblLike.Text = "UnLike";
						objViewHolderHomeFeedsClass.imgLike.SetImageResource(Resource.Drawable.blue);
					}
					else
					{
						objViewHolderHomeFeedsClass.lblLike.SetTextColor(Color.ParseColor("#808080"));
						objViewHolderHomeFeedsClass.lblLike.Text = "Like";
						objViewHolderHomeFeedsClass.imgLike.SetImageResource(Resource.Drawable.like);
					}
					//comment only last one
					if (_items[position].comments != null)
					{
						objViewHolderHomeFeedsClass.rlyCommentLayout.Visibility = ViewStates.Visible;
						objViewHolderHomeFeedsClass.lblDotLine.Visibility = ViewStates.Visible;
						var commentList = _items[position].comments.LastOrDefault();
						Picasso.With(_context).Load(commentList.Modifiedimage).Into(objViewHolderHomeFeedsClass.imgUserImage);
						objViewHolderHomeFeedsClass.lblCommentUserName.Text = commentList.Fname;
						objViewHolderHomeFeedsClass.lblCommentUser.Text = commentList.Comments;
						objViewHolderHomeFeedsClass.lblCommentTime.Text = commentList.Created_at;

					}
					else
					{
						objViewHolderHomeFeedsClass.rlyCommentLayout.Visibility = ViewStates.Gone;
						objViewHolderHomeFeedsClass.lblDotLine.Visibility = ViewStates.Gone;
					}
				}
			}
			else if (_items[position].allfeedsflag == 2)//events
			{
				objViewHolderHomeFeedsClass.rlyImagePostLayout.Visibility = ViewStates.Gone;
				objViewHolderHomeFeedsClass.rlyCreateEventLayout.Visibility = ViewStates.Visible;
				objViewHolderHomeFeedsClass.rlyPostLayout.Visibility = ViewStates.Visible;
				objViewHolderHomeFeedsClass.rlyLikeCommentShareLayout.Visibility = ViewStates.Visible;
				objViewHolderHomeFeedsClass.imgImageArrow.Visibility = ViewStates.Gone;
				objViewHolderHomeFeedsClass.imgDownArrow.Visibility = ViewStates.Invisible;
				objViewHolderHomeFeedsClass.lblShareCount.Visibility = ViewStates.Gone;
				objViewHolderHomeFeedsClass.lblShareText.Visibility = ViewStates.Invisible;
				objViewHolderHomeFeedsClass.imgShare.Visibility = ViewStates.Gone;
				objViewHolderHomeFeedsClass.lblShare.Visibility = ViewStates.Gone;


				objViewHolderHomeFeedsClass.lblImagePostFName.Text = _items[position].Fname;
				objViewHolderHomeFeedsClass.lblImagePostGroupName.SetTextColor(Color.Gray);
				objViewHolderHomeFeedsClass.lblImagePostGroupName.Text = "created an event";
				objViewHolderHomeFeedsClass.lblImagePostDayAgo.Text = _items[position].Created_at;
				objViewHolderHomeFeedsClass.lblEventName.Text = _items[position].Eventname;
				objViewHolderHomeFeedsClass.lblEventDescription.SetText(Html.FromHtml(_items[position].Eventdesc), TextView.BufferType.Spannable);
				//objViewHolderHomeFeedsClass.lblEventDescription.Text = _items[position].Eventdesc;
				objViewHolderHomeFeedsClass.lblEventAddress.Text = _items[position].CityName;
				objViewHolderHomeFeedsClass.lblEventDateAndTime.Text = _items[position].Eventdate + " " + _items[position].Eventtime;
				objViewHolderHomeFeedsClass.lblLikeCount.Text = _items[position].eventlikes.totallikes;
				Picasso.With(_context).Load(_items[position].Modifiedimage).Into(objViewHolderHomeFeedsClass.imgImagePost);

				if (_items[position].Ispaidevent == "0")
				{
					objViewHolderHomeFeedsClass.btnBuyTicketOrFree.Text = "Free";
				}
				else if (_items[position].Ispaidevent == "1")
				{
					objViewHolderHomeFeedsClass.btnBuyTicketOrFree.Text = "Buy Ticket";
				}
				//likes
				if (_items[position].Flag == "1")
				{
					objViewHolderHomeFeedsClass.lblLike.SetTextColor(Color.ParseColor("#339AFF"));
					objViewHolderHomeFeedsClass.lblLike.Text = "UnLike";
					objViewHolderHomeFeedsClass.imgLike.SetImageResource(Resource.Drawable.blue);

				}
				else
				{
					objViewHolderHomeFeedsClass.lblLike.SetTextColor(Color.ParseColor("#808080"));
					objViewHolderHomeFeedsClass.lblLike.Text = "Like";
					objViewHolderHomeFeedsClass.imgLike.SetImageResource(Resource.Drawable.like);

				}
				//comment only last one
				if (_items[position].comments != null)
				{
					objViewHolderHomeFeedsClass.rlyCommentLayout.Visibility = ViewStates.Visible;
					objViewHolderHomeFeedsClass.lblDotLine.Visibility = ViewStates.Visible;

					var commentList = _items[position].comments.LastOrDefault();
					Picasso.With(_context).Load(commentList.Modifiedimage).Into(objViewHolderHomeFeedsClass.imgUserImage);

					objViewHolderHomeFeedsClass.lblCommentUserName.Text = commentList.Fname;
					objViewHolderHomeFeedsClass.lblCommentUser.Text = commentList.Comments;
					objViewHolderHomeFeedsClass.lblCommentTime.Text = commentList.Created_at;

				}
				else
				{
					objViewHolderHomeFeedsClass.rlyCommentLayout.Visibility = ViewStates.Gone;
					objViewHolderHomeFeedsClass.lblDotLine.Visibility = ViewStates.Gone;
				}
			}


			return view;
		}
	}
	#endregion



	#region "ViewHolderHomeFeedsClass"
	public class ViewHolderHomeFeedsClass : Java.Lang.Object
	{
		public static int likeCount = 0;
		//Create Event Layout
		public RelativeLayout rlyCreateEventLayout;
		public TextView lblEventName;
		public TextView lblEventDescription;
		public TextView lblEventAddress;
		public TextView lblEventDateAndTime;
		public Button btnBuyTicketOrFree;


		public WebView videoView;
		public RelativeLayout rlyPostLayout;
		public RelativeLayout rlyLikeCommentShareLayout;
		public RelativeLayout rlyImagePostLayout;
		public Refractored.Controls.CircleImageView imgImagePost;
		public TextView lblImagePostFName;
		public TextView lblImagePostGroupName;
		public TextView lblImagePostDayAgo;
		public ImageView imgImageArrow;
		public ImageView imgDownArrow;
		public TextView lblPostContent;
		public ImageView imgPostImage;
		public TextView lblDescription;
		public TextView lblTitle;
		public TextView lblLikeCount;
		public  TextView lblLike;
		public  TextView lblShareCount;
		public TextView lblShareText;
		public TextView lblDotLine;
		public TextView lblShare;
		public TextView lblComment;
		public ImageView imgShare;
		public ImageView imgLike;
		public RelativeLayout rlyCommentLayout;
		public Refractored.Controls.CircleImageView imgUserImage;
		public TextView lblCommentUserName;
		public TextView lblCommentUser;
		public TextView lblCommentTime;


		public Action ViewEventAddressClicked { get; set; }
		public Action ViewEventNameClicked { get; set; }
		public Action ViewEventBuyTicketClicked { get; set;}

		public Action ViewPostGroupNameClicked { get; set; }
		public Action ViewPostTitleClicked { get; set; }
		public Action ViewPostDownArrowClicked { get; set; }


		public Action ViewLike { get; set;}
		public Action ViewComment { get; set; }
		public Action ViewShare { get; set; }

		public void Initialize(View view)
		{
			view.Click += delegate (object sender, EventArgs e)
			{
				//ViewNotificationRowClicked ();
			};
			lblEventAddress.Click += delegate
			{
				ViewEventAddressClicked();
			};
			lblEventName.Click += delegate
			{
				ViewEventNameClicked();
			};
			btnBuyTicketOrFree.Click += delegate
			{
				ViewEventBuyTicketClicked();
			};
			lblImagePostGroupName.Click += delegate
			{
				ViewPostGroupNameClicked();
			};
			lblTitle.Click += delegate
			{
				ViewPostTitleClicked();
			};
			imgDownArrow.Click += delegate
			{
				ViewPostDownArrowClicked();
			};

			lblLike.Click += delegate
			{
				likeCount = 0;
				SlidingTabActivity.strFlag = lblLike.Text;
				ViewLike();

				if(SlidingTabActivity.isLikeOrUnlike)
				{
					lblLike.SetTextColor(Color.ParseColor("#339AFF"));
					lblLike.Text = "UnLike";
					likeCount=Convert.ToInt32(lblLikeCount.Text);
					var count=likeCount+1;
					 SlidingTabActivity.strFlag = lblLike.Text;
					lblLikeCount.Text = count.ToString();
					imgLike.SetImageResource(Resource.Drawable.blue);

				}
				else
				{
					lblLike.SetTextColor(Color.ParseColor("#808080"));
					lblLike.Text = "Like";
					likeCount = Convert.ToInt32(lblLikeCount.Text);
					var count=likeCount-1;
					SlidingTabActivity.strFlag = lblLike.Text;
					lblLikeCount.Text =count.ToString();
					imgLike.SetImageResource(Resource.Drawable.like);
				}
			};
			lblComment.Click += delegate
			{
				ViewComment();
			};
			lblShare.Click += delegate
			{
				ViewShare();
			};
		}
	}
	#endregion

	public class HomeFeedPromatationAdapterClass : BaseAdapter<Orgfeedspromotion>
	{
		List<Orgfeedspromotion> _items;
		Activity _context;
		private int posizione;
		int[] statoChk;
		int count = 0;
		ViewHolderHomeFeedsPromatationClass objViewHolderHomeFeedsPromatationClass;



		internal event Action<Orgfeedspromotion> PromotionNameClick;
		internal event Action<Orgfeedspromotion> PromotionButtonFollowClick;



		public HomeFeedPromatationAdapterClass(Activity context, List<Orgfeedspromotion> lstHomeFeed)
		{
			_items = lstHomeFeed;
			_context = context;


			this.statoChk = new int[lstHomeFeed.Count];
			for (int i = 0; i < lstHomeFeed.Count; i++)
			{
				statoChk[i] = 0;
			}

		}
		public override Orgfeedspromotion this[int position]
		{
			get { return _items[position]; }
		}

		public override int Count
		{
			get { return _items.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			posizione = position;
			//string imageSrc = strPath + "/" + _items [position].Interestlogo;  
			View view = convertView;
			count = 0;
			if (view == null)
			{
				view = this._context.LayoutInflater.Inflate(Resource.Layout.PromotationLayout, parent, false);
				objViewHolderHomeFeedsPromatationClass = new ViewHolderHomeFeedsPromatationClass();
				objViewHolderHomeFeedsPromatationClass.rlyPromationLayout = view.FindViewById<RelativeLayout>(Resource.Id.rlyPromotionsLayoutCHFL);
				objViewHolderHomeFeedsPromatationClass.imgPromationImage = view.FindViewById<ImageView>(Resource.Id.imgPCHFL);
				objViewHolderHomeFeedsPromatationClass.lblPromationName = view.FindViewById<TextView>(Resource.Id.lblPromotionNamePCHFL);
				objViewHolderHomeFeedsPromatationClass.btnPromationFollow = view.FindViewById<Button>(Resource.Id.btnFallowPCHFL);
				objViewHolderHomeFeedsPromatationClass.lblPromationDescription = view.FindViewById<TextView>(Resource.Id.lblPromotionsDescriptionPCHFL);

				objViewHolderHomeFeedsPromatationClass.Initialize(view);
				view.Tag = objViewHolderHomeFeedsPromatationClass;
			}
			else
			{
				objViewHolderHomeFeedsPromatationClass = (ViewHolderHomeFeedsPromatationClass)view.Tag;
			}
		
		
			objViewHolderHomeFeedsPromatationClass.ViewPromotionNameClicked = () =>
			{
				PromotionNameClick(_items[position]);
			};
			objViewHolderHomeFeedsPromatationClass.ViewPromotionButtonFollowClicked = () =>
			{
				PromotionButtonFollowClick(_items[position]);
			};

			 if (_items[position].allfeedsflag == 3)//promotion
			{
				objViewHolderHomeFeedsPromatationClass.btnPromationFollow.Text = "";
				objViewHolderHomeFeedsPromatationClass.rlyPromationLayout.Visibility = ViewStates.Visible;
				objViewHolderHomeFeedsPromatationClass.lblPromationDescription.Visibility = ViewStates.Gone;

				objViewHolderHomeFeedsPromatationClass.lblPromationName.Text = _items[position].name;
				objViewHolderHomeFeedsPromatationClass.btnPromationFollow.SetBackgroundResource(Resource.Drawable.addfollowing);
				Picasso.With(_context).Load(_items[position].logo).Into(objViewHolderHomeFeedsPromatationClass.imgPromationImage);

			}
			else if(_items[position].allfeedsflag == 4)
			{
				objViewHolderHomeFeedsPromatationClass.btnPromationFollow.Text = "";
				objViewHolderHomeFeedsPromatationClass.rlyPromationLayout.Visibility = ViewStates.Visible;
				objViewHolderHomeFeedsPromatationClass.lblPromationDescription.Visibility = ViewStates.Visible;
				objViewHolderHomeFeedsPromatationClass.lblPromationName.Text = _items[position].name;
				objViewHolderHomeFeedsPromatationClass.lblPromationDescription.SetText(Html.FromHtml(_items[position].des), TextView.BufferType.Spannable);
				objViewHolderHomeFeedsPromatationClass.btnPromationFollow.SetBackgroundResource(Resource.Drawable.addfollowing);
				Picasso.With(_context).Load(_items[position].logo).Into(objViewHolderHomeFeedsPromatationClass.imgPromationImage);


			}

			return view;
		}
	}
	#region "ViewHolderHomeFeedsClass"
	public class ViewHolderHomeFeedsPromatationClass : Java.Lang.Object
	{
		//Promation Layout
		public ImageView imgPromationImage;
		public TextView lblPromationName;
		public TextView lblPromationDescription;
		public Button btnPromationFollow;
		public RelativeLayout rlyPromationLayout;

		public Action ViewPromotionNameClicked { get; set; }
		public Action ViewPromotionButtonFollowClicked { get; set; }
		public void Initialize(View view)
		{
			view.Click += delegate (object sender, EventArgs e)
			{
				ViewPromotionNameClicked();
			};
			//lblPromationName.Click += delegate
			//	{
			//		ViewPromotionNameClicked();
			//	};
			btnPromationFollow.Click += delegate
					{
						ViewPromotionButtonFollowClicked();
						btnPromationFollow.SetBackgroundResource(Resource.Drawable.correctfollowing);
					};
		}
	}
	#endregion



	#region HomeFeed Comment   Adapter
	public class CommentOnHomeFeedAdapterClass : BaseAdapter<Comment>
	{
		List<Comment> _PostComment;
		Activity _context;

		ViewHolderHomeFeedCommentClass objViewHolderNotificationCommentClass;

		public CommentOnHomeFeedAdapterClass(Activity context, List<Comment> CommentsList)
		{
			_PostComment = CommentsList;
			_context = context;

		}
		public override Comment this[int position]
		{
			get { return _PostComment[position]; }
		}

		public override int Count
		{
			get { return _PostComment.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;

			if (view == null)
			{
				view = this._context.LayoutInflater.Inflate(Resource.Layout.CustomCommentLayout, parent, false);


				objViewHolderNotificationCommentClass = new ViewHolderHomeFeedCommentClass();
				objViewHolderNotificationCommentClass.imgCommentProfileCCL = view.FindViewById<ImageView>(Resource.Id.imgCommentProfileCCL);
				objViewHolderNotificationCommentClass.lblUserNameCCL = view.FindViewById<TextView>(Resource.Id.lblUserNameCCL);
				objViewHolderNotificationCommentClass.lblTimeCCL = view.FindViewById<TextView>(Resource.Id.lblTimeCCL);
				objViewHolderNotificationCommentClass.lblCommentCCL = view.FindViewById<TextView>(Resource.Id.lblCommentCCL);
				objViewHolderNotificationCommentClass.Initialize(view);
				view.Tag = objViewHolderNotificationCommentClass;
			}
			else 
			{
				objViewHolderNotificationCommentClass = (ViewHolderHomeFeedCommentClass)view.Tag;
			}
			if (_PostComment[position].commentflag == 1)//comments flag 1 is for post
			{
				objViewHolderNotificationCommentClass.lblUserNameCCL.Text = _PostComment[position].Fname;// .Fname +" Created the Group "+ _Notification [position].toname;
				objViewHolderNotificationCommentClass.lblTimeCCL.Text = _PostComment[position].Created_at;
				objViewHolderNotificationCommentClass.lblCommentCCL.Text = _PostComment[position].Comments;

				Picasso.With(_context).Load(_PostComment[position].Modifiedimage).Into(objViewHolderNotificationCommentClass.imgCommentProfileCCL);

			}
			else if(_PostComment[position].commentflag == 2)//comments flag 2 is for eventt
			{
				objViewHolderNotificationCommentClass.lblUserNameCCL.Text = _PostComment[position].Fname;// .Fname +" Created the Group "+ _Notification [position].toname
				objViewHolderNotificationCommentClass.lblTimeCCL.Text = _PostComment[position].Created_at;
				objViewHolderNotificationCommentClass.lblCommentCCL.Text = _PostComment[position].Comments;

				Picasso.With(_context).Load(_PostComment[position].Modifiedimage).Into(objViewHolderNotificationCommentClass.imgCommentProfileCCL);

			}
			return view;
		}
	}
	#endregion

	#region "ViewHolderNotificationCommentClass"
	public class ViewHolderHomeFeedCommentClass : Java.Lang.Object
	{
		public ImageView imgCommentProfileCCL;
		public TextView lblUserNameCCL;
		public TextView lblTimeCCL;
		public TextView lblCommentCCL;
		public void Initialize(View view)
		{
			view.Click += delegate (object sender, EventArgs e)
			{
				//ViewNotificationRowClicked ();
			};
		}
	}
	#endregion
	public class MembersAdapterClass : BaseAdapter<Feedsgroupmember>
	{
		List<Feedsgroupmember> _PostComment;
		Activity _context;

		ViewHolderMembersClass objViewHolderMembersClass;

		public MembersAdapterClass(Activity context, List<Feedsgroupmember> CommentsList)
		{
			_PostComment = CommentsList;
			_context = context;

		}
		public override Feedsgroupmember this[int position]
		{
			get { return _PostComment[position]; }
		}

		public override int Count
		{
			get { return _PostComment.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;

			if (view == null)
			{
				view = this._context.LayoutInflater.Inflate(Resource.Layout.MembersLayout, parent, false);


				objViewHolderMembersClass = new ViewHolderMembersClass();
				objViewHolderMembersClass.imgGroupImage = view.FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.img1Custom);
				objViewHolderMembersClass.lblGroupName = view.FindViewById<TextView>(Resource.Id.lblGroupName);
				objViewHolderMembersClass.lblUserName = view.FindViewById<TextView>(Resource.Id.lblUserName);
				objViewHolderMembersClass.Initialize(view);
				view.Tag = objViewHolderMembersClass;
			}
			else
			{
				objViewHolderMembersClass = (ViewHolderMembersClass)view.Tag;
			}
			if(_PostComment[position].Isanadmin=="1")
			{
				objViewHolderMembersClass.lblUserName.Visibility=ViewStates.Gone;
				objViewHolderMembersClass.lblGroupName.Visibility = ViewStates.Gone;
				objViewHolderMembersClass.imgGroupImage.Visibility = ViewStates.Gone;
			}
			else
			{
				objViewHolderMembersClass.lblUserName.Visibility = ViewStates.Visible;
				objViewHolderMembersClass.lblGroupName.Visibility = ViewStates.Gone;
				objViewHolderMembersClass.imgGroupImage.Visibility = ViewStates.Visible;
				objViewHolderMembersClass.lblUserName.Text = _PostComment[position].Fname;

				Picasso.With(_context).Load(_PostComment[position].Modifiedimage).Into(objViewHolderMembersClass.imgGroupImage);
			}
			return view;
		}
	}
	public class ViewHolderMembersClass : Java.Lang.Object
	{
		public Refractored.Controls.CircleImageView imgGroupImage;
		public TextView lblUserName;
		public TextView lblGroupName;

		public void Initialize(View view)
		{
			view.Click += delegate (object sender, EventArgs e)
			{
				//ViewNotificationRowClicked ();
			};
		}
	}

	public class MyGroupFollowerAdapterClass : BaseAdapter<GroupFollower>
	{
		List<GroupFollower> _PostComment;
		Activity _context;

		ViewHolderMembersClass objViewHolderMembersClass;

		public MyGroupFollowerAdapterClass(Activity context, List<GroupFollower> CommentsList)
		{
			_PostComment = CommentsList;
			_context = context;

		}
		public override GroupFollower this[int position]
		{
			get { return _PostComment[position]; }
		}

		public override int Count
		{
			get { return _PostComment.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;

			if (view == null)
			{
				view = this._context.LayoutInflater.Inflate(Resource.Layout.MembersLayout, parent, false);


				objViewHolderMembersClass = new ViewHolderMembersClass();
				objViewHolderMembersClass.imgGroupImage = view.FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.img1Custom);
				objViewHolderMembersClass.lblGroupName = view.FindViewById<TextView>(Resource.Id.lblGroupName);
				objViewHolderMembersClass.lblUserName = view.FindViewById<TextView>(Resource.Id.lblUserName);
				objViewHolderMembersClass.Initialize(view);
				view.Tag = objViewHolderMembersClass;
			}
			else
			{
				objViewHolderMembersClass = (ViewHolderMembersClass)view.Tag;
			}
			//if (_PostComment[position].Isamember == "1")
			//{
			//	objViewHolderMembersClass.lblUserName.Visibility = ViewStates.Gone;
			//	objViewHolderMembersClass.lblGroupName.Visibility = ViewStates.Gone;
			//	objViewHolderMembersClass.imgGroupImage.Visibility = ViewStates.Gone;
			//}
			//else
			//{
				objViewHolderMembersClass.lblUserName.Visibility = ViewStates.Visible;
				objViewHolderMembersClass.lblGroupName.Visibility = ViewStates.Gone;
				objViewHolderMembersClass.imgGroupImage.Visibility = ViewStates.Visible;
				objViewHolderMembersClass.lblUserName.Text = _PostComment[position].Fname;

				Picasso.With(_context).Load(_PostComment[position].Modifiedimage).Into(objViewHolderMembersClass.imgGroupImage);
			//}
			return view;
		}
	}


	#region "Suggested group"
	public class InterestWithinGroupsAdapterClass : BaseAdapter<Interestgroup>
	{
		List<Interestgroup> _items;
		Activity _context;
		private int posizione;
		int[] statoChk;
		int count = 0;
		internal static string isaMember = "";
		ViewHolderInterestgroupClass objViewHolderInterestgroupClass;
		internal event Action<Interestgroup> GroupFollowUnFollowButtonClick;
		public InterestWithinGroupsAdapterClass(Activity context, List<Interestgroup> lstGroupList)
		{
			_items = lstGroupList;
			_context = context;

			this.statoChk = new int[lstGroupList.Count];
			for (int i = 0; i < lstGroupList.Count; i++)
			{
				statoChk[i] = 0;
			}

		}
		public override Interestgroup this[int position]
		{
			get { return _items[position]; }
		}

		public override int Count
		{
			get { return _items.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			posizione = position;
			//string imageSrc = strPath + "/" + _items [position].Interestlogo;   
			View view = convertView;
			count = 0;
			if (view == null)
			{
				view = this._context.LayoutInflater.Inflate(Resource.Layout.CustomGroupLayout, parent, false);

				objViewHolderInterestgroupClass = new ViewHolderInterestgroupClass();
				objViewHolderInterestgroupClass.imgGroups = view.FindViewById<ImageView>(Resource.Id.imgGroupProfileCustomGL);

				objViewHolderInterestgroupClass.imgFollower1 = view.FindViewById<ImageView>(Resource.Id.img1CustomGL);
				objViewHolderInterestgroupClass.imgFollower2 = view.FindViewById<ImageView>(Resource.Id.img2CustomGL);
				objViewHolderInterestgroupClass.imgFollower3 = view.FindViewById<ImageView>(Resource.Id.img3CustomGL);
				objViewHolderInterestgroupClass.imgFollower4 = view.FindViewById<ImageView>(Resource.Id.img4CustomGL);

				objViewHolderInterestgroupClass.btnJoin = view.FindViewById<Button>(Resource.Id.btnJoin);
				objViewHolderInterestgroupClass.lblGroupName = view.FindViewById<TextView>(Resource.Id.lblGroupNameCustomGL);
				objViewHolderInterestgroupClass.lblGroupDetails = view.FindViewById<TextView>(Resource.Id.lblDetailsCustomGL);
				objViewHolderInterestgroupClass.lblMembersList = view.FindViewById<TextView>(Resource.Id.lblGroupMemberNumCustomGL);
				objViewHolderInterestgroupClass.Initialize(view);
				view.Tag = objViewHolderInterestgroupClass;
			}
			else
			{
				objViewHolderInterestgroupClass = (ViewHolderInterestgroupClass)view.Tag;
			}
			objViewHolderInterestgroupClass.ViewClicked = () =>
			{
				GroupFollowUnFollowButtonClick(_items[position]);

			};

			if (statoChk[position] == 0)
			{
				objViewHolderInterestgroupClass.btnJoin.SetBackgroundResource(Resource.Drawable.join); // set unchecked"
			}
			else
			{
				objViewHolderInterestgroupClass.btnJoin.SetBackgroundResource(Resource.Drawable.joined); // set checked"
			}
			objViewHolderInterestgroupClass.lblGroupDetails.Visibility = ViewStates.Invisible;
			objViewHolderInterestgroupClass.lblGroupName.Text = _items[position].Groupname;
			//objViewHolderGroupClass.lblGroupDetails.SetText(Html.FromHtml(_items[position].Groupdesc), TextView.BufferType.Spannable);

			Picasso.With(_context).Load(_items[position].Modifiedlogo).Into(objViewHolderInterestgroupClass.imgGroups);
			if (_items[position].membersinterest != null)
			{
				if (_items[position].memberscount == "1")
				{
					objViewHolderInterestgroupClass.lblMembersList.Text = _items[position].memberscount + " " + "Member";
					objViewHolderInterestgroupClass.imgFollower1.Visibility = ViewStates.Visible;
					objViewHolderInterestgroupClass.imgFollower2.Visibility = ViewStates.Invisible;
					objViewHolderInterestgroupClass.imgFollower3.Visibility = ViewStates.Invisible;
					objViewHolderInterestgroupClass.imgFollower4.Visibility = ViewStates.Invisible;
				}
				else if (_items[position].memberscount == "2")
				{
					objViewHolderInterestgroupClass.lblMembersList.Text = _items[position].memberscount + " " + "Members";
					objViewHolderInterestgroupClass.imgFollower1.Visibility = ViewStates.Visible;
					objViewHolderInterestgroupClass.imgFollower2.Visibility = ViewStates.Visible;
					objViewHolderInterestgroupClass.imgFollower3.Visibility = ViewStates.Invisible;
					objViewHolderInterestgroupClass.imgFollower4.Visibility = ViewStates.Invisible;
				}
				else if (_items[position].memberscount == "3")
				{
					objViewHolderInterestgroupClass.lblMembersList.Text = _items[position].memberscount + " " + "Members";
					objViewHolderInterestgroupClass.imgFollower1.Visibility = ViewStates.Visible;
					objViewHolderInterestgroupClass.imgFollower2.Visibility = ViewStates.Visible;
					objViewHolderInterestgroupClass.imgFollower3.Visibility = ViewStates.Visible;
					objViewHolderInterestgroupClass.imgFollower4.Visibility = ViewStates.Invisible;
				}
				else
				{
					objViewHolderInterestgroupClass.lblMembersList.Text = _items[position].memberscount + " " + "Members";
					objViewHolderInterestgroupClass.imgFollower1.Visibility = ViewStates.Visible;
					objViewHolderInterestgroupClass.imgFollower2.Visibility = ViewStates.Visible;
					objViewHolderInterestgroupClass.imgFollower3.Visibility = ViewStates.Visible;
					objViewHolderInterestgroupClass.imgFollower4.Visibility = ViewStates.Visible;
				}
				foreach (var groupFollowers in _items[position].membersinterest)
				{

					if (count <= 3)
					{

						if (count == 0)
						{
							Picasso.With(_context).Load(groupFollowers.Modifiedimage).Into(objViewHolderInterestgroupClass.imgFollower1);
						}
						else if (count == 1)
						{
							Picasso.With(_context).Load(groupFollowers.Modifiedimage).Into(objViewHolderInterestgroupClass.imgFollower2);
						}
						else if (count == 2)
						{
							Picasso.With(_context).Load(groupFollowers.Modifiedimage).Into(objViewHolderInterestgroupClass.imgFollower3);
						}
						else if (count == 3)
						{
							Picasso.With(_context).Load(groupFollowers.Modifiedimage).Into(objViewHolderInterestgroupClass.imgFollower4);
						}
						count++;
					}
					else
					{
						break;
					}
				}
			}

			return view;
		}
	}
	#endregion
	public class ViewHolderInterestgroupClass : Java.Lang.Object
	{
		//LinearLayout LinearLayoutListInterestSub;
		internal ImageView imgGroups;
		internal Button btnJoin;
		internal TextView lblGroupName;
		internal TextView lblGroupDetails;
		internal TextView lblMembersList;

		internal ImageView imgFollower1;
		internal ImageView imgFollower2;
		internal ImageView imgFollower3;
		internal ImageView imgFollower4;

		//Activity context;
		public Action ViewClicked { get; set; }
		public Action ViewRowClicked { get; set; }
		public void Initialize(View view)
		{
			view.Click += delegate (object sender, EventArgs e)
			{
				ViewRowClicked();
			};
			btnJoin.Click += delegate (object sender, EventArgs e)
			{
				ViewClicked();


			};
		}
	}
	#region "Suggested Events Adapter"
	public class HomeFeedEventsAdapterClass : BaseAdapter<Homefeedsfgeoevent>
	{
		List<Homefeedsfgeoevent> _items;
		List<EventsNearbyLocation> lstMyEventList;
		Activity _context;
		ViewHolderEvents objViewHolderEvents;
		internal event Action<Homefeedsfgeoevent> GoogleMapClick;
		internal event Action<Homefeedsfgeoevent> AddRemoveButtonClick;
		internal event Action<Homefeedsfgeoevent> EventRowClicke;
		private int posizione;
		int[] statoChk;
		public HomeFeedEventsAdapterClass(Activity context, List<Homefeedsfgeoevent> lstEventList, List<EventsNearbyLocation> _lstMyEventList)
		{
			_items = lstEventList;
			lstMyEventList = _lstMyEventList;
			_context = context;
			this.statoChk = new int[lstEventList.Count];
			for (int i = 0; i < lstEventList.Count; i++)
			{
				statoChk[i] = 0;
			}
		}
		public override Homefeedsfgeoevent this[int position]
		{
			get { return _items[position]; }
		}

		public override int Count
		{
			get { return _items.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			//string imageSrc = strPath +"/"+ _items [position].Eventlogo;   
			View view = convertView;

			if (view == null)
			{
				view = this._context.LayoutInflater.Inflate(Resource.Layout.CustomEventLayout, parent, false);
				objViewHolderEvents = new ViewHolderEvents();

				objViewHolderEvents.btnPlusMark = view.FindViewById<Button>(Resource.Id.btnAddEvents);
				objViewHolderEvents.btnCost = view.FindViewById<Button>(Resource.Id.btnCostEvent);
				objViewHolderEvents.imgEventLogo = view.FindViewById<ImageView>(Resource.Id.imageEvent);
				objViewHolderEvents.lblEventName = view.FindViewById<TextView>(Resource.Id.lblEventTitle);
				objViewHolderEvents.lblEventDateTime = view.FindViewById<TextView>(Resource.Id.lblEventDateTime);
				objViewHolderEvents.lblEventAddress = view.FindViewById<TextView>(Resource.Id.lblEventAddress);

				objViewHolderEvents.Initialize(view);

				view.Tag = objViewHolderEvents;
			}
			else
			{
				objViewHolderEvents = (ViewHolderEvents)view.Tag;
				//objViewHolderEvents = view.Tag as ViewHolderEvents;
			}
			objViewHolderEvents.ViewClicked = () =>
			{
				AddRemoveButtonClick(_items[position]);

			};
			objViewHolderEvents.ViewRowClicked = () =>
			{
				EventRowClicke(_items[position]);
			};
			objViewHolderEvents.ViewGoogleMap = () =>
			 {
				 GoogleMapClick(_items[position]);

			 };
			if (statoChk[position] == 0)
			{
				objViewHolderEvents.btnPlusMark.SetBackgroundResource(Resource.Drawable.plusmark);   // set unchecked"
			}
			else
			{
				objViewHolderEvents.btnPlusMark.SetBackgroundResource(Resource.Drawable.bluecheckmark);  // set checked"
			}
			List<EventsNearbyLocation> listValue = lstMyEventList.Where(x => x.Idevents.Equals(_items[position].Idevents)).ToList();
			if (listValue.Count > 0)
			{
				if (listValue[0].Idevents == _items[position].Idevents)
				{
					objViewHolderEvents.btnPlusMark.SetBackgroundResource(Resource.Drawable.bluecheckmark);
				}
				else
				{
					objViewHolderEvents.btnPlusMark.SetBackgroundResource(Resource.Drawable.plusmark);
				}
			}
			objViewHolderEvents.lblEventName.SetTypeface(null, TypefaceStyle.Bold);
			objViewHolderEvents.lblEventName.Text = _items[position].Eventname.ToUpper();

			DateTime datetime = Convert.ToDateTime(_items[position].date);
			string strEventDate = datetime.ToString("MMMMM dd yyyy");

			if (_items[position].EventEndtime == "24:00:00")
			{

				objViewHolderEvents.lblEventDateTime.Text = strEventDate + " at " + _items[position].Eventtime + "-" + "24:00";
			}
			else
			{
				objViewHolderEvents.lblEventDateTime.Text = strEventDate + " at " + _items[position].Eventtime + "-" + Convert.ToDateTime(_items[position].EventEndtime).ToString("HH:mm");//+"-"+_items[position].EventEndtime;
			}
			if (_items[position].Ispaidevent == "1")
			{
				objViewHolderEvents.btnCost.SetBackgroundResource(Resource.Drawable.shoping_cart);
			}
			else
			{
				objViewHolderEvents.btnCost.SetBackgroundResource(Resource.Drawable.freeimage);
			}
			objViewHolderEvents.lblEventAddress.Text = _items[position].Address;
			if (!string.IsNullOrEmpty(_items[position].Eventlogo))
			{
				Picasso.With(_context).Load(_items[position].Eventlogo).Into(objViewHolderEvents.imgEventLogo);
			}
			else
			{
				objViewHolderEvents.imgEventLogo.SetImageResource(Resource.Drawable.noimages);
			}
			if (SlidingTabActivity.isPosition)
			{
				if (_items[position].Eventname.Equals(GoogleMapActivity.StrEventName))
				{

				}
				else
				{

				}
			}
			return view;
		}
	}
	#endregion



	#region HomeFeed Comment   Adapter
	public class IndividualChatAdapterClass : BaseAdapter<Indivisualchat>
	{
		List<Indivisualchat> _Chat;
		Activity _context;
		string strDate = "";
		ViewHolderIndividualChatClass objViewHolderIndividualChatClass;

		internal event Action<Indivisualchat> ToImageDownLoadClick;
		internal event Action<Indivisualchat> ToVideoDownLoadClick;
		internal event Action<Indivisualchat> ToAudioDownLoadClick;
		internal event Action<Indivisualchat> ToDocDownLoadClick;

		internal event Action<Indivisualchat> FromImageDownLoadClick;
		internal event Action<Indivisualchat> FromVideoDownLoadClick;
		internal event Action<Indivisualchat> FromAudioDownLoadClick;
		internal event Action<Indivisualchat> FromDocDownLoadClick;
		int[] statoChk;
		public IndividualChatAdapterClass(Activity context, List<Indivisualchat> CommentsList)
		{
			_Chat = CommentsList;
			_context = context;
			this.statoChk = new int[_Chat.Count];
			for (int i = 0; i < _Chat.Count; i++)
			{
				statoChk[i] = 0;
			}
		}
		public override Indivisualchat this[int position]
		{
			get { return _Chat[position]; }
		}

		public override int Count
		{
			get { return _Chat.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;

			if (view == null)
			{
				view = this._context.LayoutInflater.Inflate(Resource.Layout.CustomChatMessageLayout, parent, false);
				objViewHolderIndividualChatClass = new ViewHolderIndividualChatClass();

				objViewHolderIndividualChatClass.lblDate = view.FindViewById<TextView>(Resource.Id.lbldate);
				objViewHolderIndividualChatClass.lnyToUserSet = view.FindViewById<LinearLayout>(Resource.Id.lnyToUserList);
				objViewHolderIndividualChatClass.lblToMessageText = view.FindViewById<TextView>(Resource.Id.lblTextMessageTo);
				objViewHolderIndividualChatClass.imgToImage = view.FindViewById<ImageView>(Resource.Id.imgImageTo);
				objViewHolderIndividualChatClass.lblToImageName = view.FindViewById<TextView>(Resource.Id.lblImageNameTo);
				objViewHolderIndividualChatClass.imgToDownLoadImage = view.FindViewById<ImageView>(Resource.Id.imgImageDownloadTo);
				objViewHolderIndividualChatClass.lblToDocTypeName = view.FindViewById<TextView>(Resource.Id.lblDocTypeNameTo);
				objViewHolderIndividualChatClass.imgToDownLoadDoc = view.FindViewById<ImageView>(Resource.Id.imgDocDownloadTo);
				objViewHolderIndividualChatClass.lblToAudioName = view.FindViewById<TextView>(Resource.Id.lblAudioTypeNameTo);
				objViewHolderIndividualChatClass.imgToDownLoadAudio = view.FindViewById<ImageView>(Resource.Id.imgAudioDownloadTo);
				objViewHolderIndividualChatClass.lnyToImageLayout = view.FindViewById<LinearLayout>(Resource.Id.lnyToImageLayout);
				objViewHolderIndividualChatClass.lnyToDocLayout = view.FindViewById<LinearLayout>(Resource.Id.lnyDocumentTypeTo);
				objViewHolderIndividualChatClass.lnyToAudioLayout = view.FindViewById<LinearLayout>(Resource.Id.lnyAudioTo);
				objViewHolderIndividualChatClass.lnyToVideoLayout = view.FindViewById<LinearLayout>(Resource.Id.lnyVideoTo);
				objViewHolderIndividualChatClass.lblToVideoName = view.FindViewById<TextView>(Resource.Id.lblVideoTypeNameTo);
				objViewHolderIndividualChatClass.imgToDownloadVideo = view.FindViewById<ImageView>(Resource.Id.imgVideoDownloadTo);

				objViewHolderIndividualChatClass.lnyVideoLayoutFrom = view.FindViewById<LinearLayout>(Resource.Id.lnyVideoFrom);
				objViewHolderIndividualChatClass.lblVideoNameFrom = view.FindViewById<TextView>(Resource.Id.lblVideoTypeNameFrom);
				objViewHolderIndividualChatClass.imgDownloadVideoFrom = view.FindViewById<ImageView>(Resource.Id.imgVideoDownloadFrom);
				objViewHolderIndividualChatClass.lnyDocLayoutFrom = view.FindViewById<LinearLayout>(Resource.Id.lnyDocumentTypeFrom);
				objViewHolderIndividualChatClass.lnyAudioLayoutFrom = view.FindViewById<LinearLayout>(Resource.Id.lnyAudioFrom);
				objViewHolderIndividualChatClass.lnyImageLayoutFrom = view.FindViewById<LinearLayout>(Resource.Id.lnyImageLayoutFrom);
				objViewHolderIndividualChatClass.lnyFromUserSet = view.FindViewById<LinearLayout>(Resource.Id.lnyFromUserList);
				objViewHolderIndividualChatClass.lblFromMessageText = view.FindViewById<TextView>(Resource.Id.lblTextMessageFrom);
				objViewHolderIndividualChatClass.imgFromImage = view.FindViewById<ImageView>(Resource.Id.imgImageFrom);
				objViewHolderIndividualChatClass.lblFromImageName = view.FindViewById<TextView>(Resource.Id.lblImageNameFrom);
				objViewHolderIndividualChatClass.imgFromDownLoadImage = view.FindViewById<ImageView>(Resource.Id.imgImageDownloadFrom);
				objViewHolderIndividualChatClass.lblFromDocTypeName = view.FindViewById<TextView>(Resource.Id.lblDocTypeNameFrom);
				objViewHolderIndividualChatClass.imgFromDownLoadDoc = view.FindViewById<ImageView>(Resource.Id.imgDocDownloadFrom);
				objViewHolderIndividualChatClass.lblFromAudioName = view.FindViewById<TextView>(Resource.Id.lblAudioTypeNameFrom);
				objViewHolderIndividualChatClass.imgFromDownLoadAudio = view.FindViewById<ImageView>(Resource.Id.imgAudioDownloadFrom);

				objViewHolderIndividualChatClass.Initialize(view);
				view.Tag = objViewHolderIndividualChatClass;
			}
			else
			{
				objViewHolderIndividualChatClass = (ViewHolderIndividualChatClass)view.Tag;
			}
			//
			objViewHolderIndividualChatClass.ToImageDownLoadButtonClicked = () =>
			{
				ToImageDownLoadClick(_Chat[position]);

			};
			objViewHolderIndividualChatClass.ToVideoDownLoadButtonClicked = () =>
			{
				ToVideoDownLoadClick(_Chat[position]);

			};
			objViewHolderIndividualChatClass.ToAudioDownLoadButtonClicked = () =>
			{
				ToAudioDownLoadClick(_Chat[position]);

			};
			objViewHolderIndividualChatClass.ToDocDownLoadButtonClicked = () =>
			{
				ToDocDownLoadClick(_Chat[position]);

			};
			//
			objViewHolderIndividualChatClass.FromImageDownLoadButtonClicked = () =>
			{
				FromImageDownLoadClick(_Chat[position]);

			};
			objViewHolderIndividualChatClass.FromVideoDownLoadButtonClicked = () =>
			{
				FromVideoDownLoadClick(_Chat[position]);

			};
			objViewHolderIndividualChatClass.FromAudioDownLoadButtonClicked = () =>
			{
				FromAudioDownLoadClick(_Chat[position]);

			};
			objViewHolderIndividualChatClass.FromDocDownLoadButtonClicked = () =>
			{
				FromDocDownLoadClick(_Chat[position]);

			};

			if(string.IsNullOrEmpty(strDate)||!_Chat[position].msged_at.messaged_at.Equals(strDate))//chat date 
			{
				objViewHolderIndividualChatClass.lblDate.Visibility = ViewStates.Visible;
				objViewHolderIndividualChatClass.lblDate.Text=_Chat[position].msged_at.messaged_at;
				strDate = _Chat[position].msged_at.messaged_at;
			}
			else
			{
				objViewHolderIndividualChatClass.lblDate.Visibility = ViewStates.Gone;
			}
			//*************from*************
			if(_Chat[position].messageindividual.Iduserfrom.Equals(SplashScreenActivity.strUserId))//from
			{
				objViewHolderIndividualChatClass.lnyFromUserSet.Visibility = ViewStates.Visible;
				objViewHolderIndividualChatClass.lnyToUserSet.Visibility = ViewStates.Gone;
				if(_Chat[position].messageindividual.Typeofmesasge=="0")//text
				{
					objViewHolderIndividualChatClass.lblFromMessageText.Visibility = ViewStates.Visible;
					objViewHolderIndividualChatClass.lnyImageLayoutFrom.Visibility = ViewStates.Gone;
					objViewHolderIndividualChatClass.lnyDocLayoutFrom.Visibility = ViewStates.Gone;
					objViewHolderIndividualChatClass.lnyAudioLayoutFrom.Visibility = ViewStates.Gone;
					objViewHolderIndividualChatClass.lnyVideoLayoutFrom.Visibility = ViewStates.Gone;
					
					objViewHolderIndividualChatClass.lblFromMessageText.Text = _Chat[position].messageindividual.Messagecontent.Trim();
				}
				else if (_Chat[position].messageindividual.Typeofmesasge == "2")//.(extension)
				{
					if(_Chat[position].messageindividual.Messagecontent.ToLower().Contains(".jpg")||_Chat[position].messageindividual.Messagecontent.ToLower().Contains(".png")||_Chat[position].messageindividual.Messagecontent.ToLower().Contains(".jpeg")||_Chat[position].messageindividual.Messagecontent.ToLower().Contains(".gif")||_Chat[position].messageindividual.Messagecontent.ToLower().Contains(".bmp"))//for image
					{
						objViewHolderIndividualChatClass.lnyImageLayoutFrom.Visibility = ViewStates.Visible;
						objViewHolderIndividualChatClass.lnyDocLayoutFrom.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lnyAudioLayoutFrom.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lblFromMessageText.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lnyVideoLayoutFrom.Visibility = ViewStates.Gone;

						objViewHolderIndividualChatClass.lblFromImageName.Text = _Chat[position].messageindividual.filename;
						Picasso.With(_context).Load(_Chat[position].messageindividual.Messagecontent).Into(objViewHolderIndividualChatClass.imgFromImage);
					}
					else if (_Chat[position].messageindividual.Messagecontent.ToLower().Contains(".mp4"))//for video
					{
						objViewHolderIndividualChatClass.lnyVideoLayoutFrom.Visibility = ViewStates.Visible;
						objViewHolderIndividualChatClass.lnyImageLayoutFrom.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lnyDocLayoutFrom.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lnyAudioLayoutFrom.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lblFromMessageText.Visibility = ViewStates.Gone;

						objViewHolderIndividualChatClass.lblVideoNameFrom.Text = _Chat[position].messageindividual.filename;

					}
					else if (_Chat[position].messageindividual.Messagecontent.ToLower().Contains(".mp3"))//for audio
					{
						objViewHolderIndividualChatClass.lnyVideoLayoutFrom.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lnyImageLayoutFrom.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lnyDocLayoutFrom.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lnyAudioLayoutFrom.Visibility = ViewStates.Visible;
						objViewHolderIndividualChatClass.lblFromMessageText.Visibility = ViewStates.Gone;

						objViewHolderIndividualChatClass.lblFromAudioName.Text=_Chat[position].messageindividual.filename;

					}
					else//for doc
					{
						objViewHolderIndividualChatClass.lnyImageLayoutFrom.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lnyDocLayoutFrom.Visibility = ViewStates.Visible;
						objViewHolderIndividualChatClass.lnyAudioLayoutFrom.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lblFromMessageText.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lnyVideoLayoutFrom.Visibility = ViewStates.Gone;

						objViewHolderIndividualChatClass.lblFromDocTypeName.Text = _Chat[position].messageindividual.filename;
					}
				}
			}
			//************to***************
			else//to
			{
				objViewHolderIndividualChatClass.lnyFromUserSet.Visibility = ViewStates.Gone;
				objViewHolderIndividualChatClass.lnyToUserSet.Visibility = ViewStates.Visible;
				if (_Chat[position].messageindividual.Typeofmesasge == "0")//text
				{
					objViewHolderIndividualChatClass.lblToMessageText.Visibility = ViewStates.Visible;
					objViewHolderIndividualChatClass.lnyToImageLayout.Visibility = ViewStates.Gone;
					objViewHolderIndividualChatClass.lnyToDocLayout.Visibility = ViewStates.Gone;
					objViewHolderIndividualChatClass.lnyToAudioLayout.Visibility = ViewStates.Gone;
					objViewHolderIndividualChatClass.lnyToVideoLayout.Visibility = ViewStates.Gone;

					objViewHolderIndividualChatClass.lblToMessageText.Text = _Chat[position].messageindividual.Messagecontent.Trim();

				}
				else if (_Chat[position].messageindividual.Typeofmesasge == "2")//.(extensio)n
				{
					if (_Chat[position].messageindividual.Messagecontent.ToLower().Contains(".jpg") || _Chat[position].messageindividual.Messagecontent.ToLower().Contains(".png") || _Chat[position].messageindividual.Messagecontent.ToLower().Contains(".jpeg") || _Chat[position].messageindividual.Messagecontent.ToLower().Contains(".gif") || _Chat[position].messageindividual.Messagecontent.ToLower().Contains(".bmp"))//for image
					{
						objViewHolderIndividualChatClass.lnyToImageLayout.Visibility = ViewStates.Visible;
						objViewHolderIndividualChatClass.lnyToDocLayout.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lnyToAudioLayout.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lblToMessageText.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lnyToVideoLayout.Visibility = ViewStates.Gone;

						objViewHolderIndividualChatClass.lblToImageName.Text = _Chat[position].messageindividual.filename;
						Picasso.With(_context).Load(_Chat[position].messageindividual.Messagecontent).Into(objViewHolderIndividualChatClass.imgToImage);

					}
					else if (_Chat[position].messageindividual.Messagecontent.ToLower().Contains(".mp4"))//for video
					{
						objViewHolderIndividualChatClass.lnyToVideoLayout.Visibility = ViewStates.Visible;
						objViewHolderIndividualChatClass.lnyToImageLayout.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lnyToDocLayout.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lnyToAudioLayout.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lblToMessageText.Visibility = ViewStates.Gone;

						objViewHolderIndividualChatClass.lblToVideoName.Text = _Chat[position].messageindividual.filename;
					}
					else if (_Chat[position].messageindividual.Messagecontent.ToLower().Contains(".mp3"))//for audio
					{
						objViewHolderIndividualChatClass.lnyToImageLayout.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lnyToDocLayout.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lnyToAudioLayout.Visibility = ViewStates.Visible;
						objViewHolderIndividualChatClass.lblToMessageText.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lnyToVideoLayout.Visibility = ViewStates.Gone;

						objViewHolderIndividualChatClass.lblToAudioName.Text = _Chat[position].messageindividual.filename;
					}
					else//for doc
					{
						objViewHolderIndividualChatClass.lnyToImageLayout.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lnyToDocLayout.Visibility = ViewStates.Visible;
						objViewHolderIndividualChatClass.lnyToAudioLayout.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lblToMessageText.Visibility = ViewStates.Gone;
						objViewHolderIndividualChatClass.lnyToVideoLayout.Visibility = ViewStates.Gone;

						objViewHolderIndividualChatClass.lblToDocTypeName.Text = _Chat[position].messageindividual.filename;

					}
				}
			}
				return view;
		}
	}
	#endregion

	#region "ViewHolderNotificationCommentClass"
	public class ViewHolderIndividualChatClass : Java.Lang.Object
	{
		
		public TextView lblDate;
		public LinearLayout lnyToUserSet;
		public TextView lblToMessageText;
		public LinearLayout lnyToImageLayout;
		public ImageView imgToImage;
		public TextView lblToImageName;
		public ImageView imgToDownLoadImage;
		public LinearLayout lnyToDocLayout;
		public TextView lblToDocTypeName;
		public ImageView imgToDownLoadDoc;
		public LinearLayout lnyToAudioLayout;
		public TextView lblToAudioName;
		public ImageView imgToDownLoadAudio;
		public LinearLayout lnyToVideoLayout;
		public TextView lblToVideoName;
		public ImageView imgToDownloadVideo;

		public LinearLayout lnyImageLayoutFrom;
		public LinearLayout lnyFromUserSet;
		public TextView lblFromMessageText;
		public ImageView imgFromImage;
		public TextView lblFromImageName;
		public LinearLayout lnyDocLayoutFrom;
		public ImageView imgFromDownLoadImage;
		public TextView lblFromDocTypeName;
		public ImageView imgFromDownLoadDoc;
		public LinearLayout lnyAudioLayoutFrom;
		public TextView lblFromAudioName;
		public ImageView imgFromDownLoadAudio;
		public LinearLayout lnyVideoLayoutFrom;
		public TextView lblVideoNameFrom;
		public ImageView imgDownloadVideoFrom;

		public Action ToImageDownLoadButtonClicked { get; set; }
		public Action ToVideoDownLoadButtonClicked { get; set; }
		public Action ToAudioDownLoadButtonClicked { get; set; }
		public Action ToDocDownLoadButtonClicked { get; set; }

		public Action FromImageDownLoadButtonClicked { get; set; }
		public Action FromVideoDownLoadButtonClicked { get; set; }
		public Action FromAudioDownLoadButtonClicked { get; set; }
		public Action FromDocDownLoadButtonClicked { get; set; }

		public void Initialize(View view)
		{
			view.Click += delegate (object sender, EventArgs e)
			{
				
			};
			imgToDownLoadImage.Click += delegate
			{
				ToImageDownLoadButtonClicked();
			};
			imgToDownloadVideo.Click += delegate
			{
				ToVideoDownLoadButtonClicked();
			};
			imgToDownLoadAudio.Click += delegate
			{
				ToAudioDownLoadButtonClicked();
			};
			imgToDownLoadDoc.Click += delegate
			{
				ToDocDownLoadButtonClicked();
			};

			imgFromDownLoadImage.Click += delegate
			{
				FromImageDownLoadButtonClicked();
			};
			imgFromDownLoadAudio.Click += delegate
			{
				FromAudioDownLoadButtonClicked();
			};
			imgFromDownLoadDoc.Click += delegate
			{
				FromDocDownLoadButtonClicked();
			};
			imgDownloadVideoFrom.Click += delegate
			{
				FromVideoDownLoadButtonClicked();
			};
		}
	}
	#endregion

}

