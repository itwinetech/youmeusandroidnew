﻿using System;
using System.Collections.Generic;

namespace YouMeUsAndroid
{
 

	/// <summary>
	/// /////new notification classs
	/// </summary>
	/// 
	public class NotificationPost
	{
		public string Groupname { get; set; }
		public string Idgroups { get; set; }
		public string Groupslogo { get; set; }
		public string Idusers { get; set; }
		public string Idpoststo { get; set; }
		public string Idpostfrom { get; set; }
		public string @short { get; set; }
		public string Postcontent { get; set; }
		public string Created_at { get; set; }
		public string Idposts { get; set; }
		public string Typeofpost { get; set; }
		public string Videourl { get; set; }
		public string url { get; set; }
		public string description { get; set; }
		public string image { get; set; }
		public string title { get; set; }
		public string Fname { get; set; }
		public string Imagename { get; set; }
		public string Flag { get; set; }
		public object Idcontent { get; set; }
		public string Modifiedimage { get; set; }
		public string UrlGroupslogo { get; set; }
	}



	public class NotificationComment
	{
		public string Idpostcomments { get; set; }
		public string Idposts { get; set; }
		public string Idusers { get; set; }
		public string Comment { get; set; }
		public string Videourl { get; set; }
		public string Created_at { get; set; }
		public string Fname { get; set; }
		public string Modifiedimage { get; set; }
		public string Postuserlogo { get; set; }
	}

	public class PostLikes
	{
		public string totallikes { get; set; }
	}

	public class Userimgpath
	{
		public string Fname { get; set; }
		public string Idusers { get; set; }
		public string Lname { get; set; }
		public string Email { get; set; }
		public string Active { get; set; }
		public string Dateofbirth { get; set; }
		public string Originalimage	 { get; set; }
		public string Modifiedimage { get; set; }
	}

	public class GroupAdminDetails
	{
		public string Idusers { get; set; }
		public string Idgroups { get; set; }
		public string Groupslogo { get; set; }
		public string Urlkeyword { get; set; }
		public string GroupAdmin { get; set; }
		public string Lname { get; set; }
		public string Username { get; set; }
		public string Groupname { get; set; }
		public string Privacysetting { get; set; }
		public string Groupdesc { get; set; }
		public string Created_by { get; set; }
		public string Modifiedimage { get; set; }
		public string groupslogourl { get; set; }
	}

	public class GroupMember
	{
        public string Idusergroups { get; set; }
		public string Idusers { get; set; }
		public string Lname { get; set; }
		public string Idgroups { get; set; }
		public string Isanadmin { get; set; }
		public string Fname { get; set; }
		public string Groupname { get; set; }
		public string Urlkeyword { get; set; }
		public string Groupdesc { get; set; }
		public string Modifiedimage { get; set; }
		public string Created_by { get; set; }
		public string groupmembersuserlogo { get; set; }
	}

	public class GroupInvitation
	{
		public string Idusergroups { get; set; }
 		public string Idgroups { get; set; }
		public string Fname { get; set; }
		public string Idusers { get; set; }
		public string Lname { get; set; }
		public string Modifiedimage { get; set; }
		public string grouprequestsuserlogo { get; set; }
	}

	public class EventFeedsFollower
	{
		public string Fname { get; set; }
		public string Lname { get; set; }
		public string Idusers { get; set; }
		public string Idevents { get; set; }
		public string Status { get; set; }
		public string Modifiedimage { get; set; }
		public string Eventfollowerslogo { get; set; }
	}

	public class NotificatioEventFeeds
	{
		public string Idevents { get; set; }
		public string Ispaidevent { get; set; }
		public string Paymenturl { get; set; }
		public string eventcreater { get; set; }
		public string Eventname { get; set; }
		public string Urlkeyword { get; set; }
		public string Eventlogo { get; set; }
		public string Eventdesc { get; set; }
		public string Eventdate { get; set; }
		public string Eventtime { get; set; }
		public string Created_at { get; set; }
		public string Groupname { get; set; }
		public string Fname { get; set; }
		public string CityName { get; set; }
		public string Flag { get; set; }
		public object Idcontent { get; set; }
		public string Modifiedimage { get; set; }
		public string UrlEventslogo { get; set; }
		public string eventcreaterlogo { get; set; }
	}


	public class Notification
	{
		public string idcontent { get; set; }
		public string fromuserid { get; set; }
		public string Created_at { get; set; }
		public string typeofcontent { get; set; }
		public string pp { get; set; }
		public string grouptype { get; set; }
		public string toname { get; set; }
		public string Groupname { get; set; }
		public string Urlkeyword { get; set; }
		public string Fname { get; set; }
		public string Modifiedimage { get; set; }
		public string typeid { get; set; }
		public string Iduserviewnotification { get; set; }
		public NotificationPost NotificationPost { get; set; }
		public List<NotificationComment> NotificationComment { get; set; }
		public PostLikes PostLikes { get; set; }
		public string userlikestatus { get; set; }
		public string message { get; set; }
		public Userimgpath Userimgpath { get; set; }
		public string userimgid { get; set; }
		public string Isanadmin { get; set; }
		public GroupAdminDetails GroupAdminDetails { get; set; }
		public string totalevents { get; set; }
		public List<GroupMember> GroupMembers { get; set; }
		public List<GroupInvitation> GroupInvitation { get; set; }
		public List<EventFeedsFollower> EventFeedsFollowers { get; set; }
		public NotificatioEventFeeds NotificatioEventFeeds { get; set; }
		public int? NotificationCount { get; set; }
	}

	public class NotificationDetails
	{
		public int NotificationCount { get; set; }
		public List<Notification> Notification { get; set; }
	}
	//Like and UnLike class
	public class Likes
	{
		public string totallikes { get; set; }
		public int Flag { get; set; }
		public string UserStatus { get; set; }
	}

	public class LikesDetails
	{
		public Likes Likes { get; set; }
	}

	//Group Request Approval
	public class GroupRequestApproval
	{
		public int group_requests { get; set; }
		public string Message { get; set; }
	}

	//UnRead Noti Count
	public class NotificationNewCount
	{
		public int newcount { get; set; }
	}

	public class GroupMembersAndRequests
	{
		public List<GroupMember> GroupMembers { get; set; }
		public List<GroupInvitation> GroupInvitation { get; set; }
		public List<GroupAdminDetails> GroupAdminDetails { get; set; }
	}

}

