﻿using System;
using System.Collections.Generic;

namespace YouMeUsAndroid
{
	public class Taginterest
	{
		public string id { get; set; }
		public string name { get; set; }
		public string Interestlogo { get; set; }
	}

	public class TagInterestClass
	{
		public List<Taginterest> taginterests { get; set; }
}
}
