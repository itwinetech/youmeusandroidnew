﻿using System;
using System.Collections.Generic;

namespace YouMeUsAndroid
{
		public class Intuser
		{
			public string Idinterestmaster { get; set; }
			public string Fname { get; set; }
			public string Idusers { get; set; }
			public string Modifiedimage { get; set; }
		}

		public class Inttotal
		{
			public string total { get; set; }
		}

		public class Membersinterest
		{
			public string Idusergroups { get; set; }
			public string Idusers { get; set; }
			public string Lname { get; set; }
			public string Idgroups { get; set; }
			public string Isanadmin { get; set; }
			public string Fname { get; set; }
			public string Modifiedimage { get; set; }
		}

		public class Interestgroup
		{
			public string Idgroups { get; set; }
			public string Groupname { get; set; }
			public string Urlkeyword { get; set; }
			public string Modifiedlogo { get; set; }
			public string GroupAdmin { get; set; }
			public string Groupdesc { get; set; }
			public string usrcnt { get; set; }
			public string Idusergroups { get; set; }
			public string Privacysetting { get; set; }
			public string Isamember { get; set; }
			public string Closedgroupflag { get; set; }
			public List<Membersinterest> membersinterest { get; set; }
			public string memberscount { get; set; }
		}

		public class Interestevent
		{
			public string date2 { get; set; }
			public string Idevents { get; set; }
			public string Eventname { get; set; }
			public string Urlkeyword { get; set; }
			public string Eventlogo { get; set; }
			public string Eventdesc { get; set; }
			public string EventDate { get; set; }
			public string EventStartTime { get; set; }
			public object EventEndTime { get; set; }
			public string Address { get; set; }
			public string Typeofevent { get; set; }
			public string Ispaidevent { get; set; }
			public string Paymenturl { get; set; }
			public string Eventslogo { get; set; }
		}

		public class Groupdetailspromo
		{
			public string Idusers { get; set; }
			public string Idgroups { get; set; }
			public string Groupslogo { get; set; }
			public string Urlkeyword { get; set; }
			public string GroupAdmin { get; set; }
			public string Lname { get; set; }
			public string Username { get; set; }
			public string Groupname { get; set; }
			public string Privacysetting { get; set; }
			public string Groupdesc { get; set; }
			public string Created_by { get; set; }
			public string Modifiedimage { get; set; }
		}

		public class Eventlistpromo
		{
			public string Address { get; set; }
			public string Eventdate { get; set; }
			public string Eventtime { get; set; }
			public string Eventcity { get; set; }
			public string Idevents { get; set; }
			public string Eventdesc { get; set; }
			public string Eventname { get; set; }
			public string Eventlogo { get; set; }
		}

		public class Groupeventspromo
		{
			public string Idevents { get; set; }
			public string Urlkeyword { get; set; }
			public string Ispaidevent { get; set; }
			public string Paymenturl { get; set; }
			public string Address { get; set; }
			public string Eventdate { get; set; }
			public string EventStartTime { get; set; }
			public object EventEndTime { get; set; }
			public string Eventcity { get; set; }
			public string Eventdesc { get; set; }
			public string Eventname { get; set; }
			public string Eventlogo { get; set; }
		}

		public class Groupeventscountpromo
		{
			public string totalevents { get; set; }
		}

		public class Memberspromo
		{
			public string Idusergroups { get; set; }
			public string Idusers { get; set; }
			public string Lname { get; set; }
			public string Idgroups { get; set; }
			public string Isanadmin { get; set; }
			public string Fname { get; set; }
			public string Groupname { get; set; }
			public string Urlkeyword { get; set; }
			public string Groupdesc { get; set; }
			public string Modifiedimage { get; set; }
			public string Created_by { get; set; }
		}

		public class Groupinfopromo
		{
			public string Idgroups { get; set; }
			public string Groupname { get; set; }
			public string Groupdesc { get; set; }
			public string Urlkeyword { get; set; }
			public string Groupslogo { get; set; }
			public string Privacysetting { get; set; }
			public string Isamember { get; set; }
			public string Closedgroupflag { get; set; }
		}

		public class Groupdescriptionpostpromo
		{
			public string Idposts { get; set; }
			public string Created_by { get; set; }
			public string Groupname { get; set; }
			public string Idpostfrom { get; set; }
			public string Videourl { get; set; }
			public string Postcontent { get; set; }
			public string Fname { get; set; }
			public string Modified_at { get; set; }
			public string Typeofpost { get; set; }
			public string image { get; set; }
			public string title { get; set; }
			public string canonicalUrl { get; set; }
			public string url { get; set; }
			public string description { get; set; }
			public string Imagename { get; set; }
			public object Idpostlikes { get; set; }
			public string Flag { get; set; }
			public string Modifiedimage { get; set; }
			public string @short { get; set; }
			public string Idcontent { get; set; }
			public string Actiontaken { get; set; }
		}

		public class Groupdescription
		{
			public List<Groupdetailspromo> groupdetailspromo { get; set; }
			public List<Eventlistpromo> eventlistpromo { get; set; }
			public List<Groupeventspromo> groupeventspromo { get; set; }
			public List<Groupeventscountpromo> groupeventscountpromo { get; set; }
			public List<Memberspromo> memberspromo { get; set; }
			public Groupinfopromo groupinfopromo { get; set; }
			public bool isamembers { get; set; }
			public string groupfeedcountpromo { get; set; }
			public List<Groupdescriptionpostpromo> groupdescriptionpostpromo { get; set; }
		}

		public class Orgfeedspromotion
		{
			public string type { get; set; }
			public string id { get; set; }
			public string Url_keyword { get; set; }
			public string logo { get; set; }
			public string name { get; set; }
			public string des { get; set; }
			public List<Intuser> intusers { get; set; }
			public Inttotal inttotal { get; set; }
			public int allfeedsflag { get; set; }
			public List<Interestgroup> interestgroups { get; set; }
			public List<Interestevent> interestevents { get; set; }
			public string Urlkeyword { get; set; }
			public Groupdescription groupdescription { get; set; }
		}

		public class PromationClass
		{
			public List<Orgfeedspromotion> Orgfeedspromotion { get; set; }
		}
}

