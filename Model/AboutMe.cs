﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Newtonsoft.Json;
using Android.Content;
using System.Threading.Tasks;
using System.IO;
using Android.App;

//  profile photo and cover photo info
//string strLocalPath=objCommon.fnCreateFolderInSDCard(context.GetString(Resource.String.app_name),"UserProfile");  
//System.IO.Path.Combine(strLocalPath,"ProfilePhoto101.jpg")  
//System.IO.Path.Combine(strLocalPath,"coverPhoto002.jpg") 

namespace YouMeUsAndroid
{
//
	public class AboutMe
	{
		public string baseurl { get; set; }
		public List<Userlist> userlist { get; set; } 
//		public List<AllCity> AllCities { get; set; }
		 internal static string strUserId="";
		internal static string strUserName = "";
		internal static string strUserProfileImage = "";
		internal static string strUserEmail = "";
		internal static List< Userlist> lstAboutMe ;
//		internal static List<AllCity> lstAllCity;

		string strPath; 
		async public Task<string> GetProfileInfo( string strEmail)
		{
			RestServiceCall clsRestCall = new RestServiceCall ();
			ClsCommonFunctions objCommon = new ClsCommonFunctions ();
			UserlistClass objUserlistClass=new UserlistClass();
			string strResult=null;

			Queue<Uri> qAboutMeURI=new Queue<Uri>();
			string strImageNullCheck;
			string strProfilePic=string.Empty;
			string strCoverPic=string.Empty;
			string strCurrentCity=string.Empty;
			string strHomeTown=string.Empty;


			string  strAboutMeJSON=await clsRestCall.ProfileList ( strEmail); 

			if ( strAboutMeJSON == "Exception" )
			{
				strResult = strAboutMeJSON;
				return strResult;
			}
			//YouMeUsLogin
			if ( MainActivity.strTypeLogin == "YouMeUsLogin" )
			{
				objUserlistClass = JsonConvert.DeserializeObject<UserlistClass> ( strAboutMeJSON );
				if ( objUserlistClass.userlist == null )
				{
					return "DataNotFound";
				}
				else
				{
					foreach(var values in objUserlistClass.userlist)
					{
						strUserId=values.Idusers;
						strUserName = values.Fname;
						if(values.Typeofimage=="1")
						{
							strUserProfileImage = values.Modifiedimage;
						}
					}

				}
				if ( lstAboutMe != null )
				{
					lstAboutMe.Clear ();
				}

			}
			//GoogleLogin
			else if(MainActivity.strTypeLogin == "GoogleLogin")
			{
				var objAboutMe1 = JsonConvert.DeserializeObject<GoogleLoginClass> ( strAboutMeJSON );
				if ( objAboutMe1.result == null )
				{
					return "DataNotFound";
				} 
				else
				{
					foreach (var values in objAboutMe1.result)
					{
						strUserId = values.Idusers;
						strUserName = values.Fname;
						if (values.Typeofimage == "1")
						{
							strUserProfileImage = values.Modifiedimage;
						}

					}
				}
			}
			//FacebookLogin
			else if(MainActivity.strTypeLogin == "FacebookLogin")
			{
				var objAboutMe2 = JsonConvert.DeserializeObject<GoogleLoginClass> (strAboutMeJSON);
				if (objAboutMe2.result == null) 
				{
					return "DataNotFound";
				}
				else
				{
					foreach (var values in objAboutMe2.result)
					{
						strUserId = values.Idusers;
						strUserName = values.Fname;
						if (values.Typeofimage == "1")
						{
							strUserProfileImage = values.Modifiedimage;
						}
					}
				}
			}
			if(MainActivity.isInstalled)
			{
				//when first time app installed then "AppInstalled" value is stored at shared Preferences
				string strData = strUserId+"," + strUserName+"," + strEmail +","+ strUserProfileImage;
				var prefs = Application.Context.GetSharedPreferences("YouMeUs", FileCreationMode.Private);
				var prefEditor = prefs.Edit();
				prefEditor.PutString("InstallOrNotApp",strData);
				prefEditor.Commit();
				SplashScreenActivity.isLogout = true;
				MainActivity.isInstalled = false;

			}
			else
			{
			SplashScreenActivity.strUserId= strUserId;
				SplashScreenActivity.strUserName=strUserName;
				SplashScreenActivity.strEmail =strEmail;
				SplashScreenActivity.strUserProfileImage =strUserProfileImage;
				SplashScreenActivity.isLogout = false;
			}
			return strResult;
		}
	}


	#region "YouMeUs Login Class"
	public class Userlist
	{
		public string Idusers { get; set; }
		public string Fname { get; set; }
		public string Lname { get; set; }
		public string Sex { get; set; }
		public string Modifiedimage { get; set; }
		public string Typeofimage { get; set; }
		public object Typeflag { get; set; }
		public object CityName { get; set; }
	}

	public class UserlistClass
	{
		public string baseurl { get; set; }
		public List<Userlist> userlist { get; set; }
	}
	#endregion

	#region "Google Login/Facebook Login Class"
	public class Results
	{
		public string Idusers { get; set; }
		public string Fname { get; set; }
		public string Lname { get; set; }
		public string Sex { get; set; }
		public string Modifiedimage { get; set; }
		public string Typeofimage { get; set; }
		public string Typeflag { get; set; }
		public string CityName { get; set; }
	}

	public class GoogleLoginClass
	{
		public List<Results> result { get; set; }
	}
	#endregion
}

