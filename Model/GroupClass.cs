﻿using System;
using System.Collections.Generic;

namespace YouMeUsAndroid
{
	//public class GroupFollower
	//{
	//	public string Idgroups { get; set; }
	//	public string Fname { get; set; }
	//	public string Idusers { get; set; }
	//	public string Modifiedimage { get; set; }
	//	public string Isamember { get; set; }
	//	public string Closedgroupflag { get; set; }
	//}

	//public class Usersgroupstatu
	//{
	//	public string Idusergroups { get; set; }
	//	public string Idgroups { get; set; }
	//	public string Idusers { get; set; }
	//	public string Isamember { get; set; }
	//	public string Created_at { get; set; }
	//	public string Modified_at { get; set; }
	//	public string Closedgroupflag { get; set; }
	//	public string Isanadmin { get; set; }
	//	public string Notificationflag { get; set; }
	//}

	//public class Mygroup
	//{
	//	public string Idgroups { get; set; }
	//	public string Privacysetting { get; set; }
	//	public string Groupname { get; set; }
	//	public string Groupslogo { get; set; }
	//	public string usrcnt { get; set; }
	//	public string Isamember { get; set; }
	//	public string Groupdesc { get; set; }
	//	public string Urlkeyword { get; set; }
	//	public int owner { get; set; }
	//	public List<GroupFollower> GroupFollowers { get; set; }
	//	public string totalfollowers { get; set; }
	//	public List<Usersgroupstatu> usersgroupstatus { get; set; }
	//}


	//public class GroupDetailsClass
	//{
	//	public List<Mygroup> Mygroups { get; set; }
	//}
	public class GroupFollower
	{
		public string Idgroups { get; set; }
		public string Fname { get; set; }
		public string Idusers { get; set; }
		public string Modifiedimage { get; set; }
		public string Isamember { get; set; }
		public string Closedgroupflag { get; set; }
	}

	public class Usersgroupstatu
	{
		public string Idusergroups { get; set; }
		public string Idgroups { get; set; }
		public string Idusers { get; set; }
		public string Isamember { get; set; }
		public string Created_at { get; set; }
		public string Modified_at { get; set; }
		public string Closedgroupflag { get; set; }
		public string Isanadmin { get; set; }
		public string Notificationflag { get; set; }
	}

	public class Admin
	{
		public string Idusers { get; set; }
		public string Fname { get; set; }
		public string Lname { get; set; }
		public string Sex { get; set; }
		public string Modifiedimage { get; set; }
		public string Typeofimage { get; set; }
		public object Typeflag { get; set; }
		public object CityName { get; set; }
	}

	public class Userdetail
	{
		public string Idusers { get; set; }
		public string Fname { get; set; }
		public string Lname { get; set; }
		public string Sex { get; set; }
		public string Modifiedimage { get; set; }
		public string Typeofimage { get; set; }
		public string Typeflag { get; set; }
		public string CityName { get; set; }
	}

	public class Mygroup
	{
		public string Idgroups { get; set; }
		public string Created_by { get; set; }
		public string Privacysetting { get; set; }
		public string Groupname { get; set; }
		public string Groupslogo { get; set; }
		public string usrcnt { get; set; }
		public string Isamember { get; set; }
		public string Groupdesc { get; set; }
		public string Urlkeyword { get; set; }
		public int owner { get; set; }
		public List<GroupFollower> GroupFollowers { get; set; }
		public string totalfollowers { get; set; }
		public List<Usersgroupstatu> usersgroupstatus { get; set; }
		public List<Admin> admin { get; set; }
		public List<Userdetail> userdetails { get; set; }
	}

	public class GroupDetailsClass
	{
		public List<Mygroup> Mygroups { get; set; }
	}
	public class Array
	{
		public string Idusergroups { get; set; }
		public string Isamember { get; set; }
		public string Closedgroupflag { get; set; }
	}

	public class GroupAdding
	{
		public List<Array> array { get; set; }
		public string message { get; set; }
	}
}
