﻿using System;
using System.Collections.Generic;

namespace YouMeUsAndroid
{
public class Interest
{
	public string Idinterestmaster { get; set; }
	public string Interestname { get; set; }
	public string Interestdesc { get; set; }
	public string Interestlogo { get; set; }
	public List<object> Followers { get; set; }
}

public class InterestDetailSearchClass
{
	public List<Interest> Interests { get; set; }
}
}