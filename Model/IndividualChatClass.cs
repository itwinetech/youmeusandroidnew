﻿using System;
using System.Collections.Generic;

namespace YouMeUsAndroid
{

	//public class Messageindividual
	//{
	//	public string Iduserfrom { get; set; }
	//	public string Idchatmaster { get; set; }
	//	public string Iduserto { get; set; }
	//	public string Messagecontent { get; set; }
	//	public string Modifiedimage { get; set; }
	//	public string Fname { get; set; }
	//	public string Typeofmesasge { get; set; }
	//	public string filename { get; set; }
	//}

	//public class MsgedAt
	//{
	//	public string Iduserfrom { get; set; }
	//	public string Iduserto { get; set; }
	//	public string messaged_at { get; set; }
	//}

	//public class Indivisualchat
	//{
	//	public Messageindividual messageindividual { get; set; }
	//	public MsgedAt msged_at { get; set; }
	//}

	//public class Datauser
	//{
	//	public string Idusers { get; set; }
	//	public string Fname { get; set; }
	//	public string Lname { get; set; }
	//	public string Sex { get; set; }
	//	public string Modifiedimage { get; set; }
	//	public string Typeofimage { get; set; }
	//}

	//public class IndividualChatClass
	//{
	//	public List<Indivisualchat> Indivisualchat { get; set; }
	//	public Datauser datauser { get; set; }
	//}

	public class Messageindividual
	{
		public string Iduserfrom { get; set; }
		public string Idchatmaster { get; set; }
		public string Iduserto { get; set; }
		public string Messagecontent { get; set; }
		public string Modifiedimage { get; set; }
		public string Fname { get; set; }
		public string Typeofmesasge { get; set; }
		public string filename { get; set; }
	}

	public class MsgedAt
	{
		public string Iduserfrom { get; set; }
		public string Iduserto { get; set; }
		public string messaged_at { get; set; }
	}

	public class Indivisualchat
	{
		public Messageindividual messageindividual { get; set; }
		public MsgedAt msged_at { get; set; }
	}

	public class Datauser
	{
		public string Idusers { get; set; }
		public string Fname { get; set; }
		public string Lname { get; set; }
		public string Sex { get; set; }
		public string Modifiedimage { get; set; }
		public string Typeofimage { get; set; }
	}

	public class IndividualChatClass
	{
		public List<Indivisualchat> Indivisualchat { get; set; }
		public Datauser datauser { get; set; }
	}
}

