﻿using System;
using System.Collections.Generic;

namespace YouMeUsAndroid
{
//	public class EventsDetailsClass
//	{
		
		public class EventsNearbyLocation
		{
		public string Idevents { get; set; }
		public string Eventname { get; set; }
		public string Urlkeyword { get; set; }
		public string Eventdesc { get; set; }
		public string Eventlogo { get; set; }
		public string Address { get; set; }
		public string Eventcity { get; set; }
		public string dt { get; set; }
		public string date { get; set; }
		public string Eventdate { get; set; }
		public string Eventtime { get; set; }
		public string EventEndtime { get; set; }
		public string Ispaidevent { get; set; }
		public string Paymenturl { get; set; }
		public string Typeofevent { get; set; }
		public string usrcount { get; set; }
		public string Status { get; set; }
		public string UrlEventslogo { get; set; }
		public string IdgroupInterests { get; set; }
		}

		public class EventLocation
		{
			public List<EventsNearbyLocation> EventsNearbyLocation { get; set; }
		}

	public class EventsAttending
	{
		public int attending { get; set; }
	}
//	}
}

