﻿using System;
using System.Collections.Generic;

namespace YouMeUsAndroid
{
	public class Chatmemberlist
	{
		public string Idusers { get; set; }
		public string Isonline { get; set; }
		public string Iduserfrom { get; set; }
		public string Idchatmaster { get; set; }
		public string Typeofmesasge { get; set; }
		public string Iduserto { get; set; }
		public string Fname { get; set; }
		public string Messagecontent { get; set; }
		public string tott { get; set; }
		public string Seen_at { get; set; }
		public string Modifiedimage { get; set; }
		public string Typeofmessage { get; set; }
		public int typeofflag { get; set; }
		public string Idgroupchat { get; set; }
		public string Chatname { get; set; }
		public string Created_at { get; set; }
		public string Status { get; set; }
	}

	public class ChatServiceClass
	{
		public List<Chatmemberlist> Chatmemberlist { get; set; }
	}
}
