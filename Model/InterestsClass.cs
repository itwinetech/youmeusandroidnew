﻿using System;
using System.Collections.Generic;

namespace YouMeUsAndroid
{
public class Follower
{
	public string Iduser { get; set; }
	public string Idinterestmaster { get; set; }
	public string Fname { get; set; }
	public string Modifiedimage { get; set; }
}
public class PopularInterest
{
	public string Idinterestmaster { get; set; }
	public string Interestname { get; set; }
	public string Interestdesc { get; set; }
	public string Interestlogo { get; set; }
	public string totalfollowers { get; set; }
	public List<Follower> Followers { get; set; }
	public string UrlInterestlogo { get; set; }
}

public class InterestsDetails
{
	public List<PopularInterest> PopularInterests { get; set; }
}
	public class InterestFollowing
	{
		public string Following { get; set; }
	}
}