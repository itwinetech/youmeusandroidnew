﻿using System;
namespace YouMeUsAndroid
{
	public class Help
	{
		public string Idcontentmanagement { get; set; }
		public string Contentitle { get; set; }
		public string Description { get; set; }
		public string Typeofcontent { get; set; }
		public string Status { get; set; }
		public string Upddate { get; set; }
		public string Upduser { get; set; }
		public string Header { get; set; }
		public object Imageurl { get; set; }
	}

	public class HelpDetails
	{
		public Help Help { get; set; }
	}

	//About Us
	public class AboutUs
	{
		public string Idcontentmanagement { get; set; }
		public string Contentitle { get; set; }
		public string Description { get; set; }
		public string Typeofcontent { get; set; }
		public string Status { get; set; }
		public string Upddate { get; set; }
		public string Upduser { get; set; }
		public string Header { get; set; }
		public object Imageurl { get; set; }
	}

	public class AboutUsDetails
	{
		public AboutUs AboutUs { get; set; }
	}

	public class ContanctYoumeus
	{
		public string Idcontentmanagement { get; set; }
		public string Contentitle { get; set; }
		public string Description { get; set; }
		public string Typeofcontent { get; set; }
		public string Status { get; set; }
		public string Upddate { get; set; }
		public string Upduser { get; set; }
		public string Header { get; set; }
		public object Imageurl { get; set; }
	}

	public class ContanctYoumeusDetails
	{
		public ContanctYoumeus ContanctYoumeus { get; set; }
	}

	public class TermsConditions
	{
		public string Idcontentmanagement { get; set; }
		public string Contentitle { get; set; }
		public string Description { get; set; }
		public string Typeofcontent { get; set; }
		public string Status { get; set; }
		public string Upddate { get; set; }
		public string Upduser { get; set; }
		public string Header { get; set; }
		public object Imageurl { get; set; }
	}

	public class TermsConditionsDetails
	{
		public TermsConditions TermsConditions { get; set; }
	}
}

