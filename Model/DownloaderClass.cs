﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using System.IO; 

namespace YouMeUsAndroid
{
	public class DownloaderClass
	{
		/// <summary>
		/// Async function, downloads logo from the server and saves on device's cache folder (iOS).
		/// </summary>
		string strResult;
		async internal Task<string> DownloadLogo(Queue<Uri> imageURL,string strFolderPath)
		{
			WebClient client = new WebClient ();
			try
			{
				if ( imageURL.Any () )
				{
					Uri uriPath = imageURL.Dequeue ();
					string strFileName = Path.GetFileName ( uriPath.ToString () );
					if(strFileName.Equals("Image_Not_Found.jpg"))
					{
						await DownloadLogo ( imageURL , strFolderPath );
					}
					else
					{
						if ( !File.Exists ( Path.Combine ( strFolderPath , strFileName ) ))
						{
							Console.WriteLine(" ----Please wait downloading count : "+imageURL.Count);
							Console.WriteLine(" ----Please wait downloading file---"+uriPath.ToString());
							bool doesImageExist = await RemoteFileExists ( uriPath );
							if ( doesImageExist )
							{
								await client.DownloadFileTaskAsync ( uriPath , Path.Combine ( strFolderPath , strFileName ) );
								if ( imageURL.Count == 0 )
								{
									strResult = "Downloaded";
									Console.WriteLine("Download process comlete");
								}
								else
								{
									await DownloadLogo ( imageURL , strFolderPath );
									Console.WriteLine("Single Image downloaded");
								}
							}
							else
							{
								await DownloadLogo ( imageURL , strFolderPath );
								Console.WriteLine("Image does not exists in remote");
							}
						}
						else
						{
							Console.WriteLine("File Exists in local and skipped");
							await DownloadLogo ( imageURL , strFolderPath );
						}
					}
				}
			}
			catch
			{
				strResult = "Exception";
			}
			finally
			{
				client.Dispose ();
				client = null;
			}
			return strResult;
		}
		//Check for existance of Image
		async Task<bool> RemoteFileExists(Uri filePath)
		{
			WebClient client = new WebClient ();
			try
			{
				await client.DownloadStringTaskAsync(filePath);
				return true;
			}
			catch
			{
				return false;
			}
			finally
			{
				client.Dispose ();
				client = null;
			}
		}

		//Download with Local File name
		async internal Task<bool> DownloadPicsWithFullLocalPath(Uri uriPicPath,string strLocathPath)
		{
			WebClient client = new WebClient ();
			string strFileName =System.IO.Path.GetFileName(uriPicPath.ToString());   
			try
			{
				if(strFileName.Equals("Image_Not_Found.jpg"))
				{
					return true;
				}
				bool doesImageExist = await RemoteFileExists ( uriPicPath );
				if ( doesImageExist )
				{ 
					await client.DownloadFileTaskAsync ( uriPicPath ,Path.Combine(strLocathPath,strFileName)  );
					return true;
				} 
				else
				{
				return false;
				}
			}
			catch(Exception e)
			{
				Console.WriteLine ("! exception : "+ e.Message );
				return false;
			}
		}
		//Download without local filename
		async internal Task<bool> DownloadPicsToLocalFile(Uri uriPicPath,string strLocathPath)
		{
			WebClient client = new WebClient ();
			string strFileName = Path.GetFileName ( uriPicPath.ToString () );
			try
			{
				if(strFileName.Equals("Image_Not_Found.jpg"))
				{
					return false;
				}
				bool doesImageExist = await RemoteFileExists ( uriPicPath );
				if ( doesImageExist )
				{ 
					await client.DownloadFileTaskAsync ( uriPicPath , strLocathPath  );
					return true;
				}
				return false;
			}
			catch(Exception e)
			{
				Console.WriteLine ("! exception : "+ e.Message );
				return false;
			}
		}
	}

	public class DownloadImageHelper
	{
		WebClient client;
		async internal Task<string> DownloadLogos(Queue<Uri> imageURL,string strFolderPath)
		{
			try
			{
				client =new WebClient ();
				if ( imageURL.Any () )
				{
					Uri uriPath = imageURL.Dequeue ();
					string strFileName = Path.GetFileName (uriPath.ToString ());
					string strFileSave = Path.Combine (strFolderPath, strFileName);

					if ( File.Exists (strFileSave) )
					{
						Console.WriteLine("file exists in SD card");
						await DownloadLogos (imageURL, strFolderPath);
					}
					else
					{
						bool isFileExists = await DoesFileExists (uriPath);
						if ( isFileExists )
						{

							await client.DownloadFileTaskAsync (uriPath, strFileSave);
							if ( imageURL.Count > 0 )
							{
								await DownloadLogos (imageURL, strFolderPath);
							}
						}
						else
							await DownloadLogos (imageURL, strFolderPath);
					}
				}
			}
			catch
			{
				Console.WriteLine ("inside catch");
			}
			finally
			{
				if ( client != null )
				{
					client.Dispose ();
					client = null;
				}
			}
			return "Success";
		}

		async Task<bool> DoesFileExists(Uri uriFile)
		{
			try
			{
				client=client??new WebClient();
				await client.DownloadStringTaskAsync(uriFile);
				return true;
			}
			catch
			{
				Console.WriteLine ("file does not exists");
				return false;
			}
		}

	}
}