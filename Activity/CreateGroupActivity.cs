﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Text;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using RestSharp;

namespace YouMeUsAndroid
{
	[Activity(WindowSoftInputMode = SoftInput.StateAlwaysHidden, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
	public class CreateGroupActivity : Activity
	{
		Java.IO.File fileImagePath;
		string strAbsolutePath;
		string strProductCameraImagePath;
		Button btnCamera;
		ImageView imgLogo;
		string strPrivacy="2";
		string strActive = "0";
		string strtxtInterestTagName = "";
		string strtxtInterestTagvalueId = "";
		string strFileDataCoverImage;
		string strFileDataResizedImage;
		string strFileDataThumbnailImage;
		List<Taginterest> lstInterestList = new List<Taginterest>();
		List<Taginterest> lst = new List<Taginterest>();
		EditText txtGroupName;
		EditText txtDescription;
		ImageView imgGroup;
		Button btnChooseImage;
		TextView lblImageStatus;
		MultiAutoCompleteTextView txtInterestTag;
		RadioButton rdOpenGroup;
		RadioButton rdClosedGroup;
		CheckBox chkActive;
		Button btnSaveGroup;
		List<string> lstvalue1 = new List<string>();
		ClsCommonFunctions objClsCommonFunctions;
		RestServiceCall objRestServiceCall;
		string[] strName = null;
		string strImagePath = "";
		string strImageName ="";
		byte[] coverphoto;
		byte[] resizedPhoto;
		byte[] thumbnailPhoto;
		int imageWidth;
		int imageHeight;
		Uri uri;
		ProgressDialog progress;
		protected override void OnCreate(Bundle savedInstanceState)
		{
			RequestWindowFeature(WindowFeatures.NoTitle);
			base.OnCreate(savedInstanceState);

			// Create your application here
			SetContentView(Resource.Layout.CreateGroupLayout);
			FnInitilization();
			FnClickEvents();
		}

		#region "Initilization"
		 void FnInitilization()
		{
			imgLogo = FindViewById<ImageView>(Resource.Id.imgLogoCGL);
			txtGroupName = FindViewById<EditText>(Resource.Id.txtGroupNameCGL);
			txtDescription = FindViewById<EditText>(Resource.Id.txtDescriptionCGL);
			imgGroup = FindViewById<ImageView>(Resource.Id.imgGroupProfileCGL);
			btnChooseImage = FindViewById<Button>(Resource.Id.btnChooseFileCGL);
			lblImageStatus = FindViewById<TextView>(Resource.Id.lblChoosenFileCGL);
			txtInterestTag = FindViewById<MultiAutoCompleteTextView>(Resource.Id.txtTagInterestCGL);
			rdOpenGroup = FindViewById<RadioButton>(Resource.Id.radio_red);
			rdClosedGroup= FindViewById<RadioButton>(Resource.Id.radio_blue);
			btnSaveGroup= FindViewById<Button>(Resource.Id.btnSaveGroupCGL);
			chkActive = FindViewById<CheckBox>(Resource.Id.chkActiveCGL);
			btnCamera=FindViewById<Button>(Resource.Id.btnCameraCGL);
			objClsCommonFunctions = objClsCommonFunctions ?? new ClsCommonFunctions();
			objRestServiceCall = objRestServiceCall ?? new RestServiceCall();

			 FnSearchLocation();
		}
		#endregion

		#region "Click Events"
		void FnClickEvents()
		{
			imgLogo.Click += delegate
			{
				FinishAffinity();
				HomeActivity.isHomeShows = false;
				HomeActivity.isHosmeFeedorPost = 1;
				Intent activity = new Intent(this, typeof(HomeActivity));
				activity.PutExtra("MyData", "0");
				StartActivity(activity);
			};
			btnChooseImage.Click += delegate
			{
				var imageIntent = new Intent();
				imageIntent.SetType("image/*");
				imageIntent.SetAction(Intent.ActionGetContent);
				StartActivityForResult(Intent.CreateChooser(imageIntent, "Select Image"), 0);
			};
			btnCamera.Click += delegate
			{
				if (IsThereAnAppToTakePictures())
				{
					CreateDirectoryForPictures();
				}
				TakeAPicture();

			};
			txtInterestTag.TextChanged += delegate
		   {
			   List<string> lstvalue = new List<string>();

			   int index = 0;
			   if (string.IsNullOrEmpty(txtInterestTag.Text))
			   {
				   strtxtInterestTagName = "";
				   strtxtInterestTagvalueId = "";
			   }
			   else
			   {
				   string strTextVAlue = txtInterestTag.Text;
					string lastCharacter =strTextVAlue.Split(',').Last();
				  // string lastCharacter = strTextVAlue.Substring(strTextVAlue.Length - 1);
					if (lastCharacter ==" ")
				   {

				   }
				   else
				   {
					   lst = lstInterestList.Where(x => x.name.ToLower().Contains(lastCharacter.ToLower())).ToList();
					   strName = new string[lst.Count];
					   foreach (var description in lst)
					   {
						   strName[index] = description.id + " " + description.name;
						   index++;
					   }
				   }
				   if (strName != null && strName.Length > 0)
				   {
					   CreateEventsActivity.strActivityName = "CreateGroupActivity";
					   txtInterestTag.Adapter = null;
					   TagInterestAutoCompleteListAdapter adapter = new TagInterestAutoCompleteListAdapter(this, lst);
					   adapter.OriginalItems = strName;
					   txtInterestTag.Adapter = adapter;

					   txtInterestTag.SetTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
						adapter.ItemClick -= FnTxtSourceItemClick;
					   adapter.ItemClick += FnTxtSourceItemClick;

				   }
					else
					{
						CreateEventsActivity.strActivityName = "CreateGroupActivity";
					   txtInterestTag.Adapter = null;
					   TagInterestAutoCompleteListAdapter adapter = new TagInterestAutoCompleteListAdapter(this, lst);
					   adapter.OriginalItems = strName;
					   txtInterestTag.Adapter = adapter;

					   txtInterestTag.SetTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
						adapter.ItemClick -= FnTxtSourceItemClick;
					   adapter.ItemClick += FnTxtSourceItemClick;
					}
			   }
		   };
			rdOpenGroup.Click +=RadioButtonClicked;
			rdClosedGroup.Click +=RadioButtonClicked;
			chkActive.Click += delegate
			{
				if(chkActive.Checked)
				{
					strActive = "1";
				}
				else
				{
					strActive = "0";
				}
			};

			btnSaveGroup.Click +=async delegate
			{
				progress = ProgressDialog.Show(this, "", GetString(Resource.String.please_wait));
				if (string.IsNullOrEmpty(txtGroupName.Text) || string.IsNullOrEmpty(txtDescription.Text))
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strFillValidationError, this);
				}
				else
				{
					if (string.IsNullOrEmpty(strImagePath))
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Image is not selected", this);
					}
					else
					{
						bool isConnected = objClsCommonFunctions.FnIsConnected(this);
						if (!isConnected)
						{
							objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
						}
						else
						{
							
							await Task.Run(() => FnImageResizing());
							await FnCreateNewGroupPostMethod();
						}
					}
				}

				FnDismissProgress();
			};
		}
		#endregion

		#region "Radio button click event"
	   void RadioButtonClicked(object sender, EventArgs e)
		{
			RadioButton rd = (RadioButton)sender;
			bool isChecked = rd.Checked;
			if (isChecked)
			{
				switch (rd.Id)
				{
					case Resource.Id.radio_red:
						rdClosedGroup.Checked = false;
						strPrivacy = "2";//open group

						break;
					case Resource.Id.radio_blue:
						rdOpenGroup.Checked = false;
						strPrivacy = "1";//closed group

						break;
				}
			}
		}
		#endregion

		#region "AutoCompleteView rowclick"
		void FnTxtSourceItemClick(Taginterest obj)
		{
			if(strtxtInterestTagName ==""||strtxtInterestTagvalueId=="")
			{
				strtxtInterestTagName = obj.name;
				strtxtInterestTagvalueId = obj.id;
			}
			else
			{
				strtxtInterestTagName=strtxtInterestTagName+","+obj.name;
				strtxtInterestTagvalueId =strtxtInterestTagvalueId+","+ obj.id;
			}
			txtInterestTag.Text = strtxtInterestTagName.Trim()+",";
			//txtInterestTag.Append(strtxtInterestTagName);
			txtInterestTag.SetSelection(txtInterestTag.Length());
		}
		#endregion

		#region "Search TAg interests"
		async void FnSearchLocation()
		{
			string strFullInterestsResults = await objRestServiceCall.CallTagInterestSearchService();
		if (strFullInterestsResults == "Exception" || string.IsNullOrEmpty(strFullInterestsResults))
		{
			//objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this.Activity);

		}
			try
			{
				var objInterestSearch = JsonConvert.DeserializeObject<TagInterestClass>(strFullInterestsResults);
				lstInterestList = objInterestSearch.taginterests;
			}
			catch(Exception e)
			{

			}
		}
		#endregion

		#region "Gallery"

		#region "Gallery image that image show in imageView"
		protected override async void OnActivityResult(int requestCode, Android.App.Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);
			if (resultCode == Android.App.Result.Ok && requestCode == 0)//gallery
			{
				strImagePath = GetPathToImage(data.Data);
				//check image size
				BitmapFactory.Options option = await objClsCommonFunctions.GetBitmapOptionsOfImage(strImagePath);
				imageWidth = option.OutWidth;
				imageHeight = option.OutHeight;
				if (imageWidth >= 1143 && imageHeight >= 329)
					
				{
					imgGroup.SetImageURI(data.Data);
					lblImageStatus.Text = "Image Selected";

				}
				else
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Image size is small", this);
					lblImageStatus.Text = "No Image Choosen";
				}
			}
			else if (resultCode == Android.App.Result.Ok && requestCode == 1)
			{
				Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
				Android.Net.Uri contentUri = Android.Net.Uri.FromFile(fileImagePath);
				strImagePath = fileImagePath.AbsolutePath;
				BitmapFactory.Options option = await objClsCommonFunctions.GetBitmapOptionsOfImage(strImagePath);
				imageWidth = option.OutWidth;
				imageHeight = option.OutHeight;
				if (imageWidth >= 1143 && imageHeight >= 329)
				{
					var bitmap = BitmapFactory.DecodeFile(strImagePath);
					imgGroup.SetImageBitmap(bitmap);
					lblImageStatus.Text = "Image Selected";
				}
				else
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Image size is small", this);
					lblImageStatus.Text = "No Image Choosen";
				}

			}
			else if (resultCode == Android.App.Result.Canceled)
			{

			}

		}
		#endregion

		#region "Get image absolute path"
		//finding image absolute path
		string GetPathToImage(Android.Net.Uri uri)
		{
			string doc_id = "";
			using (var c1 = ContentResolver.Query(uri, null, null, null, null))
			{
				c1.MoveToFirst();
				String document_id = c1.GetString(0);
				doc_id = document_id.Substring(document_id.LastIndexOf(":") + 1);
			}

			string path = null;

			// The projection contains the columns we want to return in our query.
			string selection = Android.Provider.MediaStore.Images.Media.InterfaceConsts.Id + " =? ";
			using (var cursor = ContentResolver.Query(Android.Provider.MediaStore.Images.Media.ExternalContentUri, null, selection, new string[] { doc_id }, null))
			{
				if (cursor == null) return path;
				var columnIndex = cursor.GetColumnIndexOrThrow(Android.Provider.MediaStore.Images.Media.InterfaceConsts.Data);
				cursor.MoveToFirst();
				path = cursor.GetString(columnIndex);
			}
			return path;
		}
		#endregion


		#region "ImageResizing"
		async void FnImageResizing()
		{
			
			strImageName = strImagePath.Substring(strImagePath.LastIndexOf("/", StringComparison.Ordinal) + 1);
			if (imageWidth >= 1143 && imageHeight >= 329)
			{
				//cover image
				coverphoto =await objClsCommonFunctions.FnConvertByteArray(strImagePath);

			}
			else
			{
				FnDismissProgress();
			}

		}
		#endregion


		#endregion

		#region "CreateNewGroupPostMethod"
		async Task FnCreateNewGroupPostMethod()
		{
			
			string strCreateNewGroupAPI = "";
			//Create New group
			//++++++++++++++  production url     +++++++++++++++++//
			strCreateNewGroupAPI = "http://www.youmeus.com";
			//++++++++++++++ test url      +++++++++++++++++//
			// strCreateNewGroupAPI = "http://202.83.19.104/youmeuslocalserver";
			uri = uri ?? new Uri(strCreateNewGroupAPI);
			RestClient _client = new RestClient(uri);

			bool isConnected = objClsCommonFunctions.FnIsConnected(this);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
			}
			else
			{
				try
				{
					var request = new RestRequest("helloservice/createnewgroup", Method.POST) { RequestFormat = DataFormat.Json };
					request.AddFile("file", coverphoto, strImageName);

					request.AlwaysMultipartFormData = true;

					//Add one of these for each form boundary you need
					request.AddParameter("Group", txtGroupName.Text, ParameterType.GetOrPost);
					request.AddParameter("GroupDescription", txtDescription.Text, ParameterType.GetOrPost);
					request.AddParameter("Interest", strtxtInterestTagvalueId, ParameterType.GetOrPost);
					request.AddParameter("Privacy", strPrivacy, ParameterType.GetOrPost);
					request.AddParameter("Active", strActive, ParameterType.GetOrPost);
					request.AddParameter("Iduser", SplashScreenActivity.strUserId, ParameterType.GetOrPost);

					var response = await Task.Run(() => _client.Execute(request));
					if (response.StatusCode.ToString() == "OK" && response.ResponseStatus.ToString() == "Completed")
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Successfully Created Group ", this);
						FnClearTextFields();
						objClsCommonFunctions.FnDeleteDirectory(Constants.strFolderPath);
					}
					else
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Group Not Created", this);
						FnDismissProgress();
					}
				}
				catch (Exception e)
				{
					FnDismissProgress();
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Unable to connect to Server. Please try after sometime", this);
				}
			}

		}
		#endregion


		void FnClearTextFields()
		{
			txtGroupName.Text = string.Empty;
			txtDescription.Text = string.Empty;
			txtInterestTag.Text = string.Empty;
			imgGroup.SetImageResource(Resource.Drawable.noimages);
			chkActive.Checked = false;
			lblImageStatus.Text = "No Image Choosen";

		}

		#region "Dismiss Progress"
		void FnDismissProgress()
		{
			if (progress != null)
			{
				progress.Dismiss();
				progress = null;
			}

		}
		#endregion

		#region "Camara Click"
		private void CreateDirectoryForPictures()
		{
			string strCreated;
			bool iscreated = objClsCommonFunctions.FnCreateFolderInSDCard("YouMeUs", "Camera Images", out strCreated);
			if (iscreated)
			{
				strProductCameraImagePath = System.IO.Path.Combine(strCreated);
			}
		}

		bool IsThereAnAppToTakePictures()
		{
			Intent intent = new Intent(MediaStore.ActionImageCapture);
			IList<ResolveInfo> availableActivities = PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);
			return availableActivities != null && availableActivities.Count > 0;
		}

		void TakeAPicture()
		{
			Intent intent = new Intent(MediaStore.ActionImageCapture);
			string strGUID = Guid.NewGuid().ToString();
			strAbsolutePath = System.IO.Path.Combine(strProductCameraImagePath, String.Format("Camera_image{0}.jpg", strGUID));
			fileImagePath = new Java.IO.File(strProductCameraImagePath, String.Format("Camera_image{0}.jpg", strGUID));
			intent.PutExtra(MediaStore.ExtraOutput, Android.Net.Uri.FromFile(fileImagePath));
			StartActivityForResult(intent, 1);
		}
		#endregion
	}



}
//Note:
//strActive=> 0:not checked(off),1:checked(on){CheckBox}
