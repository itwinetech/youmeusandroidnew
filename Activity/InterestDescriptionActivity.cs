﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Square.Picasso;

namespace YouMeUsAndroid
{
	[Activity(WindowSoftInputMode = SoftInput.StateAlwaysHidden, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
	public class InterestDescriptionActivity : Activity
	{
		ImageView imgLogo;
		internal static Orgfeedspromotion InterestOrgfeed { get; set; }
		internal static PopularInterest InterestPopularInterest { get; set; }
		int countValue = 0;
		string strInterestId="0";
		string strLayoutName;
		string strName;
		RestServiceCall objRestServiceCall;
		//interest page
		ImageView imgDescriptionInterest;
		TextView lblDescriptionInterest;
		Button btnDescriptionFollow;
		TextView lblShowEventWithinInterest;
		TextView lblShowGroupWithinInterest;
		Refractored.Controls.CircleImageView imgUserImage1;
		Refractored.Controls.CircleImageView imgUserImage2;
		Refractored.Controls.CircleImageView imgUserImage3;
		ListView listViewEvents;
		ListView listViewGroups;

		//
		TextView lblInterestName;
		ListView listInterest;

		List<PopularInterest> lst = new List<PopularInterest>();
		List<PopularInterest> lstMyListInterests = new List<PopularInterest>();
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.InterestPopUpLayout);
			FnInitilization();
			FnFillValue();
			FnClickEvent();
		}
		public override void OnBackPressed()
		{
			HomeActivity.values = 2;
			base.OnBackPressed();
		}
		void FnInitilization()
		{
			imgLogo = FindViewById<ImageView>(Resource.Id.imgLogoIPL);
			imgDescriptionInterest = FindViewById<ImageView>(Resource.Id.imgMICHFL);
			lblDescriptionInterest = FindViewById<TextView>(Resource.Id.lblInterestNameMICHFL);
			btnDescriptionFollow = FindViewById<Button>(Resource.Id.btnFallowMICHFL);
			imgUserImage1 = FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.imgUserMICHFL1);
			imgUserImage2 = FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.imgUserMICHFL2);
			imgUserImage3 = FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.imgUserMICHFL3);
			lblShowEventWithinInterest = FindViewById<TextView>(Resource.Id.lblShowAllEventsMICHFL);
			lblShowGroupWithinInterest = FindViewById<TextView>(Resource.Id.lblShowAllGroupsMICHFL);
			listViewEvents = FindViewById<ListView>(Resource.Id.listViewShowAllEvents);
			listViewGroups = FindViewById<ListView>(Resource.Id.listViewShowAllGroups);
			objRestServiceCall = objRestServiceCall ?? new RestServiceCall();
			 strLayoutName = Intent.GetStringExtra("MyData") ?? "Data not available";
			if (strLayoutName == "HomeInterest")
			{
				strInterestId = InterestOrgfeed.id;
				strName = InterestOrgfeed.name;
			}
			else if (strLayoutName == "Interest")
			{
				strInterestId = InterestPopularInterest.Idinterestmaster;
				strName = InterestPopularInterest.Interestname;
			}



		}

		async void FnFillValue()
		{
			
			try
			{

				string strUserInterests = await objRestServiceCall.CallUserInterestsFetechService(SplashScreenActivity.strEmail);
				if (strUserInterests == "Exception" || string.IsNullOrEmpty(strUserInterests))
				{
					//objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this.Activity);
				}
				var objInterest = JsonConvert.DeserializeObject<InterestsDetails>(strUserInterests);
				if (objInterest.PopularInterests != null)
				{
					lst = objInterest.PopularInterests.Where(x => x.Idinterestmaster.Equals(strInterestId)).ToList();
					lstMyListInterests = objInterest.PopularInterests;
				}
			}
			catch
			{

			}

			if (lst.Count > 0)
			{
				btnDescriptionFollow.SetBackgroundResource(Resource.Drawable.correctfollowing);
			}
			else
			{
				btnDescriptionFollow.SetBackgroundResource(Resource.Drawable.addfollowing);
			}
			if (strLayoutName == "HomeInterest")//home feed
			{

				lblDescriptionInterest.Text = InterestOrgfeed.name;
				Picasso.With(this).Load(InterestOrgfeed.logo).Into(imgDescriptionInterest);
				if (InterestOrgfeed.intusers != null)
				{
					int count = InterestOrgfeed.intusers.Count;

					if (count == 1)
					{
						imgUserImage1.Visibility = ViewStates.Visible;
						imgUserImage2.Visibility = ViewStates.Gone;
						imgUserImage3.Visibility = ViewStates.Gone;
					}
					else if (count == 2)
					{
						imgUserImage1.Visibility = ViewStates.Visible;
						imgUserImage2.Visibility = ViewStates.Visible;
						imgUserImage3.Visibility = ViewStates.Gone;
					}
					else if (count == 3)
					{
						imgUserImage1.Visibility = ViewStates.Visible;
						imgUserImage2.Visibility = ViewStates.Visible;
						imgUserImage3.Visibility = ViewStates.Visible;

					}
					foreach (var values in InterestOrgfeed.intusers)
					{
						if (countValue == 0)
						{
							Picasso.With(this).Load(values.Modifiedimage).Into(imgUserImage1);
						}
						else if (countValue == 1)
						{
							Picasso.With(this).Load(values.Modifiedimage).Into(imgUserImage2);
						}
						else if (countValue == 2)
						{
							Picasso.With(this).Load(values.Modifiedimage).Into(imgUserImage3);
						}
						countValue++;
					}
				}
				else
				{
					imgUserImage1.Visibility = ViewStates.Invisible;
					imgUserImage2.Visibility = ViewStates.Invisible;
					imgUserImage3.Visibility = ViewStates.Invisible;
				}
			}
			else if (strLayoutName == "Interest")//interest
			{
				lblDescriptionInterest.Text = InterestPopularInterest.Interestname;
				if (SlidingTabActivity.isSelectedTitle == 1)
				{
					Picasso.With(this).Load(InterestPopularInterest.Interestlogo).Into(imgDescriptionInterest);
				}
				else if (SlidingTabActivity.isSelectedTitle == 2)
				{
					Picasso.With(this).Load(InterestPopularInterest.UrlInterestlogo).Into(imgDescriptionInterest);
				}
				if (InterestPopularInterest.Followers != null)
				{
					int count = InterestPopularInterest.Followers.Count;

					if (count == 1)
					{
						imgUserImage1.Visibility = ViewStates.Visible;
						imgUserImage2.Visibility = ViewStates.Gone;
						imgUserImage3.Visibility = ViewStates.Gone;
					}
					else if (count == 2)
					{
						imgUserImage1.Visibility = ViewStates.Visible;
						imgUserImage2.Visibility = ViewStates.Visible;
						imgUserImage3.Visibility = ViewStates.Gone;
					}
					else if (count == 3)
					{
						imgUserImage1.Visibility = ViewStates.Visible;
						imgUserImage2.Visibility = ViewStates.Visible;
						imgUserImage3.Visibility = ViewStates.Visible;

					}
					foreach (var values in InterestPopularInterest.Followers)
					{
						if (countValue == 0)
						{
							Picasso.With(this).Load(values.Modifiedimage).Into(imgUserImage1);
						}
						else if (countValue == 1)
						{
							Picasso.With(this).Load(values.Modifiedimage).Into(imgUserImage2);
						}
						else if (countValue == 2)
						{
							Picasso.With(this).Load(values.Modifiedimage).Into(imgUserImage3);
						}
						countValue++;
					}
				}
				else
				{
					imgUserImage1.Visibility = ViewStates.Invisible;
					imgUserImage2.Visibility = ViewStates.Invisible;
					imgUserImage3.Visibility = ViewStates.Invisible;
				}
			}
			FnGroupListBind();
			FnEventListBind();
		}
		void FnClickEvent()
		{
			imgLogo.Click += delegate
			{
				FinishAffinity();
				HomeActivity.isHomeShows = false;
				HomeActivity.isHosmeFeedorPost = 1;
				Intent activity = new Intent(this, typeof(HomeActivity));
				activity.PutExtra("MyData", "0");
				StartActivity(activity);
			};
			btnDescriptionFollow.Click +=async delegate
			{
				try
				{
					
					string strStoreInterests = await objRestServiceCall.CallInterestsStoreService(strInterestId, SplashScreenActivity.strEmail);
					var interest = JsonConvert.DeserializeObject<InterestFollow>(strStoreInterests);
					if (strStoreInterests == "Exception" || string.IsNullOrEmpty(strStoreInterests))
					{
							return;
					}
					if(interest.Following =="1")
						{
							btnDescriptionFollow.SetBackgroundResource(Resource.Drawable.correctfollowing);
						}
						else if (interest.Following == "2")
						{
							btnDescriptionFollow.SetBackgroundResource(Resource.Drawable.addfollowing);
						}
				}
				catch
				{
					
				}
			};

		}
		void FnEventListBind()
		{
			if (strLayoutName == "HomeInterest")//home feed
			{
				if (InterestOrgfeed.interestevents != null)
				{
					//listViewEvents.Adapter = null;
					//listViewEvents.Adapter=new
				}
				else
				{
					lblShowEventWithinInterest.Text = "No Events";
				}
			}
			else if (strLayoutName == "Interest")
			{
				lblShowEventWithinInterest.Text = "No Events";
			}
		}
		void FnGroupListBind()
		{
			if (strLayoutName == "HomeInterest")//home feed
			{

				if (InterestOrgfeed.interestgroups != null)
				{
					listViewGroups.Adapter = null;
					listViewGroups.Adapter = new InterestWithinGroupsAdapterClass(this, InterestOrgfeed.interestgroups);
				}
				else
				{
					lblShowGroupWithinInterest.Text = "No Groups";
				}
			}
			else if (strLayoutName == "Interest")
			{
				
					lblShowGroupWithinInterest.Text = "No Groups";

			}
		}
	}



	public class InterestFollow
	{
		public string Following { get; set; }
	}
}

