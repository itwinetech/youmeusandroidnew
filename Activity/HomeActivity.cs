﻿using Android.Views;
using Android.Widget;
using Android.OS;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using SupportFragment = Android.Support.V4.App.Fragment;
using Android.Support.V7.App;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;
using Android.Support.V4.Widget;
using System.IO;
using System.Linq;
using Android.Content;
using Android.Graphics;
using System.Net;
using System.Threading.Tasks;
using Android.Views.InputMethods;
using Android.App;
using System.Text.RegularExpressions;
using Square.Picasso;
using Android.Text;
using Android.Text.Method;
using Android.Support.V4.View;
using Android.Support.V4.App;
using com.refractored;
using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;
using String = Java.Lang.String;
using Android.Webkit;
using Android.Locations;
using Android.Runtime;

using Xamarin.Auth;
using System.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using Android;
using Android.Content.PM;
using Android.Support.V4.Content;

namespace YouMeUsAndroid
{
	[Activity (WindowSoftInputMode = SoftInput.StateAlwaysHidden,ScreenOrientation=Android.Content.PM.ScreenOrientation.Portrait )]
	//[Activity (Theme="@style/MyTheme",WindowSoftInputMode = SoftInput.StateAlwaysHidden, ConfigurationChanges=Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]

	public class HomeActivity : AppCompatActivity,IOnTabReselectedListener,ViewPager.IOnPageChangeListener,ILocationListener
	{
		public void OnPageScrollStateChanged ( int state )
		{
			throw new NotImplementedException ();
		}

		public void OnPageScrolled ( int position, float positionOffset, int positionOffsetPixels )
		{
			throw new NotImplementedException ();
		}

		public void OnPageSelected ( int position )
		{
			throw new NotImplementedException ();
		}

		#region IOnTabReselectedListener implementation

		public void OnTabReselected ( int position )
		{
			throw new NotImplementedException ();
		}

		#endregion
		internal static bool isEventScreen = true;
		internal static int valueOnceEditProfile = 0;
		internal static	bool isimgCoverClicked=false;
		internal static bool isHomeShows = true;
		LocationManager locationManager;
		internal static int values;
		static public ViewPager pager;
		internal static	PagerSlidingTabStrip tabs;
		RestServiceCall objRestServiceCall;
		ProgressDialog progressDialog;
		ClsCommonFunctions objClsCommonFunctions;
		internal static ImageView imgLogo;
		List<PopularInterest> lstInterestSearch;
		internal static bool isArrow = true;
		internal static bool isHome = false;
		bool isOpenPopUp=true;
		string strProductImageFolder;
		int countValue=0;
		internal	static int homevalue;
		bool issearch=true;
		bool isBrowserViewed=false;
		string strText = "MainActivityScreen";
		bool isFirstTime=false;
		string strInterestsFilePath;
		string strEventsFilePath;

		//internal static    bool isMyInterests;
		Refractored.Controls.CircleImageView imgProfile;
		ListView listViewMenu;

		//AutoCompleteListAdapter objMenuListAdapter;
		ImageView imgMenu;
		static public ImageView imgArrow;
		internal static	EditText txtSearch;
		Button btnLetsGo;
		internal static Button btnClearSearchText;

		internal static string strTextName = "LogOut";

		internal static int isHosmeFeedorPost=0;
		internal static RelativeLayout lnySearchLayout;

		//dropdown popup
		internal static	LinearLayout lnyDropDownPopUp;
		TextView lblHelp;
		TextView lblSettings;
		TextView lblAboutUs;
		TextView lblContactYouMeUs;
		TextView lblLogOut;
		TextView lblEditProfile;

		//Notification
		ImageView imgNotificationbellHL;
		TextView lblNotificationCount;
		ListView listNotification;
		LinearLayout lnyNotificationLayoutHL;

		Button btnContentCost;
		NotificationDetails objNotificationDetails;
		List<Notification> lstNotification;
		//NotificationAdapterClass objNotificationAdapterClass;

		//Notification Row Tapped 
		LinearLayout lnyNotificationShowLayoutHL;
		Refractored.Controls.CircleImageView imgCreaterProfile;
		TextView lblCreaterName;
		TextView lblContentMessageName;
		TextView lblContentDescription;
		TextView lblContentAddress;
		TextView lblContentDateTime;
		LinearLayout flContantFromNotificationHL;
		Refractored.Controls.CircleImageView imgCreaterProfileModified;
		TextView lblFromName;
		TextView lblToGroupName;
		ImageView imgPostedImageName;
		TextView lblPostedText;
		TextView lblLikesCount;
		TextView lblLikePost;
		Refractored.Controls.CircleImageView imgUserProfileImage;
		EditText txtCommentPostOnNotificationContent;
		Button btnCommentonContent;
		TextView lblPostYearOrMonthAgo;
		ListView listCommentContent;
		string strIdpost;
		string strUserImagePath;
		//likes class
		LikesDetails objLikesDetails;
		VideoView vdoViewPostedVideo;

		List<NotificationComment> lstNotificationComment;
		CommentOnNotificationAdapterClass objCommentOnNotificationAdapterClass;

		//Notification

		NotificationAdapterClass objNotificationAdapterClass;
		//Group Request Layout
		LinearLayout lnyGroupDetailsRequestLayoutHL;
		ImageView imgGroupImage;
		TextView lblGroupName;
		TextView lblGroupMembersTab;
		View viewLine1;
		TextView lblGroupRequestTab;
		View viewLine2;
		Refractored.Controls.CircleImageView imgGroupAdminProfile;
		TextView lblGroupAdminName;
		ListView ListGroupMembersName;

		List<GroupMember> lstGroupMember;
		GroupMembersAdapterClass objGroupMembersAdapterClass;
		RelativeLayout RlGroupMembers;
		RelativeLayout RlGroupRequests;
		ListView listGroupRequest;
		GroupMembersRequestAdapterClass objGroupMembersRequestAdapterClass;

		List<GroupInvitation> lstGroupInvitation;
		GroupMembersAndRequests objGroupMembersAndRequests;

		string strGroupId;
		string strVIDEO_ID;
		string strYoutubeViewId;

		WebView videoView;
		string strVideoUrl;
		//to display video width in webview
		int intDisplayWidth;
		int intDisplayHeight;


		//Help
		LinearLayout lnyHelpLayout;
		TextView lblText;


		//Contact YouMeUs
		TextView lblContactDetails;
		TextView lblContactEmailId;

		//Change Password
		RelativeLayout rlyChangePassword;
		EditText txtCurrentPassword;
		EditText txtNewPassword;
		EditText txtConfirmPassword;
		Button btnSave;

		//contact Youmeus
		RelativeLayout rlyContactLayout;

		//Aboutus 
		LinearLayout lnyAboutUs;
		TextView lblAboutUsDetails;



		WebClient objWebClient;
		GeoCodeJSONClass objGeoCodeJSONClass;
		string strLatLong;
		string strSearchLocationValue;
		string strDistanceData;
		string strValueName;
		string strEventCostUrl; //used in lnyNotificationShowLayoutHL for event Ticket info

		private InputMethodManager KeyBoard;




		protected async override void OnCreate (Bundle bundle)
		{
			base.OnCreate ( bundle );

			// Set our view from the "main" layout resource
			SetContentView ( Resource.Layout.HomeLayout );

			//Initialize GAService
			GAService.GetGASInstance().Initialize(this);
			//Trackpage
			GAService.GetGASInstance().Track_App_Page("Home Page");
			objClsCommonFunctions = objClsCommonFunctions ?? new ClsCommonFunctions();

			bool isTrue = await FnLocationManager();
				FnInitilization();
				FnClickEvents();
				
		}
		protected override void OnStart ()
		{
			base.OnStart ();
		}
		protected  override void OnResume()
		{
			base.OnResume();

		}

		public override void OnBackPressed ()
		{
			if (strTextName == "Help")
			{
				tabs.Visibility = ViewStates.Visible;
				lnyHelpLayout.Visibility = ViewStates.Gone;
				strTextName = "LogOut";
			}
			else if (strTextName == "Setting")
			{
				tabs.Visibility = ViewStates.Visible;
				rlyChangePassword.Visibility = ViewStates.Gone;
				strTextName = "LogOut";
			}
			else if (strTextName == "AboutUs")
			{
				tabs.Visibility = ViewStates.Visible;
				lnyAboutUs.Visibility = ViewStates.Gone;
				strTextName = "LogOut";
			}
			else if (strTextName == "ContactYoumeus")
			{
				tabs.Visibility = ViewStates.Visible;
				rlyContactLayout.Visibility = ViewStates.Gone;
				strTextName = "LogOut";
			}
			else if (strTextName == "Notifications")
			{
				videoView.OnPause();
				tabs.Visibility = ViewStates.Visible;
				lnyNotificationLayoutHL.Visibility = ViewStates.Gone;
				strTextName = "LogOut";
			}
			else if (strTextName == "TypeofContentEvent")
			{
				videoView.OnPause();
				tabs.Visibility = ViewStates.Visible;
				lnyNotificationShowLayoutHL.Visibility = ViewStates.Gone;
				strTextName = "LogOut";
			}
			else if (strTextName == "TypeofContentTextImage")
			{
				videoView.OnPause();
				tabs.Visibility = ViewStates.Visible;
				flContantFromNotificationHL.Visibility = ViewStates.Gone;
				strTextName = "LogOut";
			}
			else if (strTextName == "LogOut")
			{
				if (SplashScreenActivity.isLogout)
				{
					FinishAffinity();
				}
			}
		}

		#region "Initilization"
		async void FnInitilization()
		{
			imgProfile = FindViewById<Refractored.Controls.CircleImageView> ( Resource.Id.imgProfileHL );
			imgLogo=FindViewById<ImageView>(Resource.Id.imgLogo);
			pager=FindViewById<ViewPager>(Resource.Id.pager);
			tabs = FindViewById<PagerSlidingTabStrip>(Resource.Id.tabs);
			pager.Adapter = new MyPagerAdapter(SupportFragmentManager);
			tabs.SetViewPager(pager);
			btnClearSearchText = FindViewById<Button> ( Resource.Id.imgRemovetext );
			lnySearchLayout = FindViewById<RelativeLayout> ( Resource.Id.sd );
			imgArrow=FindViewById<ImageView> (Resource.Id.imageArrow);
			txtSearch = FindViewById<EditText> ( Resource.Id.txtSearchHL );
			btnLetsGo = FindViewById<Button> ( Resource.Id.btnLetsGo );

			lnyDropDownPopUp = FindViewById<LinearLayout> ( Resource.Id.lnyDropdownPopUpHL );
			lblHelp = FindViewById<TextView> ( Resource.Id.lblHelpHL );
			lblSettings = FindViewById<TextView> ( Resource.Id.lblSettingsHL );
			lblAboutUs = FindViewById<TextView> ( Resource.Id.lblAboutUsHL );
			lblContactYouMeUs = FindViewById<TextView> ( Resource.Id.lblContactYoumeusHL );
			lblLogOut = FindViewById<TextView> ( Resource.Id.lblLogOutHl );
			lblEditProfile = FindViewById<TextView>(Resource.Id.lblProfileHL);


			rlyChangePassword = FindViewById<RelativeLayout>(Resource.Id.rlyChangePassword);
			txtCurrentPassword=FindViewById<EditText>(Resource.Id.txtCurrentPassword);
			txtNewPassword = FindViewById<EditText>(Resource.Id.txtNewPassword);
			txtConfirmPassword = FindViewById<EditText>(Resource.Id.txtConfirmPassword);
			btnSave = FindViewById<Button>(Resource.Id.btnSave);

			lblText = FindViewById<TextView>(Resource.Id.lblTextView1HL);

			lblAboutUsDetails=FindViewById<TextView>(Resource.Id.lblAboutUsDetails);

			lblContactDetails = FindViewById<TextView>(Resource.Id.lblDetails);
			lblContactEmailId = FindViewById<TextView>(Resource.Id.lblMailid);

			rlyContactLayout = FindViewById<RelativeLayout>(Resource.Id.rlyContactLayout);
			lnyHelpLayout=FindViewById<LinearLayout>(Resource.Id.lnyHelpLayout);
			lnyAboutUs=FindViewById<LinearLayout>(Resource.Id.lnyAboutUsLayout);

			//Notification Initilization 
			imgNotificationbellHL = FindViewById<ImageView>(Resource.Id.imgNotificationbellHL);
			lblNotificationCount = FindViewById<TextView>(Resource.Id.lblNotificationCount);
			listNotification = FindViewById<ListView>(Resource.Id.listNotification);
			lnyNotificationLayoutHL = FindViewById<LinearLayout>(Resource.Id.lnyNotificationLayoutHL);
			objNotificationDetails = objNotificationDetails ?? new NotificationDetails();
			objLikesDetails = objLikesDetails ?? new LikesDetails();
			lstNotificationComment = lstNotificationComment ?? new List<NotificationComment>();
 
			lnyNotificationShowLayoutHL = FindViewById<LinearLayout>(Resource.Id.lnyNotificationShowLayoutHL);
			imgCreaterProfile = FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.imgCreaterProfile);
			lblCreaterName = FindViewById<TextView>(Resource.Id.lblCreaterName);
			lblContentMessageName = FindViewById<TextView>(Resource.Id.lblContentMessageName);
			lblContentDescription = FindViewById<TextView>(Resource.Id.lblContentDescription);
			lblContentAddress = FindViewById<TextView>(Resource.Id.lblContentAddress);
			lblContentDateTime = FindViewById<TextView>(Resource.Id.lblContentDateTime);
			
			flContantFromNotificationHL = FindViewById<LinearLayout>(Resource.Id.lnyContantFromNotificationHL);
			imgCreaterProfileModified = FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.imgCreaterProfileModified);
			lblFromName = FindViewById<TextView>(Resource.Id.lblFromName);
			lblToGroupName = FindViewById<TextView>(Resource.Id.lblToGroupName);
			imgPostedImageName = FindViewById<ImageView>(Resource.Id.imgPostedImageName);
			lblLikesCount = FindViewById<TextView>(Resource.Id.lblLikesCount);
			lblLikePost = FindViewById<TextView>(Resource.Id.lblLikePost);
			imgUserProfileImage = FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.imgUserProfileImage);
			txtCommentPostOnNotificationContent = FindViewById<EditText>(Resource.Id.txtCommentPostOnNotificationContent);
			btnCommentonContent = FindViewById<Button>(Resource.Id.btnCommentonContent);
			lblPostYearOrMonthAgo = FindViewById<TextView>(Resource.Id.lblPostYearOrMonthAgo);
			lblPostedText = FindViewById<TextView>(Resource.Id.lblPostedText);
			listCommentContent = FindViewById<ListView>(Resource.Id.commentListView);
			vdoViewPostedVideo = FindViewById<VideoView>(Resource.Id.vdoViewPostedVideo);
			btnContentCost = FindViewById < Button>(Resource.Id.btnContentCost);
			//Group Request
			lnyGroupDetailsRequestLayoutHL = FindViewById<LinearLayout>(Resource.Id.lnyGroupDetailsRequestLayoutHL);
			imgGroupImage = FindViewById<ImageView>(Resource.Id.imgGroupImage);
			lblGroupName = FindViewById<TextView>(Resource.Id.lblGroupName);
			lblGroupMembersTab = FindViewById<TextView>(Resource.Id.lblGroupMembersTab);
			viewLine1 = FindViewById<View>(Resource.Id.viewLine1);
			lblGroupRequestTab = FindViewById<TextView>(Resource.Id.lblGroupRequestTab);
			viewLine2 = FindViewById<View>(Resource.Id.viewLine2);
			imgGroupAdminProfile = FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.imgGroupAdminProfile);
			lblGroupAdminName = FindViewById<TextView>(Resource.Id.lblGroupAdminName);
			ListGroupMembersName = FindViewById<ListView>(Resource.Id.ListGroupMembersName);
			imgProfile.SetImageResource(Resource.Drawable.UserProfile);
			var preference = GetSharedPreferences("YouMeUs", FileCreationMode.Private);
			string strExistsApp = preference.GetString("InstallOrNotApp", string.Empty);
			if(!string.IsNullOrEmpty(strExistsApp))
			{
				bool isNetworkConnected = objClsCommonFunctions.FnIsConnected(this);
				if (isNetworkConnected)
				{
					await FnLocationManager();
				}
				string[] strHomeValues = Regex.Split(strExistsApp, ",");
				SplashScreenActivity.strUserId = strHomeValues[0];
				SplashScreenActivity.strUserName = strHomeValues[1];
				SplashScreenActivity.strEmail = strHomeValues[2];
				SplashScreenActivity.strUserProfileImage = strHomeValues[3];

			}
			else
			{
				
			}


			lstGroupMember = lstGroupMember ?? new List<GroupMember>();

			objGroupMembersAndRequests = objGroupMembersAndRequests ?? new GroupMembersAndRequests();
			RlGroupMembers = FindViewById<RelativeLayout>(Resource.Id.RlGroupMembers);
			RlGroupRequests = FindViewById<RelativeLayout>(Resource.Id.RlGroupRequests);
			listGroupRequest = FindViewById<ListView>(Resource.Id.ListGroupRequest);
			lstGroupInvitation = lstGroupInvitation ?? new List<GroupInvitation>();

			viewLine2.Visibility = ViewStates.Gone;


			videoView = FindViewById<WebView>(Resource.Id.videoView);
			if (string.IsNullOrEmpty(SplashScreenActivity.strUserProfileImage))
			{
				imgProfile.SetImageResource(Resource.Drawable.UserProfile);
			}
			else
			{
				Picasso.With(this).Load(SplashScreenActivity.strUserProfileImage).Into(imgProfile);
			}
			KeyBoard = (InputMethodManager)GetSystemService(Context.InputMethodService);
			objWebClient = objWebClient ?? new WebClient ();
			objGeoCodeJSONClass = objGeoCodeJSONClass ?? new GeoCodeJSONClass ();
			objRestServiceCall = objRestServiceCall ?? new RestServiceCall();

			lnyDropDownPopUp.Visibility = ViewStates.Gone;
			btnSave.SetTextColor(Color.White);
			lblNotificationCount.Visibility = ViewStates.Gone;

			btnClearSearchText.Visibility = ViewStates.Invisible;
			isHome = false;
			bool isConnected = objClsCommonFunctions.FnIsConnected(this);
			if (isConnected)
			{
				FnLoadNotifications();
			}
			isArrow = true;
			var strLayoutName = Intent.GetStringExtra("MyData") ?? "Data not available";
			if (strLayoutName == "0")
			{
				isHome = true;
				GoogleMapActivity.isSuccess = false;
				HomeActivity.pager.SetCurrentItem(0, true);
			}
			if (strLayoutName == "1")
			{
				GoogleMapActivity.isSuccess = false;
				HomeActivity.pager.SetCurrentItem(0, true);
			}
			if(strLayoutName=="2")
			{
				HomeActivity.pager.SetCurrentItem(2, true);
			}
			else if (strLayoutName == "4")
			{
				HomeActivity.pager.SetCurrentItem(4, true);
			}
			else if (strLayoutName == "5")
			{
				HomeActivity.pager.SetCurrentItem(5, true);
			}
			//await Task.Delay(3000);
		}
		#endregion

		#region "Click Events"
		void FnClickEvents()
		{
			imgProfile.Click += delegate(object sender , EventArgs e )
			{
				if(isOpenPopUp)
				{
					lnyDropDownPopUp.Visibility=ViewStates.Visible;
					isOpenPopUp=false;	
				}
				else
				{
					lnyDropDownPopUp.Visibility=ViewStates.Gone;
					isOpenPopUp=true;
				}
			};

			txtSearch.Click += delegate(object sender , EventArgs e )
			{
				txtSearch.Focusable = true;
				txtSearch.FocusableInTouchMode = true;
				if ( txtSearch.Text=="" )
				{
					
				}
				else 
				{
					KeyBoard.HideSoftInputFromWindow ( txtSearch.WindowToken , 0 );
				}
			};

			btnClearSearchText.Click += delegate(object sender , EventArgs e )
			{
				txtSearch.Text=string.Empty;
			};
			lblHelp.Click += delegate
			{
				bool isConnected = objClsCommonFunctions.FnIsConnected(this);
				if (!isConnected)
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
				}
				else
				{
					isOpenPopUp = true;
					lblText.Text = "";
					strTextName = "Help";
					tabs.Visibility = ViewStates.Gone;
					lnyHelpLayout.Visibility = ViewStates.Visible;
					rlyChangePassword.Visibility = ViewStates.Gone;
					lnyAboutUs.Visibility = ViewStates.Gone;
					rlyContactLayout.Visibility = ViewStates.Gone;
					lnyDropDownPopUp.Visibility = ViewStates.Gone;
					lnyNotificationShowLayoutHL.Visibility = ViewStates.Gone;
					flContantFromNotificationHL.Visibility = ViewStates.Gone;
					lnyGroupDetailsRequestLayoutHL.Visibility = ViewStates.Gone;
					lnyNotificationLayoutHL.Visibility = ViewStates.Gone; //added by Narendra
					KeyBoard.HideSoftInputFromWindow(txtSearch.WindowToken, 0);
					FnHelp();
				}
			};
			lblSettings.Click+=delegate 
			{
				isOpenPopUp = true;
				strTextName = "Setting";
				tabs.Visibility = ViewStates.Gone;
				rlyChangePassword.Visibility = ViewStates.Visible;
				lnyHelpLayout.Visibility = ViewStates.Gone;
				lnyAboutUs.Visibility = ViewStates.Gone;
				rlyContactLayout.Visibility = ViewStates.Gone;
				lnyDropDownPopUp.Visibility = ViewStates.Gone;
				lnyNotificationShowLayoutHL.Visibility = ViewStates.Gone;
				flContantFromNotificationHL.Visibility = ViewStates.Gone;
				lnyGroupDetailsRequestLayoutHL.Visibility = ViewStates.Gone;
				lnyNotificationLayoutHL.Visibility = ViewStates.Gone; //added by Narendra
				KeyBoard.HideSoftInputFromWindow(txtSearch.WindowToken, 0);
			};
			lblContactYouMeUs.Click += delegate
			{
				bool isConnected = objClsCommonFunctions.FnIsConnected(this);
				if (!isConnected)
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
				}
				else
				{
					isOpenPopUp = true;
					lblContactEmailId.Text = "";
					lblContactDetails.Text = "";
					strTextName = "ContactYoumeus";
					tabs.Visibility = ViewStates.Gone;
					rlyContactLayout.Visibility = ViewStates.Visible;
					lnyHelpLayout.Visibility = ViewStates.Gone;
					rlyChangePassword.Visibility = ViewStates.Gone;
					lnyAboutUs.Visibility = ViewStates.Gone;
					lnyDropDownPopUp.Visibility = ViewStates.Gone;
					lnyNotificationShowLayoutHL.Visibility = ViewStates.Gone;
					flContantFromNotificationHL.Visibility = ViewStates.Gone;
					lnyGroupDetailsRequestLayoutHL.Visibility = ViewStates.Gone;
					lnyNotificationLayoutHL.Visibility = ViewStates.Gone; //added by Narendra
					KeyBoard.HideSoftInputFromWindow(txtSearch.WindowToken, 0);
					FnContact();
				}
			};
			lblAboutUs.Click += delegate
			{
				bool isConnected = objClsCommonFunctions.FnIsConnected(this);
				if (!isConnected)
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
				}
				else
				{
					isOpenPopUp = true;
					lblAboutUsDetails.Text = "";
					strTextName = "AboutUs";
					tabs.Visibility = ViewStates.Gone;
					lnyAboutUs.Visibility = ViewStates.Visible;
					lnyHelpLayout.Visibility = ViewStates.Gone;
					rlyChangePassword.Visibility = ViewStates.Gone;
					rlyContactLayout.Visibility = ViewStates.Gone;
					lnyDropDownPopUp.Visibility = ViewStates.Gone;
					KeyBoard.HideSoftInputFromWindow(txtSearch.WindowToken, 0);
					lnyNotificationShowLayoutHL.Visibility = ViewStates.Gone;
					flContantFromNotificationHL.Visibility = ViewStates.Gone;
					lnyGroupDetailsRequestLayoutHL.Visibility = ViewStates.Gone;
					lnyNotificationLayoutHL.Visibility = ViewStates.Gone; //added by Narendra
					FnAboutUs();
				}
			};
			lblEditProfile.Click += async delegate
			{
				KeyBoard.HideSoftInputFromWindow(txtSearch.WindowToken, 0);
				bool isConnected = objClsCommonFunctions.FnIsConnected(this);
				if (!isConnected)
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
				}
				else {
					isOpenPopUp = true;
					string strRes = await objRestServiceCall.CallViewProfileService(SplashScreenActivity.strUserId,"1");
					if (strRes == "Exception" || string.IsNullOrEmpty(strRes))
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this);
						return;
					}
					try
					{
						ViewProfileClass viewProfileClass = JsonConvert.DeserializeObject<ViewProfileClass>(strRes);
						if (viewProfileClass.ViewProfile != null)
						{
							EditProfileActivity.viewProfile = viewProfileClass.ViewProfile;
							StartActivity(typeof(EditProfileActivity));
						}
						else 
						{

						}
					}
					catch(Exception e)
					{

					}
				}
				lnyDropDownPopUp.Visibility = ViewStates.Gone;
			};
			lblLogOut.Click+=delegate
			{
				isOpenPopUp = true;
				var prefs = Application.Context.GetSharedPreferences("YouMeUs", FileCreationMode.Private);
				var prefEditor = prefs.Edit();
				prefEditor.PutString("InstallOrNotApp", "");
				prefEditor.Commit();

				KeyBoard.HideSoftInputFromWindow(txtSearch.WindowToken, 0);
				strTextName = "LogOut";
				SplashScreenActivity.login = 0;
				SplashScreenActivity.isLogout = false;
				base.OnBackPressed();
				StartActivity(typeof(MainActivity));
				Finish();
				lnyDropDownPopUp.Visibility = ViewStates.Gone;
			};

			btnSave.Click += delegate
			  {
				  if (string.IsNullOrEmpty(txtCurrentPassword.Text) || string.IsNullOrEmpty(txtNewPassword.Text) || string.IsNullOrEmpty(txtConfirmPassword.Text))
				  {
					  objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strFillValidationError, this);
				  }
				  else
				  {
					  if (txtNewPassword.Text != txtConfirmPassword.Text)
					  {
						  objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strPasswordValidationError, this);
					  }
					  else
					  {
						  bool isTrue = objClsCommonFunctions.FnIsConnected(this);
						  if (!isTrue)
						  {
							objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
						  }
						  else
						  {
							  FnChangePassword(txtCurrentPassword.Text, txtNewPassword.Text);
						  }
					  }
				  }
			  };

			imgNotificationbellHL.Click += delegate
			 {

				 strTextName = "Notifications";

				 tabs.Visibility = ViewStates.Gone;
				 lnyNotificationLayoutHL.Visibility = ViewStates.Visible;
				 rlyContactLayout.Visibility = ViewStates.Gone;
				 lnyHelpLayout.Visibility = ViewStates.Gone;
				 rlyChangePassword.Visibility = ViewStates.Gone;
				 lnyAboutUs.Visibility = ViewStates.Gone;
				 lnyDropDownPopUp.Visibility = ViewStates.Gone;

				 lnyNotificationShowLayoutHL.Visibility = ViewStates.Gone;
				 flContantFromNotificationHL.Visibility = ViewStates.Gone;
				 lnyGroupDetailsRequestLayoutHL.Visibility = ViewStates.Gone;
				FnLoadNotifications();
			 };


			//Like and unlike on posts getting after notification
			lblLikePost.Click += delegate
			{
				FnLikeUnLikePost();
			};

			btnCommentonContent.Click += delegate
			{
				FnCommentOnContentPost();
			};

			lblGroupMembersTab.Click += delegate
			 {
				 RlGroupMembers.Visibility = ViewStates.Visible;
				 RlGroupRequests.Visibility = ViewStates.Gone;
				 viewLine1.Visibility = ViewStates.Visible;
				 viewLine2.Visibility = ViewStates.Gone;
				 FnLoadGroupMembersAndRequests();

			 };
			lblGroupRequestTab.Click += delegate
			{
				RlGroupRequests.Visibility = ViewStates.Visible;
				RlGroupMembers.Visibility = ViewStates.Gone;
				viewLine1.Visibility = ViewStates.Gone;
				viewLine2.Visibility = ViewStates.Visible;
			};

			btnContentCost.Click += delegate (object sender, EventArgs e)
			{
				
				var uri = Android.Net.Uri.Parse(strEventCostUrl);
				var intent = new Intent(Intent.ActionView, uri);
				StartActivity(intent);

			};
		}
		#endregion

		#region "Change Password"
		async void FnChangePassword(string strOldPassword,string strNewPassword)
		{
			progressDialog = ProgressDialog.Show(this, "", "Please wait...");
			string strJsonResponse = await objRestServiceCall.CallChangePasswordService(SplashScreenActivity.strUserId,strOldPassword, strNewPassword);
			if (strJsonResponse == "Exception" || string.IsNullOrEmpty(strJsonResponse))
			{
				FnProgressDialogClose();
			}
			else
			{
				try
				{
					var objChangePassword = JsonConvert.DeserializeObject<ChangePassword>(strJsonResponse);
					if(objChangePassword.changepwd=="1")//1:ChangePassword success ,2:old password did not match
					{
						FnProgressDialogClose();
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName,"Password succesfully changed", this);

					}
					else
					{
						FnProgressDialogClose();
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Old password did'not match", this);

					}
				}
				catch(Exception e)
				{
					FnProgressDialogClose();
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Whoops...Unable to process! try again later", this);

				}
			}
			FnProgressDialogClose();
		}
		#endregion


		#region "Help Function"
		async void FnHelp()
		{
			string strHelpResults = await objRestServiceCall.CallHelpService();
			if (strHelpResults == "Exception" || string.IsNullOrEmpty(strHelpResults))
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError,this);
			}
			try
			{
				var objHelpDetails = JsonConvert.DeserializeObject<HelpDetails>(strHelpResults);


				if (objHelpDetails.Help.Description != null)
				{
					lblText.SetText(Html.FromHtml(objHelpDetails.Help.Description), TextView.BufferType.Spannable);
				}
			}
			catch
			{

			}
		}
		#endregion
		#region "AboutUs description"
		async void FnAboutUs()
		{
			string strAboutUsResults = await objRestServiceCall.CallAboutUsService();
			if (strAboutUsResults == "Exception" || string.IsNullOrEmpty(strAboutUsResults))
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this);
			}
			try
			{
				var objAboutUsDetails = JsonConvert.DeserializeObject<AboutUsDetails>(strAboutUsResults);


				if (objAboutUsDetails.AboutUs.Description != null)
				{
					lblAboutUsDetails.SetText(Html.FromHtml(objAboutUsDetails.AboutUs.Description), TextView.BufferType.Spannable);

				}
			}
			catch
			{

			}
		}
		#endregion
		#region "Contact description"
		async void FnContact()
		{
			string strContactResults = await objRestServiceCall.CallContactService();
			if (strContactResults == "Exception" || string.IsNullOrEmpty(strContactResults))
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this);
			}
			try
			{
				var objContactDetails = JsonConvert.DeserializeObject<ContanctYoumeusDetails>(strContactResults);

				if (objContactDetails.ContanctYoumeus.Description != null)
				{
					
					lblContactDetails.SetText(Html.FromHtml(objContactDetails.ContanctYoumeus.Description), TextView.BufferType.Spannable);
					lblContactEmailId.Visibility = ViewStates.Gone;

				}
			}
			catch
			{

			}
		}
		#endregion


		#region "Stop ProgressDialog"
		void FnProgressDialogClose()
		{
			if (progressDialog != null)
			{
				progressDialog.Dismiss();
				progressDialog = null;
			}
		}
		#endregion




		#region Load Notification
		async void FnLoadNotifications()
		{
			List<Notification> lstNotifications = new List<Notification>();
			bool isConnected = objClsCommonFunctions.FnIsConnected(this);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
			}
			else
			{
				string strNotificationResults = await objRestServiceCall.CallFullNotificationService(SplashScreenActivity.strUserId);
				if (strNotificationResults == "Exception" || string.IsNullOrEmpty(strNotificationResults))
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this);
					return;
				}
				try
				{
					objNotificationDetails = JsonConvert.DeserializeObject<NotificationDetails>(strNotificationResults);
					if(objNotificationDetails.NotificationCount!=null)
					{
						lblNotificationCount.Visibility = ViewStates.Visible;
						lblNotificationCount.Text = objNotificationDetails.NotificationCount.ToString();
						lblNotificationCount.TextSize = 10.0f;
					}
					else
					{
						
					}
					objNotificationDetails.Notification.TrimExcess();
					lstNotification = objNotificationDetails.Notification;
					//foreach(var s in objNotificationDetails.Notification)
					//{
					//	if(s.Created_at!=null)
					//	{
					//		lstNotifications.Add(s);//objNotificationDetails.Notification;
					//	}
					//	lstNotification.AddRange(lstNotifications);
					//	lstNotifications.Clear();
					//}
					objNotificationAdapterClass = new NotificationAdapterClass(this, lstNotification);
					listNotification.Adapter = objNotificationAdapterClass;
					objNotificationAdapterClass.NotificationRowClick -= FnNotificationRowTapped;
					objNotificationAdapterClass.NotificationRowClick += FnNotificationRowTapped;
				}
				catch(Exception e)
				{
					
				}
			}
		}
		#endregion

		async void FnNotificationRowTapped(Notification objNotification)
		{
			string strIdGroupss;
			if (objNotification.NotificationPost != null)
			{
				strIdGroupss = objNotification.NotificationPost.Idgroups;
			}
			else //if (string.IsNullOrEmpty (strIdGroupss)) 
			{
				strIdGroupss = null;
			}

			//Updated Notification Count on RowTap
			bool isConnected = objClsCommonFunctions.FnIsConnected(this);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
			}
			else {
				string strNotificationNewCount = await objRestServiceCall.CallUpdatenotifycountService(SplashScreenActivity.strUserId, objNotification.typeofcontent, objNotification.typeid, strIdGroupss); //objNotification.NotificationPost.Idgroups
				if (strNotificationNewCount == "Exception" || string.IsNullOrEmpty(strNotificationNewCount))
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this);
					return;
				}
				try
				{
					NotificationNewCount objNotificationNewCount = new NotificationNewCount();
					objNotificationNewCount = JsonConvert.DeserializeObject<NotificationNewCount>(strNotificationNewCount);
					lblNotificationCount.Text = objNotificationNewCount.newcount.ToString();
				}
				catch(Exception e)
				{
					
				}
			}
			//

			if (objNotification.typeofcontent == "7" && objNotification.grouptype == "1") //Typeofcontent=7 is Group request accepted and GroupType=1 is Closed Group
			{
				lnyNotificationLayoutHL.Visibility = ViewStates.Gone;
				tabs.Visibility = ViewStates.Visible;
			}
			else if (objNotification.typeofcontent == "8" && objNotification.grouptype == "1")//Typeofcontent=8 is Group request rejected
			{
				lnyNotificationLayoutHL.Visibility = ViewStates.Gone;
				tabs.Visibility = ViewStates.Visible;
			}

			else if (objNotification.typeofcontent == Convert.ToString(2))
			{
				strTextName = "TypeofContentEvent";
				lnyNotificationShowLayoutHL.Visibility = ViewStates.Visible;
				lnyNotificationLayoutHL.Visibility = ViewStates.Gone;
				lnyGroupDetailsRequestLayoutHL.Visibility = ViewStates.Gone;
				flContantFromNotificationHL.Visibility = ViewStates.Gone;
				lblCreaterName.Text = objNotification.message;
				strEventCostUrl = objNotification.NotificatioEventFeeds.Paymenturl;
				Picasso.With(this).Load(objNotification.NotificatioEventFeeds.eventcreaterlogo).Into(imgCreaterProfile);
				lblContentMessageName.Text = objNotification.NotificatioEventFeeds.Eventname;
				lblContentDescription.Text = objNotification.NotificatioEventFeeds.Eventdesc;
				lblContentAddress.Text = objNotification.NotificatioEventFeeds.CityName;
				lblContentDateTime.Text = objNotification.NotificatioEventFeeds.Eventdate + " " + objNotification.NotificatioEventFeeds.Eventtime;
				if (objNotification.NotificatioEventFeeds.Ispaidevent == "1")
				{
					btnContentCost.SetBackgroundResource(Resource.Drawable.buttonTicketInfoimg);
				}
				else if((objNotification.NotificatioEventFeeds.Ispaidevent == "0"))
				{
					btnContentCost.SetBackgroundResource(Resource.Drawable.freeimage);
				}
 			}
			else if (objNotification.typeofcontent == Convert.ToString(1) || objNotification.typeofcontent == Convert.ToString(3) || objNotification.typeofcontent == Convert.ToString(4) || objNotification.typeofcontent == Convert.ToString(5))
			{
 				Console.Write(objNotification.typeofcontent);
				strTextName = "TypeofContentTextImage";
				flContantFromNotificationHL.Visibility = ViewStates.Visible;
				lnyNotificationShowLayoutHL.Visibility = ViewStates.Gone;
				lnyNotificationLayoutHL.Visibility = ViewStates.Gone;
				lnyGroupDetailsRequestLayoutHL.Visibility = ViewStates.Gone;
				if (!string.IsNullOrEmpty(objNotification.Modifiedimage))
				{
					Picasso.With(this).Load(objNotification.Modifiedimage).Into(imgCreaterProfileModified);
				}
				else
				{
					imgCreaterProfileModified.SetImageResource(Resource.Drawable.noimages);
				}
				lblFromName.Text = objNotification.NotificationPost.Fname;//    objNotification.toname;
				lblToGroupName.Text = objNotification.NotificationPost.Groupname;// objNotification.Groupname;

				lblPostedText.Visibility = ViewStates.Gone;
				imgPostedImageName.Visibility = ViewStates.Gone;
				vdoViewPostedVideo.Visibility = ViewStates.Gone;
				videoView.Visibility = ViewStates.Gone;
				//if (!string.IsNullOrEmpty(objNotification.NotificationPost.Postcontent))
				//{
				//	lblPostedText.Visibility = ViewStates.Visible;
				//	lblPostedText.Text = objNotification.NotificationPost.Postcontent;
				//}

				if (objNotification.NotificationPost.Typeofpost == "1")
				{
					if (!string.IsNullOrEmpty(objNotification.NotificationPost.Postcontent))
					{
						lblPostedText.Visibility = ViewStates.Visible;
						lblPostedText.Text = objNotification.NotificationPost.Postcontent;
					}
					if (!string.IsNullOrEmpty(objNotification.NotificationPost.url))
					{
						if (objNotification.NotificationPost.url.Contains("youtube"))
						{
							videoView.Visibility = ViewStates.Visible;
							strVideoUrl = objNotification.NotificationPost.url;
							var metrics = Resources.DisplayMetrics;
							//fix video screen height and width
							intDisplayWidth = metrics.WidthPixels + 200;
							intDisplayHeight = (FnConvertPixelsToDp(metrics.HeightPixels));// / (2));
							FnPlayInWebView();
						}
						else if (objNotification.NotificationPost.url.Contains(".MP4"))
						{
							vdoViewPostedVideo.Visibility = ViewStates.Visible;
							vdoViewPostedVideo.SetVideoURI(Android.Net.Uri.Parse(objNotification.NotificationPost.url));
							MediaController vidControl = new MediaController(this);
							vidControl.SetAnchorView(vdoViewPostedVideo);
							vdoViewPostedVideo.SetMediaController(vidControl);
							vdoViewPostedVideo.Start();
						}
					}


				}
				//TpyeOfImage =2 for GroupLogo (commented or Liked by someone)
				else if (objNotification.NotificationPost.Typeofpost == "2")
				{
					if (!string.IsNullOrEmpty(objNotification.NotificationPost.Postcontent))
					{
						lblPostedText.Visibility = ViewStates.Visible;
						lblPostedText.Text = objNotification.NotificationPost.Postcontent;
					}

					if (!string.IsNullOrEmpty(objNotification.NotificationPost.UrlGroupslogo))
					{
						imgPostedImageName.Visibility = ViewStates.Visible;
						if (!string.IsNullOrEmpty(objNotification.NotificationPost.UrlGroupslogo)) //objNotification.NotificationPost.image //objNotification.Modifiedimage
						{
							Picasso.With(this).Load(objNotification.NotificationPost.UrlGroupslogo).Into(imgPostedImageName);
						}
						else {
							imgPostedImageName.SetImageResource(Resource.Drawable.noimages);
						}
					}


				}

				if (!string.IsNullOrEmpty(objNotification.Userimgpath.Modifiedimage))
				{
					Picasso.With(this).Load(objNotification.Userimgpath.Modifiedimage).Into(imgUserProfileImage);
				}
				else {
					imgCreaterProfileModified.SetImageResource(Resource.Drawable.noimages);
				}

				//like n unlike button
				if (objNotification.userlikestatus == "1")
				{
					lblLikePost.Text = "UNLIKE";
				}
				else {
					lblLikePost.Text = "LIKE";
				}
				lblLikesCount.Text = objNotification.PostLikes.totallikes;
				lblPostYearOrMonthAgo.Text = objNotification.NotificationPost.Created_at;
				strIdpost = objNotification.NotificationPost.Idposts;
				strUserImagePath = objNotification.Userimgpath.Modifiedimage;
				//binding comment list
				if (objNotification.NotificationComment != null)
				{
					try
					{
						lstNotificationComment = objNotification.NotificationComment;
						objCommentOnNotificationAdapterClass = new CommentOnNotificationAdapterClass(this, lstNotificationComment);
						listCommentContent.Adapter = objCommentOnNotificationAdapterClass;

					}
					catch (Exception e)
					{
						
					}
				}
				else
				{
					listCommentContent.Adapter = null;
				}
			}
			else if (objNotification.typeofcontent == Convert.ToString(6) && objNotification.grouptype == "1")
			{
				strTextName = "TypeofContentGroupRequest";
				flContantFromNotificationHL.Visibility = ViewStates.Gone;
				lnyNotificationShowLayoutHL.Visibility = ViewStates.Gone;
				lnyNotificationLayoutHL.Visibility = ViewStates.Gone;
				lnyGroupDetailsRequestLayoutHL.Visibility = ViewStates.Visible;
				strGroupId = objNotification.GroupAdminDetails.Idgroups;
				//Go to Groups Requests
				if (!string.IsNullOrEmpty(objNotification.GroupAdminDetails.groupslogourl))
				{
					Picasso.With(this).Load(objNotification.GroupAdminDetails.groupslogourl).Into(imgGroupImage);
				}
				else
				{
					imgGroupImage.SetImageResource(Resource.Drawable.noimages);
				}
				lblGroupName.Text = objNotification.GroupAdminDetails.Groupname;
				if (!string.IsNullOrEmpty(objNotification.GroupAdminDetails.Modifiedimage))
				{
					Picasso.With(this).Load(objNotification.GroupAdminDetails.Modifiedimage).Into(imgGroupAdminProfile);
				}
				else {
					imgGroupAdminProfile.SetImageResource(Resource.Drawable.noimages);
				}

				lblGroupAdminName.Text = objNotification.GroupAdminDetails.GroupAdmin + " " + objNotification.GroupAdminDetails.Lname;
				if (objNotification.GroupMembers != null)
				{
					lstGroupMember = objNotification.GroupMembers;
					objGroupMembersAdapterClass = new GroupMembersAdapterClass(this, lstGroupMember);
					ListGroupMembersName.Adapter = objGroupMembersAdapterClass;
				}

				//binding Group invitations
				if (objNotification.GroupInvitation != null)
				{
					lstGroupInvitation = objNotification.GroupInvitation;
					objGroupMembersRequestAdapterClass = new GroupMembersRequestAdapterClass(this, lstGroupInvitation);

					objGroupMembersRequestAdapterClass.RejectGroupInvitationButtonClick -= FnRejectGroupInvitation;
					objGroupMembersRequestAdapterClass.RejectGroupInvitationButtonClick += FnRejectGroupInvitation;

					objGroupMembersRequestAdapterClass.AcceptGroupInvitationButtonClick -= FnAcceptGroupInvitation;
					objGroupMembersRequestAdapterClass.AcceptGroupInvitationButtonClick += FnAcceptGroupInvitation;

					listGroupRequest.Adapter = objGroupMembersRequestAdapterClass;
				}
			}
		}
		async void FnRejectGroupInvitation(GroupInvitation objGroupInvitation)
		{
			bool isConnected = objClsCommonFunctions.FnIsConnected(this);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
			}
			else {
				string strGroupRequestReject = await objRestServiceCall.CallGroupAcceptRejectAPI(objGroupInvitation.Idusers, objGroupInvitation.Idusergroups, "0", objGroupInvitation.Idgroups); //flag=0 reject
				if (strGroupRequestReject == "Exception" || string.IsNullOrEmpty(strGroupRequestReject))
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this);
					return;
				}
				try
				{

					GroupRequestApproval objGroupRequestApproval = new GroupRequestApproval();
					objGroupRequestApproval = JsonConvert.DeserializeObject<GroupRequestApproval>(strGroupRequestReject);
					if (objGroupRequestApproval.Message == "rejected")
					{
						FnLoadGroupMembersAndRequests();
					}
					else
					{
						//somthing went wrong
					}
				}
				catch(Exception e)
				{
					FnProgressDialogClose();
				}
			}
		}
		async void FnAcceptGroupInvitation(GroupInvitation objGroupInvitation)
		{
			bool isConnected = objClsCommonFunctions.FnIsConnected(this);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
			}
			else {
				string strGroupRequestApproval = await objRestServiceCall.CallGroupAcceptRejectAPI(objGroupInvitation.Idusers, objGroupInvitation.Idusergroups, "1", objGroupInvitation.Idgroups); //flag=1 accept
				if (strGroupRequestApproval == "Exception" || string.IsNullOrEmpty(strGroupRequestApproval))
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this);
					return;
				}
				try
				{

					GroupRequestApproval objGroupRequestApproval = new GroupRequestApproval();
					objGroupRequestApproval = JsonConvert.DeserializeObject<GroupRequestApproval>(strGroupRequestApproval);
					if (objGroupRequestApproval.Message == "Accepted")
					{
						FnLoadGroupMembersAndRequests();
					}
					else
					{
						//somthing went wrong
					}
				}
				catch(Exception e)
				{
					FnProgressDialogClose();
				}
			}
		}

		async void FnLoadGroupMembersAndRequests()
		{
			bool isConnected = objClsCommonFunctions.FnIsConnected(this);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
			}
			else
			{
				string strGroupMembersRequestReload = await objRestServiceCall.CallGroupMembersAndRequestsAPI(strGroupId);
				if (strGroupMembersRequestReload == "Exception" || string.IsNullOrEmpty(strGroupMembersRequestReload))
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this);
					return;
				}
				try
				{
					objGroupMembersAndRequests = JsonConvert.DeserializeObject<GroupMembersAndRequests>(strGroupMembersRequestReload);
					if (objGroupMembersAndRequests.GroupInvitation != null)
					{
						objGroupMembersAndRequests.GroupInvitation.TrimExcess();
						//lstRequestlist = objGroupMembersAndRequests.GroupInvitation;
						lstGroupInvitation = null;
						lstGroupInvitation = objGroupMembersAndRequests.GroupInvitation;
						objGroupMembersRequestAdapterClass = new GroupMembersRequestAdapterClass(this, lstGroupInvitation);
						listGroupRequest.Adapter = objGroupMembersRequestAdapterClass;
					}
					else
					{
						listGroupRequest.Adapter = null;
					}

					//Binding Group Members

					if (objGroupMembersAndRequests.GroupMembers != null)
					{
						lstGroupMember = null;
						objGroupMembersAndRequests.GroupMembers.TrimExcess();
						lstGroupMember = objGroupMembersAndRequests.GroupMembers;
						objGroupMembersAdapterClass = new GroupMembersAdapterClass(this, lstGroupMember);
						ListGroupMembersName.Adapter = objGroupMembersAdapterClass;
					}
					else
					{
						ListGroupMembersName.Adapter = null;
					}
				}
				catch (Exception e)
				{
					Console.WriteLine(e);
				}
			}
		}
		#region Like UnLike Post in Notifications
		async void FnLikeUnLikePost()
		{
			bool isConnected = objClsCommonFunctions.FnIsConnected(this);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
			}
			else
			{
				string strUserLikesResults = await objRestServiceCall.CallLikeUnLikeNotificationAPI(strIdpost, SplashScreenActivity.strUserId);
				if (strUserLikesResults == "Exception" || string.IsNullOrEmpty(strUserLikesResults))
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this);
					return;
				}
				try
				{
					objLikesDetails = JsonConvert.DeserializeObject<LikesDetails>(strUserLikesResults);
					lblLikesCount.Text = objLikesDetails.Likes.totallikes;
					if (objLikesDetails.Likes.UserStatus == "1")
					{
						lblLikePost.Text = "UNLIKE";
					}
					else 
					{
						lblLikePost.Text = "LIKE";
					}
				}
				catch(Exception e)
				{
					FnProgressDialogClose();
				}
			}
			FnProgressDialogClose();
		}
		#endregion
		#region Comment Post in Notifications content
		async void FnCommentOnContentPost()
		{
			bool isConnected = objClsCommonFunctions.FnIsConnected(this);

			if (string.IsNullOrEmpty(txtCommentPostOnNotificationContent.Text))
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Comment cannot be empty", this);
			}
			else if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
			}
			else
			{
				string strCommentResults = await objRestServiceCall.CallCommentOnNotificationContentAPI(SplashScreenActivity.strUserId, strIdpost, txtCommentPostOnNotificationContent.Text.Trim());
				if (strCommentResults == "Exception" || string.IsNullOrEmpty(strCommentResults))
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this);
					return;
				}
				try
				{
					

					lstNotificationComment.Add(new NotificationComment { Comment = txtCommentPostOnNotificationContent.Text.Trim(), Created_at = "JUST NOW", Fname =SplashScreenActivity.strUserName, Postuserlogo = strUserImagePath });
					txtCommentPostOnNotificationContent.Text = string.Empty;
				  	objCommentOnNotificationAdapterClass = new CommentOnNotificationAdapterClass(this, lstNotificationComment);
					listCommentContent.Adapter = objCommentOnNotificationAdapterClass;
				}
				catch
				{
					FnProgressDialogClose();
				}
			}
			FnProgressDialogClose();
		}
		#endregion

		#region "To show Youtube video in Webview"
		void FnPlayInWebView()
		{
			string id = FnGetVideoID(strVideoUrl);

			if (!string.IsNullOrEmpty(id))
			{
				strVideoUrl = string.Format("http://youtube.com/embed/{0}", id);
				//strVideoUrl = string.Format ("http://youtube.com/v/{0}", id); //using "v" insted of "embed" for full screen
			}
			else 
			{
				Toast.MakeText(this, "Video url is not in correct format", ToastLength.Long).Show();
				return;
			}

			string html = @"<html><body><iframe width=""videoWidth"" height=""videoHeight"" src=""strVideoUrl"" ></iframe> </body></html>"; //<html><body> </body></html>
			var myWebView = (WebView)FindViewById(Resource.Id.videoView);
			var settings = myWebView.Settings;
			settings.JavaScriptEnabled = true;
			settings.UseWideViewPort = true;
			settings.LoadWithOverviewMode = true;
			settings.JavaScriptCanOpenWindowsAutomatically = true;
			settings.DomStorageEnabled = true;
			settings.SetRenderPriority(WebSettings.RenderPriority.High);
			settings.BuiltInZoomControls = false;
			settings.JavaScriptCanOpenWindowsAutomatically = true;
			myWebView.SetWebChromeClient(new WebChromeClient());
			settings.AllowFileAccess = true;
			settings.AllowFileAccessFromFileURLs = true;
			settings.SetPluginState(WebSettings.PluginState.On);
			string strYouTubeURL = html.Replace("videoWidth", intDisplayWidth.ToString()).Replace("videoHeight", intDisplayHeight.ToString()).Replace("strVideoUrl", strVideoUrl);

			myWebView.LoadDataWithBaseURL(null, strYouTubeURL, "text/html", "UTF-8", null);

		}
		int FnConvertPixelsToDp(float pixelValue)
		{
			var dp = (int)((pixelValue) / Resources.DisplayMetrics.Density);
			return dp;
		}
		static string FnGetVideoID(string strVideoURL)
		{
			const string regExpPattern = @"youtu(?:\.be|be\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)";
			//for Vimeo: vimeo\.com/(?:.*#|.*/videos/)?([0-9]+)
			var regEx = new Regex(regExpPattern);
			var match = regEx.Match(strVideoURL);
			string strYUrl=match.Success ? match.Groups[0].Value : null;
			return match.Success ? match.Groups[1].Value : null;
		}

		#endregion


		public void Dispose()
		{
			throw new NotImplementedException();
		}


		#region "Location "

		public void OnProviderDisabled(string provider) { }

		public void OnProviderEnabled(string provider) { }

		public void OnStatusChanged(string provider, Availability status, Bundle extras) { }

		async Task<bool> FnGetLocation()
		{
			if (string.IsNullOrEmpty(SplashScreenActivity.strAddress))
			{
				Geocoder geocoder = new Geocoder(this);
				IList<Address> addressList = await geocoder.GetFromLocationAsync(Convert.ToDouble(SplashScreenActivity.strLatitude), Convert.ToDouble(SplashScreenActivity.strLangitude), 10);

				Address address = addressList.FirstOrDefault();
				if (address != null)
				{
					SplashScreenActivity.strAddress = address.Locality;
				}
				else
				{
					Toast.MakeText(this, "Unable to find current location", ToastLength.Long).Show();
				}
			}
			return true;
		}

		#endregion
		 
		#region "FnLocationManager"
		async Task<bool> FnLocationManager()
		{
			bool isSuccessLocation = false;
			bool isConnected = objClsCommonFunctions.FnIsConnected(this);
			if (isConnected)
			{
				locationManager = GetSystemService(Context.LocationService) as LocationManager;

				if (locationManager.AllProviders.Contains(LocationManager.NetworkProvider)
					&& locationManager.IsProviderEnabled(LocationManager.GpsProvider))
				{
					locationManager.RequestLocationUpdates(LocationManager.NetworkProvider, 100, 1, this);
					if (SplashScreenActivity.strLatitude != null & SplashScreenActivity.strLangitude != null)
					{
						
						isSuccessLocation = true;
					}
					else
					{
						isSuccessLocation = false;
					}
				}
				else
				{
					SplashScreenActivity.strSetting = "Location";
					FnAlertMsgTwoInput("YouMeUs", Constants.strGPSDisableErrorMsg, "Ok", "Cancel", this);
					isSuccessLocation = false;
				}
			}
			else
			{
				SplashScreenActivity.strSetting = "Internet";
				FnAlertMsgTwoInput("YouMeUs", Constants.strNoInternet, "Ok", "Cancel", this);
				isSuccessLocation = false;
			}
			return isSuccessLocation;
		}
		#endregion
		#region "AlertMsgTwoInput(2 button)"
		internal void FnAlertMsgTwoInput(string strTitle, string strMsg, string strOk, string strCancel, Context context)
		{
			Android.App.AlertDialog alertMsg = new Android.App.AlertDialog.Builder(context).Create();
			alertMsg.SetCancelable(false);
			alertMsg.SetTitle(strTitle);
			alertMsg.SetMessage(strMsg);
			alertMsg.SetButton2(strOk, delegate (object sender, DialogClickEventArgs e)
		 {
			 if (e.Which == -2)
			 {
				 if (SplashScreenActivity.strSetting == "Location")
				 {
					 Intent intent = new Intent(Android.Provider.Settings.ActionLocationSourceSettings);
					 StartActivityForResult(intent, 0);
					 
				 }
				 else if (SplashScreenActivity.strSetting == "Internet")
				 {
					 Intent intent = new Intent(Android.Provider.Settings.ActionWifiSettings);
					 StartActivityForResult(intent, 0);
					 
				 }
			 }
		 });
			alertMsg.SetButton(strCancel, delegate (object sender, DialogClickEventArgs e)
			 {
				 if (e.Which == -1)
				 {  
					 alertMsg.Dismiss();
					 alertMsg = null;
				 }
			 });
			alertMsg.Show();
		}

		public async void OnLocationChanged(Android.Locations.Location location)
		{
			SplashScreenActivity.strLatitude = location.Latitude.ToString();
			SplashScreenActivity.strLangitude = location.Longitude.ToString();
			if (!string.IsNullOrEmpty(SplashScreenActivity.strLatitude) && !string.IsNullOrEmpty(SplashScreenActivity.strLangitude))
				await FnGetLocation();
		}
		#endregion
		protected override  void OnActivityResult(int requestCode, Android.App.Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);
			if (requestCode == 0)
			{
				Finish();
				StartActivity(typeof(HomeActivity));
			}
		}

	}

	public class MyPagerAdapter : FragmentPagerAdapter{
		private  string[] Titles = {"Home","Interest", "Events","Groups","Locations","Messages"};

		public MyPagerAdapter(Android.Support.V4.App.FragmentManager fm) : base(fm)
		{
		}

		public override Java.Lang.ICharSequence GetPageTitleFormatted (int position)
		{
			return new Java.Lang.String (Titles [position]);
		}
		#region implemented abstract members of PagerAdapter
		public override int Count {
			get {
				return Titles.Length;
			}
		}
		#endregion
		#region implemented abstract members of FragmentPagerAdapter
		public override Android.Support.V4.App.Fragment GetItem (int position)
		{
			return SlidingTabActivity.NewInstance (position);
		}
		#endregion
	}
}