﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using Newtonsoft.Json;
using RestSharp;
using Uri = Android.Net.Uri;
using Square.Picasso;

namespace YouMeUsAndroid
{
	[Activity(WindowSoftInputMode = SoftInput.StateAlwaysHidden, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
	public class DetailChatActivity : Activity
	{
		ProgressDialog progress;
		Java.IO.File fileImagePath;
		string strAbsolutePath;
		string strProductCameraImagePath;
		int imageWidth;
		int imageHeight;
		string strRecipientMessage;
		System.Timers.Timer timer;
		string strImagePath;
		bool isTrue = true;
		string strIdUserTo;
		string strTypeofMessage = "";
		string strImageName = "";
		string strFileName = "";
		byte[] chatImage;
		internal static Chatmemberlist PropertyChatmemberlist { get; set; }
		ListView listViewDetailChat;
		TextView lblUserName;
		TextView lblOnlineOrOffline;
		ImageView imgChatProfile;
		ImageView imgAttach;
		ImageView imgSend;
		EditText txtNewMessage;
		MultiAutoCompleteTextView txtNewChatTo;
		string strTypeOfFile = "Text";
		RelativeLayout rlySingleChatLayout;
		RelativeLayout rlyGroupChatLayout;
		RelativeLayout rlyLayoutSet;
		ImageView imgCamera;
		ImageView imgGallery;
		ImageView imgVideo;
		ImageView imgAudio;
		ImageView imgDocument;
		string strFolderPathImages;
		string strFolderPathVideo;
		string strFolderPathAudio;
		string strFolderPathDoc;
		ClsCommonFunctions objClsCommonFunctions;
		RestServiceCall objRestServiceCall;
		IndividualChatAdapterClass objIndividualChatAdapterClass;
		List<Indivisualchat> listIndividualChat = new List<Indivisualchat>();
		List<NewMessgeToList> listNewMessgeToList = new List<NewMessgeToList>();
		List<NewMessgeToList> lst = new List<NewMessgeToList>();
		string[] strName = null;
		string strMemberName = "";
		string strMemberUserId = "";
		System.Uri uri;
		bool isMessage = true;
		string strTextOrOtherChat = "Text";
		private Context context;
		protected override void OnCreate(Bundle savedInstanceState)
		{
			RequestWindowFeature(WindowFeatures.NoTitle);
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.DetailChatLayout);
			context = Application.Context;

			FnInitilization();
			CreateTimerFunction();
			FnClickEvents();
		}
		protected override void OnStop()
		{

			DisposeTimerFunction();
			base.OnStop();
		}
		public override void OnBackPressed()
		{
			base.OnBackPressed();
			FinishAffinity();
			Intent activity = new Intent(this, typeof(HomeActivity));
			activity.PutExtra("MyData", "5");
			StartActivity(activity);


		}
		#region "Initilization"
		async void FnInitilization()
		{
			listViewDetailChat = FindViewById<ListView>(Resource.Id.msgListView);
			lblUserName = FindViewById<TextView>(Resource.Id.lblUserNameML);
			lblOnlineOrOffline = FindViewById<TextView>(Resource.Id.lblOnlineML);
			imgAttach = FindViewById<ImageView>(Resource.Id.imgTagBtnML);
			txtNewMessage = FindViewById<EditText>(Resource.Id.txtMsgContentML);
			imgSend = FindViewById<ImageView>(Resource.Id.imgSendML);
			imgChatProfile = FindViewById<ImageView>(Resource.Id.imgUserProfileML);
			rlySingleChatLayout = FindViewById<RelativeLayout>(Resource.Id.rlyHeaderMsgLayout);
			rlyGroupChatLayout = FindViewById<RelativeLayout>(Resource.Id.rlyNewMessageLayout);
			rlyLayoutSet = FindViewById<RelativeLayout>(Resource.Id.rlylistMsgLayout);
			txtNewChatTo = FindViewById<MultiAutoCompleteTextView>(Resource.Id.txtNewChatTo);
			imgCamera = FindViewById<ImageView>(Resource.Id.imgCamera);
			imgGallery = FindViewById<ImageView>(Resource.Id.imgGallery);
			imgVideo = FindViewById<ImageView>(Resource.Id.imgVideo);
			imgAudio = FindViewById<ImageView>(Resource.Id.imgAudio);
			imgDocument = FindViewById<ImageView>(Resource.Id.imgDocument);
			rlyLayoutSet.Visibility = ViewStates.Gone;
			strRecipientMessage = "";
			objClsCommonFunctions = objClsCommonFunctions ?? new ClsCommonFunctions();
			objRestServiceCall = objRestServiceCall ?? new RestServiceCall();
			bool isCreated = objClsCommonFunctions.FnCreateFolderInSDCard("YouMeUs", "Images", out strFolderPathImages);
			bool isCreated2 = objClsCommonFunctions.FnCreateFolderInSDCard("YouMeUs", "Video", out strFolderPathVideo);
			bool isCreated3 = objClsCommonFunctions.FnCreateFolderInSDCard("YouMeUs", "Audio", out strFolderPathAudio);
			bool isCreated4 = objClsCommonFunctions.FnCreateFolderInSDCard("YouMeUs", "Doc", out strFolderPathDoc);

			if (SlidingTabActivity.isChatSingleOrGroup)
			{
				strIdUserTo = PropertyChatmemberlist.Idusers;
				rlySingleChatLayout.Visibility = ViewStates.Visible;
				rlyGroupChatLayout.Visibility = ViewStates.Gone;
				lblUserName.Text = PropertyChatmemberlist.Fname;
				if (PropertyChatmemberlist.Isonline == "1")
				{
					lblOnlineOrOffline.Text = "Online";
				}
				else
				{
					lblOnlineOrOffline.Text = "Offline";
				}
				Picasso.With(this).Load(PropertyChatmemberlist.Modifiedimage).Into(imgChatProfile);
				strRecipientMessage = PropertyChatmemberlist.Idusers;
				await FnIndividualChatList();
			}
			else
			{
				rlySingleChatLayout.Visibility = ViewStates.Gone;
				rlyGroupChatLayout.Visibility = ViewStates.Visible;
				FnSearchMembers();
			}
		}
		#endregion

		#region "ClickEvents"
		void FnClickEvents()
		{
			imgAttach.Click += delegate
			{
				if (isTrue)
				{
					rlyLayoutSet.Visibility = ViewStates.Visible;
					isTrue = false;
				}
				else
				{
					rlyLayoutSet.Visibility = ViewStates.Gone;
					isTrue = true;
				}

			};
			imgSend.Click += async delegate
			{
				
				if (string.IsNullOrEmpty(txtNewMessage.Text))
				{
					//objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
				}
				else
				{
					if (SlidingTabActivity.isChatSingleOrGroup)
					{
						await FnMessageSendAndReceive();
					}
					else
					{
						if(string.IsNullOrEmpty(txtNewChatTo.Text))
						{
							objClsCommonFunctions.FnAlertMsg(Constants.strAppName,"Add Recipients Name", this);
						}
						else
						{
							await FnMessageSendAndReceive();
						}
					}
				}
			};
			imgCamera.Click += delegate
			{
				if (SlidingTabActivity.isChatSingleOrGroup)
				{
					FnCameraImageIntent();
				}
				else
				{
					if (string.IsNullOrEmpty(txtNewChatTo.Text))
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Add Recipients Name", this);
					}
					else
					{
						FnCameraImageIntent();
					}
				}

			};
			imgGallery.Click += delegate
			{
				if (SlidingTabActivity.isChatSingleOrGroup)
				{
					FnGalleryImageIntent();
				}
				else
				{
					if (string.IsNullOrEmpty(txtNewChatTo.Text))
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Add Recipients Name", this);
					}
					else
					{
						FnGalleryImageIntent();
					}
				}
			};
			imgVideo.Click += delegate
			{
				if (SlidingTabActivity.isChatSingleOrGroup)
				{
					FnVideoImageIntent();
				}
				else
				{
					if (string.IsNullOrEmpty(txtNewChatTo.Text))
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Add Recipients Name", this);
					}
					else
					{
						FnVideoImageIntent();
					}
				}
			};
			imgAudio.Click += delegate
			{
				if (SlidingTabActivity.isChatSingleOrGroup)
				{
					FnAudioImageIntent();
				}
				else
				{
					if (string.IsNullOrEmpty(txtNewChatTo.Text))
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Add Recipients Name", this);
					}
					else
					{
						FnAudioImageIntent();
					}
				}
			};
			imgDocument.Click += delegate
			{
				if (SlidingTabActivity.isChatSingleOrGroup)
				{
					FnDocImageIntent();
				}
				else
				{
					if (string.IsNullOrEmpty(txtNewChatTo.Text))
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Add Recipients Name", this);
					}
					else
					{
						FnDocImageIntent();
					}
				}

			};
			txtNewChatTo.TextChanged += delegate
			{
				List<string> lstvalue = new List<string>();

				int index = 0;
				if (string.IsNullOrEmpty(txtNewChatTo.Text))
				{
					strMemberName = "";
					strMemberUserId = "";
				}
				else
				{
					string strTextVAlue = txtNewChatTo.Text;
					string lastCharacter = strTextVAlue.Split(',').Last();
					// string lastCharacter = strTextVAlue.Substring(strTextVAlue.Length - 1);
					if (lastCharacter == " ")
					{

					}
					else
					{
						lst = listNewMessgeToList.Where(x => x.Fname.ToLower().Contains(lastCharacter.ToLower())).ToList();
						strName = new string[lst.Count];
						foreach (var description in lst)
						{
							strName[index] = description.Idusers + " " + description.Fname;
							index++;
						}
					}
					if (strName != null && strName.Length > 0)
					{
						txtNewChatTo.Adapter = null;
						NewMessageAutoCompleteListAdapter adapter = new NewMessageAutoCompleteListAdapter(this, lst);
						adapter.OriginalItems = strName;
						txtNewChatTo.Adapter = adapter;

						// txtInterestTag.Threshold = 1;
						txtNewChatTo.SetTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
						adapter.ItemClick -= FnTxtSourceItemClick;
						adapter.ItemClick += FnTxtSourceItemClick;

					}
					else
					{
						txtNewChatTo.Adapter = null;
						NewMessageAutoCompleteListAdapter adapter = new NewMessageAutoCompleteListAdapter(this, lst);
						adapter.OriginalItems = strName;
						txtNewChatTo.Adapter = adapter;

						// txtInterestTag.Threshold = 1;
						txtNewChatTo.SetTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
						adapter.ItemClick -= FnTxtSourceItemClick;
						adapter.ItemClick += FnTxtSourceItemClick;
					}
				}
			};
		}
		#endregion

		#region "Intent Navigation"
		void FnCameraImageIntent()
		{
			strTypeOfFile = "Image";
			if (IsThereAnAppToTakePictures())
			{
				CreateDirectoryForPictures();
			}
			TakeAPicture();
		}
		void FnGalleryImageIntent()
		{
			strTypeOfFile = "Image";
			var imageIntent = new Intent();
			imageIntent.SetType("image/*");
			imageIntent.SetAction(Intent.ActionGetContent);
			StartActivityForResult(Intent.CreateChooser(imageIntent, "Select Image"), 0);
		}
		void FnAudioImageIntent()
		{
			strTypeOfFile = "Audio";

			Intent intentAudio = new Intent(Intent.ActionPick, Android.Provider.MediaStore.Audio.Media.ExternalContentUri);
			StartActivityForResult(intentAudio, 2);
		}
		void FnVideoImageIntent()
		{
			strTypeOfFile = "Video";

			Intent intent = new Intent(Intent.ActionPick, Android.Provider.MediaStore.Video.Media.ExternalContentUri);

			StartActivityForResult(Intent.CreateChooser(intent, "Select Video"), 1);
		}
		void FnDocImageIntent()
		{
			strTypeOfFile = "Doc";
			Intent intent = new Intent(Intent.ActionOpenDocument);
			intent.SetType("*/*");
			intent.AddCategory(Intent.CategoryOpenable);
			intent.SetAction(Intent.ActionGetContent);
			StartActivityForResult(Intent.CreateChooser(intent, "Select PDF"), 3);
		}
		#endregion

		#region "AutoCompleteView rowclick"
		void FnTxtSourceItemClick(NewMessgeToList obj)
		{
			if (strMemberName == "" || strRecipientMessage == "")
			{
				strMemberName = obj.Fname;
				strRecipientMessage = obj.Idusers;
			}
			else
			{
				strMemberName = strMemberName + "," + obj.Fname;
				strRecipientMessage = strRecipientMessage + "," + obj.Idusers;
			}
			txtNewChatTo.Text = strMemberName.Trim() + ",";
			txtNewChatTo.SetSelection(txtNewChatTo.Length());
		}
		#endregion
		async void FnSearchMembers()
		{
			string strChatMemberResults = await objRestServiceCall.CallChatMemberAPI(SplashScreenActivity.strUserId);
			if (strChatMemberResults == "Exception" || string.IsNullOrEmpty(strChatMemberResults))
			{


			}
			try
			{
				var objChatMemberList = JsonConvert.DeserializeObject<ChatMemberList>(strChatMemberResults);
				listNewMessgeToList = objChatMemberList.NewMessgeToList;
			}
			catch (Exception e)
			{

			}
		}


		async Task FnMessageSendAndReceive()
		{
			FnDismissProgress();
			if (strTypeOfFile == "Image")
			{
				progress = ProgressDialog.Show(this, "","Image Uploading...");
				await Task.Run(() => FnImageResizing());
			}
			else if (strTypeOfFile == "Video")
			{
				progress = ProgressDialog.Show(this, "","Video Uploading...");
				FnConvertFiles();
			}
			else if (strTypeOfFile == "Audio")
			{
				progress = ProgressDialog.Show(this, "", "Audio Uploading...");
				FnConvertFiles();
			}
			else if (strTypeOfFile == "Doc")
			{
				progress = ProgressDialog.Show(this, "","File Uploading...");
				FnConvertFiles();
			}
			if (strTypeOfFile == "Text")
			{
				FnDismissProgress();
				strTypeofMessage = "";
			}

			string strMessageSendAndReceiveAPI = "";

			//++++++++++++++  production url     +++++++++++++++++//
			strMessageSendAndReceiveAPI = "http://www.youmeus.com";
			//++++++++++++++ test url      +++++++++++++++++//
			//strMessageSendAndReceiveAPI = "http://202.83.19.104/youmeusencrypted";                                                                    
			uri = uri ?? new System.Uri(strMessageSendAndReceiveAPI);
			RestClient _client = new RestClient(uri);

			bool isConnected = objClsCommonFunctions.FnIsConnected(this);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
			}
			else
			{
				try
				{
					//post services api
					var request = new RestRequest("helloservice/fnreply", Method.POST) { RequestFormat = DataFormat.Json };
					if (strTypeOfFile == "Image")
					{
						request.AddFile("file", chatImage, strImageName);
					}
					else if (strTypeOfFile == "Video")
					{
						request.AddFile("file", chatImage, strImageName);
					}
					else if (strTypeOfFile == "Audio")
					{
						request.AddFile("file", chatImage, strImageName);
					}
					else if (strTypeOfFile == "Doc")
					{
						request.AddFile("file", chatImage, strImageName);
					}
					else if (strTypeOfFile == "Text")
					{

					}

					request.AlwaysMultipartFormData = true;
					//Add one of these for each form boundary you need
					request.AddParameter("from", SplashScreenActivity.strUserId, ParameterType.GetOrPost);
					request.AddParameter("to", strRecipientMessage, ParameterType.GetOrPost);


					request.AddParameter("replymsg", txtNewMessage.Text.Trim(), ParameterType.GetOrPost);

					request.AddParameter("typeofmessage", strTypeofMessage, ParameterType.GetOrPost);

					var response = await Task.Run(() => _client.Execute(request));
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            					if (response.StatusCode.ToString() == "OK" && response.ResponseStatus.ToString() == "Completed")
					{

						txtNewMessage.Text = string.Empty;
						strTypeOfFile = "Text";
						chatImage = null;
						strImageName = "";
						if (SlidingTabActivity.isChatSingleOrGroup)
						{
							isMessage = true;
							await FnIndividualChatList();
							FnDismissProgress();
							objClsCommonFunctions.FnDeleteDirectory(Constants.strFolderPath);
						}
						else
						{
							FnDismissProgress();
							objClsCommonFunctions.FnDeleteDirectory(Constants.strFolderPath);
							FinishAffinity();
							Intent activity = new Intent(this, typeof(HomeActivity));
							activity.PutExtra("MyData", "5");
							StartActivity(activity);

						}

					}
				}
				catch (Exception e)
				{
					FnDismissProgress();
				}
			}
			FnDismissProgress();
		}

		async Task<bool> FnIndividualChatList()
		{
			bool isSuccess = false;
			bool isConnected = objClsCommonFunctions.FnIsConnected(this);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
			}
			else
			{
				string strIndividualChatResults = await objRestServiceCall.CallIndividualAPIService(SplashScreenActivity.strUserId, strIdUserTo);
				if (strIndividualChatResults == "Exception" || string.IsNullOrEmpty(strIndividualChatResults))
				{
					isSuccess = false;
				}
				try
				{

					var objChatServiceClass = JsonConvert.DeserializeObject<IndividualChatClass>(strIndividualChatResults);
					objChatServiceClass.Indivisualchat.TrimExcess();
					listIndividualChat = objChatServiceClass.Indivisualchat;
					listViewDetailChat.Visibility = ViewStates.Visible;
					if (isMessage)
					{
						FnBindData();
					}
					isSuccess = true;
				}
				catch (Exception e)
				{

					isSuccess = false;
				}
			}
			return isSuccess;
		}

		void FnBindData()
		{
			objIndividualChatAdapterClass = null;
			objIndividualChatAdapterClass = new IndividualChatAdapterClass(this, listIndividualChat);


				objIndividualChatAdapterClass.ToImageDownLoadClick += FnToImageDownLoad;
				objIndividualChatAdapterClass.ToVideoDownLoadClick += FnToVideoDownLoad;
				objIndividualChatAdapterClass.ToAudioDownLoadClick += FnToAudioDownLoad;
				objIndividualChatAdapterClass.ToDocDownLoadClick += FnToDocDownLoad;

				objIndividualChatAdapterClass.FromImageDownLoadClick += FnFromImageDownLoad;
				objIndividualChatAdapterClass.FromVideoDownLoadClick += FnFromVideoDownLoad;
				objIndividualChatAdapterClass.FromAudioDownLoadClick += FnFromAudioDownLoad;
				objIndividualChatAdapterClass.FromDocDownLoadClick += FnFromDocDownLoad;
				listViewDetailChat.Adapter = objIndividualChatAdapterClass;
				listViewDetailChat.TranscriptMode = TranscriptMode.Normal;
				listViewDetailChat.StackFromBottom = true;

			}


		void FnToImageDownLoad(Indivisualchat objIndivisualchat)
		{
			FnDownloadFile(objIndivisualchat.messageindividual.Messagecontent, strFolderPathImages);
		}
		void FnToVideoDownLoad(Indivisualchat objIndivisualchat)
		{
			FnDownloadFile(objIndivisualchat.messageindividual.Messagecontent, strFolderPathVideo);
		}
		void FnToAudioDownLoad(Indivisualchat objIndivisualchat)
		{
			FnDownloadFile(objIndivisualchat.messageindividual.Messagecontent, strFolderPathAudio);
		}
		void FnToDocDownLoad(Indivisualchat objIndivisualchat)
		{
			FnDownloadFile(objIndivisualchat.messageindividual.Messagecontent, strFolderPathDoc);
		}
		void FnFromImageDownLoad(Indivisualchat objIndivisualchat)
		{
			FnDownloadFile(objIndivisualchat.messageindividual.Messagecontent, strFolderPathImages);
		}
		void FnFromVideoDownLoad(Indivisualchat objIndivisualchat)
		{
			FnDownloadFile(objIndivisualchat.messageindividual.Messagecontent, strFolderPathVideo);
		}
		void FnFromAudioDownLoad(Indivisualchat objIndivisualchat)
		{
			FnDownloadFile(objIndivisualchat.messageindividual.Messagecontent, strFolderPathAudio);
		}
		void FnFromDocDownLoad(Indivisualchat objIndivisualchat)
		{
			FnDownloadFile(objIndivisualchat.messageindividual.Messagecontent, strFolderPathDoc);
		}



		void FnDownloadFile(string strUrl, string strTypeofFile)
		{
			string strLocalFilename = strUrl.Substring(strUrl.LastIndexOf("/", StringComparison.Ordinal) + 1);
			if (!string.IsNullOrEmpty(strUrl))
			{
				var webClient = new WebClient();
				var url = new System.Uri(strUrl);
				Toast.MakeText(this,"Downloading...", ToastLength.Long).Show();
				webClient.DownloadDataAsync(url);
				webClient.DownloadDataCompleted += (s, e) =>
				{
					try
					{
						var result = e.Result;
						string localPath = System.IO.Path.Combine(strTypeofFile, strLocalFilename);
						File.WriteAllBytes(localPath, result);
						Toast.MakeText(this,"Download Completed", ToastLength.Long).Show();
					}
					catch
					{
						Toast.MakeText(this,"Sorry unable to download", ToastLength.Long).Show();
					}

				};
			}
		}


		#region "Gallery"

		#region "Gallery image that image show in imageView"
		protected override async void OnActivityResult(int requestCode, Android.App.Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);
			if (resultCode == Android.App.Result.Ok && requestCode == 0)//gallery
			{

				strImagePath = GetPathToImage(data.Data);
				//check image size
				BitmapFactory.Options option = await objClsCommonFunctions.GetBitmapOptionsOfImage(strImagePath);
				imageWidth = option.OutWidth;
				imageHeight = option.OutHeight;
				strTypeofMessage = "1";

				if (SlidingTabActivity.isChatSingleOrGroup)
				{
					await FnMessageSendAndReceive();
				}
				else
				{
					if (string.IsNullOrEmpty(txtNewChatTo.Text))
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Add Recipients Name", this);
					}
					else
					{
						await FnMessageSendAndReceive();
					}
				}

			}
			else if (resultCode == Android.App.Result.Ok && requestCode == 1)//video
			{
				strTypeofMessage = "2";
				strImagePath = GetVideoPath(data.Data);
				if (SlidingTabActivity.isChatSingleOrGroup)
				{
					await FnMessageSendAndReceive();
				}
				else
				{
					if (string.IsNullOrEmpty(txtNewChatTo.Text))
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Add Recipients Name", this);
					}
					else
					{
						await FnMessageSendAndReceive();
					}
				}
			}
			else if (resultCode == Android.App.Result.Ok && requestCode == 2)//Audio
			{
				strTypeofMessage = "2";
				strImagePath = GetAudioPath(data.Data);
				if (SlidingTabActivity.isChatSingleOrGroup)
				{
					await FnMessageSendAndReceive();
				}
				else
				{
					if (string.IsNullOrEmpty(txtNewChatTo.Text))
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Add Recipients Name", this);
					}
					else
					{
						await FnMessageSendAndReceive();
					}
				}
			}
			else if (resultCode == Android.App.Result.Ok && requestCode == 3)//doc
			{
				strTypeofMessage = "2";
				var s = data.Data;
				var d=s.LastPathSegment.ToString();

				strImagePath = GetAnyFilePath(context,data.Data);
				if (SlidingTabActivity.isChatSingleOrGroup)
				{
					await FnMessageSendAndReceive();
				}
				else
				{
					if (string.IsNullOrEmpty(txtNewChatTo.Text))
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Add Recipients Name", this);
					}
					else
					{
						await FnMessageSendAndReceive();
					}
				}
			}
			else if (resultCode == Android.App.Result.Ok && requestCode == 4)//camera
			{
				
					Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
					Uri contentUri =Uri.FromFile(fileImagePath);
					//mediaScanIntent.SetData(contentUri);
					//SendBroadcast(mediaScanIntent);
					strTypeofMessage = "1";
				strImagePath = fileImagePath.AbsolutePath;
				if (SlidingTabActivity.isChatSingleOrGroup)
				{
					await FnMessageSendAndReceive();
				}
				else
				{
					if (string.IsNullOrEmpty(txtNewChatTo.Text))
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Add Recipients Name", this);
					}
					else
					{
						await FnMessageSendAndReceive();
					}
				}
			}
			else if(resultCode == Android.App.Result.Canceled)
			{
				strTypeofMessage = "";
				strTypeOfFile = "Text";

			}
			rlyLayoutSet.Visibility = ViewStates.Gone;
			isTrue = true;
		}
		#endregion

		#region "ImageResizing"
		async void FnImageResizing()
		{
			strFileName = strImagePath;
			strImageName = strImagePath.Substring(strImagePath.LastIndexOf("/", StringComparison.Ordinal) + 1);
			chatImage = await objClsCommonFunctions.FnConvertByteArray(strImagePath);
		}
		#endregion
		void FnConvertFiles()
		{
			strImageName = strImagePath.Substring(strImagePath.LastIndexOf("/", StringComparison.Ordinal) + 1);
			Java.IO.FileInputStream fis = new Java.IO.FileInputStream(strImagePath);
			Java.IO.ByteArrayOutputStream bos = new Java.IO.ByteArrayOutputStream();
			byte[] b = new byte[1024];

			for (int readNum; (readNum = fis.Read(b)) != -1;)
			{
				bos.Write(b, 0, readNum);
			}

			chatImage = bos.ToByteArray();
		}
		#region "Get image absolute path"
		//finding image absolute path
		string GetPathToImage(Android.Net.Uri uri)
		{
			string doc_id = "";
			using (var c1 = ContentResolver.Query(uri, null, null, null, null))
			{
				c1.MoveToFirst();
				String document_id = c1.GetString(0);
				doc_id = document_id.Substring(document_id.LastIndexOf(":") + 1);
			}

			string path = null;

			// The projection contains the columns we want to return in our query.
			string selection = Android.Provider.MediaStore.Images.Media.InterfaceConsts.Id + " =? ";
			using (var cursor = ContentResolver.Query(Android.Provider.MediaStore.Images.Media.ExternalContentUri, null, selection, new string[] { doc_id }, null))
			{
				if (cursor == null) return path;
				var columnIndex = cursor.GetColumnIndexOrThrow(Android.Provider.MediaStore.Images.Media.InterfaceConsts.Data);
				cursor.MoveToFirst();
				path = cursor.GetString(columnIndex);
			}
			return path;
		}

		string GetVideoPath(Android.Net.Uri uri)
		{
			string path = null;

			String[] projection = { Android.Provider.MediaStore.Video.Media.InterfaceConsts.Data };
			var cursor1 = ContentResolver.Query(uri, projection, null, null, null);
			if (cursor1 != null)
			{
				int column_index = cursor1.GetColumnIndexOrThrow(Android.Provider.MediaStore.Video.Media.InterfaceConsts.Data);
				cursor1.MoveToFirst();
				path = cursor1.GetString(column_index);
			}
			return path;
		}
		string GetAudioPath(Android.Net.Uri uri)
		{
			string path = null;

			String[] projection = { Android.Provider.MediaStore.Audio.Media.InterfaceConsts.Data };
			var cursor1 = ContentResolver.Query(uri, projection, null, null, null);
			if (cursor1 != null)
			{
				int column_index = cursor1.GetColumnIndexOrThrow(Android.Provider.MediaStore.Audio.Media.InterfaceConsts.Data);
				cursor1.MoveToFirst();
				path = cursor1.GetString(column_index);
			}
			return path;
		}
		 string GetAnyFilePath(Context ctx,Android.Net.Uri uri)
		{
			string path = null;
			try
			{
				path = DocumentsContract.GetDocumentId(uri);
				if (path.Contains(":"))
				{
					String[] split = path.Split(':');
					path = Android.OS.Environment.ExternalStorageDirectory + "/" + split[1];
				}
				else
				{
					path = Android.OS.Environment.ExternalStorageDirectory + "/" + path;
			}
			}
			catch
			{
				
			}


			return path;
		}

			#endregion
			#endregion

			#region "Timer"
			void CreateTimerFunction()
		{
			DisposeTimerFunction();
			timer = new System.Timers.Timer();
			bool isReturn;
			timer.Interval = 4000;
			timer.Enabled = true;
			timer.Elapsed += async delegate
			{
				isReturn = await TimerTickedFunction();
			};
			timer.Start();
		}
		void DisposeTimerFunction()
		{
			if (timer != null)
			{
				timer.Dispose();
				timer = null;
			}
		}
		async Task<bool> TimerTickedFunction()
		{
			DisposeTimerFunction();
			bool isSuccess = await FnLatestMessage();
			if (isSuccess)
			{
				isMessage = false;
				bool isTrue1 = await FnIndividualChatList();
				if (isTrue1)
				{

					objIndividualChatAdapterClass = null;
					objIndividualChatAdapterClass = new IndividualChatAdapterClass(this, listIndividualChat);
					RunOnUiThread(delegate
					{

						objIndividualChatAdapterClass.ToImageDownLoadClick += FnToImageDownLoad;
						objIndividualChatAdapterClass.ToVideoDownLoadClick += FnToVideoDownLoad;
						objIndividualChatAdapterClass.ToAudioDownLoadClick += FnToAudioDownLoad;
						objIndividualChatAdapterClass.ToDocDownLoadClick += FnToDocDownLoad;

						objIndividualChatAdapterClass.FromImageDownLoadClick += FnFromImageDownLoad;
						objIndividualChatAdapterClass.FromVideoDownLoadClick += FnFromVideoDownLoad;
						objIndividualChatAdapterClass.FromAudioDownLoadClick += FnFromAudioDownLoad;
						objIndividualChatAdapterClass.FromDocDownLoadClick += FnFromDocDownLoad;
						listViewDetailChat.Adapter = objIndividualChatAdapterClass;
						listViewDetailChat.TranscriptMode = TranscriptMode.Normal;
						listViewDetailChat.StackFromBottom = true;


					});
					await FnChatMessageUpdate();
				}
				else
				{
					DisposeTimerFunction();
				}
			}
			else
			{

			}
			await Task.Delay(2000); //give 1 second delay between next timer event
			CreateTimerFunction();
			return true;
		}
		#endregion

		async Task<bool> FnLatestMessage()
		{
			bool isSuccess = false;
			bool isConnected = objClsCommonFunctions.FnIsConnected(this);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
			}
			else
			{
				string strResults = await objRestServiceCall.CallLatestMessageAPI(SplashScreenActivity.strUserId);
				if (strResults == "Exception" || string.IsNullOrEmpty(strResults))
				{
					isSuccess = false;
				}
				try
				{
					var objChatServiceClass = JsonConvert.DeserializeObject<LatestMessage>(strResults);
					isSuccess = true;
				}
				catch
				{
					isSuccess = false;
				}

			}
			return isSuccess;
		}
		async Task<bool> FnChatMessageUpdate()
		{
			bool isSuccess = false;
			bool isConnected = objClsCommonFunctions.FnIsConnected(this);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
			}
			else
			{
				try
				{

					string strResults = await objRestServiceCall.CallChatUpdateSeenAPI(strRecipientMessage, SplashScreenActivity.strUserId);
					if (strResults == "Exception" || string.IsNullOrEmpty(strResults))
					{
						isSuccess = false;
					}
					isSuccess = true;
				}
				catch
				{
					isSuccess = false;
				}

			}
			return isSuccess;
		}


		#region "Camara Click"
		private void CreateDirectoryForPictures()
		{
			string strCreated;
			bool iscreated = objClsCommonFunctions.FnCreateFolderInSDCard("YouMeUs", "Camera Images", out strCreated);
			if (iscreated)
			{
				strProductCameraImagePath = System.IO.Path.Combine(strCreated);
			}
		}

		bool IsThereAnAppToTakePictures()
		{
			Intent intent = new Intent(MediaStore.ActionImageCapture);
			IList<ResolveInfo> availableActivities = PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);
			return availableActivities != null && availableActivities.Count > 0;
		}

		void TakeAPicture()
		{
			Intent intent = new Intent(MediaStore.ActionImageCapture);
			string strGUID = Guid.NewGuid().ToString();
			strAbsolutePath = System.IO.Path.Combine(strProductCameraImagePath, String.Format("Camera_image{0}.jpg", strGUID));
			 fileImagePath = new Java.IO.File(strProductCameraImagePath, String.Format("Camera_image{0}.jpg", strGUID));
			intent.PutExtra(MediaStore.ExtraOutput, Android.Net.Uri.FromFile(fileImagePath));
			StartActivityForResult(intent, 4);
		}
		#endregion
		#region "Dismiss Progress"
		void FnDismissProgress()
		{
			if (progress != null)
			{
				progress.Dismiss();
				progress = null;
			}

		}
		#endregion
	}

	public class NewMessgeToList
	{
		public string Fname { get; set; }
		public string Idusers { get; set; }
		public string Idgroups { get; set; }
		public string Groupname { get; set; }
		public string Modifiedimage { get; set; }
	}

	public class ChatMemberList
	{
		public List<NewMessgeToList> NewMessgeToList { get; set; }
	}
	//************
		public class Latestmsg
		{
			public string Idchatmaster { get; set; }
			public string Iduserfrom { get; set; }
			public string Iduserto { get; set; }
			public string Messagecontent { get; set; }
			public string Typeofmesasge { get; set; }
			public string Status { get; set; }
			public string Idgroup { get; set; }
			public string Timestamp { get; set; }
			public string Idusers { get; set; }
			public string Isonline { get; set; }
			public string Fname { get; set; }
			public string Modifiedimage { get; set; }
		}

		public class LatestMessage
		{
			public List<Latestmsg> Latestmsg { get; set; }
		}
}
//Note:
//1.from(from userid),
//2.to(if sending to more than one user send it in comma separated value),
//3.replymsg(text message content , if no text message then dont set this parameter ),
//4.fileData(image or doc files, if fileData is null then dont set this parameter ),
//5.fileName,
//6.typeofmessage(1:images, 2:docs),
//listInterestsDetails.StackFromBottom = true;//listview shows last row directly after binding data