﻿
using System;
using Android.Content;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Locations;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Xamarin.Auth;
using Newtonsoft.Json;
using System.Json;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using Android.Support.V4.App;
using Android;
using Android.Content.PM;
using Android.Support.V4.Content;
 

namespace YouMeUsAndroid
{
	[Activity(Theme = "@style/Theme.Splash",NoHistory = true, MainLauncher = true, WindowSoftInputMode = SoftInput.StateAlwaysHidden, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait, ConfigurationChanges = ConfigChanges.Locale)]
	public class SplashScreenActivity : Activity, ActivityCompat.IOnRequestPermissionsResultCallback
	{
		internal static bool isPermission = false;
		internal static string strLocationAddress;
		internal static bool isLogout = true;
		internal static int login = 0;
		internal static string strLatitude;
		internal static string strLangitude;
		internal static string strAddress;
		internal static string strUserId = "";
		internal static string strUserName = "";
		internal static string strUserProfileImage = "";
		internal static  string strEmail = null;
		internal static int homeFeedsShowMoreIndex=0;
		const int RequestLocationId = 0;
		internal static bool isInstalled;
		string strExistsApp;
		internal static	string strSetting = string.Empty;
		LocationManager locationManager;
		ClsCommonFunctions objClsCommonFunctions;

		protected async override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			objClsCommonFunctions = objClsCommonFunctions ?? new ClsCommonFunctions();

				var preference = GetSharedPreferences("YouMeUs", FileCreationMode.Private);
				strExistsApp = preference.GetString("InstallOrNotApp", string.Empty);
				if (string.IsNullOrEmpty(strExistsApp))
				{
					SplashScreenActivity.login = 0;
					StartActivity(typeof(MainActivity));
					isInstalled = false;
					Finish();
				}
				else
				{
					SplashScreenActivity.login = 0;
					StartActivity(typeof(HomeActivity));
					isInstalled = true;
					Finish();
				}


		}





	}
}


