 using System;
using Android.Content;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Locations;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Xamarin.Auth;
using Newtonsoft.Json;
using System.Json;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using Android.Support.V4.App;
using Android;
using Android.Content.PM;
using Android.Support.V4.Content;

namespace YouMeUsAndroid
{
	[Activity(WindowSoftInputMode = SoftInput.StateAlwaysHidden, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
	//[Activity (MainLauncher = true,WindowSoftInputMode = SoftInput.StateAlwaysHidden, ConfigurationChanges=Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
	public class MainActivity : Activity,ILocationListener,ActivityCompat.IOnRequestPermissionsResultCallback
	{
		
		const int RequestLocationId = 0;
		//const int RequestLocationId = 0;
		//string strSetting = string.Empty;
		//internal static string strLatitude;
		//internal static string strLangitude;
		//internal static string strAddress;
		static readonly TaskScheduler UIScheduler = TaskScheduler.FromCurrentSynchronizationContext();
		LocationManager locationManager;
		ClsCommonFunctions objClsCommonFunctions;
		RestServiceCall objRestServiceCall;
		ProgressDialog progress;
		GoogleInfo googleIngo;
		string access_token;
		internal static bool isInstalled = false;

		internal static string strTypeLogin;
		bool isSuccess = false;
		EditText txtEmailId;
		EditText txtPassword;
		Button btnSignIn;
		ImageView btnGoogleLogin;
		ImageView btnFaceBookLogin;
		TextView lblSignUp;
		TextView lblForgotPassword;
		CheckBox chkRememberMe;
		//Forgot password
		Button btnSendFPL;
		EditText txtEmailIdFPL;


		public async void OnLocationChanged(Android.Locations.Location location)
		{
			SplashScreenActivity.strLatitude = location.Latitude.ToString();
			SplashScreenActivity.strLangitude = location.Longitude.ToString();
			if (!string.IsNullOrEmpty(SplashScreenActivity.strLatitude) && !string.IsNullOrEmpty(SplashScreenActivity.strLangitude))
				await FnGetLocation();
		}

		protected override void OnCreate(Bundle savedInstanceState)
		{
			RequestWindowFeature(WindowFeatures.NoTitle);
			base.OnCreate(savedInstanceState);
			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.LoginLayout);
			FnInitilization();
			FnClickEvents();
		}
		protected override void OnStart()
		{
			base.OnStart();
		}

		protected override void OnResume()
		{
			base.OnResume();
		}
		public override void OnBackPressed()
		{
			base.OnBackPressed();

			FinishAffinity();
		}
		//protected override void OnDestroy()
		//{
		//	base.OnDestroy();
		//	Finish();
		//}
		protected override void OnPause()
		{
			base.OnPause();
		}
		protected override void OnStop()
		{
			base.OnStop();

		}
	async void FnInitilization()
		{
			//Initialize GAService
			GAService.GetGASInstance().Initialize(this);
			//Trackpage
			GAService.GetGASInstance().Track_App_Page("Landing Page");
			txtEmailId = FindViewById<EditText>(Resource.Id.txtEmailIdLL);
			txtPassword = FindViewById<EditText>(Resource.Id.txtPasswordLL);
			btnSignIn = FindViewById<Button>(Resource.Id.btnSignInLL);
			btnGoogleLogin = FindViewById<ImageView>(Resource.Id.imgGoogleLL);
			btnFaceBookLogin = FindViewById<ImageView>(Resource.Id.imgFacebookLL);
			lblSignUp = FindViewById<TextView>(Resource.Id.lblSignUpLL);
			lblForgotPassword = FindViewById<TextView>(Resource.Id.lblForgotPasswordLL);
			chkRememberMe = FindViewById<CheckBox>(Resource.Id.chkRememberMe);

			objClsCommonFunctions = objClsCommonFunctions ?? new ClsCommonFunctions();
			objRestServiceCall = objRestServiceCall ?? new RestServiceCall();
			if (Convert.ToInt32(Build.VERSION.Sdk) >= 23)
			{
				if (SplashScreenActivity.isPermission)
				{
					FnLocationManager();
				}
				else
				{
					await GetLocationPermissionAsync();
					if (SplashScreenActivity.isPermission)
					{
						FnLocationManager();
					}
				}
			}
			else
			{
				SplashScreenActivity.isPermission = true;
				FnLocationManager();
			}
			btnSignIn.SetTextColor(Android.Graphics.Color.White);

			//txtEmailId.Text = "vidyasagarzalaki4@gmail.com";
			//txtPassword.Text = "1234";
		}
		void FnClickEvents()
		{
			btnSignIn.Click += async delegate (object sender, EventArgs e)
			{
				isSuccess = true;
				strTypeLogin = "YouMeUsLogin";
				GAService.GetGASInstance().Track_App_Event(GAEventCategory.DiscoverButton, "DiscoverButton Clicked");
				SplashScreenActivity.login = 0;
				if (SplashScreenActivity.isPermission)
				{
					FnLocationManager();
				}
				else
				{
					await GetLocationPermissionAsync();
				}

			};
			lblSignUp.Click += delegate (object sender, EventArgs e)
			{
				StartActivity(typeof(RegistrationActivity));
			};
			btnGoogleLogin.Click += delegate (object sender, EventArgs e)
			{
				strTypeLogin = "GoogleLogin";
				LoginByGoogle(true);
			};
			btnFaceBookLogin.Click += delegate (object sender, EventArgs e)
			{
				strTypeLogin = "FacebookLogin";
				fnFbLogin();
			};
			lblForgotPassword.Click += delegate (object sender, EventArgs e)
			{

				FnForgetPasswordAlertPopUp();
			};
			chkRememberMe.Click += delegate
			{
				if(chkRememberMe.Checked)
				{
					isInstalled = true;
				}
				else
				{
					isInstalled = false;
				}
			};

		}

		#region "FnLocationManager"
		async void FnLocationManager()
		{
			bool isConnected = objClsCommonFunctions.FnIsConnected(this);
			if (isConnected)
			{
				locationManager = GetSystemService(Context.LocationService) as LocationManager;

				if (locationManager.AllProviders.Contains(LocationManager.NetworkProvider)
					&& locationManager.IsProviderEnabled(LocationManager.GpsProvider))
				{
					locationManager.RequestLocationUpdates(LocationManager.NetworkProvider, 2000, 1, this);
					if (SplashScreenActivity.strLatitude != null & SplashScreenActivity.strLangitude != null)
					{
						if (HomeActivity.homevalue == 0)
						{
							if (isSuccess)
							{
								await FnLogin();
								isSuccess = false;
							}
						}
						else
						{
							HomeActivity.homevalue = 0;
						}
					}
				}
				else
				{
					SplashScreenActivity.strSetting = "Location";
					FnAlertMsgTwoInput("YouMeUs", Constants.strGPSDisableErrorMsg, "Ok", "Cancel", this);
				}
			}
			else
			{
				SplashScreenActivity.strSetting = "Internet";
				FnAlertMsgTwoInput("YouMeUs", Constants.strNoInternet, "Ok", "Cancel", this);
			}
		}
		#endregion

		#region "Location "
		public void OnProviderDisabled(string provider) { }

		public void OnProviderEnabled(string provider) { }

		public void OnStatusChanged(string provider, Availability status, Bundle extras) { }

		async Task<bool> FnGetLocation()
		{
			if (string.IsNullOrEmpty(SplashScreenActivity.strAddress))
			{
				Geocoder geocoder = new Geocoder(this);
				IList<Address> addressList = await geocoder.GetFromLocationAsync(Convert.ToDouble(SplashScreenActivity.strLatitude), Convert.ToDouble(SplashScreenActivity.strLangitude), 10);

				Address address = addressList.FirstOrDefault();
				if (address != null)
				{
					SplashScreenActivity.strAddress = address.Locality;
				}
				else
				{
					Toast.MakeText(this, "Unable to find current location", ToastLength.Long).Show();
				}
			}
			return true;
		}

		#endregion


		#region "YouMeUs Login" 

		async Task<bool> FnLogin()
		{
			//soon after the login ,downloads the user profile pic and strUserId for the further processing
			SplashScreenActivity.strEmail = txtEmailId.Text.Trim();
			bool isLogged = false;
			progress = ProgressDialog.Show(this, "", GetString(Resource.String.Logging));
			if (string.IsNullOrEmpty(txtEmailId.Text) || string.IsNullOrEmpty(txtPassword.Text))
			{
				progress.Dismiss();
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Enter Username and Password", this);
				isLogged = false;
			}
			else
			{
				bool isTrue = objClsCommonFunctions.FnEmailValidation(txtEmailId.Text.Trim());
				if (!isTrue)
				{
					progress.Dismiss();
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strEmailValidationError, this);
					isLogged = false;
				}

				else
				{
					if (!objClsCommonFunctions.FnIsConnected(this))
					{
						progress.Dismiss();
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, GetString(Resource.String.no_internet), this);
						isLogged = false;

					}
					else
					{
						try
						{
							string strRes = await objRestServiceCall.CallLoginService(txtEmailId.Text.Trim(), txtPassword.Text.Trim());

							if (!objClsCommonFunctions.FnRemoteResultValidate(strRes, this, progress))
							{
								isLogged = false;
								return isLogged;
							}
							SignIn SignInClass = JsonConvert.DeserializeObject<SignIn>(strRes);
							if (SignInClass.resultset == 1)
							{
								progress.Dismiss();
								progress = null;
								isLogged = true;
								FnToHomeActivity();
							}
							else
							{
								progress.Dismiss();
								progress = null;

								objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Login Fail", this);

								isLogged = false;
							}
						}
						catch (Exception e)
						{
							if (progress != null)
							{
								progress.Dismiss();
								progress = null;

							}
							objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Login Fail", this);
						}
					}
				}
			}
			return isLogged;
		}

		#endregion



		#region "AlertMsgTwoInput(2 button)"
		internal void FnAlertMsgTwoInput(string strTitle, string strMsg, string strOk, string strCancel, Context context)
		{
			Android.App.AlertDialog alertMsg = new Android.App.AlertDialog.Builder(context).Create();
			alertMsg.SetCancelable(false);
			alertMsg.SetTitle(strTitle);
			alertMsg.SetMessage(strMsg);
			alertMsg.SetButton2(strOk, delegate (object sender, DialogClickEventArgs e)
		 {
			 if (e.Which == -2)
			 {
				 if (SplashScreenActivity.strSetting == "Location")
				 {
					 Intent intent = new Intent(Android.Provider.Settings.ActionLocationSourceSettings);
					 StartActivityForResult(intent, 0);
				 }
				 else if (SplashScreenActivity.strSetting == "Internet")
				 {
					 Intent intent = new Intent(Android.Provider.Settings.ActionWifiSettings);
					 StartActivityForResult(intent, 0);
				 }
			 }
		 });
			alertMsg.SetButton(strCancel, delegate (object sender, DialogClickEventArgs e)
			 {
				 if (e.Which == -1)
				 {
					 alertMsg.Dismiss();
					 alertMsg = null;
				 }
			 });
			alertMsg.Show();
		}
		#endregion

		#region "Google Login"
		void LoginByGoogle(bool allowCancel)
		{
			//Google  credentials  for Project, need to register in developer.google.com
			//Client id has to be generated  from google page by providing hash code from  keystore
			//redirecturi : is the page to be shown after successful login,here shown sample page from one of the running application
			//for this any of the youmeus hosted page can be taken 
			var auth = new OAuth2Authenticator(clientId: "750450932540-2u2epn1kmnu2jtrv4ia33qr7pof3u9d4.apps.googleusercontent.com",
				scope: "https://www.googleapis.com/auth/userinfo.email",
				authorizeUrl: new Uri("https://accounts.google.com/o/oauth2/auth"),
				redirectUrl: new Uri("http://qualityinaction.net/aspxDbScript.aspx"),
				getUsernameAsync: null);
			auth.AllowCancel = allowCancel;
			auth.Completed += (sender, e) =>
			{
				if (!e.IsAuthenticated)
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Not Authenticated", this);
					return;
				}
				e.Account.Properties.TryGetValue("access_token", out access_token);

				getInfo();
			};
			var intent = auth.GetUI(this);
			StartActivity(intent);
		}

		int attempt = 0;
		async void getInfo()
		{
			progress = ProgressDialog.Show(this, "", "Fetching Response...");

			string userInfo = await objRestServiceCall.GetDataFromGoogle(access_token);

			if (userInfo != "Exception")
			{
				googleIngo = JsonConvert.DeserializeObject<GoogleInfo>(userInfo);
				SplashScreenActivity.strEmail = googleIngo.email;
				FnToHomeActivity();
			}
			else
			{
				if (attempt <= 1)
				{
					attempt += 1;
					userInfo = await objRestServiceCall.GetDataFromGoogle(access_token);
					if (userInfo != "Exception")
					{
						googleIngo = JsonConvert.DeserializeObject<GoogleInfo>(userInfo);
						SplashScreenActivity.strEmail = googleIngo.email;
						if (progress != null)
						{
							progress.Dismiss();
							progress = null;
						}
						FnToHomeActivity();
					}
				}
				else
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Connection failed! Please try again", this);
				}
				if (progress != null)
				{
					progress.Dismiss();
					progress = null;
				}
			}
		}
		#endregion

		#region "Facebook login"

		void fnFbLogin()
		{
			string strValue = "";
			//For login by facebook need register the project in developer.facebook.com
			//need to follow the steps in developer page , and to create the clientId
			//below code clientId: "822515487781094" this client id generated from developer account 
			var auth = new OAuth2Authenticator(
				clientId: "1617416131886509",
				scope: "email",
				authorizeUrl: new Uri("https://m.facebook.com/dialog/oauth/"),
				redirectUrl: new Uri("http://qualityinaction.net/aspxDbScript.aspx"));

			auth.Completed += async (s, ee) =>
			{
				if (!ee.IsAuthenticated)
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Not Authenticated", this);
					return;
				}
				else
				{
					// Use eventArgs.Account to do wonderful things 
					var accessToken = ee.Account.Properties["access_token"].ToString();
					//var expiresIn = Convert.ToDouble (ee.Account.Properties ["expires_in"]);
					//var expiryDate = DateTime.Now + TimeSpan.FromSeconds (expiresIn);
					progress = ProgressDialog.Show(this, "", "Fetching Response...");
					var request = new OAuth2Request("GET", new Uri("https://graph.facebook.com/me"), null, ee.Account);
					var response = await request.GetResponseAsync();
					var obj = JObject.Parse(response.GetResponseText());

					if (accessToken != null)
					{
						strValue = await GetFBProfileAsync(accessToken);
					}
					await request.GetResponseAsync().ContinueWith(t =>
				  {
					  var builder = new AlertDialog.Builder(this);
					  if (t.IsFaulted)
					  {
						  builder.SetTitle("Error");
						  builder.SetMessage(t.Exception.Flatten().InnerException.ToString());
					  }
					  else if (t.IsCanceled)
						  builder.SetTitle("Task Canceled");
					  else
					  {
						  string json = t.Result.GetResponseText();
						  var objFacebookResponseClass = JsonConvert.DeserializeObject<FacebookResponseClass>(strValue);

							//var obj1 = JsonValue.Parse (t.Result.GetResponseText ());
							SplashScreenActivity.strEmail = objFacebookResponseClass.email;
					  }
					  builder.SetPositiveButton("Ok", (o, e3) =>
					  {
					  });
					  if (!string.IsNullOrEmpty(SplashScreenActivity.strEmail))
					  {
						  FnToHomeActivity();
					  }
				  }, UIScheduler);
				}
			};

			var intent = auth.GetUI(this);
			StartActivity(intent);
		}
		async Task<string> GetFBProfileAsync(string strAccessToken)
		{
			var requestUrl = "https://graph.facebook.com/me/" + "?fields=name,picture,cover,age_range,birthday,devices,is_verified,languages,email" + "&access_token=" + strAccessToken;
			var httpClient = new HttpClient();
			var userjson = await httpClient.GetStringAsync(requestUrl);
			Console.WriteLine(userjson);
			return userjson;
		}
		#endregion

		async void FnToHomeActivity()
		{
			if (progress != null)
			{
				progress.Dismiss();
				progress = null;
			}
			progress = ProgressDialog.Show(this, "", "Verifying YouMeUs Account...");
			try
			{
				bool verified = await fnUserAccountVerification();
				if (verified)
				{
					if (progress != null)
					{
						progress.Dismiss();
						progress = null;
					}

					txtEmailId.Text = string.Empty;
					txtPassword.Text = string.Empty;
					StartActivity(typeof(HomeActivity));
					Finish();
				}
				else
				{
					if (progress != null)
					{
						progress.Dismiss();
						progress = null;
					}
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Profile doesn't exists. Please Register and proceed.. ", this);
					//strEmail = null;
				}
			}
			catch (Exception m)
			{
				if (progress != null)
				{
					progress.Dismiss();
					progress = null;
				}
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Whoops...Error response!", this);
			}
		}

		#region "Social Login Verification"

		//to check after the social media login whether user has registered to youmeus before
		async Task<bool> fnUserAccountVerification()
		{
			AboutMe aboutMe = new AboutMe();
			string strProfileJsonResponse = await aboutMe.GetProfileInfo(SplashScreenActivity.strEmail);
			if (strProfileJsonResponse == "DataNotFound")
			{
				return false;
			}
			if (strProfileJsonResponse == "Exception")
			{
				progress.Dismiss();
				progress = null;
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this);
				return false;
			}
			if (strProfileJsonResponse == "no_sdcard")
			{
				progress.Dismiss();
				progress = null;
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "SD Card not found", this);
				return false;
			}

			return true;
		}
		#endregion

		#region "Forgot Password"
		void FnForgetPasswordAlertPopUp()
		{
			LayoutInflater password = LayoutInflater.From(this);
			View text = password.Inflate(Resource.Layout.ForgotPasswordLayout, null);

			AlertDialog builder = new AlertDialog.Builder(this).Create();
			builder.SetView(text);

			btnSendFPL = text.FindViewById<Button>(Resource.Id.btnSendFPL);
			txtEmailIdFPL = text.FindViewById<EditText>(Resource.Id.txtEmailIdFPL);
			btnSendFPL.SetTextColor(Android.Graphics.Color.White);
			btnSendFPL.Click += async delegate (object sender, EventArgs e)
			 {
				 if (string.IsNullOrEmpty(txtEmailIdFPL.Text))
				 {
					 objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Please Enter the Email Id", this);
				 }
				 else
				 {
					 bool isChecked = objClsCommonFunctions.FnEmailValidation(txtEmailIdFPL.Text.Trim());
					 if (!isChecked)
					 {
						 objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strEmailValidationError, this);
					 }
					 else
					 {
						 await ForgotPassword(txtEmailIdFPL.Text);
						 if (builder != null)
						 {
							 builder.Dismiss();
							 builder = null;
						 }
					 }
				 }
			 };
			builder.Show();
		}

		#endregion

		async Task<bool> ForgotPassword(string strMailID)
		{
			string strAlertMsg = string.Empty;
			string strResult;
			bool isSuccess = false;
			progress = ProgressDialog.Show(this, "", "Please Wait...");
			bool isConnected = objClsCommonFunctions.FnIsConnected(this);
			if (!isConnected)
			{
				strAlertMsg = Constants.strNoInternet;
			}
			else
			{
				strResult = await objRestServiceCall.CallForgotPasswordService(strMailID);
				if (!objClsCommonFunctions.FnRemoteResultValidate(strResult, this, progress))
				{
					return false;
				}
				try
				{
					ForgotPassword objPassword = JsonConvert.DeserializeObject<ForgotPassword>(strResult);
					if (string.IsNullOrEmpty(objPassword.forgotpwsuccess))
					{
						strAlertMsg = Constants.strServserError;
					}
					else
					{
						strAlertMsg = objPassword.forgotpwsuccess;
						isSuccess = true;
					}
				}
				catch (Exception e)
				{
					strAlertMsg = Constants.strServserError;
				}
			}
			if (progress != null)
			{
				progress.Dismiss();
				progress = null;
			}
			if (!string.IsNullOrEmpty(strAlertMsg))
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, strAlertMsg, this);
			}
			return isSuccess;
		}

		#region "Programmatically Adding Permission (For marshmello version)"

		async Task GetLocationPermissionAsync()
		{
			
			ActivityCompat.RequestPermissions(this,PermissionsLocation, RequestLocationId);
		}

		readonly string[] PermissionsLocation =
		{
			Manifest.Permission.AccessCoarseLocation,
			Manifest.Permission.AccessFineLocation,
			Manifest.Permission.AccessMockLocation,
			Manifest.Permission.ReadExternalStorage,
			Manifest.Permission.WriteExternalStorage,
			Manifest.Permission.Camera,
			Manifest.Permission.Flashlight


		};

		public override async void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
		{
			switch (requestCode)
			{
				case RequestLocationId:
					{
						if (grantResults[0] == Permission.Granted)
						{
							//Permission granted
							SplashScreenActivity.isPermission = true;
						}
						else 
						{
							//Permission Denied 
							SplashScreenActivity.isPermission = false;

						}
					}
					break;
			}
		}

		#endregion






	}

	//to be used for google login process
	public class GoogleInfo
	{
		public string id { get; set; }
		public string email { get; set; }
		public bool verified_email { get; set; }
		public string name { get; set; }
		public string given_name { get; set; }
		public string family_name { get; set; }
		public string link { get; set; }
		public string picture { get; set; }
		public string gender { get; set; }
	}
	public class FacebookResponseClass
	{
		

		public string id { get; set; }
		public string first_name { get; set; }
		public string gender { get; set; }
		public string last_name { get; set; }
		public string link { get; set; }
		public string locale { get; set; }
		public string name { get; set; }
		public string email { get; set; }
		public double timezone { get; set; }
		public string updated_time { get; set; }
		public bool verified { get; set; }
		public string Profile_picture { get; set; }
	}

}
