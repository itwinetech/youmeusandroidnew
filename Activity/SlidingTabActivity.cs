﻿using Android.Support.V4.App;
using Android.OS;
using Android.Views;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Android.Content;
using System.Net;
using System.Linq;
using System.Text.RegularExpressions;
using Android.Widget;
using System.Threading.Tasks;
using Android.Views.InputMethods;
using Square.Picasso;
using Android.Text;
using Android.Graphics;
using Android.Text.Method;
using Android.Support.V7.App;
using Android.Gms.Maps;
using System.Globalization;

namespace YouMeUsAndroid
{

	public class SlidingTabActivity : Android.Support.V4.App.Fragment,IOnMapReadyCallback
	{
		internal static bool isChatSingleOrGroup=true;
		internal static bool isHomeEventOrPostEvent = true;
		internal static int number = 0;
		internal EventsNearbyLocation EventProperty{ get; set; }
		internal Homefeedsfgeoevent HomeFeedEventPropertyList { get; set; }
		bool isChangeScreen = true;
		HomeFeedAdapterClass objHomeFeedAdapterClass;
		internal Orgfeed EventOrgfeed { get; set; }
		int intPreviousAdapterCount;
		bool isChangeLocation = false;
		bool isGroupPosition = true;
		internal EventsNearbyLocation PropertyList{ get; set;}
		internal Homefeedsfgeoevent HomeFeedPropertyList { get; set; }
		internal static int homevalue;
		private InputMethodManager KeyBoard;
		bool isClickeventMoreThanOnes = true;
		internal static bool isSingleOrMultipleEvent = true;
		bool isProgressTrue = false;
		int commentFlag;
		int countValue=0;
		int position;
		string strPostId;
		internal static bool isLikeOrUnlike=false;
		internal static int isSelectedTitle=1;//1->popular or suggested(interest,event,group),2->my(interest,event,group),3->create(interest,event,group)
		bool issearch=true;
		bool isBrowserViewed=false;
		string strEventName;
		int selected;
		bool isEventDescription = true;//isEventDescription = true(suggested event and my event),isEventDescription = false(home event)
		int eventDescriptionPopUp = 1;
		internal static string strFlag;
		string strIdpost;
		 internal static int screenSlide = 1;
		string strSuccess;

		string strUserImagePath;
		string strInterestId="0";
		string strEventsId = "0";
		string strGroupsId = "0";
		int isMyInterestsorMyEvents;
		bool isRemoveInterest;
		bool isManualLocationTrue=false;
		string strInterestName;
		string strLatLong;
		internal static string strSearchLocationValue;
		internal static	string strDistanceData;
		string strValueName="ManualLocation";
		string strEventCostUrl;
		string strInterestsFilePath;
		string strEventsFilePath;
		int interstexist = 0;
		internal static bool isHomeFeedMapFlag = false;
		string strpopularInterestId = "";
		string strGroupId = "";
		string strEventId = "";
		string strSuggestedGroupId;
		internal static	bool isGroupButtonTrue = false;
		internal static	bool isLocation = true;
		internal static	bool isPosition = false;
		ListView listEventDetails;
		ListView listInterestsDetails;
		ListView listGroupDetails;
		ListView listChatMember;
		ListView listHomeFeed;
		RelativeLayout rlyEmptyLayout;
		TextView lblTitlePopular;
		TextView lblTitleMy;
		TextView lblHomeFeed;
		TextView lblTitleCreate;
		View viewLine1;
		View viewLine2;
		View viewLine3;
		TextView lblCahngeLocation;
		TextView lblLocation;
		RelativeLayout lnyChangeLocation;
		ImageView imgGoogleMap;

		RelativeLayout rlyLayoutSet;

		//Event Description page
				RelativeLayout rlyEventDescriptionPage;
				ImageView imgEvent;
				TextView lblEventName;
				TextView lblEventAddress;
				TextView lblEventDateAndTime;
				TextView lblEventDescription;
				Button btnEventTicketInfo;
				Button btnAddEvent;
				Button btnEventCostOrFree;
				string strDescriptionPageEventId ;
		//suggested group page
		ImageView imgGroupImage;
		TextView lblGroupName;
		TextView lblGroupDescription;
		Button btnJoin;
		//My Group page
		ImageView imgMyGroupImage;

		 RelativeLayout rlyHomeCommentLayout;
		 ListView listHomeComment;
		 EditText txtCommentText;
		 ImageView imgSend;
		 Refractored.Controls.CircleImageView imgUserProfile;

		//
		AutoCompleteTextView txtChangeLocationSearch;
		Spinner spinnerChangeLocation;
		Button btnChange;
		ImageView imgClose;
		//post page
		Button btnCancel;
		Button btnSubmit;
		RadioButton rdButton;
		RelativeLayout rlyPostLayout;

		//Manual Location Layout
		//TextView lblChatTitle;
		//RelativeLayout rnyEmptySpace;
		RelativeLayout rlyManualLocationLayout;
		Spinner sprDistance;
		AutoCompleteTextView txtSearchLocation;

		Android.App.ProgressDialog progressDialog;
		string strManualLocationLatLongs;
		GroupsAdapterClass objGroupsAdapterClass;
		InterestAdapterClass    objInterestAdapterClass;
		InterestsDetails objInterest;
		InterestsDetails objInterestSearch;
		EventLocation objEventLocation;
		HomeFeedEventsClass objHomeFeedEventsClass;
		RestServiceCall objRestServiceCall;
		WebClient objWebClient;
		GeoCodeJSONClass objGeoCodeJSONClass;
		GroupDetailsClass objGroupDetailsClass;
		ClsCommonFunctions objClsCommonFunctions;
		string strManualLocationLatLong = "0";
		internal static string strHomeLocationLatLong = "0";
		List<PopularInterest> lstInterestSearch;
		List<PopularInterest> lstInterest;
		List<PopularInterest> lstMyInterest;
		internal static List<EventsNearbyLocation> lstEvents;
		internal static List<Homefeedsfgeoevent> lstHomeEvents=new List<Homefeedsfgeoevent>();
		internal static List<Orgfeed> lstHomeFeed=new List<Orgfeed>();
		List<Orgfeedspromotion> lstHomeFeedPromation = new List<Orgfeedspromotion>();

		List<EventsNearbyLocation> lstMyEvents;
		Button btnShowMore;
		List<Mygroup> lstGroup;
		List<Mygroup> lstMyGroup;
		List<Chatmemberlist> lstChatMemberList;
		List<PopularInterest> lstInterestsSearch = new List<PopularInterest>();
		List<EventsNearbyLocation> lstEventssSearch = new List<EventsNearbyLocation>();
		List<Mygroup> lstGroupsSearch = new List<Mygroup>();
		List<Comment> lstHomeComment = new List<Comment>();
		public static SlidingTabActivity NewInstance(int position)
		{
			var f = new SlidingTabActivity ();
			var b = new Bundle ();
			b.PutInt("position", position);
			f.Arguments = b;
			return f;
		}

		public  override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			position = Arguments.GetInt ("position");

			objInterestSearch = objInterestSearch ?? new InterestsDetails ();
			objInterest    = objInterest??new InterestsDetails ();
			objRestServiceCall = objRestServiceCall ?? new RestServiceCall ();
			objClsCommonFunctions = objClsCommonFunctions ?? new ClsCommonFunctions ();
			objEventLocation = objEventLocation ?? new EventLocation ();
			objHomeFeedEventsClass = objHomeFeedEventsClass ?? new HomeFeedEventsClass();
			KeyBoard = (InputMethodManager)Context.GetSystemService(Context.InputMethodService);
			lstInterest=new List<PopularInterest>();
			lstMyInterest = new List<PopularInterest> ();
			lstEvents=new List<EventsNearbyLocation>();
			lstMyEvents=new List<EventsNearbyLocation>();
			lstGroup = new List<Mygroup>();
			lstMyGroup = new List<Mygroup>();
			lstChatMemberList = new List<Chatmemberlist>();
			//await Task.Delay(3000);
		}

		public  override   Android.Views.View OnCreateView (Android.Views.LayoutInflater inflater, Android.Views.ViewGroup container, Bundle savedInstanceState)
		{
			
			var root = inflater.Inflate(Resource.Layout.SlidingLayout,container,false);
			viewLine1 =root.FindViewById<View> ( Resource.Id.viewLine1 );
			viewLine2 = root.FindViewById<View> ( Resource.Id.viewLine2 );
			viewLine3 = root.FindViewById<View>(Resource.Id.viewLine3);
			listInterestsDetails = root.FindViewById<ListView> (Resource.Id.listViewInterest);
			lblTitlePopular = root.FindViewById<TextView> (Resource.Id.textView1);
			lblTitleMy = root.FindViewById<TextView> (Resource.Id.textView2);
			lblTitleCreate = root.FindViewById<TextView>(Resource.Id.textView3);
			rlyLayoutSet = root.FindViewById<RelativeLayout> (Resource.Id.lny);
			listEventDetails=root.FindViewById<ListView> (Resource.Id.listViewEvents);
			listGroupDetails=root.FindViewById<ListView>(Resource.Id.listViewGroup);
			listChatMember=root.FindViewById<ListView>(Resource.Id.listViewChat);
			lblHomeFeed = root.FindViewById<TextView>(Resource.Id.lblHomeFeed);
			listHomeFeed = root.FindViewById<ListView>(Resource.Id.listViewHomeFeed);
			rlyManualLocationLayout = root.FindViewById<RelativeLayout> ( Resource.Id.rlyManualLocationLayout );
			sprDistance=root.FindViewById<Spinner> ( Resource.Id.spinnerValueLV );
			txtSearchLocation=root.FindViewById<AutoCompleteTextView> ( Resource.Id.txtSearchLocationLV );
			rlyEmptyLayout = root.FindViewById<RelativeLayout>(Resource.Id.lnyEmpty);
			lblCahngeLocation=root.FindViewById<TextView>(Resource.Id.lblChangeLocation);
			lblLocation = root.FindViewById<TextView>(Resource.Id.lblLocation);
			lnyChangeLocation = root.FindViewById<RelativeLayout>(Resource.Id.lnyChangeLocationSet);
			imgGoogleMap=root.FindViewById<ImageView>(Resource.Id.imgGoogleMap);
			//SplashScreenActivity.homeFeedsShowMoreIndex = 0;
			intPreviousAdapterCount = 0;
			btnShowMore = new Button(this.Activity);
			btnShowMore.Text = "Show More";
			btnShowMore.SetTextColor(Color.Black);

			listHomeFeed.AddFooterView(btnShowMore);
			sprDistance.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(SpinnerItemSelected);
			var adapter1 = ArrayAdapter.CreateFromResource(this.Activity, Resource.Array.Location_Distance, Android.Resource.Layout.SimpleSpinnerItem);
			adapter1.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
			sprDistance.Adapter = adapter1;

			if (isManualLocationTrue || GoogleMapActivity.isSuccess)
			{
				
			}
			else
			{
				if (GoogleMapActivity.SuggestOrMyEvent == 0)
				{
					bool isConnected = objClsCommonFunctions.FnIsConnected(this.Activity);
					if (isConnected)
					{
						 FnEventServiceCall(SplashScreenActivity.strLatitude + "-" + SplashScreenActivity.strLangitude, SplashScreenActivity.strAddress, "20");
					}
				}
			}
			FnSwitch(position);
			FnClickEvents();
			FnInterestSearch();
			return root;
		}
		void FnClickEvents()
		{
			HomeActivity.imgLogo.Click += delegate
			{
				HomeActivity.pager.SetCurrentItem(0, true);
				bool isConnecteds = objClsCommonFunctions.FnIsConnected(this.Activity);
				if (isConnecteds)
				{
					HomeActivity.isHosmeFeedorPost = 1;
					HomeActivity.pager.SetCurrentItem(0, true);
				}
			};
			txtSearchLocation.TextChanged += async delegate
			{
				GAService.GetGASInstance().Track_App_Event(GAEventCategory.ManualLocationSearch, "ManualLocation Search Cityname");
				string[] strName = null;

				if (txtSearchLocation.Text.Length >= 2)
				{
					strName = await FnSearchLocation(txtSearchLocation.Text.Trim());
				}
				if (strName != null && strName.Length > 0)
				{
					ArrayAdapter adapter = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleDropDownItem1Line, strName);
					txtSearchLocation.Adapter = adapter;
					strSearchLocationValue = txtSearchLocation.Text.Trim();
				}
			};
			lblTitlePopular.Click += delegate (object sender, EventArgs e)
			{
				isSelectedTitle = 1;
				FnSelectEventOrInterests();
			};
			lblTitleMy.Click += delegate (object sender, EventArgs e)
			{
				isSelectedTitle = 2;
				FnSelectEventOrInterests();
			};
			lblTitleCreate.Click += delegate (object sender, EventArgs e)
			{
				isSelectedTitle = 3;
				FnSelectEventOrInterests();
			};
			HomeActivity.imgArrow.Click +=async delegate (object sender, EventArgs e)
			{
				isLocation = false;
				isSingleOrMultipleEvent = false;
				if (strValueName == "ManualLocation")
				{
					GAService.GetGASInstance().Track_App_Event(GAEventCategory.ArrowButton, "ManualLocation");

					if (string.IsNullOrEmpty(strSearchLocationValue))
					{
						KeyBoard.HideSoftInputFromWindow(txtSearchLocation.WindowToken, 0);
						Toast.MakeText(this.Activity, "Enter valid city name", ToastLength.Short).Show();
							
					}
					else
					{
						GAService.GetGASInstance().Track_App_Event(GAEventCategory.ManualLocationSearch, strSearchLocationValue + " " + "(ManualLocation Search Cityname)");
						KeyBoard.HideSoftInputFromWindow(txtSearchLocation.WindowToken, 0);
						isManualLocationTrue = true;
						bool isTrue=await FnManualLocation();
						if(isTrue && HomeActivity.isArrow)
						{
							HomeActivity.isArrow = false;
							var intent = new Intent(Activity, typeof(GoogleMapActivity));
							StartActivity(intent);
						}
					}
				}
				else
				{
					GAService.GetGASInstance().Track_App_Event(GAEventCategory.ArrowButton, "Events");
					FnEventList();
				}
			};
			HomeActivity.txtSearch.TextChanged += delegate (object sender, Android.Text.TextChangedEventArgs e)
			{
				HomeActivity.btnClearSearchText.Visibility = ViewStates.Visible;
				if (string.IsNullOrEmpty(HomeActivity.txtSearch.Text))
				{
					HomeActivity.btnClearSearchText.Visibility = ViewStates.Invisible;
					listInterestsDetails.Adapter = null;

					if (selected == 1)//interests
					{
						if (isSelectedTitle == 1 || isSelectedTitle == 2)
						{
							BindInterestsList();
						}
					}
					else if (selected == 2)//events
					{
						if (isSelectedTitle == 1 || isSelectedTitle == 2)
						{
							BindEventsList();
						}

					}
					else if (selected == 3)//groups 
					{
						if (isSelectedTitle == 1 || isSelectedTitle == 2)
						{
							BindingGroupList();
						}
					}
				}
				else
				{
					FnSearchInterestsOrEvents();
				}
			};

			btnShowMore.Click += async delegate
			  {

				  SplashScreenActivity.homeFeedsShowMoreIndex++;
				  btnShowMore.Text = "Loading...";
				  if (objHomeFeedAdapterClass != null)
				  {
					  intPreviousAdapterCount = objHomeFeedAdapterClass.Count - 1;
				  }
				  bool isTrue = await FnHomeFeedValue();
				  if (isTrue)
				  {
					  if (lstHomeFeed.Count > 0)
					  {
						btnShowMore.Text = "Show More";
						objHomeFeedAdapterClass.NotifyDataSetChanged();
						  //listHomeFeed.SmoothScrollToPosition(intPreviousAdapterCount);
						  listHomeFeed.SetSelection(intPreviousAdapterCount);

					}
					else
					  {
					  }
				  }
				  else
				  {
					  
					btnShowMore.Text = "Show More";
					objHomeFeedAdapterClass = new HomeFeedAdapterClass(this.Activity, lstHomeFeed);
					objHomeFeedAdapterClass.EventAddressClick += FnEventAddressClick;
					objHomeFeedAdapterClass.EventNameClick += FnEventNameClick;
					objHomeFeedAdapterClass.EventBuyTicketClick += FnEventBuyTicketClick;
					objHomeFeedAdapterClass.PostGroupNameClick += FnPostGroupNameClick;
					objHomeFeedAdapterClass.PostTitleClick += FnPostTitleClick;
					objHomeFeedAdapterClass.PostDownArrowClick += FnPostDownArrowClick;
					objHomeFeedAdapterClass.LikeClick += FnLikeClick;
					objHomeFeedAdapterClass.CommentClick -= FnCommentClick;
					objHomeFeedAdapterClass.CommentClick += FnCommentClick;
					objHomeFeedAdapterClass.ShareClick += FnShareClick;
					listHomeFeed.Adapter = objHomeFeedAdapterClass;
					objHomeFeedAdapterClass.NotifyDataSetChanged();
					listHomeFeed.SetSelection(intPreviousAdapterCount);
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "There are no Feeds to show right now.Join Groups of your interest to get your feeds.", this.Activity);
				  }

			  };
			lblCahngeLocation.Click += delegate
			{
				FnChangeLocation();
			};
			imgGoogleMap.Click += async delegate
			{
				isLocation = false;
				isSingleOrMultipleEvent = false;
				screenSlide = 0;
				if (SplashScreenActivity.login == 0)
				{
					if (!string.IsNullOrEmpty(SplashScreenActivity.strLangitude))
					{
						string strlatlong = SplashScreenActivity.strLatitude + "-" + SplashScreenActivity.strLangitude;
						await FnHomeGoogleMapLocation(strlatlong, SplashScreenActivity.strAddress, "20");
						GoogleMapActivity.strLatLongs = strlatlong;
						GoogleMapActivity.strValue = strlatlong;
						GoogleMapActivity.strDistance = "20";
						GoogleMapActivity.strCityName = SplashScreenActivity.strAddress;
						var intent = new Intent(Activity, typeof(GoogleMapActivity));
						StartActivity(intent);
					}
				}
				else if (SplashScreenActivity.login == 1)
				{
					await FnHomeGoogleMapLocation(strHomeLocationLatLong, SplashScreenActivity.strLocationAddress, strDistanceData);
					GoogleMapActivity.strLatLongs = strHomeLocationLatLong;
					GoogleMapActivity.strValue = strHomeLocationLatLong;
					GoogleMapActivity.strDistance = strDistanceData;
					GoogleMapActivity.strCityName = SplashScreenActivity.strLocationAddress;
					var intent = new Intent(Activity, typeof(GoogleMapActivity));
					StartActivity(intent);
				}
			};

		}
		async void FnSwitch(int position)
		{
			
			KeyBoard.HideSoftInputFromWindow(HomeActivity.txtSearch.WindowToken, 0);
			switch (position)
			{
				//home Feed
				case 0:
					HomeActivity.isEventScreen = false;
					rlyEmptyLayout.Visibility = ViewStates.Gone;
					rlyLayoutSet.Visibility = ViewStates.Visible;
					rlyManualLocationLayout.Visibility = ViewStates.Gone;
					listInterestsDetails.Visibility = ViewStates.Gone;
					listEventDetails.Visibility = ViewStates.Gone;
					listGroupDetails.Visibility = ViewStates.Gone;
					listChatMember.Visibility = ViewStates.Gone;
					listHomeFeed.Visibility = ViewStates.Visible;
					HomeActivity.lnyDropDownPopUp.Visibility = ViewStates.Gone;
					lnyChangeLocation.Visibility = ViewStates.Visible;
					SlidingTabActivity.isHomeFeedMapFlag = false;


					if (HomeActivity.isHosmeFeedorPost==2)
 					{
						btnShowMore.Text="Loading...";
						selected = 6;
						isSelectedTitle = 2;
						HomeActivity.txtSearch.Hint = "Search...";
						FnSelectedLable();
						lblTitleMy.Text = "Post";
						lblTitlePopular.Text = "Suggested Event";
						lblTitleCreate.Text = "Promotions";
						lnyChangeLocation.Visibility = ViewStates.Gone;
						isLocation = false;
						isHomeEventOrPostEvent = false;
						btnShowMore.Visibility = ViewStates.Visible;

						FnHomeFeedBinding();

						HomeActivity.isHosmeFeedorPost = 0;
						HomeActivity.values = 0;
						break;
					}
					if ( HomeActivity.values == 3)
					{
						btnShowMore.Text = "Loading...";
						selected = 6;
						isSelectedTitle = 2;
						HomeActivity.txtSearch.Hint = "Search...";
						FnSelectedLable();
						lblTitleMy.Text = "Post";
						lblTitlePopular.Text = "Suggested Event";
						lblTitleCreate.Text = "Promotions";
						lnyChangeLocation.Visibility = ViewStates.Gone;
						isLocation = false;
						isHomeEventOrPostEvent = false;
						HomeActivity.isHome = false;
						btnShowMore.Visibility = ViewStates.Visible;
						SplashScreenActivity.homeFeedsShowMoreIndex = 0;
						FnHomeFeed();

						HomeActivity.isHosmeFeedorPost = 0;
						HomeActivity.values = 0;
						break;
					}
					if (HomeActivity.values == 2)
					{
						btnShowMore.Visibility = ViewStates.Gone;
						FnPromation();
						HomeActivity.values =0;
						break;
					}
					else if(HomeActivity.isHosmeFeedorPost==1 ||SplashScreenActivity.login == 1)
					{
						btnShowMore.Visibility = ViewStates.Gone;
						selected = 6;
						//number = 1;
						HomeActivity.txtSearch.Hint = "Search...";
						isSelectedTitle = 1;
						FnSelectedLable();
						isHomeEventOrPostEvent = true;
						lblTitleMy.Text = "Post";
						lblTitlePopular.Text = "Suggested Event";
						lblTitleCreate.Text = "Promotions";
						string strManualLocationLatLong = string.Empty;
						isManualLocationTrue = true;
						listInterestsDetails.Visibility = ViewStates.Gone;
						listEventDetails.Visibility = ViewStates.Gone;
						btnShowMore.Visibility = ViewStates.Gone;

						lblLocation.Text = SplashScreenActivity.strLocationAddress;

						string strDistance = "20";
						bool isConnected1 = objClsCommonFunctions.FnIsConnected(this.Activity);
						if (isConnected1)
						{
							FnChangeLocationBind();
						}
						HomeActivity.isHosmeFeedorPost = 0;
						break;
					}
					else if (!HomeActivity.isHome && SplashScreenActivity.login==0)
					{
						btnShowMore.Visibility = ViewStates.Gone;
						bool isConnecteds = objClsCommonFunctions.FnIsConnected(this.Activity);
						if (isConnecteds)
						{
							FnLocation();
						}
						else
						{
							selected = 6;
							btnShowMore.Visibility = ViewStates.Gone;
							HomeActivity.txtSearch.Hint = "Search...";
							isSelectedTitle = 1;
							FnSelectedLable();
							isHomeEventOrPostEvent = true;
							lblTitleMy.Text = "Post";
							lblTitlePopular.Text = "Suggested Event";
							lblTitleCreate.Text = "Promotions";
						}
						break;
					}

					break;
				//interest
				case 1:
					rlyEmptyLayout.Visibility = ViewStates.Gone;
					rlyLayoutSet.Visibility = ViewStates.Visible;
					rlyManualLocationLayout.Visibility = ViewStates.Gone;
					listInterestsDetails.Visibility = ViewStates.Visible;
					listEventDetails.Visibility = ViewStates.Gone;
					listGroupDetails.Visibility = ViewStates.Gone;
					listChatMember.Visibility = ViewStates.Gone;
					listHomeFeed.Visibility = ViewStates.Gone;
					HomeActivity.lnyDropDownPopUp.Visibility=ViewStates.Gone;
					lblHomeFeed.Text = "";
					number = 0;
					lblTitleMy.Visibility = ViewStates.Visible;
					lblTitleCreate.Visibility = ViewStates.Gone;
					SlidingTabActivity.isHomeFeedMapFlag = false;
					lnyChangeLocation.Visibility = ViewStates.Gone;
					bool isConnected = objClsCommonFunctions.FnIsConnected(this.Activity);
					if (isConnected)
					{
						FnInterest();
					}
					else
					{
						selected = 1;
						HomeActivity.txtSearch.Hint = "Search...";
						lblTitleCreate.Visibility = ViewStates.Gone;
						isSelectedTitle = 1;
						FnSelectedLable();
						lblTitleMy.Text = "My Interest";
						lblTitlePopular.Text = "Suggested Interest";
						lblTitleCreate.Text = "";
						listEventDetails.Visibility = ViewStates.Gone;
						listInterestsDetails.Visibility = ViewStates.Visible;
					}
					lblLocation.Text = SplashScreenActivity.strAddress;
					break;
				//events
				case 2:
					HomeActivity.isEventScreen = true;
					rlyEmptyLayout.Visibility = ViewStates.Gone;
					rlyLayoutSet.Visibility = ViewStates.Visible;
					rlyManualLocationLayout.Visibility = ViewStates.Gone;
					listInterestsDetails.Visibility = ViewStates.Gone;
					listGroupDetails.Visibility = ViewStates.Gone;
					listEventDetails.Visibility = ViewStates.Visible;
					listChatMember.Visibility = ViewStates.Gone;
					listHomeFeed.Visibility = ViewStates.Gone;
					lblTitleMy.Visibility = ViewStates.Visible;
					lblTitleCreate.Visibility = ViewStates.Visible;
					SlidingTabActivity.isHomeFeedMapFlag = false;
					HomeActivity.lnyDropDownPopUp.Visibility = ViewStates.Gone;
					lblHomeFeed.Text = "";
					number = 0;
					lnyChangeLocation.Visibility = ViewStates.Gone;
					if (isManualLocationTrue || GoogleMapActivity.isSuccess)
					{
						isPosition = true;
						progressDialog = Android.App.ProgressDialog.Show(this.Activity, "", "Loading Events...");

						if (GoogleMapActivity.strValue.Contains(","))
						{
							strManualLocationLatLong = Regex.Replace(GoogleMapActivity.strValue, ",", ".");
						}
						else
						{
							strManualLocationLatLong = GoogleMapActivity.strValue;
						}
						if (GoogleMapActivity.SuggestOrMyEvent == 1)
						{
							bool isTrue = await FnEventListServiceCall(strManualLocationLatLong, GoogleMapActivity.strCityName, GoogleMapActivity.strDistance);
							if (isTrue)
							{
								FnProgressDialogClose();
								GoogleMapActivity.SuggestOrMyEvent = 0;
							}
						}
						else if (GoogleMapActivity.SuggestOrMyEvent == 2)
						{
							FnMyEventBinding();
						}
						isManualLocationTrue = false;
						GoogleMapActivity.isSuccess = false;
						GoogleMapActivity.SuggestOrMyEvent = 0;
					}
					else
					{
						FnEventList();
						GoogleMapActivity.SuggestOrMyEvent = 0;
					}
					break;
				//groups
				case 3:
					rlyEmptyLayout.Visibility = ViewStates.Gone;
					rlyLayoutSet.Visibility = ViewStates.Visible;
					listGroupDetails.Visibility = ViewStates.Visible;
					rlyManualLocationLayout.Visibility = ViewStates.Gone;
					listInterestsDetails.Visibility = ViewStates.Gone;
					listEventDetails.Visibility = ViewStates.Gone;
					listChatMember.Visibility = ViewStates.Gone;
					listHomeFeed.Visibility = ViewStates.Gone;
					HomeActivity.lnyDropDownPopUp.Visibility = ViewStates.Gone;
					lblHomeFeed.Text = "";
					number = 0;
					lblTitleMy.Visibility = ViewStates.Visible;
					lblTitleCreate.Visibility = ViewStates.Visible;
					SlidingTabActivity.isHomeFeedMapFlag = false;
					lnyChangeLocation.Visibility = ViewStates.Gone;
					FnGroup();
					break;
				//locations
				case 4:
					HomeActivity.isEventScreen = false;
					rlyEmptyLayout.Visibility = ViewStates.Gone;
					rlyLayoutSet.Visibility = ViewStates.Visible;
					listInterestsDetails.Visibility = ViewStates.Gone;
					listGroupDetails.Visibility = ViewStates.Gone;
					listEventDetails.Visibility = ViewStates.Gone;
					listChatMember.Visibility = ViewStates.Gone;
					listHomeFeed.Visibility = ViewStates.Gone;
					rlyManualLocationLayout.Visibility = ViewStates.Visible;
					HomeActivity.lnyDropDownPopUp.Visibility = ViewStates.Gone;
					lblTitleCreate.Visibility = ViewStates.Gone;
					lblTitleMy.Visibility = ViewStates.Visible;
					SlidingTabActivity.isHomeFeedMapFlag = false;
					lnyChangeLocation.Visibility = ViewStates.Gone;
					lblHomeFeed.Text = "";
					selected = 5;
					number = 0;
					isSelectedTitle = 1;
					FnSelectedLable();
					lblTitleMy.Text = "Map";
					lblTitlePopular.Text = "Find Location";
					lblTitleCreate.Text = "";

					break;
				//messages
				case 5:
					rlyEmptyLayout.Visibility = ViewStates.Gone;
					rlyLayoutSet.Visibility = ViewStates.Visible;
					rlyManualLocationLayout.Visibility = ViewStates.Gone;
					listGroupDetails.Visibility = ViewStates.Gone;
					listInterestsDetails.Visibility = ViewStates.Gone;
					listEventDetails.Visibility = ViewStates.Gone;
					listChatMember.Visibility = ViewStates.Visible;
					listHomeFeed.Visibility = ViewStates.Gone;
					lblTitleMy.Visibility = ViewStates.Visible;
					HomeActivity.lnyDropDownPopUp.Visibility = ViewStates.Gone;
					lblTitleCreate.Visibility = ViewStates.Gone;
					SlidingTabActivity.isHomeFeedMapFlag = false;
					lnyChangeLocation.Visibility = ViewStates.Gone;
					lblHomeFeed.Text = "";
					number = 0;
					screenSlide = 3;
					FnMessageChat();
					break;
			}

		}


		#region"SpinnerItemSelected "
		private void SpinnerItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
		{
			GAService.GetGASInstance().Track_App_Event(GAEventCategory.SpinnerClick, "Selected Distance");
			Spinner spinner = (Spinner)sender;
			strDistanceData = string.Format("The planet is {0}", spinner.GetItemAtPosition(e.Position));
			strDistanceData = spinner.GetItemAtPosition(e.Position).ToString();
			var value = strDistanceData.Split(' ');
			strDistanceData = value[0];
			if (strDistanceData == "Km")
			{
				strDistanceData = "2";
			}
		}
		#endregion

		#region "label background color change"
		void FnSelectedLable()
		{
			if (isSelectedTitle == 1)
			{
				lblTitlePopular.SetBackgroundColor(Android.Graphics.Color.Gray);
				lblTitleMy.SetBackgroundColor(Android.Graphics.Color.White);
				lblTitleCreate.SetBackgroundColor(Android.Graphics.Color.White);
				viewLine1.Visibility = ViewStates.Visible;
				viewLine2.Visibility = ViewStates.Gone;
				viewLine3.Visibility = ViewStates.Gone;
			}
			else if (isSelectedTitle == 2)
			{
				lblTitleMy.SetBackgroundColor(Android.Graphics.Color.Gray);
				lblTitlePopular.SetBackgroundColor(Android.Graphics.Color.White);
				lblTitleCreate.SetBackgroundColor(Android.Graphics.Color.White);
				viewLine2.Visibility = ViewStates.Visible;
				viewLine1.Visibility = ViewStates.Gone;
				viewLine3.Visibility = ViewStates.Gone;
			}
			else if (isSelectedTitle == 3)
			{
				lblTitleCreate.SetBackgroundColor(Android.Graphics.Color.Gray);
				lblTitleMy.SetBackgroundColor(Android.Graphics.Color.White);
				lblTitlePopular.SetBackgroundColor(Android.Graphics.Color.White);
				viewLine3.Visibility = ViewStates.Visible;
				viewLine1.Visibility = ViewStates.Gone;
				viewLine2.Visibility = ViewStates.Gone;

			}
		}
		#endregion

		#region "SearchLocation"
		async Task<string[]> FnSearchLocation(string strUserEnteredName)
		{
			string[] strName = null;
			int index = 0;
			string strFullPath = await objRestServiceCall.CallGooglePlaceCityNameService(strUserEnteredName);
			if (strFullPath == "Exception" || string.IsNullOrEmpty(strFullPath))
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this.Activity);
			}
			try
			{
				GoogleLocationClass objGoogleLocationClass = JsonConvert.DeserializeObject<GoogleLocationClass>(strFullPath);
				if (objGoogleLocationClass.status == "OK")
				{
					strName = new string[objGoogleLocationClass.predictions.Count];
					foreach (var description in objGoogleLocationClass.predictions)
					{
						strName[index] = description.description;
						index++;
					}
				}
			}
			catch(Exception e)
			{

			}
			return strName;
		}
		#endregion

		#region "ManualLocation"
		async Task<bool> FnManualLocation()
		{
			bool isSuccess = false;
			string strManualLocationLatLong = string.Empty;
			isManualLocationTrue = true;
			listInterestsDetails.Visibility = ViewStates.Gone;
			listEventDetails.Visibility = ViewStates.Visible;
			string strResult = await objRestServiceCall.CallGooglePlaceService(strSearchLocationValue);
			if (strResult == "Exception" || strResult == "")
			{
				//objClsCommonFunctions.FnAlertMsg ( Constants.strAppName , Constants.strServserError , this.Activity );
			}
			try
			{
				objGeoCodeJSONClass = JsonConvert.DeserializeObject<GeoCodeJSONClass>(strResult);

				string strLantitude = objGeoCodeJSONClass.results[0].geometry.location.lat.ToString();
				string strLongitude = objGeoCodeJSONClass.results[0].geometry.location.lng.ToString();
				strLatLong = strLantitude + "-" + strLongitude;

				if (strLatLong.Contains(","))
				{
					strManualLocationLatLong = Regex.Replace(strLatLong, ",", ".");
				}
				else{
					strManualLocationLatLong = strLatLong;
				}

				GoogleMapActivity.strLatLongs = strManualLocationLatLong;
				objGeoCodeJSONClass = null;
				await FnEventListServiceCall(strManualLocationLatLong, strSearchLocationValue, strDistanceData);
				GoogleMapActivity.strValue=strManualLocationLatLong;
				GoogleMapActivity.strDistance = strDistanceData;
				GoogleMapActivity.strCityName = strSearchLocationValue;
				isSuccess = true;
			}
			catch(Exception e)
			{
				isSuccess = false;
			}
			return isSuccess;
		}
		#endregion

		#region "Interest and Events and groups"
	async	void FnSelectEventOrInterests()
		{
			if (selected == 1)
			{
				if (isSelectedTitle == 1)
				{
					GAService.GetGASInstance().Track_App_Event(GAEventCategory.Lable1, "Popular Interests");
					lblTitlePopular.SetBackgroundColor(Android.Graphics.Color.Gray);
					lblTitleMy.SetBackgroundColor(Android.Graphics.Color.White);
					lblTitleCreate.Visibility = ViewStates.Gone;
					viewLine1.Visibility = ViewStates.Visible;
					viewLine2.Visibility = ViewStates.Gone;
					viewLine3.Visibility = ViewStates.Gone;
					lnyChangeLocation.Visibility = ViewStates.Gone;
					number = 0;
					HomeActivity.lnyDropDownPopUp.Visibility = ViewStates.Gone;
					BindInterestsList();
				}
				else if (isSelectedTitle == 2)
				{
					GAService.GetGASInstance().Track_App_Event(GAEventCategory.Lable2, "My Interests");
					lblTitleMy.SetBackgroundColor(Android.Graphics.Color.Gray);
					lblTitlePopular.SetBackgroundColor(Android.Graphics.Color.White);
					lblTitleCreate.Visibility = ViewStates.Gone;
					viewLine2.Visibility = ViewStates.Visible;
					viewLine1.Visibility = ViewStates.Gone;
					viewLine3.Visibility = ViewStates.Gone;
					number = 0;
					lnyChangeLocation.Visibility = ViewStates.Gone;
					HomeActivity.lnyDropDownPopUp.Visibility = ViewStates.Gone;
					FnMyInterestList();
				}
			}
			else if (selected == 2)
			{
				if (isSelectedTitle == 1)
				{
					GAService.GetGASInstance().Track_App_Event(GAEventCategory.Lable1, "Suggested Events");
					lblTitlePopular.SetBackgroundColor(Android.Graphics.Color.Gray);
					lblTitleMy.SetBackgroundColor(Android.Graphics.Color.White);
					lblTitleCreate.SetBackgroundColor(Android.Graphics.Color.White);
					viewLine1.Visibility = ViewStates.Visible;
					viewLine2.Visibility = ViewStates.Gone;
					viewLine3.Visibility = ViewStates.Gone;
					number = 0;
					HomeActivity.isEventScreen = true;
					lnyChangeLocation.Visibility = ViewStates.Gone;
					HomeActivity.lnyDropDownPopUp.Visibility = ViewStates.Gone;
					FnEventList();
				}
				else if (isSelectedTitle == 2)
				{
					GAService.GetGASInstance().Track_App_Event(GAEventCategory.Lable2, "My Events");
					lblTitleMy.SetBackgroundColor(Android.Graphics.Color.Gray);
					lblTitlePopular.SetBackgroundColor(Android.Graphics.Color.White);
					lblTitleCreate.SetBackgroundColor(Android.Graphics.Color.White);
					viewLine1.Visibility = ViewStates.Gone;
					viewLine2.Visibility = ViewStates.Visible;
					viewLine3.Visibility = ViewStates.Gone;
					number = 0;
					lnyChangeLocation.Visibility = ViewStates.Gone;
					HomeActivity.lnyDropDownPopUp.Visibility = ViewStates.Gone;
					FnMyEventList();
				}
				if (isSelectedTitle == 3)
				{
					lblTitlePopular.SetBackgroundColor(Android.Graphics.Color.White);
					lblTitleMy.SetBackgroundColor(Android.Graphics.Color.White);
					lblTitleCreate.SetBackgroundColor(Android.Graphics.Color.Gray);
					viewLine1.Visibility = ViewStates.Gone;
					viewLine2.Visibility = ViewStates.Gone;
					viewLine3.Visibility = ViewStates.Visible;
					listEventDetails.Visibility = ViewStates.Gone;
					listInterestsDetails.Visibility = ViewStates.Gone;
					listEventDetails.Visibility = ViewStates.Gone;
					listInterestsDetails.Visibility = ViewStates.Gone;
					lnyChangeLocation.Visibility = ViewStates.Gone;
					number = 0;
					HomeActivity.lnyDropDownPopUp.Visibility = ViewStates.Gone;
					var intent = new Intent(Activity, typeof(CreateEventsActivity));

					StartActivity(intent);
				}
			}
			else if (selected == 3)
			{
				if (isSelectedTitle == 1)
				{
					GAService.GetGASInstance().Track_App_Event(GAEventCategory.Lable1, "Suggested Group");
					lblTitlePopular.SetBackgroundColor(Android.Graphics.Color.Gray);
					lblTitleMy.SetBackgroundColor(Android.Graphics.Color.White);
					lblTitleCreate.SetBackgroundColor(Android.Graphics.Color.White);
					viewLine1.Visibility = ViewStates.Visible;
					viewLine2.Visibility = ViewStates.Gone;
					viewLine3.Visibility = ViewStates.Gone;
					lnyChangeLocation.Visibility = ViewStates.Gone;
					number = 0;
					HomeActivity.lnyDropDownPopUp.Visibility = ViewStates.Gone;
					BindingGroupList();
				}
				else if (isSelectedTitle == 2)
				{
					GAService.GetGASInstance().Track_App_Event(GAEventCategory.Lable2, "My Group");
					lblTitleMy.SetBackgroundColor(Android.Graphics.Color.Gray);
					lblTitlePopular.SetBackgroundColor(Android.Graphics.Color.White);
					lblTitleCreate.SetBackgroundColor(Android.Graphics.Color.White);
					viewLine1.Visibility = ViewStates.Gone;
					viewLine2.Visibility = ViewStates.Visible;
					viewLine3.Visibility = ViewStates.Gone;
					number = 0;
					lnyChangeLocation.Visibility = ViewStates.Gone;
					HomeActivity.lnyDropDownPopUp.Visibility = ViewStates.Gone;
					FnMyGroupList();
				}
				else if (isSelectedTitle == 3)
				{
					lblTitlePopular.SetBackgroundColor(Android.Graphics.Color.White);
					lblTitleMy.SetBackgroundColor(Android.Graphics.Color.White);
					lblTitleCreate.SetBackgroundColor(Android.Graphics.Color.Gray);
					viewLine1.Visibility = ViewStates.Gone;
					viewLine2.Visibility = ViewStates.Gone;
					lnyChangeLocation.Visibility = ViewStates.Gone;
					viewLine3.Visibility = ViewStates.Visible;
					listGroupDetails.Visibility = ViewStates.Gone;
					listEventDetails.Visibility = ViewStates.Gone;
					listInterestsDetails.Visibility = ViewStates.Gone;
					number = 0;
					HomeActivity.lnyDropDownPopUp.Visibility = ViewStates.Gone;
					var intent = new Intent(Activity, typeof(CreateGroupActivity));
					StartActivity(intent);
				}
			}
			else if (selected == 4)
			{
				if (isSelectedTitle == 1)
				{
					lblTitlePopular.SetBackgroundColor(Android.Graphics.Color.Gray);
					lblTitleMy.SetBackgroundColor(Android.Graphics.Color.White);
					lblTitleCreate.Visibility = ViewStates.Gone;
					viewLine1.Visibility = ViewStates.Visible;
					viewLine2.Visibility = ViewStates.Gone;
					viewLine3.Visibility = ViewStates.Gone;
					number = 0;
					lnyChangeLocation.Visibility = ViewStates.Gone;
					HomeActivity.lnyDropDownPopUp.Visibility = ViewStates.Gone;
					FnMessageChat();
				}
				else if (isSelectedTitle == 2)
				{
					lblTitleMy.SetBackgroundColor(Android.Graphics.Color.Gray);
					lblTitlePopular.SetBackgroundColor(Android.Graphics.Color.White);
					lblTitleCreate.Visibility = ViewStates.Gone;
					viewLine2.Visibility = ViewStates.Visible;
					viewLine1.Visibility = ViewStates.Gone;
					viewLine3.Visibility = ViewStates.Gone;
					number = 0;
					listInterestsDetails.Visibility = ViewStates.Gone;
					listEventDetails.Visibility = ViewStates.Gone;
					listChatMember.Visibility = ViewStates.Gone;
					listHomeFeed.Visibility = ViewStates.Gone;
					lnyChangeLocation.Visibility = ViewStates.Gone;
					HomeActivity.lnyDropDownPopUp.Visibility = ViewStates.Gone;
					isChatSingleOrGroup = false;
					var intent = new Intent(Activity, typeof(DetailChatActivity));
					StartActivity(intent);

				}
			}
			else if (selected == 5)
			{
				if (isSelectedTitle == 1)
				{
					lblTitlePopular.SetBackgroundColor(Android.Graphics.Color.Gray);
					lblTitleMy.SetBackgroundColor(Android.Graphics.Color.White);
					lblTitleCreate.Visibility = ViewStates.Gone;
					viewLine1.Visibility = ViewStates.Visible;
					viewLine2.Visibility = ViewStates.Gone;
					viewLine3.Visibility = ViewStates.Gone;
					number = 0;
					rlyManualLocationLayout.Visibility = ViewStates.Visible;
					HomeActivity.lnyDropDownPopUp.Visibility = ViewStates.Gone;
					lnyChangeLocation.Visibility = ViewStates.Gone;
					screenSlide = 3;
					selected = 5;
					isSelectedTitle = 1;
					FnSelectedLable();
					lblTitleMy.Text = "Map";
					lblTitlePopular.Text = "Find Location";
					lblTitleCreate.Text = "";
				}
				else if (isSelectedTitle == 2)
				{
					isLocation = true;
					isSingleOrMultipleEvent = false;
					lblTitleMy.SetBackgroundColor(Android.Graphics.Color.Gray);
					lblTitlePopular.SetBackgroundColor(Android.Graphics.Color.White);
					lblTitleCreate.Visibility = ViewStates.Gone;
					viewLine2.Visibility = ViewStates.Visible;
					viewLine1.Visibility = ViewStates.Gone;
					viewLine3.Visibility = ViewStates.Gone;
					listInterestsDetails.Visibility = ViewStates.Gone;
					listEventDetails.Visibility = ViewStates.Gone;
					listChatMember.Visibility = ViewStates.Gone;
					listHomeFeed.Visibility = ViewStates.Gone;
					listHomeFeed.Visibility = ViewStates.Gone;
					lnyChangeLocation.Visibility = ViewStates.Gone;
					screenSlide = 3;
					number = 0;
					bool isConnected1 = objClsCommonFunctions.FnIsConnected(this.Activity);
					if (isConnected1)
					{
						bool isTrue = await FnEventServiceCall(SplashScreenActivity.strLatitude + "-" + SplashScreenActivity.strLangitude, SplashScreenActivity.strAddress, "20");
						if (isTrue)
						{
							rlyManualLocationLayout.Visibility = ViewStates.Gone;
							GoogleMapActivity.strValue = SplashScreenActivity.strLatitude + "-" + SplashScreenActivity.strLangitude;
							GoogleMapActivity.strDistance = "20";
							GoogleMapActivity.SuggestOrMyEvent = 1;
							HomeActivity.lnyDropDownPopUp.Visibility = ViewStates.Gone;
							var intent = new Intent(Activity, typeof(GoogleMapActivity));
							StartActivity(intent);
						}
					}
					else
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this.Activity);
					}

				}
			}
			else if (selected == 6)//home
			{
				if (isSelectedTitle == 1)
				{
					lblTitlePopular.SetBackgroundColor(Android.Graphics.Color.Gray);
					lblTitleMy.SetBackgroundColor(Android.Graphics.Color.White);
					lblTitleCreate.SetBackgroundColor(Android.Graphics.Color.White);
					viewLine1.Visibility = ViewStates.Visible;
					viewLine2.Visibility = ViewStates.Gone;
					viewLine3.Visibility = ViewStates.Gone;
					listGroupDetails.Visibility = ViewStates.Gone;
					listEventDetails.Visibility = ViewStates.Gone;
					listInterestsDetails.Visibility = ViewStates.Gone;
					lnyChangeLocation.Visibility = ViewStates.Visible;
					btnShowMore.Visibility = ViewStates.Gone;
					HomeActivity.lnyDropDownPopUp.Visibility = ViewStates.Gone;
					lblLocation.Text = SplashScreenActivity.strLocationAddress;
					bool isConnected1 = objClsCommonFunctions.FnIsConnected(this.Activity);
					if (isConnected1)
					{
						FnChangeLocationBind();
					}
					else
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this.Activity);
					}
				}
				else if (isSelectedTitle == 2)
				{
					lblTitlePopular.SetBackgroundColor(Android.Graphics.Color.White);
					lblTitleMy.SetBackgroundColor(Android.Graphics.Color.Gray);
					lblTitleCreate.SetBackgroundColor(Android.Graphics.Color.White);
					viewLine1.Visibility = ViewStates.Gone;
					viewLine2.Visibility = ViewStates.Visible;
					viewLine3.Visibility = ViewStates.Gone;
					listGroupDetails.Visibility = ViewStates.Gone;
					listEventDetails.Visibility = ViewStates.Gone;
					listInterestsDetails.Visibility = ViewStates.Gone;
					lnyChangeLocation.Visibility = ViewStates.Gone;
					HomeActivity.lnyDropDownPopUp.Visibility = ViewStates.Gone;
					lstHomeFeed.Clear();
					SplashScreenActivity.homeFeedsShowMoreIndex = 0;
					btnShowMore.Text = "Loading...";
					FnHomeFeed();
				}
				else if (isSelectedTitle == 3)
				{
					lblTitlePopular.SetBackgroundColor(Android.Graphics.Color.White);
					lblTitleMy.SetBackgroundColor(Android.Graphics.Color.White);
					lblTitleCreate.SetBackgroundColor(Android.Graphics.Color.Gray);
					viewLine1.Visibility = ViewStates.Gone;
					viewLine2.Visibility = ViewStates.Gone;
					viewLine3.Visibility = ViewStates.Visible;
					listGroupDetails.Visibility = ViewStates.Gone;
					listEventDetails.Visibility = ViewStates.Gone;
					listInterestsDetails.Visibility = ViewStates.Gone;
					lnyChangeLocation.Visibility = ViewStates.Gone;
					HomeActivity.lnyDropDownPopUp.Visibility = ViewStates.Gone;
					btnShowMore.Visibility = ViewStates.Gone;
					FnPromation();
				}
			}
		}
		#endregion


		#region "Interests"

		//binding popularinterests list to listview
		async void FnInterest()
		{
			//txtSearch.Focusable = true;
			selected = 1;
			HomeActivity.txtSearch.Hint="Search...";
			lblTitleCreate.Visibility = ViewStates.Gone;
			isSelectedTitle = 1;
			FnSelectedLable();
			lblTitleMy.Text = "My Interest";
			lblTitlePopular.Text = "Suggested Interest"; 
			lblTitleCreate.Text = "";
			listEventDetails.Visibility = ViewStates.Gone;
			listInterestsDetails.Visibility = ViewStates.Visible;
			bool isConnected = objClsCommonFunctions.FnIsConnected(this.Activity);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this.Activity);
			}
			else
			{
				string strInterestsResults = await objRestServiceCall.CallInterestsService();
				if (strInterestsResults == "Exception" || string.IsNullOrEmpty(strInterestsResults))
				{
					//objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this.Activity);
					return;
				}
				try
				{
					objInterest = JsonConvert.DeserializeObject<InterestsDetails>(strInterestsResults);
					objInterest.PopularInterests.TrimExcess();
					lstInterest = objInterest.PopularInterests;
					BindInterestsList();
				}
				catch(Exception e)
				{
					
				}
			}
		}


		//Binding popular and my interests list
		async void BindInterestsList()
		{
			int count = 0;
			listInterestsDetails.Visibility = ViewStates.Visible;
			try
			{
				if(isProgressTrue)
				{
					if (isSelectedTitle == 1)
					{
						progressDialog = Android.App.ProgressDialog.Show(this.Activity, "", "Loading PopularInterests...");
					}
					else if (isSelectedTitle == 2)
					{
						progressDialog = Android.App.ProgressDialog.Show(this.Activity, "", "Loading MyInterests...");
					}
					isProgressTrue = false;
				}
				else
				{
					
				}
					objInterest.PopularInterests = null;
					lstMyInterest.Clear();
					string strUserInterests = await objRestServiceCall.CallUserInterestsFetechService(SplashScreenActivity.strEmail);
					objInterest = JsonConvert.DeserializeObject<InterestsDetails>(strUserInterests);
					if (objInterest.PopularInterests != null)
					{
						lstMyInterest = objInterest.PopularInterests;
					}

				if (isSelectedTitle==1)
				{
					if (objInterestAdapterClass != null)
					{
						//Un-subscribe to click event
						objInterestAdapterClass.FollowUnFollowButtonClick -= FnInterestsRowButtonTapped;
						objInterestAdapterClass.InterestRowClick -= FnInterestsRowTapped;
						objInterestAdapterClass = null;
					}
					objInterestAdapterClass = new InterestAdapterClass(this.Activity, lstInterest, lstMyInterest);
					objInterestAdapterClass.FollowUnFollowButtonClick += FnInterestsRowButtonTapped;
					objInterestAdapterClass.InterestRowClick += FnInterestsRowTapped;
					listInterestsDetails.Adapter = objInterestAdapterClass;
					if (strInterestId != "0")
					{
						foreach (var values in lstInterest)
						{
							if (values.Idinterestmaster.Equals(strInterestId))
							{
								break;
							}
							count++;
						}
						listInterestsDetails.SetSelection(count);
					}
				}
				else if(isSelectedTitle == 2)
				{
					MyInterestAdapterClass objMyInterestAdapterClass = new MyInterestAdapterClass(this.Activity, lstMyInterest);
					objMyInterestAdapterClass.FollowUnFollowButtonClick -= FnMyInterestButtonRowTapped;
					objMyInterestAdapterClass.FollowUnFollowButtonClick += FnMyInterestButtonRowTapped;
					objMyInterestAdapterClass.InterestRowClick -= FnInterestsRowTapped;
					objMyInterestAdapterClass.InterestRowClick += FnInterestsRowTapped;

					listInterestsDetails.Adapter = objMyInterestAdapterClass;
					if (strInterestId != "0")
					{
						foreach (var values in lstMyInterest)
						{
							if (values.Idinterestmaster.Equals(strInterestId))
							{
								break;
							}
							count++;
						}
						listInterestsDetails.SetSelection(count);
					}
				}

			}
			catch(Exception e)
			{
				FnProgressDialogClose();
			}
			FnProgressDialogClose();
			strInterestId = "0";
		}
		void FnInterestsRowTapped(PopularInterest popularInterest)
		{
			InterestDescriptionActivity.InterestPopularInterest = popularInterest;
			var intent = new Intent(Activity, typeof(InterestDescriptionActivity));
			intent.PutExtra("MyData", "Interest");
			StartActivity(intent);
		}

		//interest button row tapp
		async void FnInterestsRowButtonTapped(PopularInterest popularInterest)
		{
			isSelectedTitle = 1;
			strInterestId = popularInterest.Idinterestmaster;
			List<PopularInterest> lst = new List<PopularInterest>();
			List<PopularInterest> lstMyListInterests = new List<PopularInterest>();
			try
			{
				objInterest.PopularInterests = null;
				string strUserInterests = await objRestServiceCall.CallUserInterestsFetechService(SplashScreenActivity.strEmail);
				if (strUserInterests == "Exception" || string.IsNullOrEmpty(strUserInterests))
				{
					//objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this.Activity);
				}
				objInterest = JsonConvert.DeserializeObject<InterestsDetails>(strUserInterests);
				if (objInterest.PopularInterests != null)
				{
					lst = objInterest.PopularInterests.Where(x => x.Idinterestmaster.Equals(popularInterest.Idinterestmaster)).ToList();
					lstMyListInterests = objInterest.PopularInterests;
				}
				if (lst.Count == 0)
				{
					GAService.GetGASInstance().Track_App_Event(GAEventCategory.InterestListRowButton, strInterestName + " " + "(Interests Followed)");
					string strStoreInterests = await objRestServiceCall.CallInterestsStoreService(popularInterest.Idinterestmaster, SplashScreenActivity.strEmail);
					Console.WriteLine("Follow" + strStoreInterests);
					if (strStoreInterests == "Exception" || string.IsNullOrEmpty(strStoreInterests))
					{
						//objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this.Activity);
					}
				}
				else
				{
					strpopularInterestId = popularInterest.Idinterestmaster;
					isRemoveInterest = true;
					//strInterestId = popularInterest.Idinterestmaster;
					FnAlertMsgTwoInput(Constants.strAppName, Constants.strInterestsUnfollowMsg, "Yes", "No", this.Activity);
				}
			}
			catch (Exception e)
			{

			}
		}

		//binding myinterests to listview
		void FnMyInterestList()
		{
			selected = 1;
			isSelectedTitle = 2;
			listInterestsDetails.Adapter = null;
			BindInterestsList();
		}

		//MyInterestRowTapped
		void FnMyInterestButtonRowTapped(PopularInterest objInterests)
		{
			strpopularInterestId = objInterests.Idinterestmaster;
			isMyInterestsorMyEvents = 1;
			isRemoveInterest = false;
			strInterestId = objInterests.Idinterestmaster;
			strInterestName = objInterests.Interestname;
			FnAlertMsgTwoInput(Constants.strAppName, Constants.strInterestsUnfollowMsg, "Yes", "No", this.Activity);
		}

		async void FnRemoveInterests()
		{
			isProgressTrue = true;
			string strStoreInterests = await objRestServiceCall.CallInterestsStoreService(strpopularInterestId, SplashScreenActivity.strEmail);
			var obj = JsonConvert.DeserializeObject<InterestFollowing>(strStoreInterests);
			string strValue = obj.Following;
			if(strValue =="2")
			{
				if (isRemoveInterest)
				{
					BindInterestsList();
				}
				else
				{
					FnMyInterestList();
				}
			}
		}

		async void FnInterestSearch()
		{
			string strFullInterestsResults = await objRestServiceCall.CallFullInterestsService();
			if (strFullInterestsResults == "Exception" || string.IsNullOrEmpty(strFullInterestsResults))
			{
				return;
			}
			try
			{
				objInterestSearch = JsonConvert.DeserializeObject<InterestsDetails>(strFullInterestsResults);
				objInterestSearch.PopularInterests.TrimExcess();
				lstInterestSearch = objInterestSearch.PopularInterests;

			}
			catch(Exception e)
			{
				
			}
		}


		#endregion

		#region "Events"
		//binding suggested events list to listview
		async void FnEventList()
		{
			string strlatlong = SplashScreenActivity.strLatitude + "-" + SplashScreenActivity.strLangitude;
			string strDistance = "20";
			listInterestsDetails.Visibility = ViewStates.Gone;
			listEventDetails.Visibility = ViewStates.Visible;

				await FnEventListServiceCall(strlatlong, SplashScreenActivity.strAddress, strDistance);

			GoogleMapActivity.strValue = strlatlong;
			GoogleMapActivity.strDistance=strDistance;
			GoogleMapActivity.strCityName = SplashScreenActivity.strAddress;
		}

		async Task<bool> FnEventListServiceCall(string strLatLong, string strAddress, string strDistance)
		{
			bool isSuccess = false;
			GoogleMapActivity.SuggestOrMyEvent = 1;
			selected = 2;
			//strText = "EventsPage";
			if (HomeActivity.isEventScreen)
			{
				HomeActivity.txtSearch.Hint = "Search...";
				isSelectedTitle = 1;
				FnSelectedLable();
				lblTitleMy.Text = "My Event";
				lblTitlePopular.Text = "Suggested Event";
				lblTitleCreate.Text = "Create Event";
			}
			bool isConnected = objClsCommonFunctions.FnIsConnected(this.Activity);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this.Activity);
			}
			else
			{
				string strLatLongValue = string.Empty;
				if (strLatLong.Contains(","))
				{
					strLatLongValue = Regex.Replace(strLatLong, ",", ".");
				}
				else
				{
					strLatLongValue = strLatLong;
				}
				string strEventResults = await objRestServiceCall.CallEventService(strLatLongValue, strAddress, strDistance);
				strLatLongValue = string.Empty;
				strAddress = string.Empty;
				strDistance = string.Empty;

				if (strEventResults == "Exception" || string.IsNullOrEmpty(strEventResults))
				{
					return isSuccess;
				}
				try
				{
					objEventLocation = JsonConvert.DeserializeObject<EventLocation>(strEventResults);
					lstEvents.Clear();
					objEventLocation.EventsNearbyLocation.TrimExcess();
					lstEvents = objEventLocation.EventsNearbyLocation;
					if (HomeActivity.isEventScreen)
					{
						BindEventsList();
					}
					isSuccess = true;
				}
				catch(Exception e)
				{
					isSuccess = false;
					if (HomeActivity.isEventScreen)
					{
						BindEventsList();
					}
				}

			}
			return isSuccess;
		}
		//navigating to google map
		async void FnMyEventBinding()
		{
			int count = 0;
			HomeActivity.txtSearch.Hint = "Search...";
			selected = 2;
			isSelectedTitle = 2;
			FnSelectedLable();
			lblTitleMy.Text = "My Event";
			lblTitlePopular.Text = "Suggested Event";
			lblTitleCreate.Text = "Create Event";
			objEventLocation.EventsNearbyLocation = null;
			lstMyEvents.Clear();
			string strUserEvents = await objRestServiceCall.CallUserEventsFetechService(SplashScreenActivity.strEmail);
			objEventLocation = JsonConvert.DeserializeObject<EventLocation>(strUserEvents);
			if (objEventLocation.EventsNearbyLocation != null)
			{
				lstMyEvents = objEventLocation.EventsNearbyLocation;
			}
			MyEventsAdapterClass objMyEventsAdapterClass = new MyEventsAdapterClass(this.Activity, lstMyEvents);

			objMyEventsAdapterClass.AddRemoveButtonClick -= FnMyEventsButtonTapped;
			objMyEventsAdapterClass.AddRemoveButtonClick += FnMyEventsButtonTapped;
			objMyEventsAdapterClass.EventRowClick -= FnMyEventsRowTapped;
			objMyEventsAdapterClass.EventRowClick += FnMyEventsRowTapped;
			objMyEventsAdapterClass.GoogleMapClick -= FnMyEventsGoogleMapAddress;
			objMyEventsAdapterClass.GoogleMapClick += FnMyEventsGoogleMapAddress;
			listEventDetails.Adapter = objMyEventsAdapterClass;
			FnProgressDialogClose();
			if (isPosition)
			{
				foreach (var values in lstEvents)
				{
					if (values.Eventname.Equals(GoogleMapActivity.StrEventName))
					{
						break;
					}
					count++;
				}
				listEventDetails.SetSelection(count);
				isPosition = false;
			}
		}
		//Binding Suggested and my Events list
		async void BindEventsList()
		{
			//bool isSuccess = false;
			int count = 0;
			listEventDetails.Visibility = ViewStates.Visible;
			try
			{
				if (isProgressTrue)
				{
					if (isSelectedTitle == 1)
					{
						progressDialog = Android.App.ProgressDialog.Show(this.Activity, "", "Loading SuggestedEvents...");
					}
					else if (isSelectedTitle == 2)
					{
						progressDialog = Android.App.ProgressDialog.Show(this.Activity, "", "Loading MyEvents...");
					}
					isProgressTrue = false;
				}
				objEventLocation.EventsNearbyLocation = null;
				lstMyEvents.Clear();
				string strUserEvents = await objRestServiceCall.CallUserEventsFetechService(SplashScreenActivity.strEmail);
				objEventLocation = JsonConvert.DeserializeObject<EventLocation>(strUserEvents);
				if (objEventLocation.EventsNearbyLocation != null)
				{
					lstMyEvents = objEventLocation.EventsNearbyLocation;
				}
				if (isSelectedTitle == 1)
				{
					isChangeScreen = true;
					EventsAdapterClass objEventsAdapterClass = new EventsAdapterClass(this.Activity, lstEvents, lstMyEvents);
					//button tapped
					objEventsAdapterClass.AddRemoveButtonClick -= FnEventsButtonTapped;
					objEventsAdapterClass.AddRemoveButtonClick += FnEventsButtonTapped;
					objEventsAdapterClass.EventRowClicke -= FnEventRowTapped;
					objEventsAdapterClass.EventRowClicke += FnEventRowTapped;
					objEventsAdapterClass.GoogleMapClick -= FnEventsGoogleMapAddress;
					objEventsAdapterClass.GoogleMapClick += FnEventsGoogleMapAddress;
					listEventDetails.Adapter = objEventsAdapterClass;

					if (strEventsId != "0")
					{
						foreach (var values in lstEvents)
						{
							if (values.Idevents.Equals(strEventsId))
							{
								break;
							}
							count++;
						}
						listEventDetails.SetSelection(count);
					}
				}
				else if (isSelectedTitle == 2)
				{

					MyEventsAdapterClass objMyEventsAdapterClass = new MyEventsAdapterClass(this.Activity,lstMyEvents );

					objMyEventsAdapterClass.AddRemoveButtonClick -= FnMyEventsButtonTapped;
					objMyEventsAdapterClass.AddRemoveButtonClick += FnMyEventsButtonTapped;
					objMyEventsAdapterClass.EventRowClick -= FnMyEventsRowTapped;
					objMyEventsAdapterClass.EventRowClick += FnMyEventsRowTapped;
					objMyEventsAdapterClass.GoogleMapClick -= FnMyEventsGoogleMapAddress;
					objMyEventsAdapterClass.GoogleMapClick += FnMyEventsGoogleMapAddress;
					listEventDetails.Adapter = objMyEventsAdapterClass;

					if (strEventsId != "0")
					{
						foreach (var values in lstMyEvents)
						{
							if (values.Idevents.Equals(strEventsId))
							{
								break;
							}
							count++;
						}
						listEventDetails.SetSelection(count);
					}

				}
			}
			catch(Exception e)
			{
				FnProgressDialogClose();
			}
			FnProgressDialogClose();
			if (isPosition)
			{
				foreach(var values in lstEvents)
				{
					if(values.Eventname.Equals(GoogleMapActivity.StrEventName))
					{
						break;
					}
					count++;
				}
				listEventDetails.SetSelection(count);
				isPosition = false;
			}
			strEventsId = "0";
		}

		//my event row tapp
		void FnMyEventsRowTapped(EventsNearbyLocation objEvents)
		{
			GAService.GetGASInstance().Track_App_Event(GAEventCategory.EventListRow, objEvents.Eventname + " " + "Event Description Page(My Events)");
			isEventDescription = true;
			eventDescriptionPopUp = 1;
			EventProperty = objEvents;
			FnDescriptionLayout();
		}

		//suggested event row tapp
		void FnEventRowTapped(EventsNearbyLocation objEvents)
		{
			GAService.GetGASInstance().Track_App_Event(GAEventCategory.EventListRow, objEvents.Eventname + " " + "Event Description Page(Suggested Events)");
			PropertyList = objEvents;
			isEventDescription = true;
			eventDescriptionPopUp = 1;
			EventProperty = objEvents;
			FnDescriptionLayout();

		}

		//DescriptionLayout popup
		async void FnDescriptionLayout()
		{
			LayoutInflater password = LayoutInflater.From(this.Activity);
			View view = password.Inflate(Resource.Layout.DescriptionLayout, null);

			AlertDialog builder = new AlertDialog.Builder(this.Activity).Create();
			builder.SetView(view);
			lblEventName = view.FindViewById<TextView>(Resource.Id.lblEventTitleHL);
			lblEventDateAndTime = view.FindViewById<TextView>(Resource.Id.lblEventDateTimeHL);
			lblEventAddress = view.FindViewById<TextView>(Resource.Id.lblEventAddressHL);
			lblEventDescription = view.FindViewById<TextView>(Resource.Id.lblEventDescriptionHL);
			rlyEventDescriptionPage = view.FindViewById<RelativeLayout>(Resource.Id.rlyEventDescriptionPageHL);
			btnEventTicketInfo = view.FindViewById<Button>(Resource.Id.btnTicketInfoHL);
			btnAddEvent = view.FindViewById<Button>(Resource.Id.btnAddEventsHL);
			btnEventCostOrFree = view.FindViewById<Button>(Resource.Id.btnCostEventHL);
			imgEvent = view.FindViewById<ImageView>(Resource.Id.imageEventHL);


			objEventLocation.EventsNearbyLocation = null;
			if (eventDescriptionPopUp == 1)//suggested events and my events
			{
			string strUserEvents = await objRestServiceCall.CallUserEventsFetechService(SplashScreenActivity.strEmail);
			objEventLocation = JsonConvert.DeserializeObject<EventLocation>(strUserEvents);
			if (objEventLocation.EventsNearbyLocation != null)
			{
				lstMyEvents = objEventLocation.EventsNearbyLocation;
			}
			List<EventsNearbyLocation> value = lstMyEvents.Where(x => x.Idevents.Equals(EventProperty.Idevents)).ToList();
			if (value.Count > 0)
			{
				countValue = 1;
				btnAddEvent.SetBackgroundResource(Resource.Drawable.bluecheckmark);
			}
			else
			{
				countValue = 0;
				btnAddEvent.SetBackgroundResource(Resource.Drawable.plusmark);
			}
			rlyEventDescriptionPage.Visibility = ViewStates.Visible;

				//lnySearchLayout.Visibility = ViewStates.Invisible;

				DateTime datetime = Convert.ToDateTime(EventProperty.date);
				string strEventDate = datetime.ToString("MMMMM dd yyyy");

				lblEventName.SetTypeface(null, TypefaceStyle.Bold);
				strEventName = lblEventName.Text = EventProperty.Eventname.ToUpper();
				if (EventProperty.EventEndtime == "24:00:00")
				{
					lblEventDateAndTime.Text = strEventDate + " at " + EventProperty.Eventtime + "-" + "24:00";
				}
				else
				{
					if (EventProperty.EventEndtime == null || EventProperty.EventEndtime == "")
					{
						lblEventDateAndTime.Text = strEventDate + " at " + EventProperty.Eventtime + "-" + "00:00";

					}
					else
					{

						lblEventDateAndTime.Text = strEventDate + " at " + EventProperty.Eventtime + "-" + Convert.ToDateTime(EventProperty.EventEndtime).ToString("HH:mm"); 
					}
				}
				lblEventAddress.Text = EventProperty.Address;
				lblEventDescription.SetText(Html.FromHtml(EventProperty.Eventdesc), TextView.BufferType.Spannable);
				lblEventDescription.Clickable = true;
				lblEventDescription.MovementMethod = LinkMovementMethod.Instance;

				strEventCostUrl = EventProperty.Paymenturl;
				strDescriptionPageEventId = EventProperty.Idevents;
				if (string.IsNullOrEmpty(strEventCostUrl))
				{
					btnEventTicketInfo.Visibility = ViewStates.Gone;
				}
				else
				{
					btnEventTicketInfo.Visibility = ViewStates.Visible;
				}
				if (EventProperty.Ispaidevent == "0")
				{
					btnEventTicketInfo.Visibility = ViewStates.Gone;
					btnEventCostOrFree.SetBackgroundResource(Resource.Drawable.freeimage);

				}
				else
				{
					btnEventTicketInfo.Visibility = ViewStates.Visible;
					btnEventCostOrFree.SetBackgroundResource(Resource.Drawable.shoping_cart);
				}
				if (isSelectedTitle == 1)
				{
					if (!string.IsNullOrEmpty(EventProperty.Eventlogo))
					{
						Picasso.With(this.Activity).Load(EventProperty.Eventlogo).Into(imgEvent);
					}
					else
					{
						imgEvent.SetImageResource(Resource.Drawable.noimages);
					}
				}
				else if (isSelectedTitle == 2)
				{
					if (!string.IsNullOrEmpty(EventProperty.UrlEventslogo))
					{
						Picasso.With(this.Activity).Load(EventProperty.UrlEventslogo).Into(imgEvent);
					}
					else
					{
						imgEvent.SetImageResource(Resource.Drawable.noimages);
					}
				}


			}
			else if (eventDescriptionPopUp == 3)//home suggested events
			{
				string strUserEvents = await objRestServiceCall.CallUserEventsFetechService(SplashScreenActivity.strEmail);
				objEventLocation = JsonConvert.DeserializeObject<EventLocation>(strUserEvents);
				if (objEventLocation.EventsNearbyLocation != null)
				{
					lstMyEvents = objEventLocation.EventsNearbyLocation;
				}
				List<EventsNearbyLocation> value = lstMyEvents.Where(x => x.Idevents.Equals(HomeFeedEventPropertyList.Idevents)).ToList();
				if (value.Count > 0)
				{
					countValue = 1;
					btnAddEvent.SetBackgroundResource(Resource.Drawable.bluecheckmark);
				}
				else
				{
					countValue = 0;
					btnAddEvent.SetBackgroundResource(Resource.Drawable.plusmark);
				}
				rlyEventDescriptionPage.Visibility = ViewStates.Visible;

				//lnySearchLayout.Visibility = ViewStates.Invisible;

				DateTime datetime = Convert.ToDateTime(HomeFeedEventPropertyList.date);
				string strEventDate = datetime.ToString("MMMMM dd yyyy");

				lblEventName.SetTypeface(null, TypefaceStyle.Bold);
				strEventName = lblEventName.Text = HomeFeedEventPropertyList.Eventname.ToUpper();
				if (HomeFeedEventPropertyList.EventEndtime == "24:00:00")
				{
					lblEventDateAndTime.Text = strEventDate + " at " + HomeFeedEventPropertyList.Eventtime + "-" + "24:00";
				}
				else
				{
					if (HomeFeedEventPropertyList.EventEndtime == null || HomeFeedEventPropertyList.EventEndtime == "")
					{
						lblEventDateAndTime.Text = strEventDate + " at " + HomeFeedEventPropertyList.Eventtime + "-" + "00:00";

					}
					else
					{

						lblEventDateAndTime.Text = strEventDate + " at " + HomeFeedEventPropertyList.Eventtime + "-" + Convert.ToDateTime(HomeFeedEventPropertyList.EventEndtime).ToString("HH:mm");
					}
				}
				lblEventAddress.Text = HomeFeedEventPropertyList.Address;
				lblEventDescription.SetText(Html.FromHtml(HomeFeedEventPropertyList.Eventdesc), TextView.BufferType.Spannable);
				lblEventDescription.Clickable = true;
				lblEventDescription.MovementMethod = LinkMovementMethod.Instance;

				strEventCostUrl = HomeFeedEventPropertyList.Paymenturl;
				strDescriptionPageEventId = HomeFeedEventPropertyList.Idevents;
				if (string.IsNullOrEmpty(strEventCostUrl))
				{
					btnEventTicketInfo.Visibility = ViewStates.Gone;
				}
				else
				{
					btnEventTicketInfo.Visibility = ViewStates.Visible;
				}
				if (HomeFeedEventPropertyList.Ispaidevent == "0")
				{
					btnEventTicketInfo.Visibility = ViewStates.Gone;
					btnEventCostOrFree.SetBackgroundResource(Resource.Drawable.freeimage);

				}
				else
				{
					btnEventTicketInfo.Visibility = ViewStates.Visible;
					btnEventCostOrFree.SetBackgroundResource(Resource.Drawable.shoping_cart);
				}
				if (isSelectedTitle == 1)
				{
					if (!string.IsNullOrEmpty(HomeFeedEventPropertyList.Eventlogo))
					{
						Picasso.With(this.Activity).Load(HomeFeedEventPropertyList.Eventlogo).Into(imgEvent);
					}
					else
					{
						imgEvent.SetImageResource(Resource.Drawable.noimages);
					}
				}



			}
			else if (eventDescriptionPopUp == 2)//home post event
			{
				string strUserEvents = await objRestServiceCall.CallUserEventsFetechService(SplashScreenActivity.strEmail);
				objEventLocation = JsonConvert.DeserializeObject<EventLocation>(strUserEvents);
				if (objEventLocation.EventsNearbyLocation != null)
				{
					lstMyEvents = objEventLocation.EventsNearbyLocation;
				}
				List<EventsNearbyLocation> value = lstMyEvents.Where(x => x.Idevents.Equals(EventOrgfeed.Idevents)).ToList();
				if (value.Count > 0)
				{
					countValue = 1;
					btnAddEvent.SetBackgroundResource(Resource.Drawable.bluecheckmark);
				}
				else
				{
					countValue = 0;
					btnAddEvent.SetBackgroundResource(Resource.Drawable.plusmark);
				}
				rlyEventDescriptionPage.Visibility = ViewStates.Visible;

				DateTime datetime = Convert.ToDateTime(EventOrgfeed.Eventdate);
				string strEventDate = datetime.ToString("MMMMM dd yyyy");

				lblEventName.SetTypeface(null, TypefaceStyle.Bold);
				strEventName = lblEventName.Text = EventOrgfeed.Eventname.ToUpper();
				if (EventOrgfeed.Eventtime == "24:00:00")
				{
					lblEventDateAndTime.Text = strEventDate + " at " + EventOrgfeed.Eventtime + "-" + "24:00";
				}
				else
				{
					if (EventOrgfeed.Eventtime == null || EventOrgfeed.Eventtime == "")
					{
						lblEventDateAndTime.Text = strEventDate + " at " + EventOrgfeed.Eventtime + "-" + "00:00";

					}
					else
					{

						lblEventDateAndTime.Text = strEventDate + " at " + Convert.ToDateTime(EventOrgfeed.Eventtime).ToString("HH:mm");
					}
				}
				lblEventAddress.Text = EventOrgfeed.CityName;
				lblEventDescription.SetText(Html.FromHtml(EventOrgfeed.Eventdesc), TextView.BufferType.Spannable);
				lblEventDescription.Clickable = true;
				lblEventDescription.MovementMethod = LinkMovementMethod.Instance;
				strEventCostUrl = EventOrgfeed.Paymenturl;

				if (string.IsNullOrEmpty(strEventCostUrl))
				{
					btnEventTicketInfo.Visibility = ViewStates.Gone;
				}
				else
				{
					btnEventTicketInfo.Visibility = ViewStates.Visible;
				}
				if (EventOrgfeed.Ispaidevent == "0")
				{
					btnEventTicketInfo.Visibility = ViewStates.Gone;
					btnEventCostOrFree.SetBackgroundResource(Resource.Drawable.freeimage);

				}
				else
				{
					btnEventTicketInfo.Visibility = ViewStates.Visible;
					btnEventCostOrFree.SetBackgroundResource(Resource.Drawable.shoping_cart);
				}
				Picasso.With(this.Activity).Load(EventOrgfeed.Eventslogo).Into(imgEvent);

			}
			btnAddEvent.Click += async delegate (object sender, EventArgs e)
			{
				if (eventDescriptionPopUp==1)
				{
					if (countValue == 1)
					{
						if (isSelectedTitle == 1)
						{
							isRemoveInterest = true;
						}
						else if (isSelectedTitle == 2)
						{
							isRemoveInterest = false;
							isMyInterestsorMyEvents = 2;
						}

						strEventId = strDescriptionPageEventId;
						FnAlertMsgTwoInput(Constants.strAppName, Constants.strEventUnfollowMsg, "Yes", "No", this.Activity);

					}
					else
					{
						FnAlertMsgTwoInput1(Constants.strAppName, "Do You Want to Join This Event", "Yes", "No", this.Activity);
					}
				}
				else if (eventDescriptionPopUp == 3)
				{
					string strStoreEvents = await objRestServiceCall.CallEventsStoreService(HomeFeedEventPropertyList.Idevents, SplashScreenActivity.strEmail);
					var obj = JsonConvert.DeserializeObject<EventsAttending>(strStoreEvents);
					int strValue = obj.attending;
					if (strValue == 3)//2->event added 3->event removed
					{
						btnAddEvent.SetBackgroundResource(Resource.Drawable.plusmark);
					}
					else if (strValue == 2)
					{
						btnAddEvent.SetBackgroundResource(Resource.Drawable.bluecheckmark);
					}
				}
				else if (eventDescriptionPopUp == 2)
				{
					string strStoreEvents = await objRestServiceCall.CallEventsStoreService(EventOrgfeed.Idevents, SplashScreenActivity.strEmail);
					var obj = JsonConvert.DeserializeObject<EventsAttending>(strStoreEvents);
					int strValue = obj.attending;
					if (strValue == 3)//2->event added 3->event removed
					{
						btnAddEvent.SetBackgroundResource(Resource.Drawable.plusmark);
					}
					else if(strValue == 2)
					{
						btnAddEvent.SetBackgroundResource(Resource.Drawable.bluecheckmark);
					}
				}
			};
			btnEventTicketInfo.Click += delegate (object sender, EventArgs e)
			{
				isBrowserViewed = true;
				var uri = Android.Net.Uri.Parse(strEventCostUrl);
				var intent = new Intent(Intent.ActionView, uri);
				StartActivity(intent);

			};
			lblEventDescription.Click += delegate (object sender, EventArgs e)
			{
				isBrowserViewed = true;
			};
			builder.Show();
		}
		//suggested event button tapp
		void FnEventsButtonTapped(EventsNearbyLocation objEventsNearbyLocation)
		{
			strEventsId = objEventsNearbyLocation.Idevents;
			FnEventSave(objEventsNearbyLocation);
			strEventId = objEventsNearbyLocation.Idevents;
		}
		//my event button tapp
		void FnMyEventsButtonTapped(EventsNearbyLocation objEvents)
		{
			strEventsId = objEvents.Idevents;
			isMyInterestsorMyEvents = 2;
			isRemoveInterest = false;
			strEventId = objEvents.Idevents;
			strEventName = objEvents.Eventname;
			FnAlertMsgTwoInput(Constants.strAppName, Constants.strEventUnfollowMsg, "Yes", "No", this.Activity);
		}
		//suggested event address click
		void FnEventsGoogleMapAddress(EventsNearbyLocation objEventsNearbyLocation)
		{
			GoogleMapActivity.SuggestOrMyEvent = 1;
			isLocation = false;
			if (isChangeScreen)
			{
				number = 0;
				screenSlide = 2;
			}
			else
			{
				number = 1;
				screenSlide = 1;
			}
			isSingleOrMultipleEvent = true;
			GoogleMapActivity.strLatLongs = objEventsNearbyLocation.Eventcity;
			GoogleMapActivity.SingleEventProperty = objEventsNearbyLocation;
			var intent = new Intent(Activity, typeof(GoogleMapActivity));
			StartActivity(intent);
			//Activity.Finish();

		}
		//my event address click
		void FnMyEventsGoogleMapAddress(EventsNearbyLocation objEventsNearbyLocation)
		{
			screenSlide = 2;
			GoogleMapActivity.SuggestOrMyEvent = 2;
			isLocation = false;
			isSingleOrMultipleEvent = true;
			GoogleMapActivity.strLatLongs = objEventsNearbyLocation.Eventcity;
			GoogleMapActivity.SingleEventProperty = objEventsNearbyLocation;
			var intent = new Intent(Activity, typeof(GoogleMapActivity));
			StartActivity(intent);
			//Activity.Finish();

		}


		//events Removing
		async void FnRemoveEvents()
		{
			isProgressTrue = true;
			string strStoreEvents = await objRestServiceCall.CallEventsStoreService(strEventId, SplashScreenActivity.strEmail);
			var obj = JsonConvert.DeserializeObject<EventsAttending>(strStoreEvents);
			int strValue = obj.attending;
			if (strValue == 3)//2->event added 3->event removed
			{
				if (isRemoveInterest)
				{
					isSelectedTitle = 1;
					BindEventsList();
					Console.WriteLine("event removed");
				}
				else
				{
					FnMyEventList();
				}
			}

		}



		void FnMyEventList()
		{
			isSelectedTitle = 2;
			listEventDetails.Adapter = null;
			BindEventsList();
		}

		//save events
		async void FnEventSave(EventsNearbyLocation objEventsNearbyLocation)
		{
			if (objEventsNearbyLocation == null)
			{
				objEventsNearbyLocation = PropertyList;
			}
			else
			{
				//isSelectedTitle = true;
				List<EventsNearbyLocation> lst = new List<EventsNearbyLocation>();
				EventsNearbyLocation objEvents = new EventsNearbyLocation();
				List<EventsNearbyLocation> lstMyListEvents = new List<EventsNearbyLocation>();

				try
				{
					objEventLocation.EventsNearbyLocation = null;
					string strUserEvents = await objRestServiceCall.CallUserEventsFetechService(SplashScreenActivity.strEmail);
					if (strUserEvents == "Exception" || string.IsNullOrEmpty(strUserEvents))
					{
						//objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this.Activity);
					}
					objEventLocation = JsonConvert.DeserializeObject<EventLocation>(strUserEvents);
					if (objEventLocation.EventsNearbyLocation != null)
					{
						lst = objEventLocation.EventsNearbyLocation.Where(x => x.Idevents.Equals(objEventsNearbyLocation.Idevents)).ToList();
						lstMyListEvents.AddRange(objEventLocation.EventsNearbyLocation);
					}
					if (lst.Count == 0 )
					{
						string strStoreEvent = await objRestServiceCall.CallEventsStoreService(objEventsNearbyLocation.Idevents, SplashScreenActivity.strEmail);
						Console.WriteLine("a" + strStoreEvent);
						if (strStoreEvent == "Exception" || string.IsNullOrEmpty(strStoreEvent))
						{
							//objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this.Activity);
						}
						GAService.GetGASInstance().Track_App_Event(GAEventCategory.EventListRowButton, strEventName + " " + "(Event Joined )");
						objEventLocation.EventsNearbyLocation = lstMyListEvents;
						var strResult = JsonConvert.SerializeObject(objEventLocation);
						if (isSelectedTitle == 1)
						{
							BindEventsList();
						}
						else if (isSelectedTitle == 2)
						{
							FnMyEventList();
						}
					}
					else
					{
						isRemoveInterest = true;
						strEventId = objEventsNearbyLocation.Idevents;
						FnAlertMsgTwoInput(Constants.strAppName, Constants.strEventUnfollowMsg, "Yes", "No", this.Activity);
					}
				}
				catch(Exception e)
				{

				}
			}
		}
		//home feed event save
		async void FnHomeFeedEventSave(Homefeedsfgeoevent objEventsNearbyLocation)
		{
			int count = 0;
			string strStoreEvent = "";
			if (objEventsNearbyLocation == null)
			{
				objEventsNearbyLocation = HomeFeedPropertyList;
			}
			else
			{
				//isSelectedTitle = true;
				List<EventsNearbyLocation> lst = new List<EventsNearbyLocation>();
				EventsNearbyLocation objEvents = new EventsNearbyLocation();
				List<EventsNearbyLocation> lstMyListEvents = new List<EventsNearbyLocation>();

				try
				{
					objEventLocation.EventsNearbyLocation = null;
					string strUserEvents = await objRestServiceCall.CallUserEventsFetechService(SplashScreenActivity.strEmail);
					if (strUserEvents == "Exception" || string.IsNullOrEmpty(strUserEvents))
					{
						//objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this.Activity);
					}
					objEventLocation = JsonConvert.DeserializeObject<EventLocation>(strUserEvents);
					if (objEventLocation.EventsNearbyLocation != null)
					{
						
						lst = objEventLocation.EventsNearbyLocation.Where(x => x.Idevents.Equals(objEventsNearbyLocation.Idevents)).ToList();
						lstMyListEvents.AddRange(objEventLocation.EventsNearbyLocation);
					}
					if (lst.Count == 0)
					{
						 strStoreEvent = await objRestServiceCall.CallEventsStoreService(objEventsNearbyLocation.Idevents, SplashScreenActivity.strEmail);
						Console.WriteLine("a" + strStoreEvent);
						if (strStoreEvent == "Exception" || string.IsNullOrEmpty(strStoreEvent))
						{
							//objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this.Activity);
						}

					}
					else
					{
						strStoreEvent = await objRestServiceCall.CallEventsStoreService(objEventsNearbyLocation.Idevents, SplashScreenActivity.strEmail);
						Console.WriteLine("a" + strStoreEvent);
						if (strStoreEvent == "Exception" || string.IsNullOrEmpty(strStoreEvent))
						{
							//objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this.Activity);
						}

					}
					strUserEvents = await objRestServiceCall.CallUserEventsFetechService(SplashScreenActivity.strEmail);
					objEventLocation = JsonConvert.DeserializeObject<EventLocation>(strUserEvents);
					lstMyEvents = objEventLocation.EventsNearbyLocation;

					HomeFeedEventsAdapterClass objEventsAdapterClass = new HomeFeedEventsAdapterClass(this.Activity, lstHomeEvents, lstMyEvents);
					//button tapped
					objEventsAdapterClass.AddRemoveButtonClick -= FnHomeFeedEventsButtonTapped;
					objEventsAdapterClass.AddRemoveButtonClick += FnHomeFeedEventsButtonTapped;
					objEventsAdapterClass.EventRowClicke -= FnHomeFeedEventRowTapped;
					objEventsAdapterClass.EventRowClicke += FnHomeFeedEventRowTapped;
					objEventsAdapterClass.GoogleMapClick -= FnHomeFeedEventsGoogleMapAddress;
					objEventsAdapterClass.GoogleMapClick += FnHomeFeedEventsGoogleMapAddress;
					listHomeFeed.Adapter = objEventsAdapterClass;

						foreach (var values in lstHomeEvents)
						{
						if (values.Eventname.Equals(objEventsNearbyLocation.Eventname))
							{
								break;
							}
							count++;
						}
						listHomeFeed.SetSelection(count);
					}

				catch (Exception e)
				{

				}
			}
		}
		//Event Decription Page
		async void FnEventDescriptionPage(EventsNearbyLocation objEventsNearbyLocation)
		{
			
			objEventLocation.EventsNearbyLocation = null;
			string strUserEvents = await objRestServiceCall.CallUserEventsFetechService(SplashScreenActivity.strEmail);
			objEventLocation = JsonConvert.DeserializeObject<EventLocation>(strUserEvents);
			if (objEventLocation.EventsNearbyLocation != null)
			{
				lstMyEvents = objEventLocation.EventsNearbyLocation;
			}
			List<EventsNearbyLocation> value = lstMyEvents.Where(x => x.Idevents.Equals(objEventsNearbyLocation.Idevents)).ToList();
			if (value.Count > 0)
			{
				countValue = 1;
				btnAddEvent.SetBackgroundResource(Resource.Drawable.bluecheckmark);
			}
			else
			{
				countValue = 0;
				btnAddEvent.SetBackgroundResource(Resource.Drawable.plusmark);
			}
			//lnySearchLayout.Visibility = ViewStates.Invisible;
			rlyEventDescriptionPage.Visibility = ViewStates.Visible;
			DateTime datetime = Convert.ToDateTime(objEventsNearbyLocation.date);
			string strEventDate = datetime.ToString("MMMMM dd yyyy");

			lblEventName.SetTypeface(null, TypefaceStyle.Bold);
			strEventName = lblEventName.Text = objEventsNearbyLocation.Eventname.ToUpper();
			if (objEventsNearbyLocation.EventEndtime == "24:00:00")
			{
				lblEventDateAndTime.Text = strEventDate + " at " + objEventsNearbyLocation.Eventtime + "-" + "24:00";
			}
			else
			{
				if (objEventsNearbyLocation.EventEndtime == null || objEventsNearbyLocation.EventEndtime == "")
				{
					lblEventDateAndTime.Text = strEventDate + " at " + objEventsNearbyLocation.EventEndtime + "-" + "00:00";

				}
				else
				{
					lblEventDateAndTime.Text = strEventDate + " at " + objEventsNearbyLocation.Eventtime + "-" + Convert.ToDateTime(objEventsNearbyLocation.EventEndtime).ToString("HH:mm"); ;
				}
			}
			lblEventAddress.Text = objEventsNearbyLocation.Address;
			lblEventDescription.SetText(Html.FromHtml(objEventsNearbyLocation.Eventdesc), TextView.BufferType.Spannable);
			lblEventDescription.Clickable = true;
			lblEventDescription.MovementMethod = LinkMovementMethod.Instance;

			strEventCostUrl = objEventsNearbyLocation.Paymenturl;
			strDescriptionPageEventId = objEventsNearbyLocation.Idevents;
			if (string.IsNullOrEmpty(strEventCostUrl))
			{
				btnEventTicketInfo.Visibility = ViewStates.Gone;
			}
			else
			{
				btnEventTicketInfo.Visibility = ViewStates.Visible;
			}
			if (objEventsNearbyLocation.Ispaidevent == "0")
			{
				btnEventTicketInfo.Visibility = ViewStates.Gone;
				btnEventCostOrFree.SetBackgroundResource(Resource.Drawable.freeimage);

			}
			else
			{
				btnEventTicketInfo.Visibility = ViewStates.Visible;
				btnEventCostOrFree.SetBackgroundResource(Resource.Drawable.shoping_cart);
			}
			if (!string.IsNullOrEmpty(objEventsNearbyLocation.Eventlogo))
			{
				Picasso.With(this.Activity).Load(objEventsNearbyLocation.Eventlogo).Into(imgEvent);
			}
			else
			{
				imgEvent.SetImageResource(Resource.Drawable.noimages);
			}
		}

		#endregion

		#region "Group"
		async void FnGroup()
		{
			selected = 3;
			isSelectedTitle = 1;
			HomeActivity.txtSearch.Hint = "Search...";
			lblTitleMy.Text = "My Group";
			lblTitlePopular.Text = "Suggested Group";
			lblTitleCreate.Text = "Create Group";
			FnSelectedLable();
			listGroupDetails.Visibility = ViewStates.Visible;
			listEventDetails.Visibility = ViewStates.Gone;
			listInterestsDetails.Visibility = ViewStates.Gone;

			bool isConnected = objClsCommonFunctions.FnIsConnected(this.Activity);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this.Activity);
			}
			else
			{
				string strGroupResults = await objRestServiceCall.CallGroupsSuggestedService(SplashScreenActivity.strEmail);
				if (strGroupResults == "Exception" || string.IsNullOrEmpty(strGroupResults))
				{
					//objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this.Activity);
					return;
				}
				try
				{
					objGroupDetailsClass = JsonConvert.DeserializeObject<GroupDetailsClass>(strGroupResults);
					objGroupDetailsClass.Mygroups.TrimExcess();
					lstGroup = objGroupDetailsClass.Mygroups;
					BindingGroupList();
				}
				catch(Exception e)
				{

				}
			}
		}

		async	void BindingGroupList()
		{
			int count = 0;
			listGroupDetails.Visibility = ViewStates.Visible;
			try
			{
				if (isProgressTrue)
				{
					if (isSelectedTitle == 1)
					{
						progressDialog = Android.App.ProgressDialog.Show(this.Activity, "", "Loading SuggestedGroup...");
					}
					else if (isSelectedTitle == 2)
					{
						progressDialog = Android.App.ProgressDialog.Show(this.Activity, "", "Loading MyGroups...");
					}
					isProgressTrue = false;
				}

				objGroupDetailsClass.Mygroups = null;
				lstMyGroup.Clear();
				string strUserGroups = await objRestServiceCall.CallUserGroupsFetechService(SplashScreenActivity.strEmail);
				objGroupDetailsClass = JsonConvert.DeserializeObject<GroupDetailsClass>(strUserGroups);
				if (objGroupDetailsClass.Mygroups != null)
				{
					lstMyGroup =objGroupDetailsClass.Mygroups;
				}

				if (isSelectedTitle == 1)
				{
					if (objGroupsAdapterClass != null)
					{
						//Un-subscribe to click event
						objGroupsAdapterClass.GroupFollowUnFollowButtonClick -= FnGroupFollowButtonTapped;
						objGroupsAdapterClass = null;
					}
					objGroupsAdapterClass = new GroupsAdapterClass(this.Activity, lstGroup, lstMyGroup);
					objGroupsAdapterClass.GroupFollowUnFollowButtonClick += FnGroupFollowButtonTapped;
					objGroupsAdapterClass.GroupRowClick -= FnGroupRowTapped;
					objGroupsAdapterClass.GroupRowClick += FnGroupRowTapped;
					listGroupDetails.Adapter = objGroupsAdapterClass;
					if (strGroupsId != "0")
					{
						foreach (var values in lstGroup)
						{
							if (values.Idgroups.Equals(strGroupsId))
							{
								break;
							}
							count++;
						}
						listGroupDetails.SetSelection(count);
					}
				}
				else if (isSelectedTitle == 2)
				{
					List<Mygroup> lstFilterList = lstMyGroup.Where(x => x.Isamember == "1").ToList();
					MyGroupAdapterClass objMyInterestAdapterClass = new MyGroupAdapterClass(this.Activity, lstFilterList);
					objMyInterestAdapterClass.GroupFollowUnFollowButtonClick -= FnMyGroupButtonTapped;
					objMyInterestAdapterClass.GroupFollowUnFollowButtonClick += FnMyGroupButtonTapped;
					objMyInterestAdapterClass.MyGroupRowClick -= FnMyGroupRowTapped;
					objMyInterestAdapterClass.MyGroupRowClick += FnMyGroupRowTapped;
					listGroupDetails.Adapter = objMyInterestAdapterClass;
						if (strGroupsId != "0")
						{
							foreach (var values in lstFilterList)
							{
								if (values.Idgroups.Equals(strGroupsId))
								{
									break;
								}
								count++;
							}
							listGroupDetails.SetSelection(count);
						}
				}
			}
			catch(Exception e)
			{
				FnProgressDialogClose();
			}
			FnProgressDialogClose();
			strGroupsId = "0";
			if (!isGroupPosition)
			{
				foreach (var values in lstGroup)
				{
					if (values.Idgroups.Equals(strSuggestedGroupId))
					{
						break;
					}
					count++;
				}
				listGroupDetails.SetSelection(count);
				isGroupPosition = true;

			}
		}


		//MygroupbuttonTapped
		void FnMyGroupButtonTapped(Mygroup objMygroup)
		{
			strGroupsId = objMygroup.Idgroups;
			strGroupId = objMygroup.Idgroups;
			isMyInterestsorMyEvents = 3;
			isRemoveInterest = false;
			strInterestName = objMygroup.Groupname;
			FnAlertMsgTwoInput(Constants.strAppName, Constants.strInterestsUnfollowMsg, "Yes", "No", this.Activity);
		}
		//suggested group row tapped
		async void FnGroupRowTapped(Mygroup objMyGroup)
		{
			isGroupPosition = false;
			strSuggestedGroupId = objMyGroup.Idgroups;
			LayoutInflater password = LayoutInflater.From(this.Activity);
			View view = password.Inflate(Resource.Layout.SuggestedGroupLayout, null);

			AlertDialog builder = new AlertDialog.Builder(this.Activity).Create();
			builder.SetView(view);
			imgGroupImage = view.FindViewById<ImageView>(Resource.Id.imgGCHFL);
			lblGroupName = view.FindViewById<TextView>(Resource.Id.lblGroupNameGCHFL);
			lblGroupDescription = view.FindViewById<TextView>(Resource.Id.lblGroupDescriptionGCHFL);
			btnJoin = view.FindViewById<Button>(Resource.Id.btnJoinGCHFL);

			lblGroupName.Text = objMyGroup.Groupname;
			lblGroupDescription.SetText(Html.FromHtml(objMyGroup.Groupdesc), TextView.BufferType.Spannable);
			Picasso.With(this.Activity).Load(objMyGroup.Groupslogo).Into(imgGroupImage);

			lstMyGroup.Clear();
			string strUserGroups = await objRestServiceCall.CallUserGroupsFetechService(SplashScreenActivity.strEmail);
			objGroupDetailsClass = JsonConvert.DeserializeObject<GroupDetailsClass>(strUserGroups);
			if (objGroupDetailsClass.Mygroups != null)
			{
				List<Mygroup> listValue = objGroupDetailsClass.Mygroups.Where(x => x.Idgroups.Equals(objMyGroup.Idgroups)).ToList();
				if (listValue.Count > 0)
				{
					if (listValue[0].Idgroups == objMyGroup.Idgroups)
					{
						if (listValue[0].Isamember == "2")
						{
							btnJoin.SetBackgroundResource(Resource.Drawable.request);
						}
						else if (listValue[0].Isamember == "1")
						{
							btnJoin.SetBackgroundResource(Resource.Drawable.joined);
						}
					}
					else
					{
						btnJoin.SetBackgroundResource(Resource.Drawable.join);
					}
				}
				else
				{
					btnJoin.SetBackgroundResource(Resource.Drawable.join);
				}
			}

			btnJoin.Click += async delegate
			{
				progressDialog = Android.App.ProgressDialog.Show(this.Activity, "", "Loading...");
				bool isTrues = await FnGroupJoinOrUnJoin(strSuggestedGroupId);
				if (isTrues)
				{
					FnProgressDialogClose();
					bool isConnected = objClsCommonFunctions.FnIsConnected(this.Activity);
					if (!isConnected)
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this.Activity);
					}
					else
					{
						string strUserGroups1 = await objRestServiceCall.CallUserGroupsFetechService(SplashScreenActivity.strEmail);
						objGroupDetailsClass = JsonConvert.DeserializeObject<GroupDetailsClass>(strUserGroups1);
						foreach (var s in objGroupDetailsClass.Mygroups)
						{
							if (s.Idgroups == strSuggestedGroupId)
							{
								if (s.Isamember == "2")
								{
									btnJoin.SetBackgroundResource(Resource.Drawable.request);
								}
								else if (s.Isamember == "1")
								{
									btnJoin.SetBackgroundResource(Resource.Drawable.joined);
								}
								else
								{
									btnJoin.SetBackgroundResource(Resource.Drawable.join);
								}
								break;
							}
						}
					}
				}
				FnProgressDialogClose();
			};
			builder.Show();
		}
		void FnMyGroupRowTapped(Mygroup objMygroup)
		{
			MyGroupDescriptionActivity.MyGroupProperty = objMygroup;
			var intent = new Intent(Activity, typeof(MyGroupDescriptionActivity));
			intent.PutExtra("MyData", "Group");
			StartActivity(intent);
		}
		async void FnGroupFollowButtonTapped(Mygroup objMyGroup)
		{
			strGroupsId = objMyGroup.Idgroups;
			await FnGroupJoinOrUnJoin(objMyGroup.Idgroups);
		}
		async Task<bool> FnGroupJoinOrUnJoin(string strGroupIds)
		{
			bool isSuccess = false;
			isSelectedTitle = 1;
			List<Mygroup> lst = new List<Mygroup>();
			try
			{
				objGroupDetailsClass.Mygroups = null;
				string strUserGroups = await objRestServiceCall.CallUserGroupsFetechService(SplashScreenActivity.strEmail);
				if (strUserGroups == "Exception" || string.IsNullOrEmpty(strUserGroups))
				{
					//objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this.Activity);
				}
				objGroupDetailsClass = JsonConvert.DeserializeObject<GroupDetailsClass>(strUserGroups);
				if (objGroupDetailsClass.Mygroups != null)
				{
					lst = objGroupDetailsClass.Mygroups.Where(x => x.Idgroups.Equals(strGroupIds)).ToList();
				}
				if (lst.Count == 0)
				{
					//GAService.GetGASInstance().Track_App_Event(GAEventCategory.InterestListRowButton, strInterestName + " " + "(Interests Followed)");
					string strStoreGroups = await objRestServiceCall.CallJoinOrUnjoinGroupService(strGroupIds, SplashScreenActivity.strUserId);
					if (strStoreGroups == "Exception" || string.IsNullOrEmpty(strStoreGroups))
					{
						//objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this.Activity);
					}
					BindingGroupList();
					isSuccess = true;
				}
				else
				{

					isRemoveInterest = true;
					strGroupId = strGroupIds;
					FnAlertMsgTwoInput(Constants.strAppName, Constants.strGroupUnfollowMsg, "Yes", "No", this.Activity);
					isSuccess = true;
				}
			}
			catch (Exception e)
			{
				isSuccess = false;
			}
			return isSuccess;
		}
		async void FnRemoveGroups()
		{
			isProgressTrue = true;
			string strStoreGroups = await objRestServiceCall.CallJoinOrUnjoinGroupService(strGroupId,SplashScreenActivity.strUserId);
			var obj = JsonConvert.DeserializeObject<GroupAdding>(strStoreGroups);
			foreach(var values in obj.array)
			{
				if (values.Isamember == "0")
				{
					if (isRemoveInterest)
					{
						BindingGroupList();
					}
					else
					{
						FnMyGroupList();
					}
				}
			}
		}
		void FnMyGroupList()
		{
			isSelectedTitle = 2;
			listGroupDetails.Adapter = null;
			BindingGroupList();
		}

		#endregion

		#region "SearchInterestsOrEvents"
		void FnSearchInterestsOrEvents()
		{
			if (selected == 1)
			{
				//****************interests search****************
				List<PopularInterest> lstinterestFilter = new List<PopularInterest>();
				//<><><><><><><><><><>><><>popular interests search<><><><><><><><><><>><><>
				if (isSelectedTitle==1)
				{
					GAService.GetGASInstance().Track_App_Event(GAEventCategory.Search, "Popular interests search");
					issearch = true;
					foreach (var objAllDetail in lstInterestSearch)
					{
						if (objAllDetail.Interestname.ToLower().Contains(HomeActivity.txtSearch.Text.Trim().ToLower()))
						{
							lstinterestFilter.Add(objAllDetail);
						}
					}
					listInterestsDetails.Adapter = null;
					objInterestAdapterClass = new InterestAdapterClass(this.Activity, lstinterestFilter, lstMyInterest);
					objInterestAdapterClass.FollowUnFollowButtonClick -= FnInterestsRowButtonTapped;
					objInterestAdapterClass.FollowUnFollowButtonClick += FnInterestsRowButtonTapped;
					objInterestAdapterClass.InterestRowClick -= FnInterestsRowTapped;
					objInterestAdapterClass.InterestRowClick += FnInterestsRowTapped;
					listInterestsDetails.Adapter = objInterestAdapterClass;
				}
				//<><><><><><><><><><>><><>myinterests search<><><><><><><><><><>><><>
				else if (isSelectedTitle == 2)
				{
					lstInterestsSearch.Clear();
					GAService.GetGASInstance().Track_App_Event(GAEventCategory.Search, "Myinterests search");
					issearch = false;
						foreach (var objAllDetail in lstMyInterest)
						{
							if (objAllDetail.Interestname.ToLower().Contains(HomeActivity.txtSearch.Text.Trim().ToLower()))
							{
								lstInterestsSearch.Add(objAllDetail);
							}
						}
					lstinterestFilter = lstInterestsSearch;
					MyInterestAdapterClass objMyInterestAdapterClass = new MyInterestAdapterClass(this.Activity, lstinterestFilter);
					objMyInterestAdapterClass.FollowUnFollowButtonClick -= FnMyInterestButtonRowTapped;
					objMyInterestAdapterClass.FollowUnFollowButtonClick += FnMyInterestButtonRowTapped;
					objMyInterestAdapterClass.InterestRowClick -= FnInterestsRowTapped;
					objMyInterestAdapterClass.InterestRowClick += FnInterestsRowTapped;
					listInterestsDetails.Adapter = objMyInterestAdapterClass;
				}

			}
			else if (selected == 2)
			{
				//****************event search****************
				if (lstEventssSearch != null)
				{
					lstEventssSearch.Clear();
				}
				//<><><><><><><><><><>><><>suggested event search<><><><><><><><><><>><><>
				if (isSelectedTitle == 1)
				{
					GAService.GetGASInstance().Track_App_Event(GAEventCategory.Search, "Suggested event search");
					issearch = true;
					foreach (var objAllDetail in lstEvents)
					{
						if (objAllDetail.Eventname.ToLower().Contains(HomeActivity.txtSearch.Text.Trim().ToLower()))
						{
							lstEventssSearch.Add(objAllDetail);
						}
					}
					isChangeScreen = true;
					listEventDetails.Adapter = null;
					EventsAdapterClass objEventsAdapterClass = new EventsAdapterClass(this.Activity, lstEventssSearch, lstMyEvents);
					objEventsAdapterClass.AddRemoveButtonClick -= FnEventsButtonTapped;
					objEventsAdapterClass.AddRemoveButtonClick += FnEventsButtonTapped;
					objEventsAdapterClass.GoogleMapClick -= FnEventsGoogleMapAddress;
					objEventsAdapterClass.GoogleMapClick += FnEventsGoogleMapAddress;
					objEventsAdapterClass.EventRowClicke -= FnEventRowTapped;
					objEventsAdapterClass.EventRowClicke += FnEventRowTapped;
					listEventDetails.Adapter = objEventsAdapterClass;
				}
				//<><><><><><><><><><>><><>myevent search<><><><><><><><><><>><><>
				else if (isSelectedTitle == 2)
				{
					GAService.GetGASInstance().Track_App_Event(GAEventCategory.Search, "Myevent search");
					issearch = false;
					foreach (var objAllDetail in lstMyEvents)
						{
							if (objAllDetail.Eventname.ToLower().Contains(HomeActivity.txtSearch.Text.Trim().ToLower()))
							{
								lstEventssSearch.Add(objAllDetail);
							}
						}
					MyEventsAdapterClass objMyEventsAdapterClass = new MyEventsAdapterClass(this.Activity, lstEventssSearch);

					objMyEventsAdapterClass.AddRemoveButtonClick -= FnMyEventsButtonTapped;
					objMyEventsAdapterClass.AddRemoveButtonClick += FnMyEventsButtonTapped;
					objMyEventsAdapterClass.EventRowClick -= FnMyEventsRowTapped;
					objMyEventsAdapterClass.EventRowClick += FnMyEventsRowTapped;
					objMyEventsAdapterClass.GoogleMapClick -= FnMyEventsGoogleMapAddress;
					objMyEventsAdapterClass.GoogleMapClick += FnMyEventsGoogleMapAddress;
					listEventDetails.Adapter = objMyEventsAdapterClass;
				}


			}
			//****************group search****************
			else if(selected==3)
			{
				if (lstGroupsSearch != null)
				{
					lstGroupsSearch.Clear();
				}
				//<><><><><><><><><><>><><>suggested group<><><><><><><><><><>><><>
				if (isSelectedTitle == 1)
				{
					//GAService.GetGASInstance().Track_App_Event(GAEventCategory.Search, "Suggested group search");
						issearch = true;
					foreach (var objAllDetail in lstGroup)
						{
						if (objAllDetail.Groupname.ToLower().Contains(HomeActivity.txtSearch.Text.Trim().ToLower()))
							{
								lstGroupsSearch.Add(objAllDetail);
							}
						}
					objGroupsAdapterClass = new GroupsAdapterClass(this.Activity, lstGroupsSearch, lstMyGroup);
					objGroupsAdapterClass.GroupFollowUnFollowButtonClick += FnGroupFollowButtonTapped;
					objGroupsAdapterClass.GroupRowClick -= FnGroupRowTapped;
					objGroupsAdapterClass.GroupRowClick += FnGroupRowTapped;
					listGroupDetails.Adapter = objGroupsAdapterClass;
				}
				//<><><><><><><><><><>><><>my groups<><><><><><><><><><>><><>
				else if (isSelectedTitle == 2)
				{
					//GAService.GetGASInstance().Track_App_Event(GAEventCategory.Search, "Mygroup search");
					issearch = false;
					foreach (var objAllDetail in lstMyGroup)
					{
						if (objAllDetail.Groupname.ToLower().Contains(HomeActivity.txtSearch.Text.Trim().ToLower()))
						{
							lstGroupsSearch.Add(objAllDetail);
						}
					}
					List<Mygroup> lstFilterList = lstGroupsSearch.Where(x => x.Isamember == "1").ToList();
					MyGroupAdapterClass objMyInterestAdapterClass = new MyGroupAdapterClass(this.Activity, lstFilterList);
					objMyInterestAdapterClass.GroupFollowUnFollowButtonClick -= FnMyGroupButtonTapped;
					objMyInterestAdapterClass.GroupFollowUnFollowButtonClick += FnMyGroupButtonTapped;
					objMyInterestAdapterClass.MyGroupRowClick -= FnMyGroupRowTapped;
					objMyInterestAdapterClass.MyGroupRowClick += FnMyGroupRowTapped;
					listGroupDetails.Adapter = objMyInterestAdapterClass;

				}
			}
		}
		#endregion

		#region "Message Chat"
	async void FnMessageChat()
		{
			selected = 4;
			isChatSingleOrGroup = true;
			lblTitleCreate.Visibility = ViewStates.Gone;
			HomeActivity.txtSearch.Hint="Search...";
			isSelectedTitle = 1;
			FnSelectedLable();
			lblTitleMy.Text = "New Message";
			lblTitlePopular.Text = "Member Chatlist";
			listInterestsDetails.Visibility= ViewStates.Gone;
			listEventDetails.Visibility = ViewStates.Gone;
			listChatMember.Visibility = ViewStates.Visible;
			bool isConnected = objClsCommonFunctions.FnIsConnected(this.Activity);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this.Activity);
			}
			else
			{
				string strChatResults = await objRestServiceCall.CallChatMemberService(SplashScreenActivity.strUserId);
				if (strChatResults == "Exception" || string.IsNullOrEmpty(strChatResults))
				{
					//objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this.Activity);
					return;
				}
				try
				{
					var objChatServiceClass = JsonConvert.DeserializeObject<ChatServiceClass>(strChatResults);
					objChatServiceClass.Chatmemberlist.TrimExcess();
					lstChatMemberList = objChatServiceClass.Chatmemberlist;
					MessageChatListAdapterClass objMessageChatListAdapterClass = new MessageChatListAdapterClass(this.Activity, lstChatMemberList);
					listChatMember.Adapter = objMessageChatListAdapterClass;
					objMessageChatListAdapterClass.ChatRowClick += FnChatRowClicked;
				}
				catch(Exception e)
				{

				}
			}
		}
		void FnChatRowClicked(Chatmemberlist objChatmemberlist)
		{
			DetailChatActivity.PropertyChatmemberlist = objChatmemberlist;
			var intent = new Intent(Activity, typeof(DetailChatActivity));
			StartActivity(intent);
		}
	
		#endregion

		#region "AlertMsgTwoInput(2 button)"
		internal void FnAlertMsgTwoInput(string strTitle, string strMsg, string strOk, string strCancel, Context context)
		{
			Android.App.AlertDialog alertMsg = new Android.App.AlertDialog.Builder(context).Create();
			alertMsg.SetCancelable(false);
			alertMsg.SetTitle(strTitle);
			alertMsg.SetMessage(strMsg);
			alertMsg.SetButton2(strOk, delegate (object sender, DialogClickEventArgs e)
		 {
			 if (e.Which == -2)
			 {
				 if (isRemoveInterest)
				 {
					 if (selected == 1)
					 {
						 GAService.GetGASInstance().Track_App_Event(GAEventCategory.InterestListRowButton, strInterestName + " " + "(Interests Unfollowed (Popular Interests))");
						 FnRemoveInterests();

						 if (!issearch)
						 {
							 FnMyInterestList();
						 }
					 }
					 else if (selected == 2)
					 {
						 GAService.GetGASInstance().Track_App_Event(GAEventCategory.EventListRowButton, strEventName + " " + "(Event Unjoined(Suggested Events))");
						 FnRemoveEvents();
						 if (countValue == 1)
						 {
							 btnAddEvent.SetBackgroundResource(Resource.Drawable.plusmark);
							 countValue = 0;
						 }
						 if (!issearch)
						 {
							 FnMyEventList();
						 }
					 }
					 if (selected == 3)
					 {
						 //GAService.GetGASInstance().Track_App_Event(GAEventCategory.InterestListRowButton, strInterestName + " " + "(Interests Unfollowed (Popular Interests))");
						 isGroupButtonTrue = true;
						 FnRemoveGroups();

						 if (!issearch)
						 {
							 FnMyGroupList();
						 }
					 }
				 }
				 else
				 {
					 if (isMyInterestsorMyEvents == 1)
					 {
						 GAService.GetGASInstance().Track_App_Event(GAEventCategory.InterestListRowButton, strInterestName + " " + "(Interests Unfollowed (My Interests))");
						 FnRemoveInterests();

					 }
					 else if (isMyInterestsorMyEvents == 2)
					 {
						 GAService.GetGASInstance().Track_App_Event(GAEventCategory.EventListRowButton, strEventName + " " + "(Event Unjoined(My Events))");
						 FnRemoveEvents();
						 if (countValue == 1)
						 {
							 btnAddEvent.SetBackgroundResource(Resource.Drawable.plusmark);
							 countValue = 0;
						 }
					 }
					 else if (isMyInterestsorMyEvents == 3)
					 {
						 FnRemoveGroups();
					 }
				 }
			 }
		 });
			alertMsg.SetButton(strCancel, delegate (object sender, DialogClickEventArgs e)
		 {
			 if (e.Which == -1)
			 {
				 if (isRemoveInterest)
				 {

				 }
				 else
				 {
					 if (isMyInterestsorMyEvents == 1)
					 {
						 FnMyInterestList();
					 }
					 else if (isMyInterestsorMyEvents == 2)
					 {
						 FnMyEventList();
					 }
					 else if (isMyInterestsorMyEvents == 3)
					 {
						 isGroupButtonTrue = false;
						 FnMyGroupList();
					 }
				 }
				 alertMsg.Dismiss();
				 alertMsg = null;
			 }
		 });
			alertMsg.Show();
		}

		#endregion

		#region "AlertMsgTwoInput(3 button)"
		internal void FnAlertMsgTwoInput1(string strTitle, string strMsg, string strYes, string strCancel, Context context)
		{
			Android.App.AlertDialog alertMsg = new Android.App.AlertDialog.Builder(context).Create();
			alertMsg.SetCancelable(false);
			alertMsg.SetTitle(strTitle);
			alertMsg.SetMessage(strMsg);
			alertMsg.SetButton2(strYes, delegate (object sender, DialogClickEventArgs e)
		 {
			 if (e.Which == -2)
			 {
				 GAService.GetGASInstance().Track_App_Event(GAEventCategory.DescriptionPageJoinorUnjoinButton, strEventName + " " + "(Event Joined)");
				 btnAddEvent.SetBackgroundResource(Resource.Drawable.bluecheckmark);
				 FnEventSave(PropertyList);
			 }
		 });

			alertMsg.SetButton(strCancel, delegate (object sender, DialogClickEventArgs e)
			 {
				 if (e.Which == -1)
				 {
					alertMsg.Dismiss();
					 alertMsg = null;
				 }
			 });
			alertMsg.Show();
		}
		#endregion

		#region "Stop ProgressDialog"
		void FnProgressDialogClose()
		{
			if (progressDialog != null)
			{
				progressDialog.Dismiss();
				progressDialog = null;
			}
		}

		public void OnMapReady(GoogleMap googleMaps)
		{
			try
			{
				//googleMap = googleMaps;
			}
			catch (Exception e)
			{
				string strerroe = e.Message;
				Console.WriteLine("null exception");
			}
		}
		#endregion

		async Task<bool> FnEventServiceCall(string strLatLong, string strAddress, string strDistance)
		{
			bool isSuccess = false;
			bool isConnected = objClsCommonFunctions.FnIsConnected(this.Activity);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this.Activity);
			}
			else
			{
				string strLatLongValue = string.Empty;
				if (strLatLong.Contains(","))
				{
					strLatLongValue = Regex.Replace(strLatLong, ",", ".");
				}
				else
				{
					strLatLongValue = strLatLong;
				}
				string strEventResults = await objRestServiceCall.CallEventService(strLatLongValue, strAddress, strDistance);
				strLatLongValue = string.Empty;
				strAddress = string.Empty;
				strDistance = string.Empty;

				if (strEventResults == "Exception" || string.IsNullOrEmpty(strEventResults))
				{
					isSuccess = false;
				}
				try
				{
					objEventLocation = JsonConvert.DeserializeObject<EventLocation>(strEventResults);
					lstEvents.Clear();
					objEventLocation.EventsNearbyLocation.TrimExcess();
					lstEvents = objEventLocation.EventsNearbyLocation;
					isSuccess = true;
				}
				catch (Exception e)
				{
					isSuccess = false;
				}

			}
			return isSuccess;
		}
		async void FnLocation()
		{
			
			selected = 6;
			btnShowMore.Visibility = ViewStates.Gone;
			HomeActivity.txtSearch.Hint = "Search...";
			isSelectedTitle = 1;
			FnSelectedLable();

			isHomeEventOrPostEvent = true;
			lblTitleMy.Text = "Post";
			lblTitlePopular.Text = "Suggested Event";
			lblTitleCreate.Text = "Promotions";
			string strManualLocationLatLong = string.Empty;
			isManualLocationTrue = true;

			listInterestsDetails.Visibility = ViewStates.Gone;
			lblLocation.Text = SplashScreenActivity.strAddress;
			SplashScreenActivity.strLocationAddress = SplashScreenActivity.strAddress;
			if(string.IsNullOrEmpty(SplashScreenActivity.strLocationAddress))
			{
				await Task.Delay(4000);
				listInterestsDetails.Visibility = ViewStates.Gone;
				lblLocation.Text = SplashScreenActivity.strAddress;
				SplashScreenActivity.strLocationAddress = SplashScreenActivity.strAddress;
			}
			string strDistance = "20";
			if (!string.IsNullOrEmpty(SplashScreenActivity.strAddress))
			{
				FnLocationChange(SplashScreenActivity.strAddress, strDistance);
			}

		}
		async void FnLocationChange(string strAddress,string strDistance)
		{
			//number = 1;
			lnyChangeLocation.Visibility = ViewStates.Visible;
			bool isConnected = objClsCommonFunctions.FnIsConnected(this.Activity);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this.Activity);
			}
			else
			{
				if (isChangeLocation)
				{
					string strResult = await objRestServiceCall.CallGooglePlaceService(strSearchLocationValue);
					if (strResult == "Exception" || strResult == "")
					{
						//objClsCommonFunctions.FnAlertMsg ( Constants.strAppName , Constants.strServserError , this.Activity );
					}
					try
					{
						objGeoCodeJSONClass = JsonConvert.DeserializeObject<GeoCodeJSONClass>(strResult);

						string strLantitude = objGeoCodeJSONClass.results[0].geometry.location.lat.ToString();
						string strLongitude = objGeoCodeJSONClass.results[0].geometry.location.lng.ToString();
						strLatLong = strLantitude + "-" + strLongitude;
					}

					catch (Exception e)
					{

					}
				}
				else
				{
					strLatLong = SplashScreenActivity.strLatitude + "-" + SplashScreenActivity.strLangitude;
				}
					objGeoCodeJSONClass = null;
					string strLatLongValue = string.Empty;
					if (strLatLong.Contains(","))
					{
						strLatLongValue = Regex.Replace(strLatLong, ",", ".");
					}
					else
					{
						strLatLongValue = strLatLong;
					}
				string strEventResults = await objRestServiceCall.CallHomeEventsEventService(strLatLongValue,strAddress,strDistance);
					if (strEventResults == "Exception" || string.IsNullOrEmpty(strEventResults))
					{
						return;
					}
					try
					{
					listHomeFeed.Adapter = null;
						objHomeFeedEventsClass = JsonConvert.DeserializeObject<HomeFeedEventsClass>(strEventResults);
						lstHomeEvents.Clear();
					objHomeFeedEventsClass.Homefeedsfgeoevent.TrimExcess();
					lstHomeEvents = objHomeFeedEventsClass.Homefeedsfgeoevent;

					FnChangeLocationBind();
					}
					catch (Exception e)
					{
						//BindEventsList();

					}
				
			}
		}
		async void FnChangeLocationBind()
		{
			int count = 0;
			isChangeScreen = false;
			bool isConnected = objClsCommonFunctions.FnIsConnected(this.Activity);
			if (isConnected)
			{
				if (isSelectedTitle == 1)
				{
					lstMyEvents.Clear();
					string strUserEvents = await objRestServiceCall.CallUserEventsFetechService(SplashScreenActivity.strEmail);
					objEventLocation = JsonConvert.DeserializeObject<EventLocation>(strUserEvents);
					if (objEventLocation.EventsNearbyLocation != null)
					{
						lstMyEvents = objEventLocation.EventsNearbyLocation;
					}

				}
				HomeFeedEventsAdapterClass objEventsAdapterClass = new HomeFeedEventsAdapterClass(this.Activity, lstHomeEvents, lstMyEvents);
				//button tapped
				objEventsAdapterClass.AddRemoveButtonClick -= FnHomeFeedEventsButtonTapped;
				objEventsAdapterClass.AddRemoveButtonClick += FnHomeFeedEventsButtonTapped;
				objEventsAdapterClass.EventRowClicke -= FnHomeFeedEventRowTapped;
				objEventsAdapterClass.EventRowClicke += FnHomeFeedEventRowTapped;
				objEventsAdapterClass.GoogleMapClick -= FnHomeFeedEventsGoogleMapAddress;
				objEventsAdapterClass.GoogleMapClick += FnHomeFeedEventsGoogleMapAddress;
				listHomeFeed.Adapter = objEventsAdapterClass;
				if (HomeActivity.isHomeShows)
				{
					if (HomeActivity.isHome)
					{
						foreach (var values in lstHomeEvents)
						{
							if (values.Eventname.Equals(GoogleMapActivity.StrEventName))
							{
								break;
							}
							count++;
						}
						listHomeFeed.SetSelection(count);

					}
				}
				else
				{
					HomeActivity.isHomeShows = true;
					HomeActivity.isHome = false;
			}
			}
			else
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this.Activity);
			}

		}
		void FnHomeFeedEventsButtonTapped(Homefeedsfgeoevent objEventsNearbyLocation)
		{
			strEventsId = objEventsNearbyLocation.Idevents;
			FnHomeFeedEventSave(objEventsNearbyLocation);
			strEventId = objEventsNearbyLocation.Idevents;
		}
		void FnHomeFeedEventRowTapped(Homefeedsfgeoevent objEvents)
		{
			GAService.GetGASInstance().Track_App_Event(GAEventCategory.EventListRow, objEvents.Eventname + " " + "Event Description Page(Suggested Events)");
			HomeFeedPropertyList = objEvents;
			isEventDescription = true;
			eventDescriptionPopUp = 3;
			HomeFeedEventPropertyList = objEvents;
			FnDescriptionLayout();

		}
		void FnHomeFeedEventsGoogleMapAddress(Homefeedsfgeoevent objEventsNearbyLocation)
		{
			GoogleMapActivity.SuggestOrMyEvent = 1;
			isLocation = false;
			number = 1;
			screenSlide = 1;
			isSingleOrMultipleEvent = false;
			GoogleMapActivity.strAddress = lblLocation.Text;
			GoogleMapActivity.strLatLongs = objEventsNearbyLocation.Eventcity;
			GoogleMapActivity.HomefeedsSingleEventProperty = objEventsNearbyLocation;
			var intent = new Intent(Activity, typeof(GoogleMapActivity));
			StartActivity(intent);
			//Activity.Finish();

		}

		async void FnHomeFeed()
		{
			selected = 6;
			isSelectedTitle = 2;
			HomeActivity.txtSearch.Hint = "Search...";
			FnSelectedLable();
			lblTitleMy.Text = "Post";
			lblTitlePopular.Text = "Suggested Event";
			lblTitleCreate.Text = "Promotions";
			lnyChangeLocation.Visibility = ViewStates.Gone;
			isLocation = false;
			lstHomeFeed.Clear();
			await FnHomeFeedValue();
			btnShowMore.Visibility = ViewStates.Visible;
		}
		async Task<bool> FnHomeFeedValue()
		{
			number = 0;

			isHomeEventOrPostEvent = false;
			bool isSuccess = false;

			listHomeFeed.Adapter = null;
			bool isConnected = objClsCommonFunctions.FnIsConnected(this.Activity);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this.Activity);
			}
			else
			{
				string strHomeFeedResults = await objRestServiceCall.CallHomeFeedAPIService(SplashScreenActivity.strUserId,SplashScreenActivity.homeFeedsShowMoreIndex.ToString());
				if (strHomeFeedResults == "Exception" || string.IsNullOrEmpty(strHomeFeedResults))
				{
					isSuccess=false;
				}
				try
				{
					listHomeFeed.Adapter = null;
					var objHomeFeedClass = JsonConvert.DeserializeObject<HomeFeeds>(strHomeFeedResults);
					//lstHomeFeed.Clear();
					objHomeFeedClass.Orgfeeds.TrimExcess();

					//lstHomeFeed = objHomeFeedClass.Orgfeeds;
					lstHomeFeed.AddRange(objHomeFeedClass.Orgfeeds);
					if(lstHomeFeed.Count>0)
					{
						btnShowMore.Text = "Show More";
					}
					else
					{
						
					}
					FnHomeFeedBinding();
					isSuccess = true;
				}
				catch (Exception e)
				{
					isSuccess=false;
				}
			}
			return isSuccess;
		}
		void FnHomeFeedBinding()
		{
			int count = 0;
			objHomeFeedAdapterClass = new HomeFeedAdapterClass(this.Activity, lstHomeFeed);
			objHomeFeedAdapterClass.EventAddressClick += FnEventAddressClick;
			objHomeFeedAdapterClass.EventNameClick += FnEventNameClick;
			objHomeFeedAdapterClass.EventBuyTicketClick += FnEventBuyTicketClick;

			objHomeFeedAdapterClass.PostGroupNameClick += FnPostGroupNameClick;
			objHomeFeedAdapterClass.PostTitleClick += FnPostTitleClick;
			objHomeFeedAdapterClass.PostDownArrowClick += FnPostDownArrowClick;

			objHomeFeedAdapterClass.LikeClick += FnLikeClick;
			objHomeFeedAdapterClass.CommentClick -= FnCommentClick;
			objHomeFeedAdapterClass.CommentClick += FnCommentClick;
			objHomeFeedAdapterClass.ShareClick += FnShareClick;

			listHomeFeed.Adapter = objHomeFeedAdapterClass;

			if (HomeActivity.isHome)
			{
				foreach (var values in lstHomeFeed)
				{
					if (values.allfeedsflag == 2)
					{
						if (values.Eventname.Equals(GoogleMapActivity.StrEventName))
						{
							break;
						}
					}
					else
					{
						//break;
					}
					count++;
				}
				listHomeFeed.SetSelection(count);
				HomeActivity.isHome = false;
			}
			else
			{

			}
		}
		async void FnPromation()
		{
			btnShowMore.Visibility = ViewStates.Gone;
			selected = 6;
			isSelectedTitle = 3;
			HomeActivity.txtSearch.Hint = "Search...";
			FnSelectedLable();
			lblTitleMy.Text = "Post";
			lblTitlePopular.Text = "Suggested Event";
			lblTitleCreate.Text = "Promotions";
			listHomeFeed.Adapter = null;
			lnyChangeLocation.Visibility = ViewStates.Gone;
			bool isConnected = objClsCommonFunctions.FnIsConnected(this.Activity);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this.Activity);
			}
			else
			{
				string strHomeFeedPromatationResults = await objRestServiceCall.CallPromotationAPI(SplashScreenActivity.strUserId);
				if (strHomeFeedPromatationResults == "Exception" || string.IsNullOrEmpty(strHomeFeedPromatationResults))
				{
					return;
				}
				try
				{
					listHomeFeed.Adapter = null;
					var objPromationClass = JsonConvert.DeserializeObject<PromationClass>(strHomeFeedPromatationResults);
					lstHomeFeedPromation.Clear();
					objPromationClass.Orgfeedspromotion.TrimExcess();
					lstHomeFeedPromation = objPromationClass.Orgfeedspromotion;

					HomeFeedPromatationAdapterClass objHomeFeedAdapterClass = new HomeFeedPromatationAdapterClass(this.Activity, lstHomeFeedPromation);
					objHomeFeedAdapterClass.PromotionNameClick += FnPromotionNameClick;
					objHomeFeedAdapterClass.PromotionButtonFollowClick += FnPromotionButtonFollowClick;


					listHomeFeed.Adapter = objHomeFeedAdapterClass;
				}
				catch
				{

				}
			}
		}
		//promatation click
		void FnPromotionNameClick(Orgfeedspromotion objOrgfeed)
		{
			if (objOrgfeed.allfeedsflag == 3)
			{
				InterestDescriptionActivity.InterestOrgfeed = objOrgfeed;
				var intent = new Intent(Activity, typeof(InterestDescriptionActivity));
				intent.PutExtra("MyData", "HomeInterest");
				StartActivity(intent);
			}
			else
			{

			}
		}
		async void FnPromotionButtonFollowClick(Orgfeedspromotion objOrgfeed)
		{
			try
			{
				string strStoreInterests = await objRestServiceCall.CallInterestsStoreService(objOrgfeed.id, SplashScreenActivity.strEmail);
				var interest = JsonConvert.DeserializeObject<InterestFollow>(strStoreInterests);
				if (strStoreInterests == "Exception" || string.IsNullOrEmpty(strStoreInterests))
				{
					return;
				}
			}
			catch
			{

			}
		}


		//home feed clickevent
		void FnEventAddressClick(Orgfeed objOrgfeed)
		{
			screenSlide = 1;
			number = 2;
			isHomeFeedMapFlag = true;
			var intent = new Intent(Activity, typeof(GoogleMapActivity));
			intent.PutExtra("MyData",objOrgfeed.coordinates+"/"+objOrgfeed.Eventname);
			StartActivity(intent);
			//Activity.Finish();
		}
		void FnEventNameClick(Orgfeed objOrgfeed)
		{
			isEventDescription = false;
			eventDescriptionPopUp = 2;
			EventOrgfeed = objOrgfeed;
			FnDescriptionLayout();
		}
		void FnEventBuyTicketClick(Orgfeed objOrgfeed)
		{
			if (objOrgfeed.Ispaidevent=="1")
			{
				var uri = Android.Net.Uri.Parse(objOrgfeed.Paymenturl);
				var intent = new Intent(Intent.ActionView, uri);
				StartActivity(intent);
			}
		}

		void FnPostGroupNameClick(Orgfeed objOrgfeed)
		{
			if (objOrgfeed.allfeedsflag == 1)
			{
				MyGroupDescriptionActivity.GroupOrgfeed = objOrgfeed;
				var intent = new Intent(Activity, typeof(MyGroupDescriptionActivity));
				intent.PutExtra("MyData", "HomeGroup");
				StartActivity(intent);
			}

		}
		void FnPostTitleClick(Orgfeed objOrgfeed)
		{
			var uri = Android.Net.Uri.Parse(objOrgfeed.url);
			var intent = new Intent(Intent.ActionView, uri);
			StartActivity(intent);
		}
		void FnPostDownArrowClick(Orgfeed objOrgfeed)
		{
			FnPostDownArrowPopUpScreen(objOrgfeed);
		}

		void FnPostDownArrowPopUpScreen(Orgfeed objOrgfeed)
		{
			strPostId = objOrgfeed.Idposts;
			LayoutInflater password = LayoutInflater.From(this.Activity);
			View view = password.Inflate(Resource.Layout.PostLayout, null);

			AlertDialog builder = new AlertDialog.Builder(this.Activity).Create();
			builder.SetView(view);
			btnCancel = view.FindViewById<Button>(Resource.Id.btnCancel);
			btnSubmit = view.FindViewById<Button>(Resource.Id.btnSubmit);
			rdButton = view.FindViewById<RadioButton>(Resource.Id.radioButton1);
			rlyPostLayout = view.FindViewById<RelativeLayout>(Resource.Id.rlyPostLayout);
			rdButton.Checked = false;
			builder.SetCanceledOnTouchOutside(false);
			btnCancel.Click += delegate
			{
				//builder.SetCanceledOnTouchOutside(true);
				builder.Dismiss();
			};
			btnSubmit.Click += async delegate
			{

				bool isConnected = objClsCommonFunctions.FnIsConnected(this.Activity);
				if (!isConnected)
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this.Activity);
				}
				else
				{
					if (rdButton.Checked)
					{
						try
						{
							string strHomeFeedResults = await objRestServiceCall.CallHidePostAPI(strPostId);
							if (strHomeFeedResults == "Exception" || string.IsNullOrEmpty(strHomeFeedResults))
							{
								return;
							}
							if(strHomeFeedResults=="1")
							{
								builder.Dismiss();
							}
						}
						catch
						{

						}
					}
					else
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Please Click Radiobutton", this.Activity);
					}
				}
			
			};
			builder.Show();
		}

		async void FnLikeClick(Orgfeed objOrgfeed)
		{
			string strId = "";
			if(objOrgfeed.allfeedsflag==1)
			{
				 strId = objOrgfeed.Idposts;
				if (strFlag == "UnLike")
				{
					//strFlag = "";
					isLikeOrUnlike = false;

				}
				else
				{
					//strFlag = "1";
					isLikeOrUnlike = true;
				}

				bool isConnected = objClsCommonFunctions.FnIsConnected(this.Activity);
				if (!isConnected)
				{

					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this.Activity);
				}
				else
				{
					string strUserLikesResults = await objRestServiceCall.CallLikeUnLikeNotificationAPI(strId, SplashScreenActivity.strUserId);
					if (strUserLikesResults == "Exception" || string.IsNullOrEmpty(strUserLikesResults))
					{
						//objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this.Activity);
						return;
					}
					try
					{
						var objLikesDetails = JsonConvert.DeserializeObject<LikesDetails>(strUserLikesResults);

						if (objLikesDetails.Likes.UserStatus == "1")
						{
							//strFlag ="1";
							//isLikeOrUnlike = true;
						}
						else
						{
							//strFlag = "0";
							//isLikeOrUnlike = false;
						}
					}
					catch (Exception e)
					{
						FnProgressDialogClose();
					}
				}
			}
			else if (objOrgfeed.allfeedsflag == 2)
			{
				strId = objOrgfeed.Idevents;
				if (strFlag=="UnLike")
				{
					//strFlag = "";
					isLikeOrUnlike = false;

				}
				else
				{
					//strFlag = "1";
					isLikeOrUnlike = true;
				}

				bool isConnected = objClsCommonFunctions.FnIsConnected(this.Activity);
				if (!isConnected)
				{

					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this.Activity);
				}
				else
				{

					string strUserLikesResults = await objRestServiceCall.CallEventLikeOrUnlikeAPI(strId, SplashScreenActivity.strUserId);
					if (strUserLikesResults == "Exception" || string.IsNullOrEmpty(strUserLikesResults))
					{
						//objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this.Activity);
						return;
					}
					try
					{
						var objLikesDetails = JsonConvert.DeserializeObject<EventLikes>(strUserLikesResults);

						if (strFlag == "1")
						{
							//strFlag ="1";
							//isLikeOrUnlike = true;
						}
						else
						{
							//strFlag = "0";
							//isLikeOrUnlike = false;
						}
					}
					catch (Exception e)
					{
						FnProgressDialogClose();
					}
				}
			}

		}
		void FnCommentClick(Orgfeed objOrgfeed)
		{
			LayoutInflater password = LayoutInflater.From(this.Activity);
			View view = password.Inflate(Resource.Layout.EventCommentLayout, null);

			AlertDialog CommentPopUp = new AlertDialog.Builder(this.Activity).Create();
			CommentPopUp.SetView(view);
			rlyHomeCommentLayout = view.FindViewById<RelativeLayout>(Resource.Id.HomeCommentLayout);
			listHomeComment = view.FindViewById<ListView>(Resource.Id.listViewHomeCommentListView);
			imgUserProfile = view.FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.imgHomeCommentUser);
			txtCommentText = view.FindViewById<EditText>(Resource.Id.txtMsgHomeComment);
			imgSend = view.FindViewById<ImageView>(Resource.Id.imgHomeCommentSend);
			Picasso.With(this.Activity).Load(objOrgfeed.userdata.UserProfile).Into(imgUserProfile);


			txtCommentText.Text = string.Empty;
			if (objOrgfeed.allfeedsflag == 1)
			{
				commentFlag = 1;
				strIdpost = objOrgfeed.Idposts;
			}
			else if (objOrgfeed.allfeedsflag == 2)
			{
				commentFlag = 2;
				strIdpost = objOrgfeed.Idevents;
			}
			strUserImagePath = objOrgfeed.userdata.UserProfile;
			listHomeComment.Adapter = null;
			if (objOrgfeed.comments != null)
			{
				//lstHomeComment.Clear();
				lstHomeComment = objOrgfeed.comments;
				FnBindHomeComment();

			}
			imgSend.Click += async delegate (object sender, EventArgs e)
			  {

				  if (string.IsNullOrEmpty(txtCommentText.Text))
				  {
					  objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Enter", this.Activity);
				  }
				  else
				  {
					  bool isConnected = objClsCommonFunctions.FnIsConnected(this.Activity);
					  if (!isConnected)
					  {
						  objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this.Activity);
					  }
					  else
					  {
						  string strCommentResults = await objRestServiceCall.CallHomeCommentAPI(SplashScreenActivity.strUserId, strIdpost, txtCommentText.Text.Trim(), commentFlag.ToString());
						  if (strCommentResults == "Exception" || string.IsNullOrEmpty(strCommentResults))
						  {
							  objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this.Activity);
							  return;
						  }
						  try
						  {
							  listHomeComment.Adapter = null;
							  lstHomeComment.Add(new Comment { Comments = txtCommentText.Text.Trim(), Created_at = "JUST NOW", Fname = SplashScreenActivity.strUserName, Modifiedimage = strUserImagePath, commentflag = commentFlag });
							  FnBindHomeComment();
							  txtCommentText.Text = string.Empty;
						  }
						  catch
						  {

						  }
					  }
				  }

			  };
			CommentPopUp.Show();
		}

		void FnBindHomeComment()
		{
			listHomeComment.Adapter = new CommentOnHomeFeedAdapterClass(this.Activity, lstHomeComment);
		}
		void FnShareClick(Orgfeed objOrgfeed)
		{
			//pending
		}
		void FnChangeLocation()
		{
			
			LayoutInflater password = LayoutInflater.From(this.Activity);
			View view = password.Inflate(Resource.Layout.ChangeLocationLayout, null);

			AlertDialog builder = new AlertDialog.Builder(this.Activity).Create();
			builder.SetView(view);
			txtChangeLocationSearch = view.FindViewById<AutoCompleteTextView>(Resource.Id.txtSearchLocation);
			spinnerChangeLocation = view.FindViewById<Spinner>(Resource.Id.spinnerValue);
			btnChange = view.FindViewById<Button>(Resource.Id.btnChange);
			imgClose=view.FindViewById<ImageView>(Resource.Id.imgClose);
			builder.SetCanceledOnTouchOutside(false);
			spinnerChangeLocation.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(SpinnerItemSelected);
			var adapter1 = ArrayAdapter.CreateFromResource(this.Activity, Resource.Array.Location_Distance, Android.Resource.Layout.SimpleSpinnerItem);
			adapter1.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
			spinnerChangeLocation.Adapter = adapter1;
			txtChangeLocationSearch.TextChanged += async delegate
			{
				string[] strName = null;

				if (txtChangeLocationSearch.Text.Length >= 2)
				{
					strName = await FnSearchLocation(txtChangeLocationSearch.Text.Trim());
				}
				if (strName != null && strName.Length > 0)
				{
					ArrayAdapter adapter = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleDropDownItem1Line, strName);
					txtChangeLocationSearch.Adapter = adapter;
					strSearchLocationValue = txtChangeLocationSearch.Text.Trim();
				}
			};
			imgClose.Click += delegate
			{
				isChangeLocation = false;
				builder.Dismiss();
			};
			btnChange.Click +=async delegate
			{
				isChangeLocation = true;
				SplashScreenActivity.strLocationAddress = txtChangeLocationSearch.Text;
				SplashScreenActivity.login = 1;

				string strResult = await objRestServiceCall.CallGooglePlaceService(SplashScreenActivity.strLocationAddress);
				if (strResult == "Exception" || strResult == "")
				{
					//objClsCommonFunctions.FnAlertMsg ( Constants.strAppName , Constants.strServserError , this.Activity );
				}
				try
				{
					objGeoCodeJSONClass = JsonConvert.DeserializeObject<GeoCodeJSONClass>(strResult);

					string strLantitude = objGeoCodeJSONClass.results[0].geometry.location.lat.ToString();
					string strLongitude = objGeoCodeJSONClass.results[0].geometry.location.lng.ToString();
					strLatLong = strLantitude + "-" + strLongitude;

					if (strLatLong.Contains(","))
					{
						strHomeLocationLatLong = Regex.Replace(strLatLong, ",", ".");
					}
					else {
						strHomeLocationLatLong = strLatLong;
					}
				}
				catch
				{
					
				}

				lstHomeEvents.Clear();
				FnLocationChange(SplashScreenActivity.strLocationAddress, strDistanceData);
				lblLocation.Text = SplashScreenActivity.strLocationAddress;
				txtChangeLocationSearch.Text = string.Empty;
				strSearchLocationValue = "";
				isChangeLocation = false;
				builder.Dismiss();
			};
			builder.Show();
		}

		async Task<bool> FnHomeGoogleMapLocation(string strLatLongValue,string strAddress,string strDistance)
		{
			bool isReturn = false;
			bool isTrue=await FnEventListServiceCall(strLatLongValue,strAddress,strDistance);
			if(isTrue)
			{
				isReturn = true;
			}
			else
			{
				isReturn = false;
			}
			return isReturn;
		}

	}



}
//for uploading image to server
//https://forums.xamarin.com/discussion/17170/select-and-upload-multiple-images-to-web-server#latest