﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Newtonsoft.Json;
using RestSharp;

namespace YouMeUsAndroid
{
	[Activity(WindowSoftInputMode = SoftInput.StateAlwaysHidden, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
	public class CreateEventsActivity : Activity
	{
		ScrollView scrollViewEvent;
		Java.IO.File fileImagePath;
		string strAbsolutePath;
		string strProductCameraImagePath;
		Button btnCamera;
		ImageView imgLogo;
		LinearLayout lnyEventMaster;
		AutoCompleteTextView txtGroupSearch;
		EditText txtEventName;
		EditText txtDescription;
		EditText txtEventDate;
		EditText txtEventStartTime;
		EditText txtEventEndTime;
		Button btnDate;
		Button btnStartTime;
		Button btnEndTime;
		ImageView imgEvent;
		Button btnAddLogo;
		TextView lblImageStatus;
		AutoCompleteTextView txtAddress;
		Spinner spinnerEvents;
		EditText txtEventURL;
		CheckBox chkActive;
		Button btnNext;

		LinearLayout lnyTagGroupOrInterests;
		RelativeLayout rlyTagDemographic;
		LinearLayout lnyNextContent;
		LinearLayout lnyEventTagLayout;
		Spinner spinnerEventPublicity;
		TextView lblTagGroup;
		TextView lblTagInterest;
		TextView lblTagDemographic;
		TextView lblSpinnerTitle;
		MultiAutoCompleteTextView txtGroupInterest;
		Spinner spinnerGender;
		Spinner spinnerPeople;
		EditText txtAgeFrom;
		EditText txtAgeTo;
		AutoCompleteTextView txtLocations;
		private InputMethodManager KeyBoard;
		Button btnSave;
		Button btnBack;

		View view1;
		View view2;
		View view3;
		List<Taginterest> lstTagInterestList = new List<Taginterest>();
		List<SelectDropdownList> lstTagGroupList = new List<SelectDropdownList>();
		ClsCommonFunctions objClsCommonFunctions;
		RestServiceCall objRestServiceCall;
		//date picker
		const int Date_Id = 0;
		DateTime date;
		//time picker
		private int hour;
		private int minute;
		const int TIME_DIALOG_ID = 0;
		bool isTrue = true;
		int selectedTextTitle = 0;
		byte[] photo;
		string strActive="0";
		internal static string strActivityName="CreateEventsActivity";
		string strTagInterestName = "";
		string strTagInterestsId = "";
		string strTagGroupName="";
		string strTagGroupId = "";
		string strGroupSearchName = "";
		string strGroupSearchId ="";

		string strImagePath = "";
		string strImageName = "";
		byte[] coverphoto;
		byte[] resizedPhoto;
		byte[] thumbnailPhoto;
		int imageWidth;
		int imageHeight;
		string strFileDataCoverImage;
		string strFileDataResizedImage;
		string strFileDataThumbnailImage;

		string strGender="2";
		string strEventType = "0";
		string strPeople ="0";
		string strEventPublicity = "2";
		ProgressDialog progress;
		Uri uri;
		protected override void OnCreate(Bundle savedInstanceState)
		{
			RequestWindowFeature(WindowFeatures.NoTitle);
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.CreateEventLayout);
			FnInitilizations();
			FnClickEvents();
		}
		void FnInitilizations()
		{
			imgLogo = FindViewById<ImageView>(Resource.Id.imgLogoCEL);
			lnyEventMaster = FindViewById<LinearLayout>(Resource.Id.lnyEventMasterCNEL);
			lnyEventTagLayout = FindViewById<LinearLayout>(Resource.Id.lnyEventTagDetailsCNEL);
			txtGroupSearch = FindViewById<AutoCompleteTextView>(Resource.Id.txtGroupNameCNEL);
			txtEventName = FindViewById<EditText>(Resource.Id.txtEventNameCNEL);
			txtDescription = FindViewById<EditText>(Resource.Id.txtDescriptionCNEL);
			txtEventDate = FindViewById<EditText>(Resource.Id.txtEventDateCNEL);
			txtEventStartTime = FindViewById<EditText>(Resource.Id.txtEventStartTimeCNEL);
			txtEventEndTime = FindViewById<EditText>(Resource.Id.txtEventEndTimeCNEL);
			txtAddress = FindViewById<AutoCompleteTextView>(Resource.Id.txtAddressCNEL);
			txtEventURL = FindViewById<EditText>(Resource.Id.txtTicketUrlCNEL);
			lnyNextContent = FindViewById<LinearLayout>(Resource.Id.lnyContentCNEL);
			lblTagGroup = FindViewById<TextView>(Resource.Id.lblTagGroupsCNEL);
			lblTagInterest = FindViewById<TextView>(Resource.Id.lblTagInterestCNEL);
			lblTagDemographic = FindViewById<TextView>(Resource.Id.lblTagDemographicCNEL);
			lblSpinnerTitle = FindViewById<TextView>(Resource.Id.lblTagGroupOrInterestCNEL);

			lnyTagGroupOrInterests = FindViewById<LinearLayout>(Resource.Id.lnyTagGroupOrInterestCNEL);
			rlyTagDemographic = FindViewById<RelativeLayout>(Resource.Id.rlyTagDemographicCNEL);
			btnCamera=FindViewById<Button>(Resource.Id.btnCameraCNEL);
			view1 = FindViewById<View>(Resource.Id.viewLineTagGroupsCNEL);
			view2 = FindViewById<View>(Resource.Id.viewLineTagInterestCNEL);
			view3 = FindViewById<View>(Resource.Id.viewLineTagDemographicCNEL);
			txtLocations = FindViewById<AutoCompleteTextView>(Resource.Id.txtlocationCNEL);
			txtGroupInterest = FindViewById<MultiAutoCompleteTextView>(Resource.Id.txtTagGroupOrInterestCNEL);
			txtAgeFrom = FindViewById<EditText>(Resource.Id.txtAgeFromCNEL);
			txtAgeTo = FindViewById<EditText>(Resource.Id.txtAgeToCNEL);
			//scrollViewEvent = FindViewById<ScrollView>(Resource.Id.scrollViewCNEL);
			btnDate = FindViewById<Button>(Resource.Id.btnEventDateCNEL);
			btnStartTime = FindViewById<Button>(Resource.Id.btnAddStartTimeCNEL);
			btnEndTime = FindViewById<Button>(Resource.Id.btnAddEndTimeCNEL);
			btnAddLogo = FindViewById<Button>(Resource.Id.btnChooseFileCNEL);
			btnNext = FindViewById<Button>(Resource.Id.btnNextCNEL);
			btnBack = FindViewById<Button>(Resource.Id.btnBackCNEL);
			btnSave = FindViewById<Button>(Resource.Id.btnSaveCNEL);
			chkActive = FindViewById<CheckBox>(Resource.Id.chkActiveCNEL);
			spinnerEvents = FindViewById<Spinner>(Resource.Id.spinnerEventTypeCNEL);
			spinnerEventPublicity = FindViewById<Spinner>(Resource.Id.spinnerEventPublicityCNEL);
			spinnerGender = FindViewById<Spinner>(Resource.Id.spinnerGenderCNEL);
			spinnerPeople = FindViewById<Spinner>(Resource.Id.spinnerPeopleCNEL);
			imgEvent = FindViewById<ImageView>(Resource.Id.imgEventLogoCNEL);
			lblImageStatus = FindViewById<TextView>(Resource.Id.lblChoosenFileCNEL);
			lnyNextContent.Visibility = ViewStates.Gone;

			objClsCommonFunctions = objClsCommonFunctions ?? new ClsCommonFunctions();
			objRestServiceCall = objRestServiceCall ?? new RestServiceCall();

			FnTitleSelected();
			FnTagGroupInterst();
			//1 event type
			spinnerEvents.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(SpinnerEventTypeItemSelected);
			var adapter1 = ArrayAdapter.CreateFromResource(this, Resource.Array.CreateEvent, Android.Resource.Layout.SimpleSpinnerItem);
			adapter1.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
			spinnerEvents.Adapter = adapter1;
			//2 event publicity
			spinnerEventPublicity.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(SpinnerEventPublicityItemSelected);
			var adapter2 = ArrayAdapter.CreateFromResource(this, Resource.Array.EventPublicity, Android.Resource.Layout.SimpleSpinnerItem);
			adapter2.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
			spinnerEventPublicity.Adapter = adapter2;
			//3 evennt gender
			spinnerGender.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(SpinnerEventGenderItemSelected);
			var adapter3 = ArrayAdapter.CreateFromResource(this, Resource.Array.EventGender, Android.Resource.Layout.SimpleSpinnerItem);
			adapter3.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
			spinnerGender.Adapter = adapter3;
			//4 event people
			spinnerPeople.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(SpinnerEventPeopleItemSelected);
			var adapter4 = ArrayAdapter.CreateFromResource(this, Resource.Array.EventPeople, Android.Resource.Layout.SimpleSpinnerItem);
			adapter4.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
			spinnerPeople.Adapter = adapter4;

			// Get the current time
			hour = DateTime.Now.Hour;
			minute = DateTime.Now.Minute;
			KeyBoard = (InputMethodManager)GetSystemService(Context.InputMethodService);
		}

		void SpinnerEventTypeItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
		{
			Spinner spinner = (Spinner)sender;
			string strType = spinner.GetItemAtPosition(e.Position).ToString();
			if(strType =="Free")
			{
				strEventType = "0";
				txtEventURL.Visibility = ViewStates.Gone;
			}
			else
			{
				strEventType = "1";
				txtEventURL.Visibility = ViewStates.Visible;
			}
		}
		void SpinnerEventPublicityItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
		{
			Spinner spinner = (Spinner)sender;
			 string strPublicity = spinner.GetItemAtPosition(e.Position).ToString();
			if (strPublicity == "Closed")
			{
				strEventPublicity = "1";
				lnyNextContent.Visibility = ViewStates.Gone;
			}
			else//open
			{
				strEventPublicity = "2";
				lnyNextContent.Visibility = ViewStates.Visible;
			}
		}
		void SpinnerEventGenderItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
		{
			Spinner spinner = (Spinner)sender;
			string strvalue = spinner.GetItemAtPosition(e.Position).ToString();
			if(strvalue=="All")
			{
				strGender = "2";
			}
			else if (strvalue == "Male")
			{
				strGender = "1";
			}
			else if (strvalue == "Female")
			{
				strGender = "0";
			}
		}
		void SpinnerEventPeopleItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
		{
			Spinner spinner = (Spinner)sender;
			 string strPeopleValue = spinner.GetItemAtPosition(e.Position).ToString();
			if(strPeopleValue=="All")
			{
				strPeople = "0";
			}
			else if (strPeopleValue == "Students")
			{
				strPeople = "1";
			}
			else if (strPeopleValue == "Working")
			{
				strPeople = "2";
			}
			else if (strPeopleValue == "Not Working")
			{
				strPeople = "3";
			}
		}
		void FnClickEvents()
		{
			btnCamera.Click += delegate
			{
				if (IsThereAnAppToTakePictures())
				{
					CreateDirectoryForPictures();
				}
				TakeAPicture();
			};
			imgLogo.Click += delegate
			{
				FinishAffinity();
				HomeActivity.isHomeShows = false;
				HomeActivity.isHosmeFeedorPost = 1;
				Intent activity = new Intent(this, typeof(HomeActivity));
				activity.PutExtra("MyData", "0");
				StartActivity(activity);
			};
			chkActive.Click += delegate
			{
				if (chkActive.Checked)
				{
					strActive = "1";
				}
				else
				{
					strActive = "0";
				}
			};
			btnDate.Click += delegate
			{
				CreateDialog(Date_Id).Show();
			};

			btnAddLogo.Click += delegate
			{
				var imageIntent = new Intent();
				imageIntent.SetType("image/*");
				imageIntent.SetAction(Intent.ActionGetContent);
				StartActivityForResult(Intent.CreateChooser(imageIntent, "Select Image"), 0);
			};
			btnStartTime.Click += delegate
			{
				isTrue = true;
				OnCreateDialog(TIME_DIALOG_ID).Show();
			};
			btnEndTime.Click += delegate
			{
				isTrue = false;
				OnCreateDialog(TIME_DIALOG_ID).Show();
			};
			btnNext.Click += delegate
			{
				if (string.IsNullOrEmpty(txtEventName.Text) || string.IsNullOrEmpty(txtDescription.Text) || string.IsNullOrEmpty(txtEventDate.Text) || string.IsNullOrEmpty(txtEventStartTime.Text) || string.IsNullOrEmpty(txtAddress.Text))
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strFillValidationError, this);
				}
				else
				{
					if (string.IsNullOrEmpty(strImagePath))
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName,"Image is not selected", this);
					}
					else
					{
						lnyEventMaster.Visibility = ViewStates.Gone;
						lnyEventTagLayout.Visibility = ViewStates.Visible;

					}
				}
			};
			btnBack.Click += delegate
			{
				lnyEventMaster.Visibility = ViewStates.Visible;
				lnyEventTagLayout.Visibility = ViewStates.Gone;
			};
			lblTagGroup.Click += delegate
			{
				selectedTextTitle = 0;
				FnTitleSelected();
				FnTagGroupInterst();
				txtGroupInterest.Text = strTagGroupName;
			};
			lblTagInterest.Click += delegate
			{
				selectedTextTitle = 1;
				FnTitleSelected();
				FnTagGroupInterst();
				txtGroupInterest.Text = strTagInterestName;
			};
			lblTagDemographic.Click += delegate
			{
				selectedTextTitle = 2;
				FnTitleSelected();
			};
			txtAddress.TextChanged +=async delegate
			{
				string[] strName = null;

				if (txtAddress.Text.Length >= 2)
				{
					strName = await FnSearchLocation(txtAddress.Text.Trim());
				}
				if (strName != null && strName.Length > 0)
				{
					ArrayAdapter adapter = new ArrayAdapter<string>(this, Resource.Layout.CustomAutoCompleteLayout, Resource.Id.lblPlaceName, strName);
					txtAddress.Adapter = adapter;
					string strSearchLocationValue = txtAddress.Text.Trim();
				}
			};
			txtLocations.TextChanged += async delegate
			{
				string[] strName = null;

				if (txtLocations.Text.Length >= 2)
				{
					strName = await FnSearchLocation(txtLocations.Text.Trim());
				}
				if (strName != null && strName.Length > 0)
				{
					ArrayAdapter adapter = new ArrayAdapter<string>(this, Resource.Layout.CustomAutoCompleteLayout, Resource.Id.lblPlaceName, strName);
					txtLocations.Adapter = adapter;
					string strSearchLocationValue = txtLocations.Text.Trim();
				}
			};
			txtGroupInterest.TextChanged += delegate
		   {
			   List<Taginterest> lst = new List<Taginterest>();
			   List<SelectDropdownList> lst1 = new List<SelectDropdownList>();
			   string[] strName = null;
			   string[] strName1 = null;
			   int index = 0;
			   if (string.IsNullOrEmpty(txtGroupInterest.Text))
			   {
				   if (selectedTextTitle == 0)
				   {
					   strTagGroupName = "";
					   strTagGroupId = "";

				   }
				   else if (selectedTextTitle == 1)
				   {
					   strTagInterestName = "";
					   strTagInterestsId = "";
				   }

			   }
			   else
			   {
				   //tag group
				   if (selectedTextTitle == 0)
				   {
					   string strTextValue = txtGroupInterest.Text;
						string lastCharacter = strTextValue.Split(',').Last();
					  // string lastCharacter = strTextValue.Substring(strTextValue.Length - 1);
					   if (lastCharacter == " ")
					   {

					   }
					   else
					   {
						   txtGroupInterest.Adapter = null;
						   if (lstTagGroupList.Count != 0)
						   {
							   lst1 = lstTagGroupList.Where(x => x.Groupname.ToLower().Contains(lastCharacter.ToLower())).ToList();
							   strName = new string[lst1.Count];
							   foreach (var description in lst1)
							   {
								   strName[index] = description.Idgroups + " " + description.Groupname;
								   index++;
							   }

							   if (strName != null && strName.Length > 0)
							   {
								   txtGroupInterest.Adapter = null;
								   TagGroupAutoCompleteListAdapter adapter = new TagGroupAutoCompleteListAdapter(this, lst1);
								   adapter.OriginalItems = strName;
								   txtGroupInterest.Adapter = adapter;

								   txtGroupInterest.Threshold = 1;
								   txtGroupInterest.SetTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
									adapter.ItemClick -= FnTxtTagGroupItemClick;
								   adapter.ItemClick += FnTxtTagGroupItemClick;
							   }
								else
								{
								   txtGroupInterest.Adapter = null;
								   TagGroupAutoCompleteListAdapter adapter = new TagGroupAutoCompleteListAdapter(this, lst1);
								   adapter.OriginalItems = strName;
								   txtGroupInterest.Adapter = adapter;

								   txtGroupInterest.Threshold = 1;
								   txtGroupInterest.SetTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
									adapter.ItemClick -= FnTxtTagGroupItemClick;
								   adapter.ItemClick += FnTxtTagGroupItemClick;
								}
						   }
					   }
				   }
				   //tag interests
				   else if (selectedTextTitle == 1)
				   {
					   string strTextVAlue = txtGroupInterest.Text;
					  string lastCharacter = strTextVAlue.Split(',').Last();
					   if (lastCharacter == " ")
					   {

					   }
					   else
					   {
						   if (lstTagInterestList.Count != 0)
						   {
							   lst = lstTagInterestList.Where(x => x.name.ToLower().Contains(lastCharacter.ToLower())).ToList();
							   strName1 = new string[lst.Count];
							   foreach (var description in lst)
							   {
								   strName1[index] = description.id + " " + description.name;
								   index++;
							   }

							   if (strName1 != null && strName1.Length > 0)
							   {
								   strActivityName = "CreateEventsActivity";
								   txtGroupInterest.Adapter = null;
								   TagInterestAutoCompleteListAdapter adapter = new TagInterestAutoCompleteListAdapter(this, lst);
								   adapter.OriginalItems = strName1;
								   txtGroupInterest.Adapter = adapter;

								  // txtGroupInterest.Threshold = 1;
								   txtGroupInterest.SetTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
									adapter.ItemClick -= FnTxtTagInterestItemClick;
								   adapter.ItemClick += FnTxtTagInterestItemClick;
							   }
								else
								{
								   strActivityName = "CreateEventsActivity";
								   txtGroupInterest.Adapter = null;
								   TagInterestAutoCompleteListAdapter adapter = new TagInterestAutoCompleteListAdapter(this, lst);
								   adapter.OriginalItems = strName1;
								   txtGroupInterest.Adapter = adapter;

								  // txtGroupInterest.Threshold = 1;
								   txtGroupInterest.SetTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
									adapter.ItemClick -= FnTxtTagInterestItemClick;
								   adapter.ItemClick += FnTxtTagInterestItemClick;
								}
						   }
					   }
				   }
			   }
		   };
			txtGroupSearch.TextChanged += delegate
			  {
				  List<SelectDropdownList> lst1 = new List<SelectDropdownList>();
				  string[] strName = null;
				  int index = 0;
				 
					lst1 = lstTagGroupList.Where(x => x.Groupname.ToLower().Contains(txtGroupSearch.Text.Trim().ToLower())).ToList();
							  strName = new string[lst1.Count];
							  foreach (var description in lst1)
							  {
								  strName[index] = description.Idgroups + " " + description.Groupname;
								  index++;
							  }
				if (strName != null )//&& strName.Length > 0)
						  {
							  txtGroupSearch.Adapter = null;
							  TagGroupAutoCompleteListAdapter adapter = new TagGroupAutoCompleteListAdapter(this, lst1);
							  adapter.OriginalItems = strName;
							  txtGroupSearch.Adapter = adapter;
						  		adapter.ItemClick -= FnTxtSearchGroupItemClick;
							  adapter.ItemClick += FnTxtSearchGroupItemClick;
						  }
					else
					{
						txtGroupSearch.Adapter = null;
						  TagGroupAutoCompleteListAdapter adapter = new TagGroupAutoCompleteListAdapter(this, lst1);
						  adapter.OriginalItems = strName;
						  txtGroupSearch.Adapter = adapter;
						  adapter.ItemClick -= FnTxtSearchGroupItemClick;
						  adapter.ItemClick += FnTxtSearchGroupItemClick;
					}
				  
			  };
			btnSave.Click +=async delegate
			{
				progress = ProgressDialog.Show(this, "", GetString(Resource.String.please_wait));
				if (string.IsNullOrEmpty(txtEventName.Text) || string.IsNullOrEmpty(txtDescription.Text) || string.IsNullOrEmpty(txtEventDate.Text) || string.IsNullOrEmpty(txtEventStartTime.Text) || string.IsNullOrEmpty(txtAddress.Text))
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strFillValidationError, this);
				}
				else
				{
					bool isConnected = objClsCommonFunctions.FnIsConnected(this);
					if (!isConnected)
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
					}
					else
					{
						
						if(strEventType=="1")
						{
							if (string.IsNullOrEmpty(txtEventURL.Text))
							{
								objClsCommonFunctions.FnAlertMsg(Constants.strAppName,"Enter url to buy ticket", this);
							}
							else
							{
								await Task.Run(() => FnImageResizing());
								await FnCreateNewEventsPostMethod();

							}
						}
						else
						{
							await Task.Run(() => FnImageResizing());
							await FnCreateNewEventsPostMethod();
						}
					}
				}
				FnDismissProgress();
			};
		}
		#region "AutoCompleteView rowclick"
		void FnTxtTagInterestItemClick(Taginterest obj)
		{
			if (strTagInterestName == "" || strTagInterestsId == "")
			{
				strTagInterestName = obj.name;
				strTagInterestsId = obj.id;
			}
			else
			{
				strTagInterestName = strTagInterestName + "," + obj.name;
				strTagInterestsId = strTagInterestsId + "," + obj.id;
			}
			txtGroupInterest.Text = strTagInterestName+",";
			txtGroupInterest.SetSelection(txtGroupInterest.Length());
		}
		void FnTxtTagGroupItemClick(SelectDropdownList obj)
		{
			if (strTagGroupName == "" || strTagGroupId == "")
			{
				strTagGroupName = obj.Groupname;
				strTagGroupId = obj.Idgroups.ToString();
			}
			else
			{
				strTagGroupName = strTagGroupName + "," + obj.Groupname;
				strTagGroupId = strTagGroupId + "," + obj.Idgroups;
			}
			txtGroupInterest.Text = strTagGroupName+",";
			txtGroupInterest.SetSelection(txtGroupInterest.Length());
		}
		void FnTxtSearchGroupItemClick(SelectDropdownList obj)
 		{
			txtGroupSearch.Text = obj.Groupname;
			strGroupSearchId = obj.Idgroups.ToString();
			txtGroupSearch.Focusable = false;
			txtGroupSearch.FocusableInTouchMode = true;

		}
		#endregion

		void FnTitleSelected()
		{
			if (selectedTextTitle == 0)
			{
				lblSpinnerTitle.Text = "Tag Group";
				lnyTagGroupOrInterests.Visibility = ViewStates.Visible;
				rlyTagDemographic.Visibility = ViewStates.Gone;
				lblTagGroup.SetBackgroundColor(Android.Graphics.Color.Gray);
				lblTagInterest.SetBackgroundColor(Android.Graphics.Color.White);
				lblTagDemographic.SetBackgroundColor(Android.Graphics.Color.White);
				view1.Visibility = ViewStates.Visible;
				view2.Visibility = ViewStates.Invisible;
				view3.Visibility = ViewStates.Invisible;
			}
			else if (selectedTextTitle == 1)
			{
				lblSpinnerTitle.Text = "Tag Interests";
				lnyTagGroupOrInterests.Visibility = ViewStates.Visible;
				rlyTagDemographic.Visibility = ViewStates.Gone;
				lblTagGroup.SetBackgroundColor(Android.Graphics.Color.White);
				lblTagInterest.SetBackgroundColor(Android.Graphics.Color.Gray);
				lblTagDemographic.SetBackgroundColor(Android.Graphics.Color.White);
				view1.Visibility = ViewStates.Invisible;
				view2.Visibility = ViewStates.Visible;
				view3.Visibility = ViewStates.Invisible;
			}
			else if (selectedTextTitle == 2)
			{
				lnyTagGroupOrInterests.Visibility = ViewStates.Gone;
				rlyTagDemographic.Visibility = ViewStates.Visible;
				lblTagGroup.SetBackgroundColor(Android.Graphics.Color.White);
				lblTagInterest.SetBackgroundColor(Android.Graphics.Color.White);
				lblTagDemographic.SetBackgroundColor(Android.Graphics.Color.Gray);
				view1.Visibility = ViewStates.Invisible;
				view2.Visibility = ViewStates.Invisible;
				view3.Visibility = ViewStates.Visible;
			}
		}

		#region "Datepicker"
		private void UpdateDisplay()
		{
			txtEventDate.Text = date.ToString("yyyy-MM-dd");
		}
		protected Dialog CreateDialog(int id)
		{
			DateTime currently = DateTime.Now;
			switch (id)
			{
				case Date_Id:
					return new DatePickerDialog(this, OnDateSet, currently.Year, currently.Month-1 , currently.Day);
			}
			return null;
		}
		void OnDateSet(object sender, DatePickerDialog.DateSetEventArgs e)
		{
			this.date = e.Date;
			UpdateDisplay();
		}
		#endregion

		#region "Gallery"

		#region "Gallery image that image show in imageView"
		protected override async void OnActivityResult(int requestCode, Android.App.Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);
			if (resultCode == Android.App.Result.Ok && requestCode == 0)//gallerey
			{
				strImagePath = GetPathToImage(data.Data);
				//check image size
				BitmapFactory.Options option = await objClsCommonFunctions.GetBitmapOptionsOfImage(strImagePath);
				imageWidth = option.OutWidth;
				imageHeight = option.OutHeight;
				if (imageWidth >= 1024 && imageHeight >= 768)
				{
					imgEvent.SetImageURI(data.Data);
					lblImageStatus.Text = "Image Selected";

				}
				else
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Image size is small", this);
					lblImageStatus.Text = "No Image Choosen";
				}
			}
			else if (resultCode == Android.App.Result.Ok && requestCode == 1)//camera
			{
				Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
				Android.Net.Uri contentUri = Android.Net.Uri.FromFile(fileImagePath);
				strImagePath = fileImagePath.AbsolutePath;
				BitmapFactory.Options option = await objClsCommonFunctions.GetBitmapOptionsOfImage(strImagePath);
				imageWidth = option.OutWidth;
				imageHeight = option.OutHeight;
				if (imageWidth >= 1024 && imageHeight >= 768)
				{
					var bitmap = BitmapFactory.DecodeFile(strImagePath);
									
					imgEvent.SetImageBitmap(bitmap);
					lblImageStatus.Text = "Image Selected";
				}
				else
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Image size is small", this);
					lblImageStatus.Text = "No Image Choosen";
				}

			}
			else if (resultCode == Android.App.Result.Canceled)//Canceled
			{

			}
		}
		#endregion

		#region "Get image absolute path"
		//finding image absolute path
		string GetPathToImage(Android.Net.Uri uri)
		{
			string doc_id = "";
			using (var c1 = ContentResolver.Query(uri, null, null, null, null))
			{
				c1.MoveToFirst();
				String document_id = c1.GetString(0);
				doc_id = document_id.Substring(document_id.LastIndexOf(":") + 1);
			}

			string path = null;

			// The projection contains the columns we want to return in our query.
			string selection = Android.Provider.MediaStore.Images.Media.InterfaceConsts.Id + " =? ";
			using (var cursor = ContentResolver.Query(Android.Provider.MediaStore.Images.Media.ExternalContentUri, null, selection, new string[] { doc_id }, null))
			{
				if (cursor == null) return path;
				var columnIndex = cursor.GetColumnIndexOrThrow(Android.Provider.MediaStore.Images.Media.InterfaceConsts.Data);
				cursor.MoveToFirst();
				path = cursor.GetString(columnIndex);
			}
			return path;
		}
		#endregion

		#region "ImageResizing"
		async void FnImageResizing()
		{
			strImageName = strImagePath.Substring(strImagePath.LastIndexOf("/", StringComparison.Ordinal) + 1);
			if (imageWidth >= 1024 && imageHeight >= 768)
			{
				//cover image
				coverphoto =await objClsCommonFunctions.FnConvertByteArray(strImagePath);
				//strFileDataCoverImage = Convert.ToBase64String(coverphoto);//converting byte array to Base64String

			}
			else
			{

			}

		}
		#endregion

		#endregion

		#region "Time Picker"
		protected override Dialog OnCreateDialog(int id)
		{
			if (id == TIME_DIALOG_ID)
				return new TimePickerDialog(this, TimePickerCallback,hour, minute, false);

			return null;
		}

		void TimePickerCallback(object sender, TimePickerDialog.TimeSetEventArgs e)
		{
			//hour = DateTime.Now.Hour;
			//minute= DateTime.Now.Minute;
			hour = e.HourOfDay;
			minute = e.Minute; 
			UpdateTimeDisplay();
		}
		private void UpdateTimeDisplay()
		{
			string time = string.Format("{0}:{1}", hour, minute.ToString().PadLeft(2, '0'));
			if (isTrue)
			{
				txtEventStartTime.Text = time;
				isTrue = false;
			}
			else
			{
				txtEventEndTime.Text = time;
				isTrue = true;
			}
		}
		#endregion

		#region "SearchLocation"
		async Task<string[]> FnSearchLocation(string strUserEnteredName)
		{
			string[] strName = null;
			int index = 0;
			string strFullPath = await objRestServiceCall.CallGooglePlaceCityNameService(strUserEnteredName);
			if (strFullPath == "Exception" || string.IsNullOrEmpty(strFullPath))
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this);
			}
			try
			{
				GoogleLocationClass objGoogleLocationClass = JsonConvert.DeserializeObject<GoogleLocationClass>(strFullPath);
				if (objGoogleLocationClass.status == "OK")
				{
					strName = new string[objGoogleLocationClass.predictions.Count];
					foreach (var description in objGoogleLocationClass.predictions)
					{
						strName[index] = description.description;
						index++;
					}
				}
			}
			catch(Exception e)
			{

			}
			return strName;
		}
		#endregion

		#region "Search Tag interests and group"
		async void FnTagGroupInterst()
		{
			if(selectedTextTitle==0)
			{
				string strTagGroupListResults = await objRestServiceCall.CallTagGroupListService();
				if (strTagGroupListResults == "Exception" || string.IsNullOrEmpty(strTagGroupListResults))
				{

				}
				try
				{
					var objTagGroup = JsonConvert.DeserializeObject<TagGroupClass>(strTagGroupListResults);
					lstTagGroupList = objTagGroup.SelectDropdownList;
				}
				catch(Exception e)
				{

				}
			}
			else if (selectedTextTitle == 1)
			{
				string strTagInterestResults = await objRestServiceCall.CallTagInterestSearchService();
				if (strTagInterestResults == "Exception" || string.IsNullOrEmpty(strTagInterestResults))
				{

				}
				try
				{
					var objInterestSearch = JsonConvert.DeserializeObject<TagInterestClass>(strTagInterestResults);
					lstTagInterestList = objInterestSearch.taginterests;
				}
				catch(Exception e)
				{

				}
			}
		}
		#endregion

		#region "CreateNewEventsPostMethod"
		async Task FnCreateNewEventsPostMethod()
		{
			//FnImageResizing();
			string strCreateNewEventAPI = "";
			//Create New group
			//++++++++++++++  production url     +++++++++++++++++//
			strCreateNewEventAPI = "http://www.youmeus.com";
			//++++++++++++++ test url      +++++++++++++++++//
			//strCreateNewEventAPI = "http://202.83.19.104/youmeuslocalserver";
			uri = uri ?? new Uri(strCreateNewEventAPI);
			RestClient _client = new RestClient(uri);

			bool isConnected = objClsCommonFunctions.FnIsConnected(this);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strPasswordValidationError, this);
			}
			else  
			{
				try
				{
					//post services api
					var request = new RestRequest("helloservice/createeventservice", Method.POST) { RequestFormat = DataFormat.Json };
					request.AddFile("file", coverphoto, strImageName);
					request.AlwaysMultipartFormData = true;


					//Add one of these for each form boundary you need
					request.AddParameter("Idgroup_hidden", strGroupSearchId, ParameterType.GetOrPost);
					request.AddParameter("Event", txtEventName.Text.Trim(), ParameterType.GetOrPost);
					request.AddParameter("EventDescription", txtDescription.Text.Trim(), ParameterType.GetOrPost);
					request.AddParameter("EventDate", txtEventDate.Text.Trim(), ParameterType.GetOrPost);
					request.AddParameter("EventTime", txtEventStartTime.Text.Trim(), ParameterType.GetOrPost);
					request.AddParameter("EventEndtime", txtEventEndTime.Text.Trim(), ParameterType.GetOrPost);

					request.AddParameter("Address", txtAddress.Text.Trim(), ParameterType.GetOrPost);
					request.AddParameter("eventmode", strEventType, ParameterType.GetOrPost);//1:paid ,0:free
					request.AddParameter("Paymenturl", txtEventURL.Text.Trim(), ParameterType.GetOrPost);
					request.AddParameter("Active", strActive, ParameterType.GetOrPost);//0:not checked(off),1:checked(on)
					request.AddParameter("eventtype", strEventPublicity, ParameterType.GetOrPost);//2: is open ,1: is closed
					request.AddParameter("groups", strTagGroupId, ParameterType.GetOrPost);
					request.AddParameter("interests", strTagInterestsId, ParameterType.GetOrPost);

					request.AddParameter("gender", strGender, ParameterType.GetOrPost);//0:female ,1:male: 2:All
					request.AddParameter("people", strPeople, ParameterType.GetOrPost);//1:student ,2:working ,3:non working 0:All
					request.AddParameter("agefrom", txtAgeFrom.Text.Trim(), ParameterType.GetOrPost);
					request.AddParameter("ageto", txtAgeTo.Text.Trim(), ParameterType.GetOrPost);
					request.AddParameter("locations", txtLocations.Text.Trim(), ParameterType.GetOrPost);
					request.AddParameter("Iduser", SplashScreenActivity.strUserId, ParameterType.GetOrPost);


					var response = await Task.Run(() => _client.Execute(request));
					if (response.StatusCode.ToString() == "OK" && response.ResponseStatus.ToString() == "Completed")
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Successfully Created Event ", this);
						lnyEventMaster.Visibility = ViewStates.Visible;
						lnyEventTagLayout.Visibility = ViewStates.Gone;
						FnClearTextFields();
						objClsCommonFunctions.FnDeleteDirectory(Constants.strFolderPath);
					}
					else
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Event Not Created", this);
						FnDismissProgress();
					}
				}
				catch (Exception e)
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Unable to connect to Server. Please try after sometime", this);
					FnDismissProgress();
				}
			}
			KeyBoard.HideSoftInputFromWindow(txtAgeTo.WindowToken, 0);
			KeyBoard.HideSoftInputFromWindow(txtAgeFrom.WindowToken, 0);
			KeyBoard.HideSoftInputFromWindow(txtGroupInterest.WindowToken, 0);
			FnDismissProgress();
		}
		#endregion

		void FnClearTextFields()
		{
			txtGroupSearch.Text = string.Empty;
			txtEventName.Text = string.Empty;
			txtDescription.Text = string.Empty;
			txtEventStartTime.Text = string.Empty;
			txtEventEndTime.Text = string.Empty;
			txtEventDate.Text = string.Empty;
			txtAddress.Text = string.Empty;
			txtEventURL.Text = string.Empty;
			txtAgeFrom.Text = string.Empty;
			txtAgeTo.Text = string.Empty;
			txtLocations.Text = string.Empty;
			txtGroupInterest.Text = string.Empty;
			lblImageStatus.Text = "No Image Choosen";
			imgEvent.SetImageResource(Resource.Drawable.noimages);
			chkActive.Checked = false;
		}


		#region "Dismiss Progress"
		void FnDismissProgress()
		{
			if (progress != null)
			{
				progress.Dismiss();
				progress = null;
			}
		}
		#endregion
		#region "Camara Click"
		private void CreateDirectoryForPictures()
		{
			string strCreated;
			bool iscreated = objClsCommonFunctions.FnCreateFolderInSDCard("YouMeUs", "Camera Images", out strCreated);
			if (iscreated)
			{
				strProductCameraImagePath = System.IO.Path.Combine(strCreated);
			}
		}

		bool IsThereAnAppToTakePictures()
		{
			Intent intent = new Intent(MediaStore.ActionImageCapture);
			IList<ResolveInfo> availableActivities = PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);
			return availableActivities != null && availableActivities.Count > 0;
		}

		void TakeAPicture()
		{
			Intent intent = new Intent(MediaStore.ActionImageCapture);
			string strGUID = Guid.NewGuid().ToString();
			strAbsolutePath = System.IO.Path.Combine(strProductCameraImagePath, String.Format("Camera_image{0}.jpg", strGUID));
			fileImagePath = new Java.IO.File(strProductCameraImagePath, String.Format("Camera_image{0}.jpg", strGUID));
			intent.PutExtra(MediaStore.ExtraOutput, Android.Net.Uri.FromFile(fileImagePath));
			StartActivityForResult(intent, 1);
		}
		#endregion

	}

	public class EventCreateNewClass
	{
		
			public string Idgroup_hidden { get; set; }
			public string Event { get; set; }
			public string  EventDescription { get; set; }
			public string EventDate { get; set; }
			public string EventTime { get; set; }
			public string EventEndtime { get; set; }
			public string  Address { get; set; }
			public string eventmode { get; set; }
			public string  Paymenturl { get; set; }
			public string  Active { get; set; }
			public string eventtype { get; set; }
			public string groups { get; set; }
			public string interests { get; set; }
			public string gender { get; set; }
			public string  people { get; set; }
			public string agefrom { get; set; }
			public string  ageto { get; set; }
			public string locations { get; set;}
			public string  Iduser { get; set; }
			public string fileName { get; set; }
			public string tmp_name { get; set; }
			public string fileData { get; set; }
			public string resized { get; set; }
			public string thumbnails { get; set; }

}
}
//Note:
//1:paid ,0:free{Spinner eventmode(eventType)}
//0:not checked(off),1:checked(on){Checkbox}
//2: is open ,1: is closed{Spinner eventmode(eventType)}
//0:female ,1:male: 2:All{Spinner eventmode(eventType)}
//1:student ,2:working ,3:non working 0:All

