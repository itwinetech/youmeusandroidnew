﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Text;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using RestSharp;
using Square.Picasso;

namespace YouMeUsAndroid
{
	[Activity(WindowSoftInputMode = SoftInput.StateAlwaysHidden, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
	public class MyGroupDescriptionActivity : Activity
	{
		Java.IO.File fileImagePath;
		string strAbsolutePath;
		string strProductCameraImagePath;
		ImageView imgLogo;
		Uri uri;
		bool isPostSuccess = false;
		string strImageName = "";
		byte[] byteImage;
		internal static Orgfeed GroupOrgfeed { get; set; }
		internal static Mygroup MyGroupProperty { get; set; }
		ClsCommonFunctions objClsCommonFunctions;
		ImageView imgGroup;
		Button btnRepositionCover;
		Button btnMakeGroupAdmin;
		Button btnGroupRequest;
		Button btnEdit;
		Button btnCreateEvent;
		Button btnJoinOrUnJoin;
		TextView lblGroupName;
		TextView lblGroupDescription;
		TextView lblImageViewAll;
		TextView lblEventViewAll;
		Refractored.Controls.CircleImageView imgUserImage;
		EditText txtPost;
		RelativeLayout rlyButtonList;
		Refractored.Controls.CircleImageView imgFollower1;
		Refractored.Controls.CircleImageView imgFollower2;
		Refractored.Controls.CircleImageView imgFollower3;
		Refractored.Controls.CircleImageView imgFollower4;
		Refractored.Controls.CircleImageView imgFollower5;
		Button btnPost;
		Button btnAddPhoto;
		RestServiceCall objRestServiceCall;
		//
		ImageView imgGroupImage;
		TextView lblMemberGroupName;
		ListView listViewMember;
		Refractored.Controls.CircleImageView imgGroupAdmin;
		TextView lblAdminName;

		ImageView imgCommentImage;
		TextView lblImageSelected;
		EditText txtImageTextPost;
		Button btnChooseImage;
		Button btnCamera;
		Button btnUploadImage;

		ProgressDialog progress;
		AlertDialog imagePopUpbuilder;
		string strImagePath = "";
		int imageWidth;
		int imageHeight;
		int countValue = 0;
		string strGroupId;
		string strLayoutName;
		string strVideoUrl;
		string strPostContent;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.MyGroupDescriptionLayout);
			FnInitilization();
			FnFillData();
			FnClickEvents();

		}
		public override void OnBackPressed()
		{
			HomeActivity.values = 3;
			base.OnBackPressed();
		}
		void FnInitilization()
		{
			imgLogo = FindViewById<ImageView>(Resource.Id.imgLogoMGDL);
			imgGroup = FindViewById<ImageView>(Resource.Id.imgGDCHFL);
			btnRepositionCover = FindViewById<Button>(Resource.Id.btnRepositionCoverGDCHFL);
			btnMakeGroupAdmin = FindViewById<Button>(Resource.Id.btnMakeGroupAdminGDCHFL);
			btnGroupRequest = FindViewById<Button>(Resource.Id.btnGroupRequestGDCHFL);
			btnEdit = FindViewById<Button>(Resource.Id.btnEditGDCHFL);
			btnCreateEvent = FindViewById<Button>(Resource.Id.btnCreateEventGDCHFL);
			btnJoinOrUnJoin = FindViewById<Button>(Resource.Id.btnJoinGDCHFL);
			rlyButtonList = FindViewById<RelativeLayout>(Resource.Id.rlyButtonsGDCHFL);
			lblGroupName = FindViewById<TextView>(Resource.Id.lblGroupNameGDCHFL);
			lblGroupDescription = FindViewById<TextView>(Resource.Id.lblGroupDescriptionGDCHFL);
			lblImageViewAll = FindViewById<TextView>(Resource.Id.lblViewAllGDCHFL);
			lblEventViewAll = FindViewById<TextView>(Resource.Id.lblViewAllEventsGDCHFL);
			txtPost = FindViewById<EditText>(Resource.Id.txtPostGDCHFL);
			btnPost = FindViewById<Button>(Resource.Id.btnPostGDCHFL);
			btnAddPhoto =FindViewById<Button>(Resource.Id.btnAddPhotoGDCHFL);
			imgUserImage = FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.imgUserGDCHFL);

			imgFollower1 = FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.img1GDCHFL);
			imgFollower2 = FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.img2GDCHFL);
			imgFollower3 = FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.img3GDCHFL);
			imgFollower4 = FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.img4GDCHFL);
			imgFollower5 = FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.img5GDCHFL);
			objRestServiceCall = objRestServiceCall ?? new RestServiceCall();
			objClsCommonFunctions = objClsCommonFunctions ?? new ClsCommonFunctions();
			btnJoinOrUnJoin.SetBackgroundResource(Resource.Drawable.joined);
		}
		void FnFillData()
		{
			strLayoutName = Intent.GetStringExtra("MyData") ?? "Data not available";
			if (strLayoutName == "HomeGroup")
			{
				strGroupId = GroupOrgfeed.Idgroups;
				rlyButtonList.Visibility = ViewStates.Gone;
				btnRepositionCover.Visibility = ViewStates.Gone;
				lblGroupName.Text = GroupOrgfeed.Groupname;

				foreach(var description in GroupOrgfeed.groupdescriptions.groupdetails)
				{
					lblGroupDescription.SetText(Html.FromHtml(description.Groupdesc), TextView.BufferType.Spannable);
				}

				if(GroupOrgfeed.Typeofpost=="1")
				{
					Picasso.With(this).Load(GroupOrgfeed.Groupsurllogo).Into(imgGroup);
				}
				else if (GroupOrgfeed.Typeofpost == "2")
				{
					Picasso.With(this).Load(GroupOrgfeed.Groupsurllogo).Into(imgGroup);
				}

				Picasso.With(this).Load(GroupOrgfeed.userdata.UserProfile).Into(imgUserImage);
				int count = GroupOrgfeed.groupdescriptions.feedsgroupmember.Count;
				if (count == 1)
				{
					imgFollower1.Visibility = ViewStates.Visible;
					imgFollower2.Visibility = ViewStates.Gone;
					imgFollower3.Visibility = ViewStates.Gone;
					imgFollower4.Visibility = ViewStates.Gone;
					imgFollower5.Visibility = ViewStates.Gone;
				}
				else if (count == 2)
				{
					imgFollower1.Visibility = ViewStates.Visible;
					imgFollower2.Visibility = ViewStates.Visible;
					imgFollower3.Visibility = ViewStates.Gone;
					imgFollower4.Visibility = ViewStates.Gone;
					imgFollower5.Visibility = ViewStates.Gone;
				}
				else if (count == 3)
				{
					imgFollower1.Visibility = ViewStates.Visible;
					imgFollower2.Visibility = ViewStates.Visible;
					imgFollower3.Visibility = ViewStates.Visible;
					imgFollower4.Visibility = ViewStates.Gone;
					imgFollower5.Visibility = ViewStates.Gone;
				}
				else if (count == 4)
				{
					imgFollower1.Visibility = ViewStates.Visible;
					imgFollower2.Visibility = ViewStates.Visible;
					imgFollower3.Visibility = ViewStates.Visible;
					imgFollower4.Visibility = ViewStates.Visible;
					imgFollower5.Visibility = ViewStates.Gone;
				}
				else if (count == 5)
				{
					imgFollower1.Visibility = ViewStates.Visible;
					imgFollower2.Visibility = ViewStates.Visible;
					imgFollower3.Visibility = ViewStates.Visible;
					imgFollower4.Visibility = ViewStates.Visible;
					imgFollower5.Visibility = ViewStates.Visible;
				}
				foreach (var values in GroupOrgfeed.groupdescriptions.feedsgroupmember)
				{
					if (countValue == 0)
					{
						Picasso.With(this).Load(values.Modifiedimage).Into(imgFollower1);
					}
					else if (countValue == 1)
					{
						Picasso.With(this).Load(values.Modifiedimage).Into(imgFollower2);
					}
					else if (countValue == 2)
					{
						Picasso.With(this).Load(values.Modifiedimage).Into(imgFollower3);
					}
					else if (countValue == 3)
					{
						Picasso.With(this).Load(values.Modifiedimage).Into(imgFollower4);
					}
					else if (countValue == 4)
					{
						Picasso.With(this).Load(values.Modifiedimage).Into(imgFollower5);
					}

					countValue++;
				}
			}
			else if (strLayoutName == "Group")
			{
				strGroupId = MyGroupProperty.Idgroups;
				rlyButtonList.Visibility = ViewStates.Gone;
				btnRepositionCover.Visibility = ViewStates.Gone;
				lblGroupName.Text = MyGroupProperty.Groupname;


				lblGroupDescription.SetText(Html.FromHtml(MyGroupProperty.Groupdesc), TextView.BufferType.Spannable);



				Picasso.With(this).Load(MyGroupProperty.Groupslogo).Into(imgGroup);

				foreach(var image in MyGroupProperty.userdetails)
				{
					Picasso.With(this).Load(image.Modifiedimage).Into(imgUserImage);
				}

				int count = MyGroupProperty.GroupFollowers.Count;
				if (count == 1)
				{
					imgFollower1.Visibility = ViewStates.Visible;
					imgFollower2.Visibility = ViewStates.Gone;
					imgFollower3.Visibility = ViewStates.Gone;
					imgFollower4.Visibility = ViewStates.Gone;
					imgFollower5.Visibility = ViewStates.Gone;
				}
				else if (count == 2)
				{
					imgFollower1.Visibility = ViewStates.Visible;
					imgFollower2.Visibility = ViewStates.Visible;
					imgFollower3.Visibility = ViewStates.Gone;
					imgFollower4.Visibility = ViewStates.Gone;
					imgFollower5.Visibility = ViewStates.Gone;
				}
				else if (count == 3)
				{
					imgFollower1.Visibility = ViewStates.Visible;
					imgFollower2.Visibility = ViewStates.Visible;
					imgFollower3.Visibility = ViewStates.Visible;
					imgFollower4.Visibility = ViewStates.Gone;
					imgFollower5.Visibility = ViewStates.Gone;
				}
				else if (count == 4)
				{
					imgFollower1.Visibility = ViewStates.Visible;
					imgFollower2.Visibility = ViewStates.Visible;
					imgFollower3.Visibility = ViewStates.Visible;
					imgFollower4.Visibility = ViewStates.Visible;
					imgFollower5.Visibility = ViewStates.Gone;
				}
				else if (count == 5)
				{
					imgFollower1.Visibility = ViewStates.Visible;
					imgFollower2.Visibility = ViewStates.Visible;
					imgFollower3.Visibility = ViewStates.Visible;
					imgFollower4.Visibility = ViewStates.Visible;
					imgFollower5.Visibility = ViewStates.Visible;
				}
				foreach (var values in MyGroupProperty.GroupFollowers)
				{
					if (countValue == 0)
					{
						Picasso.With(this).Load(values.Modifiedimage).Into(imgFollower1);
					}
					else if (countValue == 1)
					{
						Picasso.With(this).Load(values.Modifiedimage).Into(imgFollower2);
					}
					else if (countValue == 2)
					{
						Picasso.With(this).Load(values.Modifiedimage).Into(imgFollower3);
					}
					else if (countValue == 3)
					{
						Picasso.With(this).Load(values.Modifiedimage).Into(imgFollower4);
					}
					else if (countValue == 4)
					{
						Picasso.With(this).Load(values.Modifiedimage).Into(imgFollower5);
					}

					countValue++;
				}
			}


		}
		void FnClickEvents()
		{
			imgLogo.Click += delegate
			{
				FinishAffinity();
				HomeActivity.isHomeShows = false;
				HomeActivity.isHosmeFeedorPost = 1;
				Intent activity = new Intent(this, typeof(HomeActivity));
				activity.PutExtra("MyData", "0");
				StartActivity(activity);
			};
			btnCreateEvent.Click += delegate
			{
				StartActivity(typeof(CreateEventsActivity));
			};
			lblImageViewAll.Click += delegate
			{
				LayoutInflater password = LayoutInflater.From(this);
				View view = password.Inflate(Resource.Layout.GroupMemberLayout, null);

				AlertDialog builder = new AlertDialog.Builder(this).Create();
				builder.SetView(view);
				imgGroupImage = view.FindViewById<ImageView>(Resource.Id.imageViewGroup);
				lblMemberGroupName = view.FindViewById<TextView>(Resource.Id.lblGroupName);
				listViewMember = view.FindViewById<ListView>(Resource.Id.listViewGroupMembers);
				lblAdminName = view.FindViewById<TextView>(Resource.Id.lblGroupAdminName);
				imgGroupAdmin = view.FindViewById<Refractored.Controls.CircleImageView>(Resource.Id.imgAdmin);
				if (strLayoutName == "HomeGroup")
				{
					lblMemberGroupName.Text = GroupOrgfeed.Groupname;
					Picasso.With(this).Load(GroupOrgfeed.Groupsurllogo).Into(imgGroupImage);
					listViewMember.Adapter = new MembersAdapterClass(this, GroupOrgfeed.groupdescriptions.feedsgroupmember);
					foreach (var groupdetails in GroupOrgfeed.groupdescriptions.groupdetails)
					{
						lblAdminName.Text = groupdetails.GroupAdmin;
						Picasso.With(this).Load(groupdetails.Modifiedimage).Into(imgGroupAdmin);
					}
				}
				else if (strLayoutName == "Group")
				{
					lblMemberGroupName.Text = MyGroupProperty.Groupname;
					Picasso.With(this).Load(MyGroupProperty.Groupslogo).Into(imgGroupImage);
					listViewMember.Adapter = new MyGroupFollowerAdapterClass(this, MyGroupProperty.GroupFollowers);

					foreach(var admin in MyGroupProperty.admin)
					{
						lblAdminName.Text =admin.Fname;
						Picasso.With(this).Load(admin.Modifiedimage).Into(imgGroupAdmin);
					}
						
					//imgGroupAdmin.SetImageResource(Resource.Drawable.UserProfile);
						

				}
				builder.Show();
			};
			btnJoinOrUnJoin.Click += async delegate
			   {
				   if (strLayoutName == "HomeGroup")
				   {
					   string strStoreGroups = await objRestServiceCall.CallJoinOrUnjoinGroupService(strGroupId, SplashScreenActivity.strUserId);

					   var obj = JsonConvert.DeserializeObject<GroupAdding>(strStoreGroups);
					   foreach (var values in obj.array)
					   {
						   if (values.Isamember == "0")//Isamember=0=>remove,Isamember=1=>add
						   {
							   btnJoinOrUnJoin.SetBackgroundResource(Resource.Drawable.join);
						   }
						   else
						   {
							   btnJoinOrUnJoin.SetBackgroundResource(Resource.Drawable.joined);
						   }
					   }
				   }
				   else if(strLayoutName == "Group")
				   {
					   string strStoreGroups = await objRestServiceCall.CallJoinOrUnjoinGroupService(strGroupId, SplashScreenActivity.strUserId);

					   var obj = JsonConvert.DeserializeObject<GroupAdding>(strStoreGroups);
					   foreach (var values in obj.array)
					   {
						   if (values.Isamember == "0")//Isamember=0=>remove,Isamember=1=>add
						   {
							   btnJoinOrUnJoin.SetBackgroundResource(Resource.Drawable.join);
						   }
						   else
						   {
							   btnJoinOrUnJoin.SetBackgroundResource(Resource.Drawable.joined);
						   }
					   }
				   }
			   };
			btnPost.Click += async delegate
				{
					progress = ProgressDialog.Show(this, "", GetString(Resource.String.please_wait));
					if (string.IsNullOrEmpty(txtPost.Text))
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "This post appears blank.Please write something", this);
					}
					else
					{
						bool isConnected = objClsCommonFunctions.FnIsConnected(this);
						if (!isConnected)
						{
							objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
						}
						else
						{
							bool isTrue = await FnGroupPostTextAndVideo();
							if (isTrue)
							{
							HomeActivity.values = 3;
								Intent activity = new Intent(this, typeof(HomeActivity));
								activity.PutExtra("MyData", "0");
								StartActivity(activity);
								Finish();
							}
							else
							{
							objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Unable to post,please try later", this);
							}
							FnDismissProgress();
						}
					}
					FnDismissProgress();
				};
			btnAddPhoto.Click += delegate
				{
					FnChooseImage();
				};
		}
		void FnChooseImage()
		{
			LayoutInflater imagePopUp = LayoutInflater.From(this);
			View view1 = imagePopUp.Inflate(Resource.Layout.ImagePopUpLayout, null);

			imagePopUpbuilder = new AlertDialog.Builder(this).Create();
			imagePopUpbuilder.SetView(view1);
			imgCommentImage = view1.FindViewById<ImageView>(Resource.Id.imgGroupProfile);
			btnChooseImage = view1.FindViewById<Button>(Resource.Id.btnChooseFile);
			btnUploadImage = view1.FindViewById<Button>(Resource.Id.btnUploadImage);
			txtImageTextPost= view1.FindViewById<EditText>(Resource.Id.txtimgTextPost);
			lblImageSelected = view1.FindViewById<TextView>(Resource.Id.lblChoosenFile);
			btnCamera = view1.FindViewById<Button>(Resource.Id.btnCamera);
			imagePopUpbuilder.SetCanceledOnTouchOutside( false);
			btnCamera.Click += delegate
			{
				if (IsThereAnAppToTakePictures())
				{
					CreateDirectoryForPictures();
				}
				TakeAPicture();
			};
			btnChooseImage.Click += delegate
			{
				var imageIntent = new Intent();
				imageIntent.SetType("image/*");
				imageIntent.SetAction(Intent.ActionGetContent);
				StartActivityForResult(Intent.CreateChooser(imageIntent, "Select Image"), 0);
			};
			btnUploadImage.Click += async delegate
			{
				progress = ProgressDialog.Show(this, "", GetString(Resource.String.please_wait));
				if(!string.IsNullOrEmpty(strImagePath))
				{
					await Task.Run(() => FnImageResizing());
					bool isTrue=await FnImageUploadingToServer();
					if (isTrue)
					{
						HomeActivity.values = 3;

						Intent activity = new Intent(this, typeof(HomeActivity));
						activity.PutExtra("MyData", "0");
						StartActivity(activity);
						Finish();
					}
					else
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Unable to Upload image,please try later", this);
					}
				}
				else
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Image is not selected", this);
				}
				FnDismissProgress();
			};
			imagePopUpbuilder.Show();
		}

		async Task<bool> FnImageUploadingToServer()
		{
			bool isSuccess = false;
			string strCreateNewGroupAPI = "";
			//Create New group
			//++++++++++++++  production url     +++++++++++++++++//
			strCreateNewGroupAPI = "http://www.youmeus.com";
			//++++++++++++++ test url      +++++++++++++++++//
			// strCreateNewGroupAPI = "http://202.83.19.104/youmeuslocalserver";
			uri = uri ?? new Uri(strCreateNewGroupAPI);
			RestClient _client = new RestClient(uri);

			bool isConnected = objClsCommonFunctions.FnIsConnected(this);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
				isSuccess = false;
			}
			else
			{
				try
				{
					var request = new RestRequest("helloservice/addphoto", Method.POST) { RequestFormat = DataFormat.Json };
					request.AddFile("file", byteImage, strImageName);

					request.AlwaysMultipartFormData = true;
					//Add one of these for each form boundary you need
					request.AddParameter("groupid", strGroupId, ParameterType.GetOrPost);
					request.AddParameter("iduser",SplashScreenActivity.strUserId , ParameterType.GetOrPost);
					request.AddParameter("txt",txtImageTextPost.Text.Trim() , ParameterType.GetOrPost);
					var response = await Task.Run(() => _client.Execute(request));
					if (response.StatusCode.ToString() == "OK" && response.ResponseStatus.ToString() == "Completed")
					{
						// "Successfully  uploaded image "
						FnDismissProgress();
						objClsCommonFunctions.FnDeleteDirectory(Constants.strFolderPath);
						imagePopUpbuilder.Dismiss();
						isSuccess = true;
					}
					else
					{
						//"Not Image uploaded"
						FnDismissProgress();
						isSuccess = false;
					}
				}
				catch (Exception e)
				{
					FnDismissProgress();
					isSuccess = false;
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Unable to connect to Server. Please try after sometime", this);
				}
			}
			return isSuccess;
		}

		async Task<bool> FnGroupPostTextAndVideo()
		{
			bool isSuccess = false;
			bool isConnected = objClsCommonFunctions.FnIsConnected(this);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
			}
			else
			{
				try
				{
					if(!string.IsNullOrEmpty(txtPost.Text))
					{
						strPostContent = txtPost.Text.Trim();
					}
					if (txtPost.Text.Trim().Contains("https://"))
					{
						string strVideoID = GetVideoID(txtPost.Text.Trim());
						if (!string.IsNullOrEmpty(strVideoID))
						{
							string[] strval = Regex.Split(txtPost.Text.Trim(), " ");
							strVideoUrl = strval[0];
							strVideoUrl = strVideoID.Equals("failed") ? string.Empty : string.Format("https://www.youtube.com/watch?v={0}", strVideoID);


							strPostContent = string.Empty;
						}
						else
						{
							strPostContent=txtPost.Text.Trim();
						}

					}
					string strPostContentResult = await objRestServiceCall.CallPostOnGroupDescription(strGroupId, SplashScreenActivity.strUserId,strPostContent,strVideoUrl, "1");
					var objPostOnGroupWall = JsonConvert.DeserializeObject<PostOnGroupWall>(strPostContentResult);
					if(objPostOnGroupWall.posttext)
					{
						
						isSuccess=objPostOnGroupWall.posttext;
					}
					else
					{
						isSuccess=objPostOnGroupWall.posttext;
					}
				}
				catch
				{
					isSuccess = false;
				}
			}
			return isSuccess;
		}
		static string GetVideoID(string strVideoURL)
		{
			const string regExpPattern = @"youtu(?:\.be|be\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)";
			Regex regEx = new Regex(regExpPattern);
			Match match = regEx.Match(strVideoURL);
			return match.Success ? match.Groups[1].Value : "failed";
		}



		#region "ImageResizing"
		async void FnImageResizing()
		{

			strImageName = strImagePath.Substring(strImagePath.LastIndexOf("/", StringComparison.Ordinal) + 1);
			if (imageWidth >= 1024 && imageHeight >= 768)
			{
				//cover image
				 byteImage = await objClsCommonFunctions.FnConvertByteArray(strImagePath);
				var s=byteImage.Length;
			}
			else
			{
				
			}

		}
		#endregion

		#region "Gallery"
		#region "Gallery image that image show in imageView"
		protected override async void OnActivityResult(int requestCode, Android.App.Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);

			if (resultCode == Android.App.Result.Ok && requestCode == 0)//gallery
				{
				strImagePath = GetPathToImage(data.Data);
				//check image size
				BitmapFactory.Options option = await objClsCommonFunctions.GetBitmapOptionsOfImage(strImagePath);
				imageWidth = option.OutWidth;
				imageHeight = option.OutHeight;
				if (imageWidth >= 1024 && imageHeight >= 768)
				{
					imgCommentImage.SetImageURI(data.Data);
					lblImageSelected.Text = "Image Selected";
				}
				else
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Image size is small", this);
					lblImageSelected.Text = "No Image Choosen";
				}
			}
			else if(resultCode == Android.App.Result.Ok && requestCode == 1)
			{
				Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
				Android.Net.Uri contentUri = Android.Net.Uri.FromFile(fileImagePath);
				strImagePath = fileImagePath.AbsolutePath;
				BitmapFactory.Options option = await objClsCommonFunctions.GetBitmapOptionsOfImage(strImagePath);
				imageWidth = option.OutWidth;
				imageHeight = option.OutHeight;
				if (imageWidth >= 1024 && imageHeight >= 768)
				{
					var bitmap = BitmapFactory.DecodeFile(strImagePath);
					//Matrix matrix = new Matrix();
					//matrix.PostRotate(90);
					//var bitmap1= Bitmap.CreateBitmap(bitmap, 0, 0, bitmap.Width, bitmap.Height,
					//						   matrix, true);
					imgCommentImage.SetImageBitmap(bitmap);
					lblImageSelected.Text = "Image Selected";
				}
				else
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Image size is small", this);
					lblImageSelected.Text = "No Image Choosen";
				}

			}

		}
		#endregion

		#region "Get image absolute path"
		//finding image absolute path
		string GetPathToImage(Android.Net.Uri uri)
		{
			string doc_id = "";
			using (var c1 = ContentResolver.Query(uri, null, null, null, null))
			{
				c1.MoveToFirst();
				String document_id = c1.GetString(0);
				doc_id = document_id.Substring(document_id.LastIndexOf(":") + 1);
			}

			string path = null;

			// The projection contains the columns we want to return in our query.
			string selection = Android.Provider.MediaStore.Images.Media.InterfaceConsts.Id + " =? ";
			using (var cursor = ContentResolver.Query(Android.Provider.MediaStore.Images.Media.ExternalContentUri, null, selection, new string[] { doc_id }, null))
			{
				if (cursor == null) return path;
				var columnIndex = cursor.GetColumnIndexOrThrow(Android.Provider.MediaStore.Images.Media.InterfaceConsts.Data);
				cursor.MoveToFirst();
				path = cursor.GetString(columnIndex);
			}
			return path;
		}
		#endregion

		#endregion

		#region "Dismiss Progress"
		void FnDismissProgress()
		{
			if (progress != null)
			{
				progress.Dismiss();
				progress = null;
			}

		}
		#endregion

		#region "Camara Click"
		private void CreateDirectoryForPictures()
		{
			string strCreated;
			bool iscreated = objClsCommonFunctions.FnCreateFolderInSDCard("YouMeUs", "Camera Images", out strCreated);
			if (iscreated)
			{
				strProductCameraImagePath = System.IO.Path.Combine(strCreated);
			}
		}

		bool IsThereAnAppToTakePictures()
		{
			Intent intent = new Intent(MediaStore.ActionImageCapture);
			IList<ResolveInfo> availableActivities = PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);
			return availableActivities != null && availableActivities.Count > 0;
		}

		void TakeAPicture()
		{
			Intent intent = new Intent(MediaStore.ActionImageCapture);
			string strGUID = Guid.NewGuid().ToString();
			strAbsolutePath = System.IO.Path.Combine(strProductCameraImagePath, String.Format("Camera_image{0}.jpg", strGUID));
			fileImagePath = new Java.IO.File(strProductCameraImagePath, String.Format("Camera_image{0}.jpg", strGUID));
			intent.PutExtra(MediaStore.ExtraOutput, Android.Net.Uri.FromFile(fileImagePath));
			StartActivityForResult(intent,1);
		}
		#endregion

	}
	//group post(text+video) service class
	public class PostOnGroupWall
	{
		public bool posttext { get; set; }
		public int lastInsertPostID { get; set; }
	}
}

