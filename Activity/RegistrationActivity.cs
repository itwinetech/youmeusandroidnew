﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Xamarin.Auth;
using System.Threading.Tasks;
//using System.EnterpriseServices;
using System.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using Android.Text;

namespace YouMeUsAndroid
{
	[Activity (WindowSoftInputMode = SoftInput.StateAlwaysHidden,ScreenOrientation=Android.Content.PM.ScreenOrientation.Portrait )]			
	public class RegistrationActivity : Activity
	{
		EditText txtUserName;
		EditText txtEmailId;
		EditText txtPassword;
		EditText txtConfirmPassword;
		Button btnSignUp;
		TextView lblTermsAndConditions;
		ImageView btnGoogleSignUp;
		ImageView btnFaceBookSignUp;
		//terms and conditions
		LinearLayout lnyTermsAndConditionsLayout;
		TextView lblText;
		static readonly TaskScheduler UIScheduler = TaskScheduler.FromCurrentSynchronizationContext(); 

		GoogleInfo googleIngo;
		string access_token; 
		static internal  string strEmail =null; 
		ClsCommonFunctions objClsCommonFunctions;
		FacebookResponseClass objFacebookResponseClass;
		RestServiceCall objRestServiceCall;
		ProgressDialog progress;
		string strEmailId;
		string strValue="SignInPage";

		protected override void OnCreate ( Bundle savedInstanceState )
		{
			RequestWindowFeature ( WindowFeatures.NoTitle );
			base.OnCreate ( savedInstanceState );
			SetContentView ( Resource.Layout.RegistrationLayout );
			FnInitilization ();
			FnClickEvents ();
		}

		void FnInitilization()
		{
			txtUserName = FindViewById<EditText> ( Resource.Id.txtUserName );
			txtEmailId = FindViewById<EditText> ( Resource.Id.txtEmailId );
			txtPassword = FindViewById<EditText> ( Resource.Id.txtPassword );
			txtConfirmPassword = FindViewById<EditText> ( Resource.Id.txtConfirmPassword );
			btnSignUp = FindViewById<Button> ( Resource.Id.btnSignUp );
			lblTermsAndConditions=FindViewById<TextView> (Resource.Id.lblTermsAndConditions);
			btnGoogleSignUp=FindViewById<ImageView> (Resource.Id.imgGoogle);
			btnFaceBookSignUp=FindViewById<ImageView> (Resource.Id.imgFacebook);

			lnyTermsAndConditionsLayout = FindViewById<LinearLayout> ( Resource.Id.lnyTermsAndConditionsLayout );
			lblText = FindViewById<TextView> (Resource.Id.lbltextView1);
			btnSignUp.SetTextColor(Android.Graphics.Color.White);

			objClsCommonFunctions = objClsCommonFunctions ?? new ClsCommonFunctions ();
			objRestServiceCall = objRestServiceCall ?? new RestServiceCall ();
		}

		public override void OnBackPressed ()
		{
			
			if ( strValue == "TermsAndCondition" )
			{
				lnyTermsAndConditionsLayout.Visibility = ViewStates.Gone;
				strValue="SignInPage";
			}
			else
			{
				base.OnBackPressed ();
			}
		}

		void FnClickEvents()
		{
			btnSignUp.Click += async delegate (object sender, EventArgs e)
			{
				if ((string.IsNullOrEmpty(txtUserName.Text) || string.IsNullOrEmpty(txtEmailId.Text) || string.IsNullOrEmpty(txtPassword.Text) || string.IsNullOrEmpty(txtConfirmPassword.Text)))
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strFillValidationError, this);
				}
				else
				{
					bool isTrue = objClsCommonFunctions.FnEmailValidation(txtEmailId.Text.Trim());
					if (!isTrue)
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strEmailValidationError, this);
					}
					else
					{
						if (txtPassword.Text.Trim() != txtConfirmPassword.Text.Trim())
						{
							objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strPasswordValidationError, this);
						}
						else
						{
							bool isConnected = objClsCommonFunctions.FnIsConnected(this);
							if (!isConnected)
							{
								objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
							}
							else
							{
								progress = ProgressDialog.Show(this, "", "Please wait...");
								string strJsonResponse = await objRestServiceCall.CallSignUpService(txtUserName.Text, txtEmailId.Text, txtPassword.Text);
								if (strJsonResponse == "Exception" || string.IsNullOrEmpty(strJsonResponse))
								{
									objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this);
									progress.Dismiss();
									progress = null;
									return;
								}
								else
								{
									try
									{
										SignUp objSignUp;
										strEmailId = txtEmailId.Text;
										objSignUp = JsonConvert.DeserializeObject<SignUp>(strJsonResponse);
										int intResult = objSignUp.result;
										if (intResult == 1)
										{
											progress.Dismiss();
											progress = null;
											objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strRegisteredSuccessfully, this);
											FnClearFields();
										}
										else if (intResult == 0)
										{
											progress.Dismiss();
											progress = null;
											objClsCommonFunctions.FnAlertMsg(Constants.strAppName, string.Format(Constants.strAccountExists, strEmailId), this);
											txtEmailId.Text = string.Empty;
										}
									}
									catch(Exception m)
									{
										progress.Dismiss();
										progress = null;
										objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Whoops...Unable to process! try again later", this);
										return;
									}
								}
							}
						}
					}
				}
			};
			lblTermsAndConditions.Click += delegate(object sender , EventArgs e )
			{
				bool isConnected = objClsCommonFunctions.FnIsConnected(this);
				if (!isConnected)
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
				}
				else
				{
					FnTermsAndConditions();
				}
			};
			btnFaceBookSignUp.Click += delegate(object sender , EventArgs e )
			{
				fnFbLogin();
			};
			btnGoogleSignUp.Click += delegate (object sender, EventArgs e)
			{
				LoginByGoogle(true);
			};
		}
		#region "TermsAndConditions"
	async	void FnTermsAndConditions()
		{
			
			lblText.Text = "";
			strValue="TermsAndCondition";
			lnyTermsAndConditionsLayout.Visibility = ViewStates.Visible;
			string strTermsAndConditionResults = await objRestServiceCall.CallTermsAndConditionAPIService();
			if (strTermsAndConditionResults == "Exception" || string.IsNullOrEmpty(strTermsAndConditionResults))
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this);
			}
			try
			{
				var objTermsConditionsDetails = JsonConvert.DeserializeObject<TermsConditionsDetails>(strTermsAndConditionResults);

				if (objTermsConditionsDetails.TermsConditions.Description != null)
				{
					lblText.SetText(Html.FromHtml(objTermsConditionsDetails.TermsConditions.Description), TextView.BufferType.Spannable); 
				}
			}
			catch
			{

			}
		}
		#endregion

		#region "ClearFields"
		void	FnClearFields()
		{
			txtUserName.Text = string.Empty;
			txtEmailId.Text = string.Empty;
			txtPassword.Text = string.Empty;
			txtConfirmPassword.Text = string.Empty;
		}
		#endregion

		#region "Google SignUp(Login)"
		void LoginByGoogle (bool allowCancel)
		{  
			//Google  credentials  for Project, need to register in developer.google.com
			//Client id has to be generated  from google page by providing hash code from  keystore
			//redirecturi : is the page to be shown after successful login,here shown sample page from one of the running application
			//for this any of the youmeus hosted page can be taken 
			var auth = new OAuth2Authenticator ( clientId: "750450932540-2u2epn1kmnu2jtrv4ia33qr7pof3u9d4.apps.googleusercontent.com" ,
				scope: "https://www.googleapis.com/auth/userinfo.email" ,
				authorizeUrl: new Uri ( "https://accounts.google.com/o/oauth2/auth" ) , 
				redirectUrl: new Uri ( "http://qualityinaction.net/aspxDbScript.aspx" ) , 
				getUsernameAsync: null ); 
			auth.AllowCancel = allowCancel;   
			auth.Completed += (sender , e ) =>
			{  
				if ( !e.IsAuthenticated )
				{ 
					objClsCommonFunctions.FnAlertMsg ( Constants.strAppName , "Not Authenticated" , this );
					return;
				}
				e.Account.Properties.TryGetValue ( "access_token" , out access_token ); 
				getInfo ();  
			};  
			var intent = auth.GetUI ( this );
			StartActivity ( intent ); 
		}

		int  attempt=0;
		async void getInfo()
		{ 
			progress =  ProgressDialog.Show (this,"","Fetching Response...");

			string userInfo = await objRestServiceCall.GetDataFromGoogle (access_token ); 

			if ( userInfo != "Exception" )
			{ 
				googleIngo = JsonConvert.DeserializeObject<GoogleInfo> ( userInfo ); 
				strEmailId=strEmail = googleIngo.email; 
				FnGoogleSignUp (googleIngo);
			}
			else
			{
				if ( attempt <= 1 )
				{
					attempt += 1; 
					userInfo = await objRestServiceCall.GetDataFromGoogle (access_token ); 
					if ( userInfo != "Exception" )
					{
						googleIngo = JsonConvert.DeserializeObject<GoogleInfo> ( userInfo ); 
						strEmailId=strEmail = googleIngo.email;  
						if ( progress != null )
						{
							progress.Dismiss ();
							progress = null;
						}  
						FnGoogleSignUp (googleIngo);
					} 
				}
				else
				{
					objClsCommonFunctions.FnAlertMsg ( Constants.strAppName , "Connection failed! Please try again" , this );
				}
				if ( progress != null )
				{
					progress.Dismiss ();
					progress = null;
				}
			}
		}
		#endregion

		#region "FacebookSignUp"
		async void FnFacebookSignUp(FacebookResponseClass objFacebookInfo)
		{
			if (progress != null)
			{
				progress.Dismiss();
				progress = null;
			}
			progress = ProgressDialog.Show(this, "", "Please wait...");
			try
			{
				string strJsonResponse = await objRestServiceCall.CallFacebookSignUp(objFacebookInfo.name,objFacebookInfo.email,objFacebookInfo.gender);
				if (strJsonResponse == "Exception" || string.IsNullOrEmpty(strJsonResponse))
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this);
					progress.Dismiss();
					progress = null;
					return;
				}
				else
				{
					try
					{
						FacebookSignUp objGoogleSignUp;
						objGoogleSignUp = JsonConvert.DeserializeObject<FacebookSignUp>(strJsonResponse);
						int intResult = objGoogleSignUp.result;
						if (intResult == 1)//inserting
						{
							progress.Dismiss();
							progress = null;
							objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "You have SignUp successfully", this);
						}
						else if (intResult == 0)//alerdy exist
						{
							progress.Dismiss();
							progress = null;
							objClsCommonFunctions.FnAlertMsg(Constants.strAppName, string.Format(Constants.strAccountExists, strEmailId), this);
							txtEmailId.Text = string.Empty;
						}
					}
					catch (Exception m)
					{
						if (progress != null)
						{
							progress.Dismiss();
							progress = null;
						}
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Whoops...Error response!", this);
					}
				}
			}
			catch (Exception m)
			{
				if (progress != null)
				{
					progress.Dismiss();
					progress = null;
				}
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Whoops...Error response!", this);
			}
		}
		#endregion

		#region "GoogleSignUp"
		async void FnGoogleSignUp(GoogleInfo objGoogleInfo)
		{
			if ( progress != null )
			{
				progress.Dismiss ();
				progress = null;
			} 
			progress = ProgressDialog.Show ( this , "" , "Please wait..." );
			try
			{
				string strJsonResponse = await objRestServiceCall.CallGoogleSignUp (objGoogleInfo.given_name,objGoogleInfo.family_name,objGoogleInfo.email,objGoogleInfo.gender );
				if ( strJsonResponse == "Exception" || string.IsNullOrEmpty ( strJsonResponse ) )
				{
					objClsCommonFunctions.FnAlertMsg ( Constants.strAppName , Constants.strServserError , this );
					progress.Dismiss ();
					progress = null;
					return;
				}
				else
				{
					try
					{
						GoogleSignUp objGoogleSignUp;
						objGoogleSignUp = JsonConvert.DeserializeObject<GoogleSignUp> ( strJsonResponse );
						int intResult = objGoogleSignUp.result; 
						if ( intResult == 1 )//inserting
						{
							progress.Dismiss ();
							progress = null;
							objClsCommonFunctions.FnAlertMsg ( Constants.strAppName ,"You have SignUp successfully", this );
							FnClearFields ();
						}
						else if ( intResult == 0 )//alerdy exist
						{
							progress.Dismiss ();
							progress = null;
							objClsCommonFunctions.FnAlertMsg ( Constants.strAppName , string.Format (Constants.strAccountExists , strEmailId ) , this );
							txtEmailId.Text = string.Empty; 
						}
					}
					catch(Exception m)
					{
						if ( progress != null )
						{ 
							progress.Dismiss ();
							progress = null;
						}
						objClsCommonFunctions.FnAlertMsg (Constants.strAppName,"Whoops...Error response!",this);
					}
				}
			}
			catch(Exception m)
			{
				if ( progress != null )
				{ 
					progress.Dismiss ();
					progress = null;
				}
				objClsCommonFunctions.FnAlertMsg (Constants.strAppName,"Whoops...Error response!",this);
			}
		}
		#endregion

		#region "Facebook login"

		void fnFbLogin()
		{
			//For login by facebook need register the project in developer.facebook.com
			//need to follow the steps in developer page , and to create the clientId
			//below code clientId: "822515487781094" this client id generated from developer account 
			var auth = new OAuth2Authenticator(
				clientId: "1617416131886509",
				scope: "",
				authorizeUrl: new Uri("https://m.facebook.com/dialog/oauth/"),
				redirectUrl: new Uri("http://qualityinaction.net/aspxDbScript.aspx"));

			auth.Completed += async (s, ee) =>
			{
				if (!ee.IsAuthenticated)
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Not Authenticated", this);
					return;
				}
				else
				{
					// Use eventArgs.Account to do wonderful things 
					var accessToken = ee.Account.Properties["access_token"].ToString();
					//var expiresIn = Convert.ToDouble (ee.Account.Properties ["expires_in"]);
					//var expiryDate = DateTime.Now + TimeSpan.FromSeconds (expiresIn);
					progress = ProgressDialog.Show(this, "", "Fetching Response...");
					var request = new OAuth2Request("GET", new Uri("https://graph.facebook.com/me"), null, ee.Account);
					var response = await request.GetResponseAsync();
					if (accessToken != null)
					{
						strValue = await GetFBProfileAsync(accessToken);
					}
					await request.GetResponseAsync().ContinueWith(t =>
				  {
					  var builder = new AlertDialog.Builder(this);
					  if (t.IsFaulted)
					  {
						  builder.SetTitle("Error");
						  builder.SetMessage(t.Exception.Flatten().InnerException.ToString());
					  }
					  else if (t.IsCanceled)
						  builder.SetTitle("Task Canceled");
					  else
					  {
						  string json = t.Result.GetResponseText();
						  objFacebookResponseClass = JsonConvert.DeserializeObject<FacebookResponseClass>(strValue);
						  strEmailId=strEmail = objFacebookResponseClass.email;
					  }
					  builder.SetPositiveButton("Ok", (o, e3) =>
					  {
					  });
					  if (!string.IsNullOrEmpty(strEmail))
					  {
						  FnFacebookSignUp(objFacebookResponseClass);
					  }
				  }, UIScheduler);
				}
			};
			var intent = auth.GetUI(this);
			StartActivity(intent);
		}

		async Task<string> GetFBProfileAsync(string strAccessToken)
		{
			var requestUrl = "https://graph.facebook.com/me/" + "?fields=name,picture,cover,age_range,birthday,devices,is_verified,languages,email" + "&access_token=" + strAccessToken;
			var httpClient = new HttpClient();
			var userjson = await httpClient.GetStringAsync(requestUrl);
			Console.WriteLine(userjson);
			return userjson;
		}
		#endregion

	}
}

