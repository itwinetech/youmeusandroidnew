﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Android.Graphics;
using Square.Picasso;
using System.Net;
using RestSharp;
using System.IO;
using Environment = Android.OS.Environment;
using Java.IO;
using Android.Provider;
using Android.Content.PM;
namespace YouMeUsAndroid
{
	[Activity(ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
	public class EditProfileActivity : Activity
	{
		internal static ViewProfile viewProfile { get; set; }
		ImageView imgLogo;
		ImageView imgCover;
		ImageView imgProfile;
		Button btnEdit;
		Button btndate;
		Button btnSave;
		EditText txtFirstName;
		EditText txtLastName;
		EditText txtDob;
		AutoCompleteTextView txtCurrentCity;
		RadioButton rbtnMale;
		RadioButton rbtnFemale;
		RadioButton rbtnOther;
		CheckBox chkHideDOB;
		DateTime date;
		const int date_Id = 0;
		string strGender = "Male";
		int imageWidth;
		int imageHeight;
		string strImagePath = "";
		string strFileData;
		Android.Net.Uri imageUri;
		byte[] photo;
		internal static string strCoverImageName;
		internal static byte[] profilePhoto;
		internal static byte[] coverPhoto;
		internal static byte[] thumbfilePhoto;
		internal static string strProfileImageName;
		Bitmap originalImage;
		//Boolean imgProfileClicked = false;
		WebClient webClient;
		ProgressDialog progress;
		RestServiceCall objRestServiceCall;
		ClsCommonFunctions objClsCommonFunctions;
		EditProfileSuccessClass objEditProfileSuccessClass;
		const string strUserImageUploadURL = "http://202.83.19.104/youmeus/uploadsimages/uploadimages.php";
		string strCoverImagePath;
		string strProfileImagePath;
		Uri uri;
		bool isCropedOrNot = true;
		string strFileImageProfileData;
		int coverImageWidth;
		int coverImageHeight;
		string strOriginalImage;
		Bundle contentUri;
		string strFileImageCoverData;

		int profileimageWidth;
		int profileimageHeight;
		string strOriginalProfileImage;
		string strOriginalCoverImage;

		string strTypeOfImageFinal = "0";
		string strFileDataCoverResizedImage;
		string strFileDataCoverCoverThumbPhotoImage;
		protected override void OnCreate(Bundle savedInstanceState)
		{
			RequestWindowFeature(WindowFeatures.NoTitle);
			base.OnCreate(savedInstanceState);
			// Create your application here
			SetContentView(Resource.Layout.EditProfileLayout);
			FnInitilization();
			FnClickEvents();
		}

		#region "Initilization"
		void FnInitilization()
		{
			imgLogo = FindViewById<ImageView>(Resource.Id.imgLogoEPL);
			imgCover = FindViewById<ImageView>(Resource.Id.imgCoverEPL);
			imgProfile = FindViewById<ImageView>(Resource.Id.imgProfileEPL);
			btnEdit = FindViewById<Button>(Resource.Id.btnEditEPL);
			btndate = FindViewById<Button>(Resource.Id.btnDateEPL);
			btnSave = FindViewById<Button>(Resource.Id.btnsaveEPL);
			txtFirstName = FindViewById<EditText>(Resource.Id.txtFNameEPL);
			txtLastName = FindViewById<EditText>(Resource.Id.txtLNameEPL);
			txtDob = FindViewById<EditText>(Resource.Id.txtDobEPL);
			txtCurrentCity = FindViewById<AutoCompleteTextView>(Resource.Id.autotxtCurrentCityEPL);
			rbtnMale = FindViewById<RadioButton>(Resource.Id.rbtnMaleEPL);
			rbtnFemale = FindViewById<RadioButton>(Resource.Id.rbtnFemaleEPL);
			rbtnOther = FindViewById<RadioButton>(Resource.Id.rbtnOtherEPL);
			chkHideDOB = FindViewById<CheckBox>(Resource.Id.chkHideDOB);
			objEditProfileSuccessClass = objEditProfileSuccessClass ?? new EditProfileSuccessClass();


			objRestServiceCall = objRestServiceCall ?? new RestServiceCall();
			objClsCommonFunctions = objClsCommonFunctions ?? new ClsCommonFunctions();
			if (HomeActivity.valueOnceEditProfile == 0)
			{
				FnDisabeleAllControls();

			}
			FnFillUserDetails();
		}
		#endregion


		void FnFillUserDetails()
		{
			
			//Assignig Data to Profile
			//cover image
			if (!string.IsNullOrEmpty(viewProfile.UserCover.CoverProfile))
			{
				Picasso.With(this).Load(viewProfile.UserCover.CoverProfile).Into(imgCover);
			}
			else
			{
				imgCover.SetImageResource(Resource.Drawable.noimages);
			}
			//Profile Image
			if (!string.IsNullOrEmpty(viewProfile.UserProfile))
			{
				Picasso.With(this).Load(viewProfile.UserProfile).Into(imgProfile);
			}
			else
			{
				imgProfile.SetImageResource(Resource.Drawable.noimages);
			}

			txtFirstName.Text = viewProfile.Fname;
			txtLastName.Text = viewProfile.Lname;
			if (!string.IsNullOrEmpty(viewProfile.Dateofbirth.ToString()) || viewProfile.Dateofbirth.ToString() != null)
			{
				txtDob.Text = viewProfile.Dateofbirth.ToString();
			}
			txtCurrentCity.Text = viewProfile.currentcity;
			//checkbox
			if(viewProfile.DOBprivacyflag=="1")
			{
				chkHideDOB.Checked = true;
				chkHideDOB.Focusable = true;
				chkHideDOB.SetButtonDrawable(Resource.Drawable.checkedcheckbox);
			}
			else
			{
				chkHideDOB.Checked = false;
				chkHideDOB.Focusable = false;
				chkHideDOB.SetButtonDrawable(Resource.Drawable.emptycheckbox);
			}
			//radio button
			if (viewProfile.Sex == "1")
			{
				rbtnMale.Checked = true;
				rbtnFemale.Checked = false;
				rbtnOther.Checked = false;
				strGender = "1";
			}
			else if (viewProfile.Sex == "2")
			{
				rbtnFemale.Checked = true;
				rbtnMale.Checked = false;
				rbtnOther.Checked = false;
				strGender = "2";
			}
			else if (viewProfile.Sex == "3")
			{
				rbtnOther.Checked = true;
				rbtnMale.Checked = false;
				rbtnFemale.Checked = false;
				strGender = "3";
			}
		}



		#region "Click Events"
		void FnClickEvents()
		{
			imgLogo.Click += delegate
			{
				FinishAffinity();
				HomeActivity.isHomeShows = false;
				HomeActivity.isHosmeFeedorPost = 1;
				Intent activity = new Intent(this, typeof(HomeActivity));
				activity.PutExtra("MyData", "0");
				StartActivity(activity);
			};
			imgCover.Click += delegate
			{
				HomeActivity.valueOnceEditProfile = 1;
				HomeActivity.isimgCoverClicked = true;
				var imageIntent = new Intent();
				imageIntent.SetType("image/*");
				imageIntent.SetAction(Intent.ActionGetContent);
				StartActivityForResult(Intent.CreateChooser(imageIntent, "Select Image"), 0);
			};
			imgProfile.Click += delegate
			{
				HomeActivity.valueOnceEditProfile = 1;
				HomeActivity.isimgCoverClicked = false;
				//imgProfileClicked = true;
				var imageIntent = new Intent();
				imageIntent.SetType("image/*");
				imageIntent.SetAction(Intent.ActionGetContent);
				StartActivityForResult(Intent.CreateChooser(imageIntent, "Select Image"), 0);
			};
			chkHideDOB.Click += (o, e) =>
			{
				if (chkHideDOB.Checked)
				{
					chkHideDOB.SetButtonDrawable(Resource.Drawable.checkedcheckbox);

				}
				else
				{
					chkHideDOB.SetButtonDrawable(Resource.Drawable.emptycheckbox);
				}

			};

			btndate.Click += delegate
			{
				CreateDialog(date_Id).Show();
			};

			btnEdit.Click += delegate
			{
				FnEnableAllControls();
			};

			rbtnMale.Click += RadioButtonClicked;
			rbtnFemale.Click += RadioButtonClicked;
			rbtnOther.Click += RadioButtonClicked;

			txtCurrentCity.TextChanged += async delegate
			{
				string[] strName = null;

				if (txtCurrentCity.Text.Length >= 2)
				{
					strName = await FnSearchLocation(txtCurrentCity.Text);
				}
				if (strName != null && strName.Length > 0)
				{
					ArrayAdapter adapter = new ArrayAdapter<string>(this, Resource.Layout.CustomAutoCompleteLayout, Resource.Id.lblPlaceName, strName);
					txtCurrentCity.Adapter = adapter;
					string strSearchLocationValue = txtCurrentCity.Text;
				}
			};

			btnSave.Click += async delegate
			{
				progress = ProgressDialog.Show(this, "", GetString(Resource.String.please_wait));
				if (string.IsNullOrEmpty(txtFirstName.Text) || string.IsNullOrEmpty(txtLastName.Text) || string.IsNullOrEmpty(txtDob.Text) || string.IsNullOrEmpty(txtCurrentCity.Text))
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strFillValidationError, this);
				}
				else {
					bool isConnected = objClsCommonFunctions.FnIsConnected(this);
					if (!isConnected)
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
					}
					else
					{
						
						await FnEditProfilePostMethod();
					}
				}
				if (progress != null)
				{
					progress.Dismiss();
					progress = null;
				}
			};
		}
		#endregion

		#region "Datepicker"
		private void UpdateDisplay()
		{
			txtDob.Text = date.ToString("yyyy-MM-dd");

		}
		protected Dialog CreateDialog(int id)
		{
			DateTime currently = DateTime.Now;
			switch (id)
			{
				case date_Id:
					return new DatePickerDialog(this, OnDateSet, currently.Year, currently.Month - 1, currently.Day);
			}
			return null;
		}
		void OnDateSet(object sender, DatePickerDialog.DateSetEventArgs e)
		{
			this.date = e.Date;
			UpdateDisplay();
		}
		#endregion

		#region "Radio Button Clicked"
		void RadioButtonClicked(Object sender, EventArgs e)
		{
			RadioButton rd = (RadioButton)sender;
			bool isChecked = rd.Checked;
			if (isChecked)
			{
				switch (rd.Id)
				{
					case Resource.Id.rbtnMaleEPL:
						rbtnFemale.Checked = false;
						rbtnOther.Checked = false;
						strGender = "1";//rbtnMale.Text;
						break;
					case Resource.Id.rbtnFemaleEPL:
						rbtnMale.Checked = false;
						rbtnOther.Checked = false;
						strGender = "2";// rbtnFemale.Text;
						break;
					case Resource.Id.rbtnOtherEPL:
						rbtnMale.Checked = false;
						rbtnFemale.Checked = false;
						strGender = "3";// rbtnOther.Text;
						break;
				}
			}
		}
		#endregion

		#region "SearchLocation"
		async Task<string[]> FnSearchLocation(string strUserEnteredName)
		{
			string[] strName = null;
			int index = 0;
			string strFullPath = await objRestServiceCall.CallGooglePlaceCityNameService(strUserEnteredName);
			if (strFullPath == "Exception" || string.IsNullOrEmpty(strFullPath))
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strServserError, this);
			}
			try
			{
				GoogleLocationClass objGoogleLocationClass = JsonConvert.DeserializeObject<GoogleLocationClass>(strFullPath);
				if (objGoogleLocationClass.status == "OK")
				{
					strName = new string[objGoogleLocationClass.predictions.Count];
					foreach (var description in objGoogleLocationClass.predictions)
					{
						strName[index] = description.description;
						index++;
					}
				}
			}
			catch
			{

			}
			return strName;
		}
		#endregion

		#region "Gallery"

		#region "Gallery image that image show in imageView"
		protected override async void OnActivityResult(int requestCode, Android.App.Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);
			if (resultCode == Android.App.Result.Ok && requestCode==0)//before crop
			{
				imageUri = data.Data;
				strImagePath = GetPathToImage(data.Data);
				//check image size
				BitmapFactory.Options option = await objClsCommonFunctions.GetBitmapOptionsOfImage(strImagePath);
				imageWidth = option.OutWidth;
				imageHeight = option.OutHeight;
				System.Console.WriteLine(strImagePath);
				if (HomeActivity.isimgCoverClicked)
				{
					if (imageWidth >= 1143 && imageHeight >= 400)//cover image
					{
						strCoverImagePath = strImagePath;
						imgCover.SetImageURI(imageUri);
						await Task.Run(() => FnImageResizing());
					}
					else
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Image size is small", this);
					}
				}
				else
				{
					if (imageWidth >= 225 && imageHeight >= 225)//profile image
					{
						cropCapturedImage(strImagePath);
					}
					else
					{
						objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Image size is small", this);
					}
				}

			}
			else if(requestCode==2)//after croping
			{
				if (data == null)
				{
					isCropedOrNot = false;
					await Task.Run(() => FnImageResizing());
				}
				else
				{
					isCropedOrNot = true;
					contentUri = data.Extras;
					Bitmap bitmap = (Android.Graphics.Bitmap)contentUri.GetParcelable("data");
					imgProfile.SetImageBitmap(bitmap);
					await Task.Run(() => FnImageResizing());
				}
			}
		}
		#endregion
		//croping Captured Image 
		public void cropCapturedImage(string contentUri)
		{
			try
			{
				//Start Crop Activity

				Intent cropIntent = new Intent("com.android.camera.action.CROP");
				//indicate image type and Uri of image
				Java.IO.File file = new Java.IO.File(contentUri);
				Android.Net.Uri contentUri1 = Android.Net.Uri.FromFile(file);
				cropIntent.SetDataAndType(contentUri1, "image/*");
				// set crop properties
				cropIntent.PutExtra("crop", "true");
				cropIntent.PutExtra("scaleUpIfNeeded", true);
				// indicate aspect of desired crop
				cropIntent.PutExtra("aspectX", 30);
				cropIntent.PutExtra("aspectY", 30);
				// indicate output X and Y
				cropIntent.PutExtra("outputX", 270);
				cropIntent.PutExtra("outputY", 270);
				// retrieve data on return
				cropIntent.PutExtra("return-data", true);
				// start the activity - we handle returning in onActivityResult
				StartActivityForResult(cropIntent, 2);
			}
			catch
			{
				
			}
		}

		#region "Get image absolute path"

		string GetPathToImage(Android.Net.Uri uri)
		{
			string doc_id = "";
			using (var c1 = ContentResolver.Query(uri, null, null, null, null))
			{
				c1.MoveToFirst();
				String document_id = c1.GetString(0);
				doc_id = document_id.Substring(document_id.LastIndexOf(":") + 1);
			}
			string path = null;

			// The projection contains the columns we want to return in our query.
			string selection = Android.Provider.MediaStore.Images.Media.InterfaceConsts.Id + " =? ";
			using (var cursor = ContentResolver.Query(Android.Provider.MediaStore.Images.Media.ExternalContentUri, null, selection, new string[] { doc_id }, null))
			{
				if (cursor == null) return path;
				var columnIndex = cursor.GetColumnIndexOrThrow(Android.Provider.MediaStore.Images.Media.InterfaceConsts.Data);
				cursor.MoveToFirst();
				path = cursor.GetString(columnIndex);
			}
			return path;
		}
		#endregion

		#region " Image Resizing"


		async void FnImageResizing()
		{
			var strImageNames = strImagePath.Substring(strImagePath.LastIndexOf("/", StringComparison.Ordinal) + 1);
			MemoryStream stream = new MemoryStream();
			if (HomeActivity.isimgCoverClicked)
			{
				if (imageWidth >= 1143 && imageHeight >= 400)
				{
					strCoverImageName = strImageNames;
					coverPhoto = await objClsCommonFunctions.FnConvertByteArray(strImagePath);

				}
			}
			else
			{
				if (imageWidth >= 225 && imageHeight >= 225)
				{
					if (isCropedOrNot)
					{
						strProfileImageName = strImageNames;
						Bitmap bitmap = (Android.Graphics.Bitmap)contentUri.GetParcelable("data");
						MemoryStream stream1 = new MemoryStream();
						bitmap.Compress(Bitmap.CompressFormat.Png, 0, stream1);
						thumbfilePhoto = stream1.ToArray();
						//imgProfile.SetImageBitmap(bitmap);
					}
					else
					{
						//coverthumb
						strProfileImageName = strImageNames;
						originalImage = BitmapFactory.DecodeFile(strImagePath);
						var thumbImage = Bitmap.CreateScaledBitmap(originalImage, 225, 225, false);
						thumbImage.Compress(Bitmap.CompressFormat.Png, 0, stream);
						thumbfilePhoto = stream.ToArray();
						imgCover.SetImageURI(imageUri);
					}
				}
				strOriginalImage = strImageNames;
				profilePhoto = await objClsCommonFunctions.FnConvertByteArray(strImagePath);//profile original
			}

		}
		#endregion





		#endregion

		#region "Disabele All Controls"
		void FnDisabeleAllControls()
		{
			txtFirstName.FocusableInTouchMode = false;
			txtFirstName.Focusable = false;
			txtLastName.FocusableInTouchMode = false;
			txtLastName.Focusable = false;
			txtDob.FocusableInTouchMode = false;
			txtDob.Focusable = false;
			txtCurrentCity.FocusableInTouchMode = false;
			txtCurrentCity.Focusable = false;
			rbtnMale.Enabled = false;
			rbtnFemale.Enabled = false;
			rbtnOther.Enabled = false;
			btndate.Enabled = false;
			btnSave.Enabled = false;
			imgCover.Enabled = false;
			imgProfile.Enabled = false;
			chkHideDOB.Enabled = false;
		}
		#endregion

		#region "Eabele All Controls"
		void FnEnableAllControls()
		{
			txtFirstName.FocusableInTouchMode = true;
			txtFirstName.Focusable = true;
			txtLastName.FocusableInTouchMode = true;
			txtLastName.Focusable = true;
			txtDob.FocusableInTouchMode = true;
			txtDob.Focusable = true;
			txtCurrentCity.FocusableInTouchMode = true;
			txtCurrentCity.Focusable = true;
			rbtnMale.Enabled = true;
			rbtnFemale.Enabled = true;
			rbtnOther.Enabled = true;
			btndate.Enabled = true;
			btnSave.Enabled = true;
			imgCover.Enabled = true;
			imgProfile.Enabled = true;
			chkHideDOB.Enabled = true;
		}                                                                                                                                                                 
		#endregion

		#region "EditProfilePostMethod"
		async Task FnEditProfilePostMethod() //for profile now
		{
			string strEditProfilePostAPI = "";
			//Create New group
			//++++++++++++++  production url     +++++++++++++++++//
			strEditProfilePostAPI = "http://www.youmeus.com";
			//++++++++++++++ test url      +++++++++++++++++//
			//strEditProfilePostAPI = "http://202.83.19.104/youmeuslocalserver";
			uri = uri ?? new Uri(strEditProfilePostAPI);
			RestClient _client = new RestClient(uri);


			if (!string.IsNullOrEmpty(strProfileImageName))
			{
				strTypeOfImageFinal = strTypeOfImageFinal + ",1";//  "1" for Updating ProfilePhoto 
			}
			if (!string.IsNullOrEmpty(strCoverImageName))
			{
				strTypeOfImageFinal = strTypeOfImageFinal + ",2";//"2" for Updating CoverPhoto
			}

			bool isConnected = objClsCommonFunctions.FnIsConnected(this);
			if (!isConnected)
			{
				objClsCommonFunctions.FnAlertMsg(Constants.strAppName, Constants.strNoInternet, this);
			}
			else
			{
				try
				{
				//post services api
				var request = new RestRequest("helloservice/EditProfileData", Method.POST) { RequestFormat = DataFormat.Json };
				if (strTypeOfImageFinal == "0")//only data update
				{

				}
				if (strTypeOfImageFinal == "0" + "," + "1")//only data+profile image update
				{
					request.AddFile("file", profilePhoto, strOriginalImage);//(Profile Original)
					request.AddFile("thumbfile", thumbfilePhoto, strProfileImageName);//crop image
				}
				if (strTypeOfImageFinal == "0" + "," + "2")//only data+cover image update
				{
					request.AddFile("file2", coverPhoto, strCoverImageName);//cover image
				}
				if (strTypeOfImageFinal == "0" + "," + "1" + "," + "2")//only data+profile+cover image update
				{
					request.AddFile("file", profilePhoto, strOriginalImage);//(Profile Original)
					request.AddFile("thumbfile", thumbfilePhoto, strProfileImageName);//crop image
					request.AddFile("file2", coverPhoto, strCoverImageName);//cover image
				}
				request.AlwaysMultipartFormData = true;
				//Add one of these for each form boundary you need
				request.AddParameter("Iduser", SplashScreenActivity.strUserId, ParameterType.GetOrPost);
				request.AddParameter("dobprivacyflag", (chkHideDOB.Checked) ? "1" : "0", ParameterType.GetOrPost);
				request.AddParameter("Fname", txtFirstName.Text.Trim(), ParameterType.GetOrPost);
				request.AddParameter("Lname", txtLastName.Text.Trim(), ParameterType.GetOrPost);
				request.AddParameter("currentcity", txtCurrentCity.Text.Trim(), ParameterType.GetOrPost);
				request.AddParameter("Dateofbirth", txtDob.Text.Trim(), ParameterType.GetOrPost);
				request.AddParameter("Sex", strGender, ParameterType.GetOrPost);
				request.AddParameter("Typeofimage", strTypeOfImageFinal, ParameterType.GetOrPost);


				var response = await Task.Run(() => _client.Execute(request));
				if (response.StatusCode.ToString() == "OK" && response.ResponseStatus.ToString() == "Completed")
				{

					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Successfully updated Profile ", this);
					strTypeOfImageFinal = "0";
					HomeActivity.valueOnceEditProfile = 0;
					HomeActivity.isimgCoverClicked = false;
					FnDisabeleAllControls();
				}
				else
				{
					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Profile not updated", this);
				}
				}
				catch (Exception e)
				{

					objClsCommonFunctions.FnAlertMsg(Constants.strAppName, "Unable to connect to Server. Please try after sometime", this);
				}
			}
		}
		#endregion


	}

	public class EditProfileClass
	{
		public string Iduser { get; set; }
		public string dobprivacyflag { get; set; }
		public string Fname { get; set; }
		public string Lname { get; set; }
		public string currentcity { get; set; }
		public string Dateofbirth { get; set; }
		public string Sex { get; set; }
		public string fileName { get; set; }
		public string fileData { get; set; } //for profile Image with 225x225
		public string Typeofimage { get; set; } //Check NOTE below

		public string fileOrginal { get; set; } //For Profile Image WithOut cropping
		public string coverName { get; set; }
		public string coverData { get; set; } //for profile Image with 501x2025
		public string coverOriginal { get; set; }
		public string coverResized { get; set; }
		public string coverthumb { get; set; }
	}

	public class EditProfileSuccessClass
	{
		public string Message { get; set; }
		public string Fname { get; set; }
	}
}

//NOTE: 
//typeofimage =0 if you r edit only data
//typeofimage = 1 /if profile
//typeofimage = 2 /if crop
//typeofimage = 0,1,2 if all three