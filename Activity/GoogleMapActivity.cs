﻿
using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Geolocator.Plugin;

namespace YouMeUsAndroid
{
	[Activity(WindowSoftInputMode = SoftInput.StateAlwaysHidden, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
	public class GoogleMapActivity : AppCompatActivity,IOnMapReadyCallback
	{
		internal static EventsNearbyLocation SingleEventProperty { get; set;}
		internal static Homefeedsfgeoevent HomefeedsSingleEventProperty { get; set; }
		internal static int SuggestOrMyEvent;
		internal static string strValue;
		internal static string strDistance;
		internal static string strCityName;
		internal static string strLatLongs;
		internal static bool isSuccess = false;
		internal static string strAddress;
		MapFragment mapFrag;
		GoogleMap googleMap;
		MarkerOptions marker;
		internal static string StrEventName;
		CameraPosition cameraPosition;
		CameraUpdate cameraUpdate;
		CultureInfo cultureInfo;
		LatLng CurrentPosition;
		double latitude;
		double longitude;
		ImageView imgLogo;
		public void OnMapReady(GoogleMap googleMaps)
		{
			try
			{
				googleMap = googleMaps;
			}
			catch (Exception e)
			{
				string strerroe = e.Message;
				Console.WriteLine("null exception");
			}
		}

		protected override async void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your application here
			SetContentView(Resource.Layout.GoogleMapLayout);
			imgLogo = FindViewById<ImageView>(Resource.Id.imgLogoGML);
			FnClickEvents();
			marker = marker ?? new MarkerOptions();
			 cultureInfo = (CultureInfo)CultureInfo.CurrentCulture.Clone();

			if (googleMap == null)
			{
				mapFrag = (MapFragment)FragmentManager.FindFragmentById(Resource.Id.map);
			}
			googleMap = mapFrag.Map;
			googleMap.MapType = GoogleMap.MapTypeNormal;
			googleMap.MyLocationEnabled = true;
			var strLayoutName = Intent.GetStringExtra("MyData") ?? "Data not available";
			if (SlidingTabActivity.isLocation)
			{

				bool isTrue = await GetCurrentPosition();
				if (isTrue)
				{
					FnUpdateCameraPosition(CurrentPosition);
					FnGoogleMapLoad();
				}

			}
			else
			{
				if (SlidingTabActivity.isHomeFeedMapFlag)
				{
					string[] strHomeValues = Regex.Split(strLayoutName, "/");
					string[] strval = Regex.Split(strHomeValues[0], "-");
					cultureInfo.NumberFormat.CurrencyDecimalSeparator = ".";
					double latitude = Double.Parse(strval[0], NumberStyles.Any, cultureInfo);
					double longitude = Double.Parse(strval[1], NumberStyles.Any, cultureInfo);

					CurrentPosition = new LatLng(latitude, longitude);
					FnUpdateCameraPosition(CurrentPosition);
					FnHomeFeedMapLoad(strHomeValues[0], strHomeValues[1]);
				}
				else
				{
					string[] strval = Regex.Split(strLatLongs, "-");
					cultureInfo.NumberFormat.CurrencyDecimalSeparator = ".";
					double latitude = Double.Parse(strval[0], NumberStyles.Any, cultureInfo);
					double longitude = Double.Parse(strval[1], NumberStyles.Any, cultureInfo);

					CurrentPosition = new LatLng(latitude, longitude);
					FnUpdateCameraPosition(CurrentPosition);
					FnGoogleMapLoad();
				}
			}

			googleMap.InfoWindowClick += FnMarker;
		}
		protected override void OnStop()
		{
			base.OnStop();
			if (googleMap != null)
			{
				googleMap.Clear();
			}
			FinishAffinity(); ;
		}
		public override void OnBackPressed()
		{
			base.OnBackPressed();
			if (googleMap != null)
			{
				googleMap.Clear();
			}
			FinishAffinity();
			if(SlidingTabActivity.screenSlide==1)
			{
				if(SlidingTabActivity.isHomeEventOrPostEvent)
				{
					HomeActivity.isHosmeFeedorPost = 1;
				}
				else
				{
					HomeActivity.isHosmeFeedorPost = 2;
				}
				HomeActivity.isHomeShows = false;
				Intent activity = new Intent(this, typeof(HomeActivity));
				activity.PutExtra("MyData", "0");
				StartActivity(activity);
			}
			else if (SlidingTabActivity.screenSlide == 2)
			{
				isSuccess = true;
				Intent activity = new Intent(this, typeof(HomeActivity));
				activity.PutExtra("MyData", "2");
				StartActivity(activity);
			}
			else if (SlidingTabActivity.screenSlide == 3)
			{
				Intent activity = new Intent(this, typeof(HomeActivity));
				activity.PutExtra("MyData", "4");
				StartActivity(activity);
			}
			else if (SlidingTabActivity.screenSlide == 0)
			{
				HomeActivity.isHomeShows = false;
				HomeActivity.isHosmeFeedorPost = 1;
				Intent activity = new Intent(this, typeof(HomeActivity));
				activity.PutExtra("MyData", "0");
				StartActivity(activity);
			}
		}
		void FnClickEvents()
		{
			imgLogo.Click += delegate
			{
				FinishAffinity();
				HomeActivity.isHomeShows = false;
				HomeActivity.isHosmeFeedorPost = 1;
				Intent activity = new Intent(this, typeof(HomeActivity));
				activity.PutExtra("MyData", "0");
				StartActivity(activity);
			};
		}
		#region "GoogleMapLoad"
		void FnGoogleMapLoad()
		{
			if (SlidingTabActivity.isSingleOrMultipleEvent)
			{
				if (SingleEventProperty != null)
				{
					string[] strval = Regex.Split(SingleEventProperty.Eventcity, "-");
					cultureInfo.NumberFormat.CurrencyDecimalSeparator = ".";
					double lat = Double.Parse(strval[0], NumberStyles.Any, cultureInfo);
					double lng = Double.Parse(strval[1], NumberStyles.Any, cultureInfo);
					LatLng latLong = new LatLng(lat, lng);
					MarkOnMap(SingleEventProperty.Eventname, latLong, Resource.Drawable.redmarker);
					SingleEventProperty = null;
				}

			}
			else if(SlidingTabActivity.screenSlide == 1 && SlidingTabActivity.number == 1)
			{
				if (HomefeedsSingleEventProperty != null)
				{
					string[] strval = Regex.Split(HomefeedsSingleEventProperty.Eventcity, "-");
					cultureInfo.NumberFormat.CurrencyDecimalSeparator = ".";
					double lat = Double.Parse(strval[0], NumberStyles.Any, cultureInfo);
					double lng = Double.Parse(strval[1], NumberStyles.Any, cultureInfo);
					LatLng latLong = new LatLng(lat, lng);
					MarkOnMap(HomefeedsSingleEventProperty.Eventname, latLong, Resource.Drawable.redmarker);
					HomefeedsSingleEventProperty = null;
					SlidingTabActivity.isSingleOrMultipleEvent = true;
				}

			}
			else if (!SlidingTabActivity.isSingleOrMultipleEvent)
			{
				if (SlidingTabActivity.lstEvents != null)
				{
					foreach (var values in SlidingTabActivity.lstEvents)
					{
						string[] strval = Regex.Split(values.Eventcity, "-");

						cultureInfo.NumberFormat.CurrencyDecimalSeparator = ".";
						double lat = Double.Parse(strval[0], NumberStyles.Any, cultureInfo);
						double lng = Double.Parse(strval[1], NumberStyles.Any, cultureInfo);


						LatLng latLong = new LatLng(lat, lng);
						MarkOnMap(values.Eventname, latLong, Resource.Drawable.redmarker);
					}
				}
			}
		}
		#endregion

		void FnHomeFeedMapLoad(string strLatLong ,string strEventName)
		{
			string[] strval = Regex.Split(strLatLong, "-");

			cultureInfo.NumberFormat.CurrencyDecimalSeparator = ".";
			double lat = Double.Parse(strval[0], NumberStyles.Any, cultureInfo);
			double lng = Double.Parse(strval[1], NumberStyles.Any, cultureInfo);


			LatLng latLong = new LatLng(lat, lng);
			MarkOnMap(strEventName, latLong, Resource.Drawable.redmarker);
		}
		#region "MarkOnMap"
		void MarkOnMap(string title, LatLng pos, int resourceId)
		{
			RunOnUiThread(() =>
		  {

			  marker.SetTitle(title);
			  marker.SetPosition(pos);
			  marker.InvokeIcon(BitmapDescriptorFactory.FromResource(resourceId));
				googleMap.AddMarker(marker);
			 

		  });
		}
		#endregion

		#region "Marker Click Event"
		void FnMarker(object sender, GoogleMap.InfoWindowClickEventArgs e)
		{
			isSuccess = true;
			if (googleMap != null)
			{
				googleMap.Clear();

			}
			StrEventName = e.Marker.Title;
			FinishAffinity();
			if (SlidingTabActivity.isHomeFeedMapFlag)// home post
			{
				Intent activity = new Intent(this, typeof(HomeActivity));
				activity.PutExtra("MyData", "0");
				StartActivity(activity);
				HomeActivity.isHosmeFeedorPost = 2;
				SlidingTabActivity.isHomeFeedMapFlag = false;
			}
			if(SlidingTabActivity.screenSlide == 1 && SlidingTabActivity.number==1 )//home suggested events
			{
				Intent activity = new Intent(this, typeof(HomeActivity));
				activity.PutExtra("MyData", "0");
				StartActivity(activity);
				//SlidingTabActivity.strLocationAddress = strAddress;
				HomeActivity.isHosmeFeedorPost = 1;
				SlidingTabActivity.isHomeFeedMapFlag = false;
			}
			else if(SlidingTabActivity.number == 0 && !SlidingTabActivity.isHomeFeedMapFlag)//events
			{
				Intent activity = new Intent(this, typeof(HomeActivity));
				activity.PutExtra("MyData", "2");
				StartActivity(activity);
			}

		}
		#endregion

		#region "CurrentPosition(Location)"
		//showing current position
		async Task<bool> GetCurrentPosition()
		{
			try
			{
				var Locator = CrossGeolocator.Current;
				Locator.DesiredAccuracy = 50;
				var position = await Locator.GetPositionAsync(timeoutMilliseconds: 10000);
				CurrentPosition = new LatLng(position.Latitude, position.Longitude);
			}
			catch
			{
				return false;
			}
			return true;
		}
		void FnUpdateCameraPosition(LatLng pos)
		{
			try
			{
				CameraPosition.Builder builder = CameraPosition.InvokeBuilder();
				CurrentPosition = CurrentPosition ?? pos;
				builder.Target(pos);
				builder.Zoom(10);
				builder.Bearing(0);
				builder.Tilt(10);
				cameraPosition = builder.Build();

				cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);
				googleMap.AnimateCamera(cameraUpdate);
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
		}
		#endregion
	}
}
